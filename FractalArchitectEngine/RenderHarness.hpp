//
//  RenderHarness.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef RenderHarness_hpp
#define RenderHarness_hpp

#include <memory>
#include <vector>
#include <set>
#include <chrono>

#include "Exports.hpp"
#include "RenderListener.hpp"
#include "RenderQueue.hpp"

class ClContext;
class CudaContext;
class FlameExpanded;
class RenderQueue;

using SharedClContext     = std::shared_ptr<ClContext>;
using SharedCudaContext   = std::shared_ptr<CudaContext>;
using SharedFlameExpanded = std::shared_ptr<FlameExpanded>;
using FlameExpandedVector = std::vector<SharedFlameExpanded>;
using UniqueQueue         = std::unique_ptr<RenderQueue>;

using namespace std::chrono;

enum class OutputType { ePNG, eJPEG, eTIFF };
enum class Engine { OpenCL, CUDA };

class FAENGINE_API RenderHarness : public RenderListener, public ViewListener {
protected:
    std::set<uint> deviceNums;
    UniqueQueue renderQueue;
    
public:
    SharedDeviceContext deviceContext; // RenderHarness is tied to EITHER a ClContext or CudaContext - not both
    static RenderHarness * openClSingleton;
    static RenderHarness * cudaSingleton;
    
    static void variationSetSetup();
    static void enqueueOpenClStop();
    static void enqueueCudaStop();
    
    uint deviceCount();
    
	static void printDeviceList(bool targetGPU);

    void openClContextSetup(bool targetGPU, std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine);
#ifndef NO_CUDA
    void cudaContextSetup(std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine);
#endif
    
    bool renderFlames(std::string path, bool useQueue, OutputType outputType);
	bool renderFlames(std::string path, bool useQueue, float width, float height, int quality, OutputType outputType);
    bool renderFlames(std::string path,
                      bool useQueue,
                      float width,
                      float height,
                      int quality,
                      uint bitsPerPixel,
                      bool hasAlpha,
                      const char * colorspaceName,
                      bool flipped,
                      float epsilon,
                      bool verbose, OutputType outputType);
    
    SharedImage flam4Render(SharedDeviceContext & deviceContext,
                            SharedFlameExpanded & fe,
                            int deviceNum,
                            std::string &url,
                            std::string &outURL,
                            float width,
                            float height,
                            int quality,
                            int bitsPerPixel,
                            bool hasAlpha,
                            const char *colorspaceName,
                            bool flipped,
                            float epsilon,
                            bool verbose,
                            duration<double> & _duration,
                            duration<double> & deDuration);
    
    int flam4QueuedRender(SharedDeviceContext & deviceContext,
                          SharedFlameExpanded & fe,
                          int deviceNum,
                          std::string &url,
                          std::string &outURL,
                          float width,
                          float height,
                          int quality,
                          int bitsPerPixel,
                          bool hasAlpha,
                          const char *colorspaceName,
                          bool flipped,
                          float epsilon,
                          bool verbose,
                          uint index,
                          uint indexMax);
    
protected:
    RenderHarness(std::set<uint> _deviceNums);
    
    FlameExpandedVector loadFromPath(const std::string & _path);
    
    virtual void beforeRender() override; // batch start
    virtual void renderDone() override;   // batch end
    
    virtual void renderStats(std::string filename,
                             duration<double> _duration,
                             duration<double> deDuration,
                             float width,
                             float height,
                             float actualQuality,
                             size_t numBatches,
                             std::string tilesLabel,
                             float fusedFraction,
                             float retainedFraction,
                             uint supersample) override;
    
    virtual void submitImage(SharedImageSlot slot) override;
    virtual void imageBatchTime(duration<double> _duration) override;
    
    virtual void setRenderResult(UniqueRender result) override;
};



#endif /* RenderHarness_hpp */
