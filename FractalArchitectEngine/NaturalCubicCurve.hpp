//
//  NaturalCubicCurve.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef NaturalCubicCurve_hpp
#define NaturalCubicCurve_hpp

#include "Exports.hpp"
#include "CurveDefinition.hpp"
#include "pugixml.hpp"

class NaturalCubicCurve;
using UniqueNatural = std::unique_ptr<NaturalCubicCurve>;


//    Sj(x) = aj + bj(x−xj) + cj(x−xj)2 + dj(x−xj)3

class FAENGINE_API NaturalCubicCurve : CurveDefinition<Point, eNaturalCubic> {
    friend class Flam4ClRuntime;
    friend class Flam4CudaRuntime;
public:
    NaturalCubicCurve();
    NaturalCubicCurve(std::vector<Point> *controlPoints,
                      bool _flipped = false,
                      bool _mirrored = false);
    NaturalCubicCurve(const SharedPointVector & controlPointsData,
                      bool _flipped = false,
                      bool _mirrored = false);
    NaturalCubicCurve(const NaturalCubicCurve &other);
    
    static UniqueNatural curveWithControlPointsData(const SharedPointVector &controlPointsData);
    
    virtual std::shared_ptr<std::vector<float>> curveForCount(int count) override;
    virtual std::shared_ptr<std::vector<float>> curveForCountAlternate(int count) override;
    virtual std::shared_ptr<std::vector<float>> curveForCountAlternate(int count, float start, float end) override;
    virtual double curveAtOffset(double u) override;
    virtual Point interpolatePointForU(float u) override;
    virtual bool flippable() override  { return true; }
    virtual bool reversable() override { return true; }
    
    
    size_t cpCount() const;
    Point cpAtIndex(size_t index);
    void setControlPoints(const std::vector<Point> &_controlPoints);
    void getCurvePoints(float *array, size_t count);
    bool isDefaultCurve();
    
    float integral();
    float integrateSimpson_1_3_rule();
    float integrateSimpson_3_8_rule();
    float integrateSimpson_1_3_ruleClipped();
    float integrateSimpson_3_8_ruleClipped();
    static float integrateSimpson_3_8_ruleForCurve(NaturalCubicCurve *curve);
    static float integrateSimpson_3_8_ruleClippedForCurve(NaturalCubicCurve *curve);
    static float integrateSimpson_3_8_ruleFor(NaturalCubicCurve *curve1, NaturalCubicCurve *curve2);
    static float integrateSimpson_3_8_ruleClippedFor(NaturalCubicCurve *curve1, NaturalCubicCurve *curve2);
    
    static SharedPointVector curveDataFromAttributeString(const std::string &string);
    static SharedPointVector curveDataFromXmlAttribute(const pugi::xml_attribute &attr);

    float evaluateForU(float u);
    
    std::string description();
private:
    void initAsLinearRamp();
    
    void solveCubics();
    float evaluateForX(float x);
    float evaluateForXClipped(float x);
    float evaluateIntegralForX(float x);
    
    float evaluateForU(float u, float start, float end);
    float evaluateForUClipped(float u);
    float evaluateForUClipped(float u, float start, float end);
    static float evaluateForU(float u, NaturalCubicCurve *curve1, NaturalCubicCurve *curve2);
    static float evaluateForUClipped(float u, NaturalCubicCurve *curve1, NaturalCubicCurve *curve2);
    
    static void k2AdjustFor(NaturalCubicCurve *rgbCurve,
                            NaturalCubicCurve *redCurve,
                            NaturalCubicCurve *greenCurve,
                            NaturalCubicCurve *blueCurve,
                            float *adjust);
    static void k2AdjustForData(const SharedPointVector &rgbData,
                                const SharedPointVector &redData,
                                const SharedPointVector &greenData,
                                const SharedPointVector &blueData,
                                float *adjust); // cl_float4 *adjust

    
    static SharedPointVector linearRampControlPointsData();
    
    // ==== Members =====
    
    UniqueFloatVector As;
    UniqueFloatVector Bs;
    UniqueFloatVector Cs;
    UniqueFloatVector Ds;
    UniqueFloatVector Xs;
};

#endif /* NaturalCubicCurve_hpp */
