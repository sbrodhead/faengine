//
//  ClPlatform.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/22/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//  Created by Steven Brodhead on 5/17/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef ClPlatform_hpp
#define ClPlatform_hpp

#ifdef __APPLE__
#include <OpenCL/cl.h>
#elif defined(_WIN32)
#include <CL/opencl.h>
#else
#include <CL/cl.h>
#endif
#include "Exports.hpp"

#include <string>

class ClContext;

using uint = unsigned int;

class FAENGINE_API ClPlatform {
    friend class ClContext;
    
    cl_platform_id    platformID;
    std::string       version;
    cl_device_id     *deviceIDs;
    uint              numDevices;
	std::string       name;
	std::string       vendor;
    
public:
    ClPlatform(cl_platform_id _platformID, cl_device_type filter);
    ~ClPlatform();
    
    static std::string getPlatformVersion(cl_platform_id platformID);
	static bool isApplePlatform(cl_platform_id  id);
	static std::string platformName(cl_platform_id  id);
	static std::string platformVendor(cl_platform_id  id);
};

#endif /* ClPlatform_hpp */
