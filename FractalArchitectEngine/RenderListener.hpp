//
//  RenderListener.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef RenderListener_hpp
#define RenderListener_hpp

#include "Exports.hpp"
#include "Enums.hpp"
#include "Image.hpp"
#include "common.hpp"
#include <memory>
#include <chrono>

struct ImageSlot;
struct RenderResult;

using SharedImageSlot = std::shared_ptr<ImageSlot>;
using SharedImage     = std::shared_ptr<FA::Image>;
using UniqueRender    = std::unique_ptr<RenderResult>;

using namespace std::chrono;

// image + their render metadata
struct FAENGINE_API  RenderResult {
    RenderResult(SharedImage _image,
                 float _fusedFraction,
                 float _retainedFraction,
                 duration<double> _renderTime,
                 enum InitialColorSpace colorSpace);
    
	SharedImage image;
    float fusedFraction;
    float retainedFraction;
    duration<double> renderTime;
    enum InitialColorSpace eColorSpace;
};


// prototype for classes that listen for render info/completion
class FAENGINE_API RenderListener
{
public:
    virtual void beforeRender() = 0; // batch start
    virtual void renderDone() = 0;   // batch end
    
    virtual void renderStats(std::string filename, // render stats for each image render
                             duration<double> _duration,
                             duration<double> deDuration,
                             float width,
                             float height,
                             float actualQuality,
                             size_t numBatches,
                             std::string tilesLabel,
                             float fusedFraction,
                             float retainedFraction,
                             uint supersample) = 0;
    
    virtual void submitImage(SharedImageSlot slot) = 0; // used for batch thumbnail generation
    virtual void imageBatchTime(duration<double> _duration) = 0; // render time for total image batch render
    
    static void logRenderStats(std::string filename, // utility method for logging render stats
                               duration<double> _duration,
                               duration<double> deDuration,
                               float width,
                               float height,
                               float actualQuality,
                               size_t numBatches,
                               std::string tilesLabel,
                               float fusedFraction,
                               float retainedFraction,
                               uint supersample);
};

// prototype for GUI classes that take the rendered image
class FAENGINE_API ViewListener
{
public:
    virtual void setRenderResult(UniqueRender result) = 0; // used for large single image previews
    virtual void setImage(SharedImage image, std::string & url, bool useAlpha, FA::Size _size, duration<double> _duration) = 0; // used for render to file
};


#endif /* RenderListener_hpp */
