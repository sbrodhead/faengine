//
//  RenderTile.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/25/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef RenderTile_hpp
#define RenderTile_hpp

#include <vector>
#include <memory>
#include <chrono>

#include "Exports.hpp"
#include "Enums.hpp"
#include "RenderEnums.hpp"
#include "common.hpp"
#include "Image.hpp"
#include "Flam4RenderDispatcher.hpp"

class rgba;
class Color;
class ColorNode;
class DeviceContext;
class ElectricSheepStuff;
class FlameExpanded;
class FlameTileArray;
class ImageRepTileArray;
class Flame;
class RenderParams;
class RenderState;

using uint                = unsigned int;
using SharedElc           = std::shared_ptr<ElectricSheepStuff>;
using SharedFE            = std::shared_ptr<FlameExpanded>;
using SharedRenderState   = std::shared_ptr<RenderState>;
using SharedImage         = std::shared_ptr<FA::Image>;
using SharedFlmTileArray  = std::shared_ptr<FlameTileArray>;
using SharedDeviceContext = std::shared_ptr<DeviceContext>;
using BladesVector        = std::vector<SharedFE>;
using SharedBlades        = std::shared_ptr<BladesVector>;

using namespace std::chrono;

class FAENGINE_API RenderTile {
public:
    
    static void normalizeXformWeights(Flame *flame);
    
    static std::vector<ColorNode> loadColorNodesFromFlame(struct Flame *flame);
    static void saveExpandedGradientToFlame(struct Flame *flame, enum PaletteMode paletteMode);
    
    static SharedFlmTileArray splitAreaIntoTiles(SharedDeviceContext clContext,
                                                 SharedFE fe,
                                                 float area,
                                                 float width,
                                                 float height,
                                                 Flame *flame,
                                                 bool flipped,
                                                 int desiredQuality,
                                                 int warpSize,
                                                 size_t maxTileMemory,
                                                 uint supersample);
    
    static SharedImage renderAsPreview(SharedDeviceContext clContext,
                                       int selectedDeviceNum,
                                       SharedBlades blades,
                                       FA::Rect rect,
                                       int quality,
                                       bool hasAlpha,
                                       int bitsPerPixel,
                                       const std::string &colorspaceName,
                                       bool flipped,
                                       float epsilon,
                                       uint maxWorkGroupSize,
                                       bool verbose,
                                       duration<double> & _duration,
                                       duration<double> & deDuration);
    
    static SharedImage doRender(RenderParams &request, float &fusedFraction, float &retainedFraction, duration<double> & _duration, duration<double> & deDuration);
    
    static bool checkProgress(SharedRenderState renderState,
                              std::string & variationSetUuid,
                              time_point<high_resolution_clock> now,
                              bool lastBlade,
                              bool lastSubSample,
                              size_t  bladeIndex,
                              size_t bladeBatches,
                              uint  subSample);

    
    static size_t cumulativeProgress(ImageRepTileArray *imageTiles, size_t tileIndex, size_t progressForTile);
    static unsigned maxProgress(FlameTileArray *tiles);
    
    static void logRenderState(std::string & prefix, FlameTileArray *tiles, ImageRepTileArray *imageTiles, int tileIndex, int progressForTile);
    
    static ProceedType checkForStop(SharedRenderState renderState,
                                    std::string & variationSetUuid,
                                    bool lastBlade,
                                    bool lastSubSample);

    static size_t renderedBatchesSoFar(SharedRenderState renderState,
                                       size_t subSampleBegin,
                                       size_t bladeIndexBegin);
    static void adjustBackgroundForCurves(SharedFE &fe);
    
    static bool renderBladeBatches(SharedRenderState renderState,
                                   std::string & variationSetUuid,
                                   SharedElc elc,
                                   size_t subSampleBegin,
                                   size_t bladeIndexBegin,
                                   size_t & renderedBatches,
                                   time_point<high_resolution_clock> now,
                                   bool alternateGPURendering);

    static SharedImage RenderTileCore(SharedRenderState renderState,
                                      SharedFE firstFe,
                                      SharedElc elc,
                                      size_t subSampleBegin,
                                      size_t bladeIndexBegin,
                                      uchar *backgroundImage,
                                      bool alternateGPURendering,
                                      duration<double> & _duration,
                                      duration<double> & deDuration);
    
    static SharedImage RenderPreviewTileSetCore(SharedRenderState renderState,
                                                FA::Image * backgroundImageRep,
                                                bool continuousMode,
                                                enum GpuPlatformUsed gpuPlatformInUse,
                                                SharedDeviceContext deviceContext,
                                                bool favorSplitMerge,
                                                duration<double> & _duration,
                                                duration<double> & deDuration);
    
    static SharedImage renderTileCoreThreaded(SharedRenderState renderState,
                                              SharedFE firstFe,
                                              SharedElc elc,
                                              size_t subSampleBegin,
                                              size_t bladeIndexBegin,
                                              uchar *backgroundImage,
                                              bool continuousMode,
                                              duration<double> & _duration,
                                              duration<double> & deDuration);
    
    static void RenderTileCoreThreadPart1(SharedRenderState renderState,
                                          SharedFE firstFe,
                                          uint selectedDeviceNum,
                                          uint subSampleBegin,
                                          duration<double> & buildTime);
    static uint RenderTileCoreThreadPart2(SharedRenderState renderState,
                                          SharedFE firstFe,
                                          SharedElc elc,
                                          SharedDispatcher dispatcher,
                                          uint selectedDeviceNum);
    static SharedImage RenderTileCoreThreadPart3(SharedRenderState renderState,
                                                 SharedFE firstFe,
                                                 SharedDispatcher dispatcher,
                                                 SharedElc elc,
                                                 uchar *backgroundImage,
                                                 uint selectedPostProcessDevNum,
                                                 duration<double> & _duration,
                                                 duration<double> & deDuration,
                                                 duration<double> buildTime);



    static const std::vector<uchar> getProfileFor(const std::string & choice);
};

#endif /* RenderTile_hpp */
