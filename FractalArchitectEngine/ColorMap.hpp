//
//  ColorMap.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/13/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef ColorMap_hpp
#define ColorMap_hpp

#include "Exports.hpp"
#include "ColorGradient.hpp"
#include "Enums.hpp"
#include <string>


class Color;
class Flame;
struct rgba;

struct FAENGINE_API ColorMap : public ColorGradient {
    
    Color background;
    
    ColorMap(Color & _background, ColorNodeVector _colorNodes, enum PaletteMode _paletteMode);
    ColorMap(rgba & _background, ColorNodeVector _colorNodes, enum PaletteMode _paletteMode);
    ColorMap(const ColorMap & c);
    
    bool operator==(const ColorMap & c);

    std::string info();
    
    static ColorMap *makeColorMapFromFlame(Flame *flame, enum PaletteMode paletteMode);
};

#endif /* ColorMap_hpp */
