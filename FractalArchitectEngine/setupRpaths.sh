#!/bin/sh

#  setupRpaths.sh
#  FractalArchitectEngine
#
#  Created by Steven Brodhead on 7/17/16.
#  Copyright © 2016 Centcom Inc. All rights reserved.

# this setus up the embedded RPATH for these libraries so they can be found at runtime
install_name_tool -id "@loader_path/Frameworks/libcudart.7.5.dylib"         libcudart.7.5.dylib
install_name_tool -id "@loader_path/Frameworks/libnvrtc.7.5.dylib"          libnvrtc.7.5.dylib
install_name_tool -id "@loader_path/Frameworks/libnvrtc-builtins.7.5.dylib" libnvrtc-builtins.7.5.dylib


#install_name_tool -add_rpath "@loader_path/../Frameworks" FA_Engine

#install_name_tool -change "@rpath/libcudart.7.5.dylib"         "@loader_path/Frameworks/libcudart.7.5.dylib"  FA_Engine
#install_name_tool -change "@rpath/libnvrtc.7.5.dylib"          "@loader_path/Frameworks/libnvrtc.7.5.dylib" FA_Engine
#install_name_tool -change "@rpath/libnvrtc-builtins.7.5.dylib" "@loader_path/Frameworks/libnvrtc-builtins.7.5.dylib" FA_Engine


install_name_tool -id "@executable_path/../Frameworks/libcudart.7.5.dylib"         libcudart.7.5.dylib
install_name_tool -id "@executable_path/../Frameworks/libnvrtc.7.5.dylib"          libnvrtc.7.5.dylib
install_name_tool -id "@executable_path/../Frameworks/libnvrtc-builtins.7.5.dylib" libnvrtc-builtins.7.5.dylib

install_name_tool -id "@rpath/libcudart.7.5.dylib"         libcudart.7.5.dylib
install_name_tool -id "@rpath/libnvrtc.7.5.dylib"          libnvrtc.7.5.dylib
install_name_tool -id "@rpath/libnvrtc-builtins.7.5.dylib" libnvrtc-builtins.7.5.dylib
