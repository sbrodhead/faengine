//
//  VariationSet.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/30/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <stdexcept>
#include <memory>
#include <set>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <ostream>
//#include <sstream>
#include <cstdlib>
#include <sys/stat.h>

#ifdef _WIN32
#include <sys/utime.h>
#else
#include <unistd.h>
#include <utime.h>
#endif

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif

#ifdef BOOST_REGEX
#include <boost/regex.hpp>
#define REGEX boost
#else
#include <regex>
#define REGEX std
#endif


#include "VariationSet.hpp"
#include "Variation.hpp"
#include "VariationParameter.hpp"
#include "Flame.hpp"
#include "Utilities.hpp"
#include "VarParInstance.hpp"
#include "VariationGroup.hpp"
#include "pugixml.hpp"
#include "FlameParse.hpp"
#include "Enums.hpp"
#include "DeviceProgram.hpp"
#include <string.h>

using std::string;

SharedVariationSet VariationSet::factoryLibrary;
SharedVariationSet VariationSet::myLibrary;
SharedVariationSet VariationSet::factoryPlusMyLibrary;
SharedVariationSet VariationSet::currentVariationSet;
VariationSetsDict  VariationSet::variationSets;
VariationSetsDict  VariationSet::variationSuperSets;

template std::string string_format<uint>(const char *, uint);
template std::string string_format<const char *>(const char *, const char *);
template std::string string_format<const char *, unsigned int>(const char *, const char *, unsigned int);
template std::string string_format<const char *, const char *>(const char *, const char *, const char *);

using namespace pugi;

#pragma mark Constructors

VariationSet::VariationSet() noexcept
: std::enable_shared_from_this<VariationSet>(),
    variationsByKey(),
    variationsByName(),
    variationParametersByKey(),
    variationParametersByAlias(),
    variationsByXformIndex(),
    transformProperties(),
    namesInUse(),
    uuid(),
    defaultVariation("linear"),
    name(uniqueUnknownName()),
    version("1.0"),
    amLibrary(false),
    inUse(false),
    is3DCapable(false),
    amTemporary(false),
    variationStructName("varpar"),
    transformPropertiesByKey{
        { "a", { "a", 0}},
        { "b", { "b", 1}},
        { "c", { "c", 2}},
        { "d", { "d", 3}},
        { "e", { "e", 4}},
        { "f", { "f", 5}},
        { "pa", { "pa", 6}},
        { "pb", { "pb", 7}},
        { "pc", { "pc", 8}},
        { "pd", { "pd", 9}},
        { "pe", { "pe", 10}},
        { "pf", { "pf", 11}},
        { "color",     { "color", 12}},
        { "symmetry",  { "symmetry", 13}},
        { "weight",    { "weight", 14}},
        { "opacity",   { "opacity", 15}},
        { "var_color", { "var_color", 16}},
        { "rotates",   { "rotates", 17}},
    }
{}

VariationSet::VariationSet(const SharedVariationSet &v) noexcept
:   std::enable_shared_from_this<VariationSet>(*v),
    variationsByKey(v->variationsByKey),
    variationsByName(v->variationsByName),
    variationParametersByKey(v->variationParametersByKey),
    variationParametersByAlias(v->variationParametersByAlias),
    variationsByXformIndex(v->variationsByXformIndex),
    transformProperties(v->transformProperties),
    namesInUse(v->namesInUse),
    uuid(v->uuid),
    defaultVariation(v->defaultVariation),
    name(v->name),
    version(v->version),
    amLibrary(v->amLibrary),
    inUse(v->inUse),
    is3DCapable(v->is3DCapable),
    amTemporary(v->amTemporary),
    variationStructName(v->variationStructName),
    transformPropertiesByKey(v->transformPropertiesByKey)
{}

VariationSet & VariationSet::operator=(const VariationSet &v) noexcept
{
    std::enable_shared_from_this<VariationSet>::operator=(v);

    variationsByKey            = v.variationsByKey;
    variationsByName           = v.variationsByName;
    variationParametersByKey   = v.variationParametersByKey;
    variationParametersByAlias = v.variationParametersByAlias;
    variationsByXformIndex     = v.variationsByXformIndex;
    transformProperties        = v.transformProperties;
    namesInUse                 = v.namesInUse;
    uuid                       = v.uuid;
    defaultVariation           = v.defaultVariation;
    name                       = v.name;
    version                    = v.version;
    amLibrary                  = v.amLibrary;
    inUse                      = v.inUse;
    is3DCapable                = v.is3DCapable;
    amTemporary                = v.amTemporary;
    variationStructName        = v.variationStructName;
    transformPropertiesByKey   = v.transformPropertiesByKey;
    return *this;
}

// when initializing from a varset file
// NOTE: caller must call loadFromStream() to actually load the variation set from the file AFTER this has been constructed
VariationSet::VariationSet(std::istream & in, bool _amLibrary)
    : VariationSet()
{
    amLibrary = _amLibrary;
}

VariationSet::VariationSet(const SharedVariationSet  &library,
                           const StringsVector &variationNames,
                           const std::string   &preferredName,
                           const std::string   &_uuid)
: VariationSet()
{
    name = preferredName.length() > 0 ? preferredName : "OnDemand";
    uuid=_uuid;
}

VariationSet::VariationSet(const SharedVariationSet  &library,
                           const StringsVector &variationNames,
                           const std::string   &preferredName,
                           const std::string   &_uuid,
                           bool                 _amTemporary)
: VariationSet(library, variationNames, preferredName, _uuid)
{
    amTemporary = _amTemporary;
}

VariationSet::VariationSet(const StringsVector &variationKeys,
                           const std::string   &preferredName,
                           const std::string   &_uuid,
                           bool                 incomplete)
: VariationSet(variationsLibraryPlus(), variationKeys, preferredName, _uuid)
{
    if (! incomplete)
        finishCopyFromOtherVariationSet(true);
}

VariationSet::VariationSet(const SharedVariationSet  &library,
                           const StringsVector &variationNames,
                           const std::string   &preferredName,
                           const std::string   &_uuid,
                           bool                 incomplete,
                           bool                 _amTemporary)
: VariationSet(library, variationNames, preferredName, _uuid, _amTemporary)
{
    amTemporary = _amTemporary;
    if (! incomplete)
        finishCopyFromOtherVariationSet(true);
}

VariationSet::~VariationSet()
{
    //fprintf(stderr, "Destructor VariationSet %s\n", name.c_str());
}

SharedVariationSet VariationSet::makeVariationSet(pugi::xml_node & variationSetEle, bool installAsIs)
{
    SharedVariationSet variationSet = std::make_shared<VariationSet>();

    // install the variation set using the variation definitions used in the Flame document
    //   (might conflict with defn in Library)
    if (installAsIs) {
        bool result = variationSet->parseXmlElement(variationSetEle);
        if (! result) throw std::runtime_error("Failed");

        variationSet->saveKernelsAmUpgradedAllPlatforms(false);
    }
    else {
        std::string varsetName;
        std::string _uuid;
        StringsVector variationNames = VariationSet::parseXmlElementForVariationNames(variationSetEle, varsetName, _uuid);

        variationSet->name = varsetName;
        variationSet->uuid = _uuid;

        StringsSet variationNamesSet;
        std::copy(variationNames.begin(), variationNames.end(), std::inserter(variationNamesSet, variationNamesSet.end()));
        const SharedVariationSet & library = VariationSet::variationsLibrary();

        variationSet->copyVariationsFrom(library, variationNames, variationNamesSet);
        variationSet->finishCopyFromOtherVariationSet(installAsIs);
    }
    return variationSet;
}

void VariationSet::copyVariationsFrom(const SharedVariationSet  &library,
                                      const StringsVector &variationNames,
                                      StringsSet & variationNamesSet)
{
    StringsSet variations3D = prerequisiteVariationsFor3D();
    uint variationIndex      = 0;

    // see if this variation set is 3D
    for (const std::string &_name : variationNames) {
        auto p = library->variationsByName.find(_name);
        if (p != library->variationsByName.end()) {
            const SharedVariation &variation = p->second;
            if (variation->dimension == dim3D)
                is3DCapable = true;
            if (variations3D.find(_name) != variations3D.end())
                is3DCapable = true;
        }
    }
    // older variation sets do not have the new matrix variations
    StringsVector preMatrixSupport = is3DCapable ? preMatrix3dSupport() : preMatrix2dSupport();
    for (const std::string &_name : preMatrixSupport) {
        if (variationNamesSet.find(_name) == variationNamesSet.end()) {
            VariationsDict::const_iterator p = library->variationsByName.find(_name);

            if (p != library->variationsByName.end()) {
                const SharedVariation &variation  = p->second;
                SharedVariation &mutableVariation = const_cast<SharedVariation &>(variation);
                copyVariation(mutableVariation, library);
                mutableVariation->setXformIndex(FIRST_VAR_OFFSET + variationIndex++);
            }
        }
    }
    //
    for (const std::string &_name : variationNames) {
        auto p = library->variationsByName.find(_name);
        if (p != library->variationsByName.end()) {
            const SharedVariation &variation  = p->second;
            SharedVariation &mutableVariation = const_cast<SharedVariation &>(variation);
            copyVariation(mutableVariation, library);
            mutableVariation->setXformIndex(FIRST_VAR_OFFSET + variationIndex++);
        }
    }
    // insert ordinary matrix variations here
    StringsVector matrixSupport = is3DCapable ? matrix3dSupport() : matrix2dSupport();
    for (const std::string &_name : matrixSupport) {
        if (variationNamesSet.find(_name) == variationNamesSet.end()) {
            VariationsDict::const_iterator p = library->variationsByName.find(_name);

            if (p != library->variationsByName.end()) {
                const SharedVariation &variation  = p->second;
                SharedVariation &mutableVariation = const_cast<SharedVariation &>(variation);
                copyVariation(mutableVariation, library);
                mutableVariation->setXformIndex(FIRST_VAR_OFFSET + variationIndex++);
            }
        }
    }
    // post matrix variations have to be the very last ones
    StringsVector postMatrixSupport = is3DCapable ? postMatrix3dSupport() : postMatrix2dSupport();
    for (const std::string &_name : postMatrixSupport) {
        if (variationNamesSet.find(_name) == variationNamesSet.end()) {
            VariationsDict::const_iterator p = library->variationsByName.find(_name);

            if (p != library->variationsByName.end()) {
                const SharedVariation &variation  = p->second;
                SharedVariation &mutableVariation = const_cast<SharedVariation &>(variation);
                copyVariation(mutableVariation, library);
                mutableVariation->setXformIndex(FIRST_VAR_OFFSET + variationIndex++);
            }
        }
    }
}

SharedParameter VariationSet::parameterForKey(const std::string &key) const
{
    auto it = variationParametersByKey.find(key);

    if (it == variationParametersByKey.end())
        return nullptr;
    return it->second;
}

void VariationSet::addCopy(SharedVariation copy, Variation &variation)
{
    copy->variationSet = shared_from_this(); // copy is in a different variationSet from the source

    size_t variationCount = variationsByKey.size();
    copy->setXformIndex((int)variationCount + FIRST_VAR_OFFSET);


    variationsByKey[copy->key]   = copy;
    variationsByName[copy->name] = copy;
    namesInUse.insert(copy->name);

    ParametersVector sortedParameters;
    for( auto it = variation.parameters.begin(); it != variation.parameters.end(); ++it ) {
        sortedParameters.push_back( it->second );
    }
    std::sort(sortedParameters.begin(), sortedParameters.end(), VariationParameter::xformIndexCompare);

    for (auto &sp1 : sortedParameters) {
        std::string parameterName = sp1->name;

        // deep copy the parameters - replace the old parameter with its copy
        SharedParameter &parameter = copy->parameters[parameterName];
        SharedParameter parameterCopy = std::make_shared<VariationParameter>(*parameter);

        parameterCopy->variation = std::weak_ptr<Variation>(variation.shared_from_this());
        copy->parameters.operator[](parameterName) = parameterCopy;

        // attach parameter copy to the correct variation copy

        SharedVariation &correctVariation = variationsByKey[parameterCopy->variation.lock()->key];
        parameterCopy->variation = correctVariation;

        variationParametersByKey[parameterCopy->key] = parameterCopy;
        namesInUse.insert(parameterCopy->key);

        if (parameterCopy->alias) {
            variationParametersByAlias[*parameterCopy->alias] = parameterCopy;
            namesInUse.insert(*parameterCopy->alias);
        }
    }
//    for( auto it = sortedParameters.begin(); it != sortedParameters.end(); ++it ) {
//        SharedParameter & param = *it;
//        std::cout << param->key << "  UseCount:" << param.use_count() << "\n";
//    }
}

void VariationSet::setupTransformProperties()
{
    for (auto & v : variationsByKey) {
        const std::string &variationKey = v.first;
        transformProperties.push_back(variationKey);
    }
    for (auto & p : variationParametersByKey) {
        const std::string &parameterKey = p.first;
        transformProperties.push_back(parameterKey);
    }
    std::unordered_set<std::string> properties
    { "a", "b", "c", "d", "e", "f",
      "pa", "pb", "pc", "pd", "pe", "pf",
      "color", "symmetry", "weight", "opacity", "var_color", "rotates",
    // these do not have associated xformIndex has they are derived properties
      "colorSpeed", "preXformMatrix", "postXformMatrix"
    };
    for (auto & property : properties) {
        transformProperties.push_back(property);
    }
}

ParametersVector VariationSet::makeSortedParamVector(SharedVariation & variation, ParameterCompareFunc f)
{
    ParametersVector sortedParameters;
    for( auto it = variation->parameters.begin(); it != variation->parameters.end(); ++it ) {
        sortedParameters.push_back( it->second );
    }
    std::sort(sortedParameters.begin(), sortedParameters.end(), f);
    return sortedParameters;
}

SharedVariationsVector VariationSet::makeSortedVariations(VariationCompareFunc f)
{
    SharedVariationsVector sortedVariations;
    for( auto it = variationsByKey.begin(); it != variationsByKey.end(); ++it ) {
        sortedVariations.push_back( it->second );
    }
    std::sort(sortedVariations.begin(), sortedVariations.end(), f);
    return sortedVariations;
}

UintPairsVector VariationSet::makeUintPairsVector(UintPairsDict & dict, UintPairCompareFunc f)
{
    UintPairsVector pairs;
    for( auto it = dict.begin(); it != dict.end(); ++it ) {

        pairs.push_back( it->second );
    }
        std::sort(pairs.begin(), pairs.end(), f);
    return pairs;
}

void VariationSet::finishCopyFromOtherVariationSet(bool installAsIs)
{
    if (variationsByKey.size() == 0)
        return;
    if (uuid.length() == 0)
        uuid = stringWithUUID();

    variationSets[uuid] = shared_from_this();

    if (variationSets.find(uuid) == variationSets.end())
        variationSets[uuid] = shared_from_this();

    // now reorder putting pre_ variations first, then normal, then post_ variations at end
    size_t newIndex = 0;
    size_t parameterIndex = 0;
    UintPairsDict dict;
    SharedVariationsVector sortedVariations = makeSortedVariations(Variation::prePostXformIndexCompare);

    for (SharedVariation & variation : sortedVariations) {
        variation->setXformIndex(FIRST_VAR_OFFSET + newIndex++);

        ParametersVector sortedParameters = makeSortedParamVector(variation, VariationParameter::xformIndexCompare);
        // we want to order parameters first by their parent variation's xformIndex, then by the own local xformIndex
        for (auto & parameter : sortedParameters) {
            dict[parameter->key] = std::make_pair(parameter->key, parameterIndex++);
        }
    }
//    printf("%s\n", description().c_str());

    // reorder parameters first by their parent variation's xformIndex, then by the own local xformIndex
    newIndex = 0;
    // adjust each parameter's xformIndex by

    UintPairsVector pairs = makeUintPairsVector(dict, pairCompare);

    for (auto & pair : pairs) {
        std::string parameterKey = pair.first;
        SharedParameter &parameter = variationParametersByKey[parameterKey];
        parameter->xformIndex = newIndex++ + (int)variationsByKey.size() + FIRST_VAR_OFFSET;
    }

    setupTransformProperties();

    // create code from templates and save it
    saveKernelsAmUpgradedAllPlatforms(installAsIs);
}

bool VariationSet::pairCompare(const UintsPair &a, const UintsPair &b)
{
    return a.second < b.second;
}


void VariationSet::copyVariation(SharedVariation &variation, const SharedVariationSet &variationSet)
{
    // check for duplicates
    if (variationsByKey.find(variation->key) != variationsByKey.end())
        return;

    SharedVariation copy = Variation::makeSharedCopy(*variation);
    addCopy(copy, *variation);
}

// when initializing from a varset file
void VariationSet::loadFromStream(std::istream & in)
{
    std::string contents((std::istreambuf_iterator<char>(in)),
                         std::istreambuf_iterator<char>());
    bool status = readFromData(contents);

    if (! status) throw std::runtime_error("Failed");

    // libraries are not saved in variationSets dictionary
    if (variationSets.find(uuid) == variationSets.end() && ! amLibrary) {
        variationSets[uuid] = shared_from_this();
        saveKernelsAmUpgradedAllPlatforms(false);
    }
}


bool VariationSet::readFromData(const std::string &string)
{
    xml_document doc;
    xml_parse_result result = doc.load_string(string.c_str(), parse_cdata | parse_comments);

    if (result.status != status_ok) {
        throw std::runtime_error(result.description());
    }

    return parseXmlElement(doc.document_element());
}

void VariationSet::saveKernelsAmUpgradedAllPlatforms(bool upgraded)
{
#if defined(OPENCL_ONLY)
    saveKernelsAmUpgraded(upgraded, useOpenCL);
#elif defined(METAL_ONLY)
    saveKernelsAmUpgraded(upgraded, useMetal);
#elif defined(CUDA_ONLY)
    saveKernelsAmUpgraded(upgraded, useCUDA);
#else
    saveKernelsAmUpgraded(upgraded, useOpenCL);
#ifndef NO_CUDA
    saveKernelsAmUpgraded(upgraded, useCUDA);
#endif
//    saveKernelsAmUpgraded(upgraded, useMetal);
#endif
}

void VariationSet::addVariation(Variation &variation)
{
    // dont overwrite earlier entries in dictionary when Variation is copied
    if (variationsByKey.find(variation.key) != variationsByKey.end())
        return;

    SharedVariation sharedVariation  = variation.shared_from_this();
    variationsByKey[variation.key]   = sharedVariation;
    variationsByName[variation.name] = sharedVariation;
    namesInUse.insert(variation.name);
}

void VariationSet::addVariationParameter(VariationParameter &parameter)
{
    if (variationParametersByKey.find(parameter.key) != variationParametersByKey.end())
        return;

    SharedParameter sharedParameter = parameter.shared_from_this();
    variationParametersByKey[parameter.key] = sharedParameter;
    namesInUse.insert(parameter.key);
}

void VariationSet::variationChangedName(SharedVariation variation, const std::string &oldName)
{
    if (variationsByName.find(variation->name) != variationsByName.end())
        variationsByName.erase(variation->name);
    variationsByName[variation->name] = variation;
    namesInUse.insert(variation->name);

    variationsByName.erase(oldName);
    namesInUse.erase(oldName);
}

void VariationSet::variationChangedKey(SharedVariation variation, const std::string &oldKey)
{
    if (variationsByKey.find(variation->key) != variationsByKey.end())
        variationsByKey.erase(variation->key);
    variationsByKey[variation->key] = variation;
    variationsByKey.erase(oldKey);
}

void VariationSet::variationChangedXformIndex(SharedVariation variation, size_t xformIndex, size_t oldXformIndex)
{
    if (variationsByXformIndex.find(xformIndex) != variationsByXformIndex.end())
        variationsByXformIndex.erase(xformIndex);
    variationsByXformIndex[xformIndex] = variation;
    variationsByXformIndex.erase(oldXformIndex);
}

void VariationSet::parameterChangedKey(SharedParameter parameter, const std::string &oldKey)
{
    if (variationParametersByKey.find(parameter->key) != variationParametersByKey.end())
        variationParametersByKey.erase(parameter->key);
    variationParametersByKey[parameter->key] = parameter;
    namesInUse.insert(parameter->key);

    variationParametersByKey.erase(oldKey);
    namesInUse.erase(oldKey);
}

// if it is my own key, it is still unique
bool VariationSet::keyIsNotUnique(const std::string &variationKey) const
{
    bool factoryNotUnique = factoryLibrary->variationsByKey.find(variationKey) != factoryLibrary->variationsByKey.end();
    bool myNotUnique      = myLibrary->variationsByKey.find(variationKey) != myLibrary->variationsByKey.end();
    return factoryNotUnique || myNotUnique;
}

// if it is my own key, it is still unique
bool VariationSet::keyIsNotUnique(SharedVariationConst variation, const std::string &variationKey) const
{
    auto factoryVarIt = factoryLibrary->variationsByKey.find(variationKey);
    auto myVarIt      = myLibrary->variationsByKey.find(variationKey);

    bool factoryNotUnique = false;
    if (factoryVarIt != factoryLibrary->variationsByKey.end()) {
        auto factoryVariation = factoryVarIt->second;
        if (factoryVariation != variation)
            factoryNotUnique = true;
    }
    bool myNotUnique = false;
    if (myVarIt != myLibrary->variationsByKey.end()) {
        auto myVariation = myVarIt->second;
        if (myVariation != variation)
            myNotUnique = true;
    }
    return factoryNotUnique || myNotUnique;
}

// if it is my own name, it is still unique
bool VariationSet::nameIsNotUnique(SharedVariationConst variation, const std::string &variationName) const
{
    auto factoryVarIt = factoryLibrary->variationsByName.find(variationName);
    auto myVarIt      = myLibrary->variationsByName.find(variationName);

    bool factoryNotUnique = false;
    if (factoryVarIt != factoryLibrary->variationsByKey.end()) {
        auto factoryVariation = factoryVarIt->second;
        if (factoryVariation != variation)
            factoryNotUnique = true;
    }
    bool myNotUnique = false;
    if (myVarIt != myLibrary->variationsByKey.end()) {
        auto myVariation = myVarIt->second;
        if (myVariation != variation)
            myNotUnique = true;
    }
    return factoryNotUnique || myNotUnique;
}

std::string VariationSet::uniqueUnknownName()
{
    std::string firstName = "Unknown";
    bool found = false;
    for (auto & pair : variationSets) {
        const SharedVariationSet &variationSet = pair.second;
        if (variationSet->name == firstName) {
            found = true;
            break;
        }
    }
    if (! found)
        return firstName;

    uint count = 1;
    for (;;) {
        std::string name = string_format("Unknown%u", count++);

        found = false;
        for (auto & pair : variationSets) {
            const SharedVariationSet &variationSet = pair.second;
            if (variationSet->name == name) {
                found = true;
                break;
            }
        }
        if (! found)
            return name;
    }
    return "";
}

StringsSet VariationSet::prerequisiteVariationsFor3D()
{
    return StringsSet{
        "pre_flatten",
        "pre_zscale",
        "pre_ztranslate",
        "pre_rotate_x",
        "pre_rotate_y",
        "zscale",
        "zblur",
        "ztranslate",
        "zcone",
        "post_rotate_x",
        "post_rotate_y",
        "post_flatten",
        "pre_matrix3d",
        "matrix3d",
        "post_matrix3d",
    };
}

StringsVector VariationSet::preMatrix3dSupport()
{
    return StringsVector{"pre_matrix2d", "pre_matrix3d"};
}

StringsVector VariationSet::matrix3dSupport()
{
    return StringsVector{"matrix2d", "matrix3d"};
}

StringsVector VariationSet::postMatrix3dSupport()
{
    return StringsVector{"post_matrix2d", "post_matrix3d"};
}

StringsVector VariationSet::preMatrix2dSupport()
{
    return StringsVector{"pre_matrix2d"};
}

StringsVector VariationSet::matrix2dSupport()
{
    return StringsVector{"matrix2d"};
}

StringsVector VariationSet::postMatrix2dSupport()
{
    return StringsVector{"post_matrix2d"};
}

std::vector<SharedVariationSet> VariationSet::variationSetsUsing(const Variation &libraryVariation)
{
    std::vector<SharedVariationSet> array;

    for (auto & v : variationSets) {
        SharedVariationSet &variationSet = v.second;
        if (variationSet->amTemporary)
            continue;
        if (variationSet->variationsByKey.find(libraryVariation.key) != variationSet->variationsByKey.end()) {
            array.push_back(variationSet);
        }
    }
    return array;
}

bool VariationSet::unknownVariation(const SharedVariation &variation)
{
    bool factory = factoryLibrary->variationsByKey.find(variation->key) != factoryLibrary->variationsByKey.end();
    bool mine    = myLibrary->variationsByKey.find(variation->key)      != myLibrary->variationsByKey.end();
    return !factory && !mine;
}

const SharedVariationSet VariationSet::legacyVariationSet()
{
    return VariationSet::variationSetForUuid(uuidForLegacyVariationSet());
}


const SharedVariationSet & VariationSet::getFactoryLibrary()
{
    return VariationSet::factoryLibrary;
}

const SharedVariationSet & VariationSet::getMyLibrary()
{
    return VariationSet::myLibrary;
}

const SharedVariationSet & VariationSet::variationsLibrary()
{
    return factoryLibrary;
}

const SharedVariationSet & VariationSet::myVariationsLibrary()
{
    return myLibrary;
}

const SharedVariationSet & VariationSet::variationsLibraryPlus()
{
    return VariationSet::factoryPlusMyLibrary;
}

StringsVector VariationSet::factoryLibraryVariationKeys(VariationCompareFunc f)
{
    SharedVariationsVector sortedVariations;
    for( auto it = factoryLibrary->variationsByKey.begin(); it != factoryLibrary->variationsByKey.end(); ++it ) {
        sortedVariations.push_back( it->second );
    }
    std::sort(sortedVariations.begin(), sortedVariations.end(), f);

    StringsVector keys;
    for (auto & sp : sortedVariations)
    {
        keys.emplace_back(sp->key);
    }
    return keys;
}

StringsVector VariationSet::myLibraryVariationKeys(VariationCompareFunc f)
{
    SharedVariationsVector sortedVariations;
    for( auto it = myLibrary->variationsByKey.begin(); it != myLibrary->variationsByKey.end(); ++it ) {
        sortedVariations.push_back( it->second );
    }
    std::sort(sortedVariations.begin(), sortedVariations.end(), f);

    StringsVector keys;
    for (auto & sp : sortedVariations)
    {
        keys.emplace_back(sp->key);
    }
    return keys;
}

bool VariationSet::canCreateFromLibraryForVariationNames(StringsSet variationNames)
{
    StringsSet variationNamesSet(variationNames);
    StringsSet variations3D(prerequisiteVariationsFor3D());
    const VariationSet &library = variationsLibraryPlus();
    bool is3DCapable = false;

    for (const std::string &_name : variationNames) {
        VariationsDict::const_iterator p = library.variationsByName.find(_name);
        if (p != library.variationsByName.end()) {
            const SharedVariation &variation  = p->second;
            if (variation->dimension == dim3D)
                is3DCapable = true;
            if (variations3D.find(_name) != variations3D.end())
                is3DCapable = true;

        }
        else {
            return false;
        }
    }

    if (is3DCapable) {
        for (const std::string &_name : variations3D) {
            variationNames.insert(_name);
        }
    }
    uint variationCount = (uint)variationNamesSet.size();
    uint paramsCount    = 0;
    for (const std::string &_name : variationNamesSet) {
        VariationsDict::const_iterator p = library.variationsByName.find(_name);
        if (p != library.variationsByName.end()) {
            const SharedVariation &variation  = p->second;
            paramsCount += variation->parameterCount();
        }
    }
    if (variationCount > MAX_VARIATIONS_IN_VARSET)
        return false;
    //    if (variationCount + paramsCount + OTHER_STUFF_COUNT > XFORM_VARS_MAX_COUNT)
    //        return NO;
    return true;
}

SharedVariationSet VariationSet::variationSetForUuid(const std::string &uuid)
{
    auto vector = ::allKeys(variationSets);
    if (variationSets.find(uuid) == variationSets.end())
        return std::shared_ptr<VariationSet>();
    SharedVariationSet variationSet = variationSets[uuid];
    return variationSet;
}

SharedVariationSet VariationSet::firstVariationSetForName(const std::string &name)
{
    for(auto iterator = variationSets.begin(); iterator != variationSets.end(); iterator++) {
        SharedVariationSet &variationSet = iterator->second;
        if (variationSet->name == name)
            return variationSet;
    }
    return std::shared_ptr<VariationSet>();
}

SharedVariationSet VariationSet::variationSuperSetForUuid(const std::string & uuid)
{
    if (variationSuperSets.find(uuid) == variationSuperSets.end())
        return std::shared_ptr<VariationSet>();
    SharedVariationSet & variationSet = variationSuperSets[uuid];
    return variationSet;
}

void VariationSet::setSuperSetFor(const std::string &subSetUUID, const SharedVariationSet &variationSet)
{
    variationSuperSets[subSetUUID] = variationSet->shared_from_this();
}

SharedVariationSet VariationSet::searchForVariationSetForUuid(const char * _uuid)
{
    std::string uuid = _uuid;
    return searchForVariationSetForUuid(uuid);
}

SharedVariationSet VariationSet::searchForVariationSetForUuid(const std::string & uuid)
{
    if (variationSets.find(uuid) != variationSets.end()) {
        SharedVariationSet &variationSet =  variationSets[uuid];
        return variationSet;
    }
    return variationSuperSetForUuid(uuid);
}

void VariationSet::updateMergedLibrary()
{
    StringsVector factoryKeys = factoryLibraryVariationKeys(Variation::prePostXformIndexCompare);
    StringsVector myLibKeys   = myLibraryVariationKeys(Variation::prePostXformIndexCompare);

    SharedVariationSet newLibrary =
        std::make_shared<VariationSet>(factoryLibrary, factoryKeys, "factoryPlusMyLibrary", stringWithUUID(), true);

    StringsSet variationNamesSet(factoryKeys.begin(), factoryKeys.end());
    newLibrary->copyVariationsFrom(factoryLibrary, factoryKeys, variationNamesSet);

    newLibrary->amLibrary = true;

    for (std::string &_key : myLibKeys) {
        SharedVariation &fromVariation = myLibrary->variationsByKey[_key];

        // only merge variations from My Library marked as Useable
        if (fromVariation->status == useable) {
            if (newLibrary->variationsByKey.find(_key) == newLibrary->variationsByKey.end())
                newLibrary->copyVariation(fromVariation, myLibrary);
        }
    }
    factoryPlusMyLibrary = newLibrary;
}

void VariationSet::duplicateVariation(const SharedVariation &variation)
{
    SharedVariation copy       = std::shared_ptr<Variation>(new Variation(*variation));
    std::string & variationKey = copy->key;
    // look for new unique variation name
    uint i = 1;
    while (keyIsNotUnique(variationKey))
        variationKey = string_format("%s_x%u", copy->key.c_str(), i++);

    copy->usageCount = 0;
    copy->variationSet = shared_from_this(); // copy is in a different variationSet from the source
    copy->setKeyAsIs(variationKey);  // dont want the changeKeyFrom: method to be called as parameters copy has not been completed
    copy->setNameAsIs(variationKey);

    size_t variationCount = variationsByKey.size();
    copy->xformIndex = (uint)variationCount + FIRST_VAR_OFFSET;

    variationsByKey[copy->key]   = copy;
    variationsByName[copy->name] = copy;
    namesInUse.insert(copy->name);

    std::string targetString  = string_format("varpar->%s", copy->key.c_str());
    std::string replaceString = string_format("varpar->%s", variationKey.c_str());

    std::string mutableSource = copy->source;

    REGEX::regex_replace(mutableSource, REGEX::regex(targetString), replaceString);
    copy->source = mutableSource;

    ParametersVector sortedParameters;
    for( auto it = copy->parameters.begin(); it != copy->parameters.end(); ++it ) {
        sortedParameters.push_back( it->second );
    }
    std::sort(sortedParameters.begin(), sortedParameters.end(), VariationParameter::xformIndexCompare);

    // we have to also adjust the parameter keys for the new unique variation key
    for (auto &sp : sortedParameters) {
        std::string parameterName = sp->name;;

        // deep copy the parameters - replace the old parameter with its copy
        SharedParameter &parameter        = copy->parameters[parameterName];
        SharedParameter  parameterCopy    = std::make_shared<VariationParameter>(*parameter);
        std::string oldParamKey           = parameter->key;
        std::string newParamKey           = string_format("%s_x%s", copy->key.c_str(), parameter->name.c_str());
        parameterCopy->setKeyAsIs(newParamKey);

        copy->parameters[parameterName]    = parameterCopy;

        // attach parameter copy to the correct variation copy
        const SharedVariation &correctVariation = variationsByKey[parameterCopy->variation.lock()->key];
        parameterCopy->variation                = correctVariation;

        variationParametersByKey[parameterCopy->key] = parameterCopy;
        namesInUse.insert(parameterCopy->key);

        if (parameterCopy->alias) {
            variationParametersByAlias[*parameterCopy->alias] = parameterCopy;
            namesInUse.insert(*parameterCopy->alias);
        }
    }
    copy->localXformIndexParamOrdering();
}

void VariationSet::addVariationToMyLibrary(SharedVariation &variation)
{
    myLibrary->duplicateVariation(variation);
    VariationSet::saveMyLibraryXML();
}

// walk through foreign variation set checking for custom variations whose source is different from MyLibrary's source
// These custom variations need to be made unique, then added to My Library
// NOTE this step only affects the foreign variation set - NOT the app's variation sets
void VariationSet::mergeForeignVariationSetIntoMyLibrary(SharedVariationSet &variationSet)
{
    // if we found new variation types in the file, add them to My Library
    bool haveNewVariations = false;

    for (const std::string & variationKey : allKeys(variationSet->variationsByKey)) {
        if (factoryLibrary->variationsByKey.find(variationKey) != factoryLibrary->variationsByKey.end())
            continue;   // not custom

        SharedVariation &fVariation  = variationSet->variationsByKey[variationKey];
        if (myLibrary->variationsByKey.find(variationKey) == myLibrary->variationsByKey.end()) {
            haveNewVariations = true;
            addVariationToMyLibrary(fVariation);
        }
        else {  // need to check for same variation definition
            SharedVariation &myVariation  = myLibrary->variationsByKey[variationKey];

            if (myVariation->isCompressedEqualTo(fVariation))  // myLibrary version is the same
                continue;

            // autoVersion the foreign Variation and add it to MyLibrary
            fVariation->autoVersionDifferentFrom(StringsSet());  // key, name, and source modified for new autoversion
            haveNewVariations = true;
            addVariationToMyLibrary(fVariation);
        }
    }
    if (haveNewVariations) {
        VariationSet::saveLibraryXML();
        updateMergedLibrary();
    }
}

#pragma mark Size Info

size_t VariationSet::varsUsed() const
{
    return variationsByKey.size();
}

size_t VariationSet::paramsUsed() const
{
    return variationParametersByKey.size();
}

size_t VariationSet::otherUsed() const
{
    return OTHER_STUFF_COUNT;
}

size_t VariationSet::slotsUsed() const
{
    return varsUsed() + paramsUsed() + otherUsed();
}

bool VariationSet::valid() const
{
    return varsUsed() <= MAX_VARIATIONS_IN_VARSET && (slotsUsed() - otherUsed()) > 0;
}

size_t VariationSet::sourceLength()
{
    size_t sourceLength = 0;

    for (auto & v : variationsByKey)  {
        const std::string &key = v.first;
        const SharedVariation &var = variationsByKey[key];
        sourceLength += var->source.length();
    }
    return sourceLength;
}

Variation * VariationSet::variationForXformIndex(size_t xformIndex)
{
    if (variationsByXformIndex.find(xformIndex) != variationsByXformIndex.end())
        return variationsByXformIndex[xformIndex].get();
    return nullptr;
}

SharedVariation VariationSet::sharedVariationForKey(const std::string &_key)
{
    std::string key = _key;
    size_t pos      = key.find_first_of('#');
    if (pos != std::string::npos)
        key = key.substr(0, pos);

    if (variationsByKey.find(key) != variationsByKey.end()) {
        return variationsByKey[key];
    }
    if (variationsByName.find(key) != variationsByName.end()) {
        SharedVariation &variation = variationsByName[key];
        if (variation->key.substr(0, 2) == "v_" && variation->key.substr(2) == key)
            return variation;
    }
    return std::shared_ptr<Variation>();
}

Variation * VariationSet::variationForKey(const std::string &_key)
{
    std::string key = _key;
    size_t pos      = key.find_first_of('#');
    if (pos != std::string::npos)
        key = key.substr(0, pos);

    if (variationsByKey.find(key) != variationsByKey.end()) {
        return variationsByKey[key].get();
    }
    if (variationsByName.find(key) != variationsByName.end()) {
        SharedVariation &variation = variationsByName[key];
        if (variation->key.substr(0, 2) == "v_" && variation->key.substr(2) == key)
            return variation.get();
    }
    return nullptr;
}

SharedVariationSet VariationSet::newDuplicateVariationSet(SharedVariationSet &variationSet) const
{
    auto vector = keysSortedByValue(variationSet->variationsByKey, Variation::prePostXformIndexPairCompare);

    SharedVariationSet vs = std::make_shared<VariationSet>(vector, variationSet->name, stringWithUUID(), false);
    variationSets[vs->uuid] = vs;
    return vs;
}

#pragma mark Current Variation Set

VariationSetsDict VariationSet::getVariationSets()
{
    return variationSets;
}

SharedVariationSet VariationSet::getCurrentVariationSet()
{
    return currentVariationSet;
}

void VariationSet::setCurrentVariationSet(const SharedVariationSet &variationSet)
{
    if (currentVariationSet.get() == variationSet.get())
        return;
    currentVariationSet = variationSet;
}

#pragma mark Class methods

std::string VariationSet::uuidForLegacyVariationSet()
{
    return "9D359843-B742-4C5D-BB0B-083E604EE902";
}

std::string VariationSet::uuidForStandard3DVariationSet()
{
    return "0D9C9B7F-D162-4F98-B498-C0BED436F556";
}

std::string VariationSet::uuidForVariationsLibrary()
{
    return "17425032-84CC-45CE-8393-4E6431E9D0EE";
}

std::string VariationSet::uuidForMyVariationsLibrary()
{
    return "D6E6E784-09DA-4020-A927-B1A579562614";
}

void VariationSet::setupDefaultVariationSet()
{
    SharedVariationSet set = VariationSet::variationSetForUuid(uuidForStandard3DVariationSet());
    VariationSet::setCurrentVariationSet(set);
}

void VariationSet::setuPreferredVariationSet()
{
    VariationSet::setupDefaultVariationSet();
}

SharedVariationSet VariationSet::makeLegacyVariationSet()
{
    StringsVector variationNames    = std::move(flam3VariationsArray());
    SharedVariationSet variationSet = std::make_shared<VariationSet>(factoryLibrary, variationNames,
                                                                     "Flam3 Legacy", uuidForLegacyVariationSet(), false);

    StringsSet variationNamesSet(variationNames.begin(), variationNames.end());
    variationSet->copyVariationsFrom(factoryLibrary, variationNames, variationNamesSet);
    if (variationSet) {
        variationSets[variationSet->uuid] = variationSet;
        variationSet->defaultVariation = "julia";
        variationSet->version ="3.0";
        variationSet->is3DCapable = false;
        variationSet->finishCopyFromOtherVariationSet(true);
    }
//    printf("%s\n", variationSet->description().c_str());
    return variationSet;
}

SharedVariationSet VariationSet::makeStandard3DVariationSet()
{
    StringsVector variationNames    = std::move(standard3DVariationsArray());
    SharedVariationSet variationSet = std::make_shared<VariationSet>(factoryLibrary, variationNames,
                                                                     "Core 3Dv2", uuidForStandard3DVariationSet(), false);
    StringsSet variationNamesSet(variationNames.begin(), variationNames.end());
    variationSet->copyVariationsFrom(factoryLibrary, variationNames, variationNamesSet);

    if (variationSet) {
        variationSets[variationSet->uuid] = variationSet;
        variationSet->defaultVariation = "hemisphere";
        variationSet->version ="3.0";
        variationSet->is3DCapable = true;
        variationSet->finishCopyFromOtherVariationSet(true);
    }
//    printf("%s\n", variationSet->description().c_str());
    return variationSet;
}

StringsVector VariationSet::flam3VariationsArray()
{
    return StringsVector{
        //           "pre_matrix2d",   // we want these to be added later to ensure layout compatiblity with upgrading older variation sets
        "pre_blur",
        "linear",
        //            "matrix2d",
        "sinusoidal",
        "spherical",
        "swirl",
        "horseshoe",
        "polar",
        "handkerchief",
        "heart",
        "disc",
        "spiral",
        "hyperbolic",
        "diamond",
        "ex",
        "julia",
        "bent",
        "waves",
        "fisheye",
        "popcorn",
        "exponential",
        "power",
        "cosine",
        "rings",
        "fan",
        "blob",
        "pdj",
        "fan2",
        "rings2",
        "eyefish",
        "bubble",
        "cylinder",
        "perspective",
        "noise",
        "julian",
        "juliascope",
        "blur",
        "gaussian_blur",
        "radial_blur",
        "pie",
        "ngon",
        "curl",
        "rectangles",
        "arch",
        "tangent",
        "square",
        "rays",
        "blade",
        "secant2",
        "twintrian",
        "cross",
        "disc2",
        "super_shape",
        "flower",
        "conic",
        "parabola",
        "bent2",
        "bipolar",
        "boarders",
        "butterfly",
        "cell",
        "cpow",
        "curve",
        "edisc",
        "elliptic",
        "escher",
        "foci",
        "lazysusan",
        "loonie",
        "modulus",
        "oscilloscope",
        "polar2",
        "popcorn2",
        "scry",
        "separation",
        "split",
        "splits",
        "stripes",
        "wedge",
        "wedge_julia",
        "wedge_sph",
        "whorl",
        "waves2",
        "exp",
        "log",
        "sin",
        "cos",
        "tan",
        "sec",
        "csc",
        "cot",
        "sinh",
        "cosh",
        "tanh",
        "sech",
        "csch",
        "coth",
        "auger",
        "flux",
    };
           //            "post_matrix2d",
}

StringsVector VariationSet::standard3DVariationsArray()
{
    return StringsVector{
            //            "pre_matrix2d",   // we want these to be added later to ensure layout compatiblity with upgrading older variation sets
            //            "pre_matrix3d",
            "pre_flatten",
            "pre_zscale",
            "pre_ztranslate",
            "pre_rotate_x",
            "pre_rotate_y",
            "pre_rotate_z",
            "pre_wave3D_wf",
            "linear",
            //            "matrix2d",
            //            "matrix3d",
            "zscale",
            "zblur",
            "ztranslate",
            "zcone",
            "popcorn2_3D",
            "julia3D",
            "juliac",
            "julia3Dz",
            "julian",
            "mask",
            "mobius",
            "inflateZ_4",
            "Li",
            "inflateZ_5",
            "cardioid",
            "waves3D",
            "epispiral",
            "farblur",
            "cpow",
            "foci_3D",
            "escher",
            "hemisphere",
            "square3D",
            "disc3d",
            "butterfly3D",
            "swirl2",
            "blade3D",
            "spherical3D_wf",
            "blob3D",
            "curl3D",
            "loonie",
            "loonie_3D",
            "inflateZ_1",
            "julia",
            "blur3d",
            "inflateZ_2",
            "inflateZ_3",
            "twoface",
            "waves2_3D",
            "juliascope3D",
            "psphere",
            "tangent3D",
            "roundspher3D",
            "linearT3D",
            "flux",
            "murl",
            "bubble_wf",
            "juliascope",
            "rings2",
            "tancos",
            "pie3D",
            "unpolar",
            "scry_3D",
            "exponential",
            "poincare3D",
            "npolar",
            "foci",
            "inflateZ_6",
            "epispiral_wf",
            "curl",
            "bubble",
            "post_rotate_x",
            "post_curl3D",
            "post_rotate_y",
            "post_ztranslate",
            "post_flatten",
            "post_rotate_z",
            "post_zscale_wf",
            "post_mirror_x",
            "post_depth",
            "post_mirror_y",
            "post_mirror_z",
            //            "post_matrix2d",
            //            "post_matrix3d",
    };
}

#pragma mark Well Known Variation Sets

SharedVariationSet VariationSet::makeVariationSetFromVariationNames(StringsSet &names, const std::string &variationSetName, bool incomplete)
{
    if (names.find("linear3D") != names.end()) {
        names.erase("linear3D");
        names.insert("linear");
    }
    if (names.find("flatten") != names.end()) {
        names.erase("flatten");
        names.insert("post_flatten");
    }
    // log the missing variations
    StringsVector namesSet;
    StringsVector namesSet2;
    std::copy(names.begin(), names.end(), std::inserter(namesSet, namesSet.end()));
    std::sort(namesSet.begin(), namesSet.end());

    const SharedVariationSet & libPlus = VariationSet::variationsLibraryPlus();
    std::copy(libPlus->namesInUse.begin(), libPlus->namesInUse.end(), std::inserter(namesSet2, namesSet2.end()));
    std::sort(namesSet2.begin(), namesSet2.end());

    StringsSet missing;
    set_difference(namesSet.begin(),namesSet.end(),namesSet2.begin(),namesSet2.end(),
                     std::inserter(missing,missing.end()));
    for (const std::string &name : missing) {
        std::cout << "Missing variation named " << name << " - not in library\n";
    }

    // build a new variation set for this incoming file
    StringsVector variationNamesNeeded;
    variationNamesNeeded.resize(names.size());

    for (const std::string & name : names) {
        if (libPlus->variationsByName.find(name) != libPlus->variationsByName.end()) {
            variationNamesNeeded.push_back(name);
        }

    }
    SharedVariationSet variationSet = std::make_shared<VariationSet>(libPlus, variationNamesNeeded, variationSetName, stringWithUUID(), incomplete, false);
    StringsSet variationNamesSet(variationNamesNeeded.begin(), variationNamesNeeded.end());
    variationSet->copyVariationsFrom(libPlus, variationNamesNeeded, variationNamesSet);

    variationSets[variationSet->uuid] = variationSet;
    return variationSet;
}

#pragma mark Xform Setter / Getter Methods

bool VariationSet::validXformKey(const std::string &key)
{
    if (transformPropertiesByKey.find(key) != transformPropertiesByKey.end())
        return true;
    if (variationsByKey.find(key) != variationsByKey.end())
        return true;
    if (variationParametersByKey.find(key) != variationParametersByKey.end())
        return true;
    // try to look up variation by name as an alternate
    if (variationsByName.find(key) != variationsByName.end())
        return true;
    // try to look up parameter by alias as an alternate
    if (variationParametersByAlias.find(key) != variationParametersByAlias.end())
        return true;
    return false;
}

size_t VariationSet::xformIndexForKey(const std::string &key)
{
    if (transformPropertiesByKey.find(key) != transformPropertiesByKey.end()) {
        TransformProperty transformProperty = transformPropertiesByKey[key];
        return transformProperty.xformIndex;
    }
    if (variationsByKey.find(key) != variationsByKey.end()) {
        const SharedVariation &variation = variationsByKey[key];
        return variation->xformIndex;
    }
    if (variationParametersByKey.find(key) != variationParametersByKey.end()) {
        const SharedParameter &parameter = variationParametersByKey[key];
        return parameter->xformIndex;
    }
    // try to look up variation by name as an alternate
    if (variationsByName.find(key) != variationsByName.end()) {
        const SharedVariation &variation = variationsByName[key];
        return variation->xformIndex;
    }
    // try to look up parameter by alias as an alternate
    if (variationParametersByAlias.find(key) != variationParametersByAlias.end()) {
        const SharedParameter &parameter = variationParametersByAlias[key];
        return parameter->xformIndex;
    }
    return std::string::npos;
}

float VariationSet::floatValueForKey(const std::string &key, size_t instanceNum, SharedVariationGroup &variationGroup) const
{
    std::string instancedKey = VarParInstance::makeInstancedKey(key, instanceNum);
    if (variationGroup->find(instancedKey) == variationGroup->end())
        return 0.f;
    return variationGroup->operator[](instancedKey)->floatValue;
}

SharedVarParInstance VariationSet::setCurrentFloatValue(float value,
                                                        const std::string &key,
                                                        size_t instanceNum,
                                                        size_t _xformIndex,
                                                        SharedVariationGroup variationGroup)
{
    return currentVariationSet->setFloatValue(value, key, instanceNum, _xformIndex, variationGroup);
}

void VariationSet::setInstance(SharedVarParInstance &instance, SharedVariationGroup variationGroup)
{
    instance->setGroup(variationGroup);
    variationGroup->operator[](instance->instancedKey()) = instance;
}

SharedVarParInstance VariationSet::setFloatValue(float value, const std::string &key, size_t instanceNum, size_t _xformIndex, SharedVariationGroup variationGroup)
{
    std::string instancedKey = VarParInstance::makeInstancedKey(key, instanceNum);
    if (variationGroup->find(instancedKey) != variationGroup->end()) {
        SharedVarParInstance &currentInstance = variationGroup->operator[](instancedKey);
        currentInstance->floatValue = value;
        return currentInstance;
    }
    else {
        SharedVarParInstance instance = std::make_shared<VarParInstance>(key, instanceNum, variationGroup, _xformIndex);
        instance->floatValue = value;
        setInstance(instance, variationGroup);
        return instance;
    }
}

SharedVarParInstance VariationSet::addFloatValueInstance(float value, const std::string &key, size_t _xformIndex, SharedVariationGroup variationGroup)
{
    SharedVarParInstance instance = std::make_shared<VarParInstance>(key, 1, variationGroup, _xformIndex);
    bool found = true;
    while (found) {
        if (variationGroup->find(instance->instancedKey()) != variationGroup->end())
            instance->setInstanceNum(instance->instanceNum + 1);
        else
            found = false;
    }
    instance->floatValue = value;
    setInstance(instance, variationGroup);
    return instance;
}

std::vector<std::string> VariationSet::parseXmlElementForVariationNames(xml_node & variationSetEle, std::string & name, std::string & uuid)
{
    name = variationSetEle.attribute("name").value();
    uuid = variationSetEle.attribute("uuid").value();

    std::vector<std::string> array;

    xml_node variationEle = variationSetEle.first_child();
    if (variationEle.type() == node_null)
        return array;

    if (variationEle.type() == node_comment)
        variationEle =variationEle.next_sibling();

    do {
        while (variationEle.type() == node_comment)
            variationEle =variationEle.next_sibling();
        if (variationEle.type() != node_element)
            return array;
        std::string _name = variationEle.attribute("name").value();
        array.push_back(_name);
        variationEle =variationEle.next_sibling();

    }  while (variationEle.type() != node_null);
    return array;
}

bool VariationSet::hasAllVariationsNeededBySet(const StringsSet & namesSet) const
{
    return std::includes(namesInUse.begin(), namesInUse.end(), namesSet.begin(), namesSet.end());
}

bool VariationSet::hasAllVariationsNeededBy(const StringsVector & namesArray) const
{
	StringsVector namesSet;
	std::copy(namesInUse.begin(), namesInUse.end(), std::inserter(namesSet, namesSet.end()));
	std::sort(namesSet.begin(), namesSet.end());

    if (! std::includes(namesSet.begin(), namesSet.end(), namesArray.begin(), namesArray.end()))
        return false;

    // sort the variation keys by prePostXformIndex ordering
    auto _orderdNames = keysSortedByValue(variationsByName, Variation::prePostXformIndexPairCompare);

    // get all pre_ prefixed variation names in variation set
    StringsVector vsOrderedNames;
    std::copy_if(_orderdNames.begin(), _orderdNames.end(), std::inserter(vsOrderedNames, vsOrderedNames.end()),
                 [](std::string & name) {return name.substr(0, 4) == "pre_";});

    // get all pre_ prefixed variation names in input namesArray
    StringsVector preNames;
    std::copy_if(namesArray.begin(), namesArray.end(), std::inserter(preNames, preNames.end()),
                 [](const std::string & name) {return name.substr(0, 4) == "pre_";});

    // does the vsOrderedNames have the names in preNames?, if not return NO
    if (! std::includes(vsOrderedNames.begin(), vsOrderedNames.end(), preNames.begin(), preNames.end()))
        return false;

    // get all post_ prefixed variation names in variation set
    vsOrderedNames.clear();
    std::copy_if(_orderdNames.begin(), _orderdNames.end(), std::inserter(vsOrderedNames, vsOrderedNames.end()),
                 [](std::string & name) {return name.substr(0, 5) == "post_";});

    // get all post_ prefixed variation names in input namesArray
    StringsVector postNames;
    std::copy_if(namesArray.begin(), namesArray.end(), std::inserter(postNames, postNames.end()),
                 [](const std::string & name) {return name.substr(0, 5) == "post_";});

    // does the vsOrderedNames have the names in postNames?, if not return NO
    if (! std::includes(vsOrderedNames.begin(), vsOrderedNames.end(), postNames.begin(), postNames.end()))
        return false;
    return true;
}

// parse the Flame XML document
VariationSetsVector VariationSet::preloadVariationSets(xml_document & doc)
{
    VariationSetsVector variationSetsInDoc;
    xml_node aNode = doc.document_element();

    if (aNode.type() == node_element && strcmp(aNode.name(),"flame") != 0)
        aNode = aNode.first_child();

    // install variation sets as needed
    do {
        if (aNode.type() == node_element && strcmp(aNode.name(),"variationSet") == 0) {
            xml_node & variationSetEle = aNode;
            std::string uuid = variationSetEle.attribute("uuid").value();
            // see if we have it already
            SharedVariationSet variationSet = VariationSet::variationSetForUuid(uuid);
            if (variationSet) {
                bool hasIt = false;
                for (auto & pair : VariationSet::variationSets) {
                    SharedVariationSet & vs = pair.second;
                    if (variationSet.get() == vs.get()) {
                        hasIt = true;
                        break;
                    }
                }

                if (!hasIt) {
                    variationSetsInDoc.push_back(variationSet);
                }
                continue;
            }
            std::string varSetName;
            std::vector<std::string> variationNames = VariationSet::parseXmlElementForVariationNames(variationSetEle, varSetName, uuid);
			std::sort(variationNames.begin(), variationNames.end());

            // see if another loaded variation set is adequate, if so use it
            for (auto & pair : VariationSet::variationSets) {
                const std::string & uuid0 = pair.first;
                SharedVariationSet aVariationSet = VariationSet::variationSetForUuid(uuid0);
                if (aVariationSet->hasAllVariationsNeededBy(variationNames)) {
                    VariationSet::setSuperSetFor(uuid, aVariationSet);

                    bool hasIt = false;
                    for (SharedVariationSet & vs : variationSetsInDoc) {
                        if (aVariationSet.get() == vs.get()) {
                            hasIt = true;
                            break;
                        }
                    }
                    if (!hasIt)
                        variationSetsInDoc.push_back(aVariationSet);
                    variationSet = aVariationSet;
                    break;
                }
            }
            if (variationSet)
                continue;

            // we dont have this variation set, so install it
            variationSet = VariationSet::makeVariationSet(variationSetEle, false);
            variationSets[variationSet->uuid] = variationSet;
            variationSetsInDoc.push_back(variationSet);
        }
    }  while (aNode = aNode.next_sibling(), aNode.type() != node_null);
    return variationSetsInDoc;
}

bool VariationSet::canProcessXmlElement(xml_node & variationEle, uint variationIndex, uint parameterIndex)
{
    // broken atttribute allows author to say a variation type is broken and not to be used (yet)
    if (! variationEle.attribute("broken").empty()) {
        std::string brokenString = variationEle.attribute("broken").as_string();
        if (brokenString == "yes" || brokenString == "YES")
            return false;
    }
    if (amLibrary) // check this second so broken variations dont get into library list
        return true;

    for (xml_node ele : variationEle.children("parameter")) {
        if (ele.type() == node_element) {
            parameterIndex++;
        }
    }
    variationIndex++; // we consume one variation slot
    return true;
}

bool VariationSet::parseXmlElement(xml_node variationSetEle)
{
    std::string _uuid;
    if (! variationSetEle.attribute("uuid"))
        _uuid = variationSetEle.attribute("variation").value();
    else
        _uuid = variationSetEle.attribute("uuid").as_string();

    uuid = _uuid;

    std::string _defaultVariation  = variationSetEle.attribute("defaultVariation").value();
    std::string _name              = variationSetEle.attribute("name").value();
    std::string _version           = variationSetEle.attribute("version").value();
    std::string _is3DCapable       = variationSetEle.attribute("is3Dcapable").value();
    std::string _structName        = variationSetEle.attribute("structName").value();
    std::string _temporary         = variationSetEle.attribute("temporary").value();


    amTemporary      = variationSetEle.attribute("temporary").empty()        ? false                             : true;
    defaultVariation = variationSetEle.attribute("defaultVariation").empty() ? "linear"                          : _defaultVariation;
    name             = variationSetEle.attribute("name").empty()             ? VariationSet::uniqueUnknownName() : _name;
    version          = variationSetEle.attribute("version").empty()          ? "1.0"                             :_version;
    is3DCapable      = variationSetEle.attribute("is3Dcapable").empty()      ? false                             :_is3DCapable == "yes";

    // older variation set XML definitions do not have the "structName" attribute
    variationStructName  = variationSetEle.attribute("structName").empty() ? "xform" : _structName;

    // see if this variation set has any variations in it (default MyLibrary has none)
    bool hasVariations = false;
    for (pugi::xml_node child : variationSetEle.children()) {
        if (child.type() == node_element) {
            hasVariations = true;
            break;
        }
        else if (child.type() == node_comment) {
//            std::cout << child.value() << "\n";
        }
    }

    if (hasVariations) {
        xml_node variationEle = variationSetEle.first_child();
        uint variationIndex = 0;
        uint parameterIndex = 0;
        do {
            while (variationEle.type() != node_element) {
                variationEle = variationEle.next_sibling();
            }

            if (canProcessXmlElement(variationEle, variationIndex, parameterIndex)) {
                variationForXmlElement(variationEle, variationIndex++, parameterIndex);
            }
            variationEle = variationEle.next_sibling();

        }  while (!variationEle.empty() && (amLibrary || variationIndex < MAX_VARIATIONS_IN_VARSET));

        // now reorder putting pre_ variations first, then normal, then post_ variations at end
        uint newIndex = 0;
        for (std::string & variationKey : keysSortedByValue(variationsByKey, Variation::prePostXformIndexPairCompare)) {
            SharedVariation & variation = variationsByKey[variationKey];
            variation->setXformIndex(FIRST_VAR_OFFSET + newIndex++);
        }

        // adjust each parameter's xformIndex by
        for (std::string & parameterKey : keysSortedByValue(variationParametersByKey, VariationParameter::xformIndexPairCompare)) {
            SharedParameter & parameter = variationParametersByKey[parameterKey];
            parameter->setXformIndex(parameter->getXformIndex() + variationIndex + FIRST_VAR_OFFSET);
        }
        transformProperties.clear();
        setupTransformProperties();
    }
    if (variationStructName == "xform") {
        variationStructName = "varpar"; // coversion to this format is complete now
        removeKernels(); // these are now out of date
    }
    return true;
}

SharedVariation VariationSet::variationForXmlElement(xml_node &variationEle, uint variationIndex, uint & parameterIndex)
{
    std::string _name     = variationEle.attribute("name").value();
    std::string key       = variationEle.attribute("key").value();
    std::string author    = variationEle.attribute("author").value();
    std::string copyright = variationEle.attribute("copyright").value();
    float defaultValue    = variationEle.attribute("default").as_float();

    enum VariationDimension dimension = both2D_3D;
    std::string dim       = variationEle.attribute("dimension").value();
    if (! variationEle.attribute("dimension").empty()) {
        if (dim == "2d" || dim == "2D")
            dimension = dim2D;
        else if (dim == "3d" || dim == "3D")
            dimension = dim3D;
        else if (dim == "both")
            dimension = both2D_3D;
        else
            dimension = both2D_3D;
    }
    bool usesDirectColor    = false;
    std::string directColor = variationEle.attribute("directColor").value();
    if (!variationEle.attribute("directColor").empty()) {
        if (directColor == "yes" || directColor == "YES")
            usesDirectColor = true;
    }
    enum VariationStatus status = useable;
    if (! variationEle.attribute("broken").empty()) {
        std::string brokenString = variationEle.attribute("broken").as_string();
        if (brokenString == "yes" || brokenString == "YES")
            status = broken;
    }
    if (! variationEle.attribute("inDevelopment").empty()) {
        std::string inDevString = variationEle.attribute("inDevelopment").as_string();
        if (inDevString == "yes" || inDevString == "YES")
            status = inDevelopment;
    }

    if (variationEle.attribute("key").empty())
        key = _name;

    // get variation source code
    xml_node sourceEle  = variationEle.child("source");
    std::string source  = sourceEle.text().get();
    std::string license = sourceEle.attribute("license").as_string();

    // get variation source functions
    std::string functionsSource;
    if (! variationEle.child("functions").empty()) {
        xml_node functionsSourceEle = variationEle.child("functions");
        functionsSource             = functionsSourceEle.text().get();
    }

    // get variation source function prototypes
    std::string functionsPrototype;
    if (! variationEle.child("prototypes").empty()) {
        xml_node prototypeEle = variationEle.child("prototypes");
        functionsPrototype    = prototypeEle.text().get();
    }
    SharedVariation variation  = std::make_shared<Variation>(shared_from_this(), _name, key, defaultValue);
    addVariation(*variation);

    variation->setXformIndex(FIRST_VAR_OFFSET + variationIndex);

    if (variationStructName == "xform") {
        source          = Variation::replaceXformWithVarpar(source);
        functionsSource = Variation::replaceXformWithVarpar(functionsSource);
    }

    variation->source             = source;
    variation->functionsSource    = functionsSource;
    variation->functionsPrototype = functionsPrototype;
    variation->license            = license;
    variation->author             = author;
    variation->copyright          = copyright;
    variation->dimension          = dimension;
    variation->usesDirectColor    = usesDirectColor;
    variation->status             = status;

    for (xml_node & parameterEle : variationEle.children("parameter")) {
        std::string _name     = parameterEle.attribute("name").value();
        std::string alias     = parameterEle.attribute("alias").value();
        std::string key       = parameterEle.attribute("key").value();
        float defaultValue    = parameterEle.attribute("default").as_float();

        if (parameterEle.attribute("key").empty())
            key = string_format("%s_%s", variation->key.c_str(), _name.c_str());

        SharedParameter param = std::make_shared<VariationParameter>(_name, key, variation->shared_from_this(), defaultValue);
        variation->parameters[_name] = param;
        addVariationParameter(*param);

        param->setXformIndex(parameterIndex);
        if (! parameterEle.attribute("alias").empty()) {
            param->alias = new std::string(alias);
            if (variationParametersByAlias.find(alias) == variationParametersByAlias.end()) {
                variationParametersByAlias[alias] = param;
                namesInUse.insert(alias);
            }
        }
        parameterIndex++;
        if (parameterEle.attribute("minRandom"))
            param->minRandom = parameterEle.attribute("minRandom").as_float();
        if (parameterEle.attribute("maxRandom"))
            param->maxRandom = parameterEle.attribute("maxRandom").as_float();
        if (parameterEle.attribute("discrete"))
            param->discrete = parameterEle.attribute("discrete").as_bool();
        if (parameterEle.attribute("nonzero"))
            param->nonZero = parameterEle.attribute("nonzero").as_bool();
    }
    return variation;
}

SharedVariationSet VariationSet::findVariationSetForNeededNames(StringsSet &neededNames,
                                                                const std::string &variationSetName,
                                                                const VariationSetsVector & variationSetsInDoc)
{
    // special handling for linear3D - an alias for linear
    if (neededNames.find("linear3D") != neededNames.end()) {
        neededNames.erase("linear3D");
        neededNames.insert("linear");
    }
    if (neededNames.find("flatten") != neededNames.end()) {
        neededNames.erase("flatten");
        neededNames.insert("post_flatten");
    }
    neededNames.insert("linear"); // must be in all variation sets

    const SharedVariationSet & library = VariationSet::variationsLibraryPlus();
    bool has3DVariations               = false;
    for (const std::string &_name : neededNames) {
        if (library->variationsByName.find(_name) != library->variationsByName.end()) {
            SharedVariation & variation = library->variationsByName[_name];
            if (variation->dimension == dim3D)
                has3DVariations = true;
        }
    }
    if (has3DVariations)
        neededNames.insert("post_flatten"); // must be in all 3D variation sets

    for (const SharedVariationSet & variationSet : variationSetsInDoc) {
        if (variationSet->hasAllVariationsNeededBySet(neededNames))
            return variationSet;
    }
    if (VariationSet::legacyVariationSet()->hasAllVariationsNeededBySet(neededNames))
        return VariationSet::legacyVariationSet();

    for (const std::string & uuid : allKeys(VariationSet::variationSets)) {
        SharedVariationSet variationSet = VariationSet::variationSetForUuid(uuid);
        if (variationSet->hasAllVariationsNeededBySet(neededNames))
            return variationSet;
    }

    // if we cannot find an adequate variation set we must make one if possible
    if (library->hasAllVariationsNeededBySet(neededNames)) {
        // build a new variation set for this incoming file
        StringsVector variationNamesNeeded;
        variationNamesNeeded.reserve(230);

        for (const std::string & _name : neededNames) {
            if (library->variationsByName.find(_name) != library->variationsByName.end())
                variationNamesNeeded.push_back(_name);
        }
        SharedVariationSet variationSet = std::make_shared<VariationSet>(library,
                                                                         variationNamesNeeded,
                                                                         variationSetName,
                                                                         stringWithUUID(),
                                                                         false,
                                                                         false);
        //    StringsSet variationNamesSet(variationNames.begin(), variationNames.end());
        //    copyVariationsFrom(library, variationNames, variationNamesSet);
        variationSets[variationSet->uuid] = variationSet;
        return variationSet;

    }
    // if we need variations and parameters not present in the library - yet
    else {
        StringsVector namesUnknown;
        std::copy(neededNames.begin(), neededNames.end(), std::inserter(namesUnknown, namesUnknown.end()));
        std::sort(namesUnknown.begin(), namesUnknown.end());

        StringsVector namesInUse;
        std::copy(library->namesInUse.begin(), library->namesInUse.end(), std::inserter(namesInUse, namesInUse.end()));
        std::sort(namesInUse.begin(), namesInUse.end());

        StringsVector netNamesInUse;
        std::set_difference(namesUnknown.begin(), namesUnknown.end(), namesInUse.begin(), namesInUse.end(), back_inserter(netNamesInUse));

        std::cout << "Unknown variations and/or parameters:\n    ";

        for (auto it = netNamesInUse.begin(); it != netNamesInUse.end(); ++it)
            std::cout << ' ' << *it;
        std::cout << '\n';
    }
    // no variants set, so return the legacy variant set
    return std::shared_ptr<VariationSet>();;
}

VariationSetsVector VariationSet::preParseDocForVariationsUsed(xml_document & doc, const std::string & path, bool onlyParseFirstFlame, bool &change)
{
    change = false;
    VariationSetsVector variationSetsInDoc     = VariationSet::preloadVariationSets(doc);
    VariationSetsVector _internalVariationSets = FlameParse::preloadInternalVariationSets(doc);
    VariationSetsDict internalVariationSets;

    for (SharedVariationSet & vs : _internalVariationSets) {
        internalVariationSets[vs->uuid] = vs;
    }

    // if we found new variation types in the file, add them to My Library
    bool haveNewVariations = false;
    for (SharedVariationSet & variationSet : variationSetsInDoc) {
        for (const std::string & variationKey  : allKeys(variationSet->variationsByKey)) {
            SharedVariation & variation = variationSet->variationsByKey[variationKey];
            if (VariationSet::unknownVariation(variation)) {
                VariationSet::addVariationToMyLibrary(variation);
                haveNewVariations = true;
            }
        }
    }
    xml_node aNode = doc.document_element();
    if (aNode.type() == node_element && strcmp(aNode.name(),"flame") != 0)
        aNode = aNode.first_child();

    // walk through flames checking for custom variations whose source is different from MyLibrary's source
    // These custom variations need to be made unique, then added to My Library, and all usage of the old
    // custom variation names need to be replaced with the new name in the document
    // NOTE this step only affects the document's internal variation set - NOT the app's variation sets
    xml_node & flameEleNode = aNode;
    do {
        if (aNode.type() == node_element && strcmp(aNode.name(),"flame") == 0) {

            xml_node &flameEle           = flameEleNode;
            xml_attribute varSetUUIDNode = flameEle.attribute("varset");
            const std::string varSetUUID = varSetUUIDNode.value();

            if (! varSetUUIDNode.empty()) {
                bool changed = VariationSet::walkEleForCustomVariationsUsed(flameEle, internalVariationSets[varSetUUID]);
                haveNewVariations = haveNewVariations ? haveNewVariations : changed;
                change = changed ? true : change;
            }
        }
        flameEleNode = flameEleNode.next_sibling();
    }  while (flameEleNode.type() != node_null);

    if (haveNewVariations) {
        VariationSet::saveLibraryXML();
        VariationSet::updateMergedLibrary();
    }

    // now find either an existing variation set to use OR create a new one with the required strings
    do {
        if (aNode.type() == node_element && strcmp(aNode.name(),"flame") == 0) {

            xml_node &flameEle           = aNode;
            xml_attribute varSetUUIDNode = flameEle.attribute("varset");
            const std::string varSetUUID = varSetUUIDNode.value();

            SharedVariationSet variationSet;
            if (! varSetUUIDNode.empty()) {
                variationSet = VariationSet::variationSetForUuid(varSetUUID);

                // not found, look for superSet
                if (! variationSet) {
                    variationSet = VariationSet::variationSuperSetForUuid(varSetUUID);
                    if (variationSet)
                        varSetUUIDNode.set_value(variationSet->uuid.c_str()); // make it var set uuid for this flame
                }
            }
            if (variationSet) {
                if (std::find(variationSetsInDoc.begin(), variationSetsInDoc.end(), variationSet) == variationSetsInDoc.end())
                    variationSetsInDoc.push_back(variationSet);
            }
            else {
                StringsSet names = std::move(FlameParse::preParseFlameEleForVariationsUsed(flameEle));
                names.insert("linear");

                std::string filename = fileNameFromPath(path);
                std::string varsetName;
                if (filename.length() == 0)
                    varsetName = "AutoCreated";
                else
                    varsetName = string_format("AutoCreated for %s", filename.c_str());

                SharedVariationSet variationSet = VariationSet::findVariationSetForNeededNames(names, varsetName, variationSetsInDoc);
                if (variationSet) {
                    variationSetsInDoc.push_back(variationSet);
                    varSetUUIDNode.set_value(variationSet->uuid.c_str()); // make it var set uuid for this flame
                }
                else { // punt, we cant find any variation set that will work for this flame
                    varSetUUIDNode.set_value("");
                }
            }
        }
        if (onlyParseFirstFlame)
            break;
    }  while (aNode = aNode.next_sibling(), aNode.type() != node_null);
    return variationSetsInDoc;
}

// walk through flames checking for custom variations whose source is different from MyLibrary's source
// These custom variations need to be made unique, then added to My Library, and all usage of the old
// custom variation names need to be replaced with the new name in the document
bool VariationSet::walkEleForCustomVariationsUsed(xml_node & flameEle, SharedVariationSet &variationSet)
{
    StringsSet names = std::move(FlameParse::preParseFlameEleForVariationsUsed(flameEle));

    // check the variation names used by this flame to see if they are factory or MyLibrary variations
    // if in myLibrary already, check the existing myVariation is equivalent to this one
    //     if not equivalent, autoVersion the variation and add the new autoversioned to MyLibrary
    // if not in myLibrary already, add them to MyLibrary
    bool haveNewVariations = false;
    for (const std::string & name : names) {
        SharedVariationSet & library = VariationSet::factoryLibrary;
        if (library->variationsByName.find(name) != library->variationsByName.end())
            continue;    // not custom

        bool hasMyVariation = VariationSet::myLibrary->variationsByName.find(name) != VariationSet::myLibrary->variationsByName.end();
        bool hasIVariation  = variationSet->variationsByName.find(name)            != variationSet->variationsByName.end();
        bool hasParam       = variationSet->variationParametersByKey.find(name)    != variationSet->variationParametersByKey.end();

        if (hasParam)  // ignore names that are parameters
            continue;

        // unknown in MyLibrary but known in embedded variationset
        if (! hasMyVariation && hasIVariation) {
            haveNewVariations          = true;
            VariationSet::addVariationToMyLibrary(variationSet->variationsByName[name]);
        }
        else if (hasMyVariation && hasIVariation)  {
            SharedVariation & myVariation = VariationSet::myLibrary->variationsByName[name];
            SharedVariation & iVariation  = variationSet->variationsByName[name];

            // need to check for same variation definition
            if (myVariation->isCompressedEqualTo(iVariation))  // myLibrary version is the same
                continue;

            // autoVersion the internal Variation and add it to MyLibrary
            std::string originalName    = iVariation->name;
            iVariation->autoVersionDifferentFrom(names);  // key, name, and source modified for new autoversion

            const std::string & newName = iVariation->name;
            haveNewVariations           = true;
            VariationSet::addVariationToMyLibrary(iVariation);

            // modify the XML and substitute the autoversioned name for the original name
            for (xml_node & xformEle : flameEle.children("xform")) {
                xml_attribute originalAttribute = xformEle.attribute(originalName.c_str());
                if (! originalAttribute.empty()) {
                    std::string oldStringValue = originalAttribute.value();
                    flameEle.remove_attribute(originalAttribute);
                    flameEle.append_attribute(newName.c_str()).set_value(oldStringValue.c_str());
                }
            }
            for (xml_node & xformEle : flameEle.children("finalxform")) {
                xml_attribute originalAttribute = xformEle.attribute(originalName.c_str());
                if (! originalAttribute.empty()) {
                    std::string oldStringValue = originalAttribute.value();
                    flameEle.remove_attribute(originalAttribute);
                    flameEle.append_attribute(newName.c_str()).set_value(oldStringValue.c_str());
                }
            }
        }
    }
    if (haveNewVariations) {
        VariationSet::saveLibraryXML();
        VariationSet::updateMergedLibrary();
    }
    return  haveNewVariations;
}

#pragma mark Description

std::string VariationSet::describeVariations()
{
    std::string s;
    s.reserve(500);
    for (std::string & variationKey : keysSortedByValue(variationsByKey, Variation::prePostXformIndexPairCompare)) {
        SharedVariation & variation = variationsByKey[variationKey];
        s.append(variation->description());
        s.append("\n");
    }
    return s;
}

StringsVector VariationSet::variationDescriptions()
{
    StringsVector v;
    for (std::string & variationKey : keysSortedByValue(variationsByKey, Variation::prePostXformIndexPairCompare)) {
        SharedVariation & variation = variationsByKey[variationKey];
        v.push_back(variation->description());
    }
    return v;
}

std::string VariationSet::describeParameters()
{
    std::string s;
    s.reserve(500);
    for (std::string & parameterKey : keysSortedByValue(variationParametersByKey, VariationParameter::xformIndexPairCompare)) {
        SharedParameter & parameter = variationParametersByKey[parameterKey];
        s.append(parameter->description());
        s.append("\n");
    }
    return s;
}

StringsVector VariationSet::parameterDescriptions()
{
    StringsVector v;
    for (std::string & parameterKey : keysSortedByValue(variationParametersByKey, VariationParameter::xformIndexPairCompare)) {
        SharedParameter & parameter = variationParametersByKey[parameterKey];
        v.push_back(parameter->description());
    }
    return v;
}

template std::string string_format<const char *, const char *, int, const char *,  const char *>
                            (const char *, const char *, const char *, int, const char *,  const char *);

std::string VariationSet::description()
{
    return string_format("%s %s  Variation Count:%lu UUID:%s %s\n%s\n",
                         "VariationSet",
                          name.c_str(),
                         variationsByKey.size(),
                         uuid.c_str(),
                         amTemporary ? "temporary" : "",
                         describeVariations().c_str());
}



std::string VariationSet::allVarsetsDescription(VariationSetsDict & dict)
{
    static char buf[100];
    std::string s;
    for (auto pair : dict) {
        SharedVariationSet &vs = pair.second;
        snprintf(buf, sizeof(buf), "VariationSet %s %s %zu variations %s\n", vs->name.c_str(), vs->uuid.c_str(),
                vs->variationsByKey.size(), vs->amTemporary ? "temporary" : "permanent");
        s.append(buf);
    }
    s.append("\n");
    return s;
}

std::string VariationSet::allVarsetsDescription(VariationSetsVector & vector)
{
    static char buf[100];
    std::string s;
    for (SharedVariationSet &vs : vector) {
        snprintf(buf, sizeof(buf), "VariationSet %s %s %zu variations\n", vs->name.c_str(), vs->uuid.c_str(), vs->variationsByKey.size());
        s.append(buf);
    }
    s.append("\n");
    return s;
}

std::string VariationSet::allVarsetsDescription()
{
    return allVarsetsDescription(VariationSet::variationSets);
}

#pragma mark Well Known Directory Locations & Loading

std::string VariationSet::variationSetsDirectoryURL()
{
#ifdef _WIN32
	return ::variationSetsDirectoryPath();
#else
	const char * tildePath =
#ifdef __APPLE__
		"~/Library/Application Support/FractalArchitectEngine/VariationSets/";
#else
		"~/.fa4/Application\\ Support/FractalArchitectEngine/VariationSets";
#endif
	return std::string(expandTildeInPath(tildePath));
#endif
}


std::string VariationSet::cachedLibraryURL()
{
    return VariationSet::variationSetsDirectoryURL() + "/library.xml";
}

std::string VariationSet::cachedMyLibraryURL()
{
    return VariationSet::variationSetsDirectoryURL() + "/myLibrary.xml";
}

std::string VariationSet::cachedLegacyVariationSetURL()
{
    return VariationSet::variationSetsDirectoryURL() + "/" +
            VariationSet::uuidForLegacyVariationSet() + "/variationSet.xml";
}

bool VariationSet::cachedLibraryExists()
{
#ifdef _WIN32
	return !  outOfDateDerived(cachedLibraryURL(), getExecutablePath());
#else
    return ::cachedLibraryExists();
#endif
}

const char * VariationSet::factoryLibraryBaseName()
{
    return "library";
}

bool VariationSet::cachedMyLibraryExists()
{
    return fileExists(VariationSet::cachedMyLibraryURL());
}

bool VariationSet::cachedLegacyVariationSetExists()
{
    return fileExists(VariationSet::cachedLegacyVariationSetURL());
}

std::string VariationSet::initialMyLibraryPath()
{
	return std::string(::variationSetsDirectoryPath()) + "/myLibrary.xml";
}

void VariationSet::setupFAEngineApplicationSupport()
{
    // create VariationSets directory in the FractalArchitect's Application Support directory structure
	std::string path = ::variationSetsDirectoryPath();
	const char * expandedVarSetsPath = path.c_str();

    if (! fileExists(expandedVarSetsPath))
        mkpath(expandedVarSetsPath, 0777);

#ifdef _WIN32
	std::string palettePath = ::applicationSupportPath() + "/flam3-palettes.xml";

	if (!fileExists(palettePath)) {
		string text = loadEmbeddedResource("flam3-palettes", "xml");
		if ((unlink(palettePath.c_str()) == -1) && (errno != ENOENT)) {
			std::string msg = "Failed to remove file: " + palettePath + "  " + strerror(errno);
			throw std::runtime_error(msg);
		}

		// copy file from path in bundle to the cached location
		std::ofstream  dst(palettePath.c_str(), std::ios::binary);
		dst << text;
	}
#endif
}

std::string VariationSet::myLibraryURL()
{
    return VariationSet::initialMyLibraryPath();
}

SharedVariationSet VariationSet::loadVariationSetFromURL(const std::string & variationSetURL, bool amLibrary)
{
    std::ifstream in(variationSetURL.c_str());
    if (in.fail()) {
        std::string msg = string_format("Failed to open file: %s", variationSetURL.c_str());
        throw std::runtime_error(msg);
    }
    SharedVariationSet variationSet;
    try
    {
        variationSet = std::make_shared<VariationSet>(in, amLibrary);
        variationSet->loadFromStream(in);
        if (!amLibrary)
            variationSets[variationSet->uuid] = variationSet;
        return variationSet;
    }

    catch(std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        std::exit(-1);
    }
    return std::shared_ptr<VariationSet>();
}

SharedVariationSet VariationSet::loadVariationsLibrary()
{
    try
    {
        return factoryLibrary = VariationSet::loadVariationSetFromURL(VariationSet::cachedLibraryURL(), true);
    }

    catch(std::runtime_error& e)
    {
        std::cerr << e.what() << std::endl;
        std::exit(-1);
    }
}

SharedVariationSet VariationSet::loadMyVariationsLibrary()
{
    try
    {
        return myLibrary = VariationSet::loadVariationSetFromURL(VariationSet::myLibraryURL(), true);
    }

    catch(std::runtime_error& e)
    {
        std::cerr << e.what() << std::endl;
        std::exit(-1);
    }
}

SharedVariationSet VariationSet::loadLegacyVariationSet()
{
    return VariationSet::loadVariationSetFromURL(VariationSet::cachedLegacyVariationSetURL(), false);
}

void VariationSet::loadSavedVariationSets()
{
    std::vector<string> variationSetDirURLs = directorySubDirContents(variationSetsDirectoryURL().c_str());
    for (string & variationSetDirURL : variationSetDirURLs) {
        string varSetURL      = variationSetsDirectoryURL() + "/" + variationSetDirURL + "/variationSet.xml";
        SharedVariationSet vs = loadVariationSetFromURL(varSetURL, false);

        // apply library updates to the cached variation set
        if (vs->upgradeCachedVariationSet()) {
            vs->saveKernelsAmUpgradedAllPlatforms(true);
        }
    }
}

void VariationSet::regenerateAllKernels()
{
    std::vector<string> variationSetDirURLs = directorySubDirContents(variationSetsDirectoryURL().c_str());
    for (string & variationSetDirURL : variationSetDirURLs) {
        string varSetURL      = variationSetsDirectoryURL() + "/" + variationSetDirURL + "/variationSet.xml";
        SharedVariationSet vs = loadVariationSetFromURL(varSetURL, false);
        vs->saveKernelsAmUpgradedAllPlatforms(true);
    }
}

void VariationSet::removeAllSavedKernels()
{
    std::vector<string> variationSetDirURLs = directorySubDirContents(variationSetsDirectoryURL().c_str());
    for (string & variationSetDirURL : variationSetDirURLs) {

        std::vector<string> savedKernelNames = directoryFileNamesContents(variationSetDirURL.c_str());
        for (string & savedKernelName : savedKernelNames) {
            if ((savedKernelName.rfind("cl") == savedKernelName.length() - 2) ||
                (savedKernelName.rfind("cu") == savedKernelName.length() - 2) ||
                (savedKernelName.rfind("metal") == savedKernelName.length() - 5)) {
                string kernelURL = variationSetsDirectoryURL() + "/" + variationSetDirURL + "/" + savedKernelName;

                unlink(kernelURL.c_str());
            }

        }
    }
}


#pragma mark XML

std::string VariationSet::pathForResource(const std::string & basename, const std::string & extension)
{
    return std::string(::pathForResource(basename.c_str(), extension.c_str()));
}

std::string VariationSet::factoryVariationSetBaseName()
{
    return "flam3_v3_VariationSet";
}

void VariationSet::cacheFactoryVariationSets()
{
	string legacyVariationSetPath = pathForResource(VariationSet::factoryVariationSetBaseName(), "xml");
	string legacyDirectoryPath = variationSetsDirectoryURL() + "/" + VariationSet::uuidForLegacyVariationSet();

	if ((mkpath(legacyDirectoryPath.c_str(), 0777) == -1) && (errno != EEXIST)) {
		std::string msg = "Failed to create directory: " + legacyDirectoryPath + "  " + strerror(errno);
		throw std::runtime_error(msg);
	}
	if ((unlink(cachedLegacyVariationSetURL().c_str()) == -1) && (errno != ENOENT)) {
		std::string msg = "Failed to remove file: " + cachedLegacyVariationSetURL() + "  " + strerror(errno);
		throw std::runtime_error(msg);
	}

	// copy file from path in bundle to the cached location
	std::ifstream  src(legacyVariationSetPath.c_str(), std::ios::binary);
	std::ofstream  dst(cachedLegacyVariationSetURL().c_str(), std::ios::binary);
	dst << src.rdbuf();
}

#ifdef _WIN32
void VariationSet::cacheFactoryLibrary()
{
	string libraryText = loadEmbeddedResource(VariationSet::factoryLibraryBaseName(), "xml");
    string varSetsDir   = variationSetsDirectoryURL();

    if ((mkpath(varSetsDir.c_str(), 0777) == -1)  && (errno != EEXIST)) {
        std::string msg = "Failed to create directory: " + varSetsDir + "  " + strerror(errno);
        throw std::runtime_error(msg);
    }
    if ((unlink(cachedLibraryURL().c_str()) == -1)  && (errno != ENOENT)) {
        std::string msg = "Failed to remove file: " + cachedLibraryURL() + "  " + strerror(errno);
        throw std::runtime_error(msg);
    }

    // copy file from path in bundle to the cached location
    std::ofstream  dst(cachedLibraryURL().c_str(), std::ios::binary);
    dst << libraryText;
}
#else
void VariationSet::cacheFactoryLibrary()
{
	string libraryPath       = pathForResource(VariationSet::factoryLibraryBaseName(), "xml");
	string varSetsDir        = variationSetsDirectoryURL();
	string cachedLibraryPath = cachedLibraryURL();

	if ((mkpath(varSetsDir.c_str(), 0777) == -1) && (errno != EEXIST)) {
		std::string msg = "Failed to create directory: " + varSetsDir + "  " + strerror(errno);
		throw std::runtime_error(msg);
	}
	if ((unlink(cachedLibraryPath.c_str()) == -1) && (errno != ENOENT)) {
		std::string msg = "Failed to remove file: " + cachedLibraryURL() + "  " + strerror(errno);
		throw std::runtime_error(msg);
	}

	// copy file from path in bundle to the cached location
	std::ifstream  src(libraryPath.c_str(), std::ios::binary);
	std::ofstream  dst(cachedLibraryPath.c_str(), std::ios::binary);
	dst << src.rdbuf();
}
#endif

#ifdef _WIN32
void VariationSet::cacheMyLibrary()
{
	string libraryText = loadEmbeddedResource("myLibrary", "xml");
	string varSetsDir = variationSetsDirectoryURL();

	if ((mkpath(varSetsDir.c_str(), 0777) == -1) && (errno != EEXIST)) {
		std::string msg = "Failed to create directory: " + varSetsDir + "  " + strerror(errno);
		throw std::runtime_error(msg);
	}
	if ((unlink(cachedMyLibraryURL().c_str()) == -1) && (errno != ENOENT)) {
		std::string msg = "Failed to remove file: " + cachedMyLibraryURL() + "  " + strerror(errno);
		throw std::runtime_error(msg);
	}

	// copy file from path in bundle to the cached location
	std::ofstream  dst(cachedMyLibraryURL().c_str(), std::ios::binary);
	dst << libraryText;
}
#else
void VariationSet::cacheMyLibrary()
{
	::cacheMyLibrary();
}
#endif

// create an XML document from the flames
void VariationSet::xmlElement(xml_node & parent)
{

    xml_node xmlElement = parent.append_child("variationSet");

    const char * licenseComment =
    "\nTranslated from original source code by Steven Brodhead Sr. and Steven Brodhead Jr. Copyright 2013-2014"
    "\n Code snippets are licensed under one of either:\n"
    " GNU General Public License v3 (GNU/GPLv3) http://www.gnu.org/licenses/gpl-3.0.html\n"
    " or GNU Lesser  Public License v2.1 (GNU/LGLPv2.1) http://www.gnu.org/licenses/lgpl.html\n"
    " See the applicable license for each source code snippet below.\n";

    xmlElement.append_child(node_comment).set_value(licenseComment);

    for (std::string & variationKey : keysSortedByValue(variationsByKey, Variation::prePostXformIndexPairCompare)) {
        SharedVariation & variation = variationsByKey[variationKey];
        variation->xmlElement(xmlElement);
    }

    if (name.length() > 0) {
        xmlElement.append_attribute("name") = name.c_str();
    }
    if (version.length() > 0) {
        xmlElement.append_attribute("version") = version.c_str();
    }
    if (defaultVariation.length() > 0) {
        xmlElement.append_attribute("defaultVariation") = defaultVariation.c_str();
    }
    xmlElement.append_attribute("is3Dcapable") = is3DCapable ? "yes" : "no";
    xmlElement.append_attribute("uuid")        = uuid.c_str();
    xmlElement.append_attribute("structName")  = variationStructName.c_str();

    if (amTemporary) {
        xmlElement.append_attribute("temporary")        = "yes";
    }

//    std::ostringstream oss;
//    xmlElement.print(oss);
//    string xml = oss.str();
//    printf("%s", xml.c_str());
}

#pragma mark Read/Write

void VariationSet::makeXmlDocument(pugi::xml_document & xmlDoc)
{
    pugi::xml_node decl = xmlDoc.prepend_child(pugi::node_declaration);
    decl.append_attribute("version") = "1.0";
    decl.append_attribute("encoding") = "UTF-8";

    xmlElement(xmlDoc);
}

void VariationSet::writeXmlToPath(const std::string & absolutePath)
{
    xml_document xmlDoc;
    makeXmlDocument(xmlDoc);

    bool status = xmlDoc.save_file(absolutePath.c_str());

    if (! status) {
        std::string msg = "Unable to save file to: " + absolutePath;
        throw std::runtime_error(msg);
    }
}

void VariationSet::writeXmlToPath(std::ostream & stream)
{
    xml_document xmlDoc;
    makeXmlDocument(xmlDoc);

    xmlDoc.save(stream);
}

void VariationSet::saveLibraryXML()
{
    VariationSet::variationsLibrary()->writeXmlToPath(cachedLibraryURL());
}

// create an XML document from the flames
void VariationSet::saveMyLibraryXML()
{
    VariationSet::myVariationsLibrary()->writeXmlToPath(myLibraryURL());
    VariationSet::updateMergedLibrary();
}

std::string VariationSet::xmlFilePath()
{
    return  VariationSet::variationSetsDirectoryURL() + "/" + uuid + "/variationSet.xml";
}

void VariationSet::saveVariationSetXML()
{
    if (amLibrary)
        VariationSet::saveLibraryXML();
    else {
        writeXmlToPath(xmlFilePath());
    }
}

#pragma mark Kernel Management

std::string VariationSet::CPUKernelURL()
{
#ifdef _WIN32
	return variationSetsDirectoryURL() + "/" + uuid + "/Flam4_Kernal.cl";
#elif defined(__linux)
	return variationSetsDirectoryURL() + "/" + uuid + "/Flam4_Kernal.cl";
#else
	return variationSetsDirectoryURL() + "/" + uuid + "/Flam4_KernalCPU.cl";
#endif
}

std::string VariationSet::GPUKernelURL(enum GpuPlatformUsed gpuPlatform)
{
    const char *extension;
    if (gpuPlatform == useOpenCL)
        extension = "cl";
    else if (gpuPlatform == useMetal)
        extension = "metal";
    else
        extension = "cu";

    return variationSetsDirectoryURL() + "/" + uuid + "/Flam4_Kernal." + extension;
}

std::string VariationSet::kernelURL(bool isGPU, enum GpuPlatformUsed gpuPlatformInUse)
{
    return isGPU ? GPUKernelURL(gpuPlatformInUse) : CPUKernelURL();
}

time_t VariationSet::modificationDateOfFile(const string & path)
{
    struct stat attributes;
    stat(path.c_str(), &attributes);
    return attributes.st_mtime;
}

void VariationSet::setFileModificationDate(time_t modDate, const std::string & path)
{
    struct stat attributes;
    stat(path.c_str(), &attributes);

    time_t atime = attributes.st_atime;

#ifdef _WIN32
	struct __utimbuf64 new_times { modDate, atime };
	_utime64(path.c_str(), &new_times);
#else
	struct utimbuf new_times { modDate, atime };
	utime(path.c_str(), &new_times);
#endif

}

bool VariationSet::outOfDateDerived(const string & derivedPath, const string &factoryPath)
{
    if (! fileExists(derivedPath))
        return true;

    struct stat factoryAttributes;
    struct stat derivedAttributes;

    stat(factoryPath.c_str(), &factoryAttributes);
    stat(derivedPath.c_str(), &derivedAttributes);

    time_t factoryModDate = factoryAttributes.st_mtime;
    time_t derivedModDate = derivedAttributes.st_mtime;

    return derivedModDate < factoryModDate;
}

bool VariationSet::kernelOutOfDate(const string & kernelPath, const string & templateBasename, const string & extension)
{
    std::string factoryTemplatePath = ::pathForResource(templateBasename.c_str(), extension.c_str());
    if (factoryTemplatePath.length() == 0)
        return false;
    return outOfDateDerived(kernelPath, factoryTemplatePath);
}

// see if the library has updated variation source or function prototypes
bool VariationSet::upgradeCachedVariationSet()
{
    bool upgraded = false;
    for (auto & v : variationsByKey)  {
        const string &variationKey         = v.first;
        const SharedVariation &variation   = variationsByKey[variationKey];
        const SharedVariationSet & library = VariationSet::variationsLibraryPlus();
        Variation *libraryVariation        = library->variationForKey(variation->key);

        // this variation set referrs to a variation we have no longer - dont use this variation set
        if (!libraryVariation) {
            // import the unknown variation into My Library
            VariationSet::myLibrary->duplicateVariation(variation);
            VariationSet::saveMyLibraryXML();
            return false;
        }
        if (variation->md5sum != libraryVariation->md5sum) {
            variation->setSource(libraryVariation->source);
            upgraded = true;
        }
        if (variation->md5sum2 != libraryVariation->md5sum2) {
            variation->functionsSource    = libraryVariation->functionsSource;
            variation->functionsPrototype = libraryVariation->functionsPrototype;
            upgraded = true;
        }
    }
    return upgraded;
}

void VariationSet::saveKernelsAmUpgraded(bool upgraded, enum GpuPlatformUsed gpuPlatformInUse)
{
    if (amLibrary)
        return;
    const string & directoryPath = VariationSet::variationSetsDirectoryURL() + "/" + uuid;
    const string & varSetPath    = directoryPath + "/variationSet.xml";

    if (! fileExists(directoryPath))
        mkpath(directoryPath.c_str(), 0777);

    if (upgraded || ! fileExists(varSetPath))
        writeXmlToPath(varSetPath);

    struct stat libraryAttributes;
    stat(cachedLibraryURL().c_str(), &libraryAttributes);

    time_t libraryModDate = libraryAttributes.st_mtime;
    setFileModificationDate(libraryModDate, varSetPath);


    if (gpuPlatformInUse == useOpenCL) {
#ifdef __APPLE
        const string & cpuKernelURL = directoryPath + "/Flam4_KernalCPU.cl";
        string cpuTemplate          = "Flam4_3dKernalCPU_Template";
#else
        const string & cpuKernelURL = directoryPath + "/Flam4_Kernal.cl";
        string cpuTemplate          = "Flam4_3dKernal_Template";
#endif

        bool cpuKernelOutOfDate = kernelOutOfDate(cpuKernelURL, cpuTemplate, "cl");
        if (upgraded || cpuKernelOutOfDate) {

            const string & factoryTemplateURL = pathForResource(cpuTemplate, "cl");
            time_t factoryModDate             = modificationDateOfFile(factoryTemplateURL);
            time_t latestDate                 = factoryModDate > libraryModDate ? factoryModDate : libraryModDate;

            writetoKernelPath(cpuKernelURL, true, "cl", gpuPlatformInUse);
            setFileModificationDate(latestDate, cpuKernelURL);
        }
    }
    string extension;
    if (gpuPlatformInUse == useOpenCL)
        extension = "cl";
    else if (gpuPlatformInUse == useMetal)
        extension = "metal";
    else
        extension = "cu";

    string gpuKernelURL  = directoryPath + "/Flam4_Kernal." + extension;
    string gpuTemplate   = "Flam4_3dKernal_Template";

    bool gpuKernelOutOfDate = kernelOutOfDate(gpuKernelURL, gpuTemplate, extension);
    if (upgraded || gpuKernelOutOfDate) {
        const string & factoryTemplateURL = pathForResource(gpuTemplate, extension);
        time_t factoryModDate             = modificationDateOfFile(factoryTemplateURL);
        time_t latestDate                 = factoryModDate > libraryModDate ? factoryModDate : libraryModDate;

        writetoKernelPath(gpuKernelURL, false, extension, gpuPlatformInUse);
        setFileModificationDate(latestDate, gpuKernelURL);
    }
}

void VariationSet::removeKernels()
{
    // dont allow them to remove this one
    if (uuid == uuidForLegacyVariationSet())
        return;
    // dont allow them to remove library
    if (amLibrary)
        return;

    const string & directoryPath = variationSetsDirectoryURL() + "/" + uuid;

    if (! fileExists(directoryPath))
        return;
    unlink(directoryPath.c_str());

    ::empty_dir(DeviceProgram::cachePath().c_str()); // empty directory's contents
}


#pragma mark Generate Kernel Source Code

string VariationSet::get3DSnippet(const string & snippet)
{
    size_t pos3dSplit    = snippet.find("__3D_SUPPORT_ONLY__");
    size_t posEnd3dSplit = snippet.find("__END_3D_SUPPORT_ONLY__");
    if (pos3dSplit == string::npos)
        return snippet;

    size_t posLineStart = snippet.rfind('\n', pos3dSplit);
    size_t posLineEnd   = snippet.find('\n',  pos3dSplit);

    string firstPart  = snippet.substr(0, posLineStart + 1);

    string secondPart;
    if (posEnd3dSplit == string::npos) {
        secondPart = snippet.substr(posLineEnd + 1);
        return firstPart + secondPart;
    }
    else {
        size_t posLineEnd2 = snippet.find('\n',  posEnd3dSplit);
        secondPart         = snippet.substr(posLineEnd + 1, posEnd3dSplit - posLineEnd - 1);
        string thirdPart   = snippet.substr(posLineEnd2 + 1);
        return firstPart + secondPart + thirdPart;
    }
}

string VariationSet::get2DSnippet(const string & snippet)
{
    size_t pos3dSplit    = snippet.find("__3D_SUPPORT_ONLY__");
    size_t posEnd3dSplit = snippet.find("__END_3D_SUPPORT_ONLY__");
    if (pos3dSplit == string::npos)
        return snippet;

    size_t posLineStart = snippet.rfind('\n', pos3dSplit);
    size_t posLineEnd   = snippet.find('\n',  pos3dSplit);

    string firstPart  = snippet.substr(0, posLineStart + 1);

    if (posEnd3dSplit == string::npos)
        return firstPart;
    else {
        posLineStart      = snippet.rfind('\n', posEnd3dSplit);
        posLineEnd        = snippet.find('\n',  posEnd3dSplit);
        string thirdPart = snippet.substr(posEnd3dSplit + 1);
        return firstPart + thirdPart;
    }
}

std::string VariationSet::kernelTemplateFromBasename(const std::string & basename, const std::string & extension)
{
#ifdef _WIN32
	return loadEmbeddedResource(basename.c_str(), extension.c_str());
#else
	string sourcePath = pathForResource(basename, extension);

	std::ifstream in(sourcePath);
	if (in.fail()) {
		std::string msg = string_format("Failed to find file: %s", sourcePath.c_str());
		throw std::runtime_error(msg);
	}
	string kernelTemplate((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
	return kernelTemplate;
#endif
}

std::string VariationSet::cpuKernelTemplate(const std::string & extension)
{
#ifdef _WIN32
	string kernelTemplate = "Flam4_3dKernalCPU_Template";
#else
	string kernelTemplate = "Flam4_3dKernalCPU_Template";
#endif
    return VariationSet::kernelTemplateFromBasename(kernelTemplate, extension);
}

std::string VariationSet::gpuKernelTemplate(const std::string & extension)
{
    string kernelTemplate = "Flam4_3dKernal_Template";
    return VariationSet::kernelTemplateFromBasename(kernelTemplate, extension);
}

//    struct VarPar__varname {
//        float varname;
//        float varpar_name1;
//        float varpar_name2;
//    } __attribute__ ((aligned (4)));

std::string VariationSet::createVarParStructs(enum GpuPlatformUsed gpuPlatform)
{
    static char buf[200];
    SharedVariationsVector array;
    for (string variationKey : keysSortedByValue(variationsByKey, Variation::xformIndexPairCompare)) {
        SharedVariation & variation = variationsByKey[variationKey];
        array.push_back(variation);
    }
    string variationDecls;
    for (SharedVariation & variation : array) {

        if (gpuPlatform == useCUDA) {
            snprintf(buf, sizeof(buf), "struct __align__(4) VarPar__%s {\n", variation->key.c_str());
            variationDecls.append(buf);
        }
        else {
            snprintf(buf, sizeof(buf), "struct VarPar__%s {\n", variation->key.c_str());
            variationDecls.append(buf);
        }
        snprintf(buf, sizeof(buf), "    float %s;\n", variation->key.c_str());
        variationDecls.append(buf);

        for (std::string & name :  keysSortedByValue(variation->getParameters(), VariationParameter::xformIndexPairCompare)) {
            SharedParameter & param = variation->getParameters()[name];
            snprintf(buf, sizeof(buf), "    float %s;\n", param->key.c_str());
            variationDecls.append(buf);
        }
        if (gpuPlatform == useCUDA)
            variationDecls.append("};\n\n");
        else
            variationDecls.append("} __attribute__ ((aligned (4)));\n\n");
    }
    return variationDecls;
}

#pragma mark Fill-in Templates

std::string VariationSet::switchCasesForVariations(SharedVariationsVector & variations, bool for3D, enum GpuPlatformUsed gpuPlatform)
{
    static char buf[200];
    string string;

    for (SharedVariation & variation : variations) {
        if (variation->status == broken)
            continue;

        snprintf(buf, sizeof(buf), "            case __VAR__%s__INDEX:\n", variation->key.c_str());
        string.append(buf);
        string.append("            {\n");

        if (gpuPlatform == useCUDA) {
            snprintf(buf, sizeof(buf), "                struct VarPar__%s *varpar = (struct VarPar__%s *)varparCluster;",
                    variation->key.c_str(), variation->key.c_str());
            string.append(buf);
        }
        else {
            snprintf(buf, sizeof(buf), "                __constant struct VarPar__%s *varpar = (__constant struct VarPar__%s *)varparCluster;",
                    variation->key.c_str(), variation->key.c_str());
            string.append(buf);
        }
        std::string snippet = for3D ? get3DSnippet(variation->source) : get2DSnippet(variation->source);

//        if (gpuPlatform == useMetal)
//            string.append(Variation::convertToMetalCompatibleSincos(snippet));
//        else if (gpuPlatform == useCUDA)
        if (gpuPlatform == useCUDA)
            string.append(Variation::convertToCudaCompatibleSincos(snippet));
        else
            string.append(snippet);
        string.append("            }\n");
        string.append("            break;\n");
    }
    return string;
}

std::string VariationSet::indentVariationSource(const std::string & _string)
{
    string indented;
    string indent       = "                    ";
    size_t stringLength = _string.length();
    size_t index        = 0;

    // break the source string into lines and append the indent in the front of each line
    while (index < stringLength) {
        size_t lineStart = _string.rfind('\n', index);
        if (lineStart == string::npos)
            lineStart = 0;

        size_t lineEnd = _string.find('\n',  index + 1);
        if (lineEnd == string::npos)
            lineEnd = _string.length();

        indented.append(indent).append(_string.substr(lineStart, lineEnd - lineStart));
        index = lineEnd;
    }
    return indented;
}

std::string VariationSet::createKernelForCPU(bool forCPU, const string & extension, enum GpuPlatformUsed gpuPlatform)
{
    string str = forCPU ? cpuKernelTemplate(extension) : gpuKernelTemplate(extension);

    const char * macro = "__VARPAR_STRUCT_DECLS__";
    size_t pos = str.find(macro);
    str.replace(pos, strlen(macro), createVarParStructs(gpuPlatform));

    bool for3D = is3DCapable;
    SharedVariationsVector array;
    for (string variationKey : keysSortedByValue(variationsByKey, Variation::prePostXformIndexPairCompare)) {
        SharedVariation & variation = variationsByKey[variationKey];
        array.push_back(variation);
    }

    string variationIndexDefines;
    for (SharedVariation  &variation : array) {
        variationIndexDefines.append(string_format("#define __VAR__%s__INDEX %u\n", variation->key.c_str(), variation->xformIndex));
    }

    macro = "__VARIATION_INDEX_DEFINES__";
    pos = str.find(macro);
    str.replace(pos, strlen(macro), variationIndexDefines);

    string functionsSource, functionsPrototype;

    for (SharedVariation  &variation : array) {
        if (variation->functionsSource.length() > 0)
            functionsSource.append(variation->functionsSource);
        if (variation->functionsPrototype.length() > 0)
            functionsPrototype.append(variation->functionsPrototype);
    }

    macro = "__VARIATION_FUNCTION_PROTOTYPES__";
    pos = str.find(macro);
    str.replace(pos, strlen(macro), functionsPrototype);

    macro = "__VARIATION_FUNCTIONS__";
    pos = str.find(macro);
    str.replace(pos, strlen(macro), functionsSource);


    string switchCases = switchCasesForVariations(array, for3D, gpuPlatform);
    macro = "__VARIATION_SWITCH_CASES__";
    pos = str.find(macro);
    str.replace(pos, strlen(macro), switchCases);


    // replace OpenCL native_func( calls with func(
    size_t match = str.find("native_", 0);
    while (match != string::npos) {
        str.replace(match, 7, "");
        match = str.find("native_", match);
    }
    return str;
}

void VariationSet::writetoKernelPath(const std::string & kernelURL, bool forCPU, const std::string & extension,  enum GpuPlatformUsed gpuPlatform)
{
    string kernelCode = createKernelForCPU(forCPU, extension, gpuPlatform);
    std::ofstream out(kernelURL);
    if (out.fail()) {
        std::string msg = string_format("Failed to open file: %s", kernelURL.c_str());
        throw std::runtime_error(msg);
    }
    out << kernelCode;
}

#pragma mark TransformProperty

TransformProperty::TransformProperty()
: key(), xformIndex(0)
{}

TransformProperty::TransformProperty(const std::string & _key)
: key(_key), xformIndex(0)
{}

TransformProperty::TransformProperty(const std::string & _key, size_t _xformIndex)
: key(_key), xformIndex(_xformIndex)
{}

TransformProperty::TransformProperty(const char * _key, size_t _xformIndex)
: key(_key), xformIndex(_xformIndex)
{}

TransformProperty::TransformProperty(const TransformProperty & o)
: key(o.key), xformIndex(o.xformIndex)
{}
