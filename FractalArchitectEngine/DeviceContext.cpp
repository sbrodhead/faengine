//
//  DeviceContext.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "CpuMemStats.hpp"
#include "DeviceContext.hpp"
#include "DeviceKind.hpp"
#include "DeviceUsage.hpp"
#include "DeviceProgram.hpp"
#include "Flame.hpp"

#include <cstdlib>
#include <algorithm>
#include <unordered_set>
#include <iterator>
#include <limits.h>

std::string DeviceContext::gpuVendor;
uint        DeviceContext::cpuFuseIterations         = 30;
uint        DeviceContext::gpuFuseIterations         = 60;
float       DeviceContext::finalXformFuseMultiplier = 1.4;


DeviceContext::DeviceContext(bool _forceRebuild, SharedVariationSet _defaultVariationSet, bool _singleDeviceAllowed, bool _canUseGPU, bool ignoreQuarantine)
:
predicate(DeviceUsage::predicateForLicensedAvailableAndUsed),
    selectedDevices(),
    singleDeviceAllowed(_singleDeviceAllowed),
    deviceIDs(nullptr),
    subDeviceIDs(nullptr),
    subDeviceCount(0),
    available(nullptr),
    quarantined(nullptr),
    numDevices(0),
    platformVersion(),
    canPartitionCPU(false),
    cpuDeviceIndex(0),
    computeUnitCountIndex(0),
    computeUnitCounts(),
    canUseGPU(_canUseGPU),
    deviceNames(),
    deviceUsages(),
    deviceVendor(),
    deviceKinds(),
    defaultVariationSet(_defaultVariationSet), 
	forceRebuild(_forceRebuild),
    mutex()
{}

DeviceContext::~DeviceContext()
{
    if (subDeviceIDs)
        free(subDeviceIDs);
    if (available)
        free(available);
    if (quarantined)
        free(quarantined);
    if (deviceIDs)
        free(deviceIDs);
}

std::string DeviceContext::getGpuVendor()
{
    return gpuVendor;
}

size_t DeviceContext::firstUsableDeviceNum()
{
    for (uint i = 0; i < numDevices; i++)
        if (available[i] && (! quarantined[i]))
            return i;
    return std::string::npos;
}

uint DeviceContext::getCpuFuseIterations()
{
    return cpuFuseIterations;
}

void DeviceContext::setCpuFuseIterations(uint _cpuFuseIterations)
{
    cpuFuseIterations = _cpuFuseIterations;
}

uint DeviceContext::getGpuFuseIterations()
{
    return gpuFuseIterations;
}

void DeviceContext::setGpuFuseIterations(uint _gpuFuseIterations)
{
    gpuFuseIterations = _gpuFuseIterations;
}

float DeviceContext::getFinalXformFuseMultiplier()
{
    return finalXformFuseMultiplier;
}

void DeviceContext::setFinalXformFuseMultiplier(float _finalXformFuseMultiplier)
{
    finalXformFuseMultiplier = _finalXformFuseMultiplier;
}

SharedDeviceKind DeviceContext::deviceKindForDevice(size_t i)
{
    if (i >= numDevices)
        return std::shared_ptr<DeviceKind>();
    
    std::string & kind = deviceNames[i];
    return deviceKinds[kind];
}

size_t DeviceContext::selectedDeviceCount()
{
    return selectedDevices.size();
}

std::string DeviceContext::uuidForDevice(size_t i)
{
    return deviceKindForDevice(i)->uuid();
}

std::string DeviceContext::uuidForSelectedDevice(size_t selectedDeviceNum)
{
    SharedDeviceKind deviceKind = deviceKindForSelectedDevice(selectedDeviceNum);
    return deviceKind->uuid();
}

std::string DeviceContext::vendorForDevice(size_t i)
{
    if (i >= numDevices)
        return std::string();
    return deviceVendor[i];
}

std::string DeviceContext::nameForDevice(size_t i)
{
    if (i >= numDevices)
        return std::string();
    return deviceNames[i];
}

void DeviceContext::rearrangeSelectedDevices() {
    selectedDevices.clear();
    copy_if(deviceUsages.begin(), deviceUsages.end(), std::inserter(selectedDevices, selectedDevices.end()), predicate);
    std::sort(selectedDevices.begin(), selectedDevices.end(), DeviceUsage::compare);
}

SharedDeviceKind DeviceContext::deviceKindForSelectedDevice(size_t selectedDeviceNum)
{
    SharedDeviceUsage deviceUsage = selectedDevices[selectedDeviceNum];
    return deviceUsage->deviceKind;
}

uint DeviceContext::warpSizeForSelectedDevice(size_t selectedDeviceNum)
{
    SharedDeviceUsage deviceUsage = selectedDevices[selectedDeviceNum];
    return deviceUsage->deviceKind->warpSize;
}

uint DeviceContext::fuseIterationsForSelectedDevice(size_t selectedDeviceNum)
{
    SharedDeviceUsage deviceUsage = selectedDevices[selectedDeviceNum];
    return deviceUsage->fuseIterations();
}

bool DeviceContext::getCanUseGPU()
{
    return canUseGPU;
}

uint DeviceContext::warpSizeForDevice(size_t i)
{
    if (i >= numDevices)
        return 1;
    std::string & kind          = deviceNames[i];
    SharedDeviceKind deviceKind = deviceKinds[kind];
    return deviceKind->warpSize;
}

size_t DeviceContext::globalMemSizeForDevice(size_t i)
{
    if (i >= numDevices)
        return 1;
    const std::string & kind    = deviceNames[i];
    SharedDeviceKind deviceKind = deviceKinds[kind];
    return deviceKind->globalMemSize;
}

size_t DeviceContext::maxMemAllocSizeForDevice(size_t i)
{
    if (i >= numDevices)
        return 1;
    const std::string & kind    = deviceNames[i];
    SharedDeviceKind deviceKind = deviceKinds[kind];
    return deviceKind->maxMemAllocSize;
}

size_t DeviceContext::indexOfDeviceUsage(const SharedDeviceUsage & deviceUsage)
{
    for (size_t i = 0; i < deviceUsages.size(); i++) {
        if (deviceUsage.get() == deviceUsages[i].get()) {
            return i;
        }
    }
    return std::string::npos;
}

size_t DeviceContext::maxWorkGroupSizeForDevice(size_t i)
{
    if (i >= numDevices)
        return 1;
    const std::string & kind      = deviceNames[i];
    SharedDeviceKind & deviceKind = deviceKinds[kind];
    return deviceKind->maxWorkGroupSize;
}

size_t DeviceContext::maxWorkGroupSizeForKernelsForDevice(size_t i, const SharedVariationSet &variationSet)
{
    if (i >= numDevices)
        return 1;

    SharedDeviceUsage &deviceUsage = deviceUsages[i];
    SharedDeviceKind deviceKind    = deviceUsage->deviceKind;

    size_t mwgs = deviceKind->maxWorkGroupSizeForKernelsForVariationSet(variationSet);
    if (deviceKind->isCPU())
        mwgs = 1;
    return mwgs;
}

size_t DeviceContext::maxWorkGroupSizeForKernelsForSelectedDevice(size_t selectedDeviceNum, const SharedVariationSet &variationSet)
{
    SharedDeviceUsage deviceUsage = selectedDevices[selectedDeviceNum];
    size_t deviceNum = indexOfDeviceUsage(deviceUsage);
    
    return maxWorkGroupSizeForKernelsForDevice(deviceNum, variationSet);
}

SharedDeviceUsage DeviceContext::deviceUsageForDeviceID(void *deviceID)
{
    for (size_t i = 0; i < numDevices; i++) {
        SharedDeviceUsage & deviceUsage = deviceUsages[i];
        if (deviceUsage->deviceID == deviceID)
            return deviceUsage;
    }
    return std::shared_ptr<DeviceUsage>();
}

size_t DeviceContext::deviceNumForDeviceID(void * deviceID)
{
    for (size_t i = 0; i < numDevices; i++) {
        SharedDeviceUsage & deviceUsage = deviceUsages[i];
        if (deviceUsage->deviceID == deviceID)
            return i;
    }
    return 0;
}

size_t DeviceContext::selectedDeviceNumForDeviceID(void *deviceID)
{
    for (size_t selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++) {
        SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
        if (deviceUsage->deviceID == deviceID)
            return selectedDeviceNum;
    }
    return 0;
}

size_t DeviceContext::selectedDeviceNumForDeviceNum(uint deviceNum)
{
    
    SharedDeviceUsage & deviceUsage = deviceUsages[deviceNum];
    return selectedDeviceNumForDeviceID(deviceUsage->deviceID);
}

size_t DeviceContext::deviceNumForSelectedDeviceNum(uint selectedDeviceNum)
{
    if (selectedDevices.size() == 0)
        return 0;
    
    SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
    return indexOfDeviceUsage(deviceUsage);
}

#pragma mark Available GPU Memory Checking

size_t DeviceContext::maxMemAllocSizeForSelectedDevice(size_t selectedDeviceNum)
{
    SharedDeviceUsage deviceUsage = selectedDevices[selectedDeviceNum];
    size_t deviceNum = std::string::npos;
    for (size_t i = 0; i < deviceUsages.size(); i++) {
        if (deviceUsage.get() == deviceUsages[i].get()) {
            deviceNum = i;
            break;
        }
    }
    return maxMemAllocSizeForDevice(deviceNum);
}

void DeviceContext::refreshProgramForVariationSet(const SharedVariationSet &variationSet, bool rebuild)
{
    size_t selectedDeviceCount = selectedDevices.size();
    std::unordered_set<SharedDeviceKind> deviceKindSet;
    for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDeviceCount; selectedDeviceNum++) {
        const SharedDeviceKind & deviceKind = deviceKindForSelectedDevice(selectedDeviceNum);
        deviceKindSet.insert(deviceKind);
        
    }
    duration<double> buildTime(0.);
    for (const SharedDeviceKind & deviceKind : deviceKindSet) {
        deviceKind->makeProgramFromSource(variationSet, rebuild, buildTime);
    }
}

bool DeviceContext::makeTestProgramForVariationSet(const SharedVariationSet & variationSet)
{
    int firstCPUDevice = -1;
    for (int i = 0; i < numDevices; i++) {
        if (available[i] && (! quarantined[i])) {
            SharedDeviceUsage & deviceUsage = deviceUsages[i];
            if (! deviceUsage->isGPU) {
                firstCPUDevice = i;
                break;
            }
        }
    }
    
    duration<double> buildTime(0.);
    const SharedDeviceKind  & deviceKind = deviceKindForDevice(firstCPUDevice);
    const DeviceProgram  * program = deviceKind->makeProgramFromSource(variationSet, true, buildTime);
    return program != nullptr;
}

bool DeviceContext::selectedDevicesHasGPU()
{
    for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++)
    {
        SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
        if (deviceUsage->isGPU)
            return true;
    }
    return false;
}

void * DeviceContext::firstCPUDeviceID()
{
    for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++)
    {
        SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
        if (!deviceUsage->isGPU)
            return deviceUsage->deviceID;
    }
    return NULL;
}

uint DeviceContext::maxFuseIterationsFromSelectedDevices(bool hasFinalXform)
{
    uint maxFuseIterations = 0;
    for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++)
    {
        SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
        if (deviceUsage->fuseIterations() > maxFuseIterations)
            maxFuseIterations = deviceUsage->fuseIterations();
            }
    if (hasFinalXform)
        return (uint)roundf(finalXformFuseMultiplier * maxFuseIterations);
        return maxFuseIterations;
}

uint DeviceContext::maxEstimatorRadiusForSelectedDevices()
{
    SharedDeviceUsage & deviceUsage     = selectedDevices[0];
    size_t deviceNum                    = deviceNumForDeviceID(deviceUsage->deviceID);
    const SharedDeviceKind & deviceKind = deviceKindForDevice(deviceNum);
    return deviceKind->maxDensityEstimationRadius();
}

void DeviceContext::unStageUsage()
{
    std::unique_lock<std::mutex> lock(mutex);
    
    bool changes = false;
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        if (deviceUsage->used != deviceUsage->stagedUsed) {
            changes = true;
            deviceUsage->used = deviceUsage->stagedUsed;
        }
        deviceUsage->enforceLicense(canUseGPU); // resets any temp licenses
    }
    if (changes)
        rearrangeSelectedDevices();
}

void DeviceContext::setCanUseGPU(bool _canUseGPU)
{
    canUseGPU = _canUseGPU;
    
    std::unique_lock<std::mutex> lock(mutex);

    bool changes = false;
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        bool licensed = deviceUsage->licensed;
        deviceUsage->enforceLicense(canUseGPU); // resets any temp licenses
        if (licensed != deviceUsage->licensed)
            changes = true;
    }
    if (changes)
        rearrangeSelectedDevices();
}

void DeviceContext::tempLicenseForDeviceID1(void *deviceID1,  void *deviceID2, uint deviceCount)
{
    if (deviceCount == 0 || deviceCount > 2)
        return;
    
    std::unordered_set<SharedDeviceUsage> usedSet;

    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        if (deviceUsage->used)
            usedSet.insert(deviceUsage);
    }
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        deviceUsage->used       = false;
        deviceUsage->stagedUsed =false;
    }
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        if (deviceUsage->deviceID == deviceID1 || deviceUsage->deviceID == deviceID2) {
            deviceUsage->used        = true;
            deviceUsage->tempLicense = true;
        }
    }
    // one shot override
    predicate = DeviceUsage::predicateForTempLicenseAvailableAndUsedIgnoreQuarantine;
    rearrangeSelectedDevices();
    
    // restage the previously used devices - they will be restored on the next image render
    for (SharedDeviceUsage deviceUsage : usedSet) {
        deviceUsage->stagedUsed = true;
    }
}

bool DeviceContext::checkOclGPUMemAvailable(size_t & memoryAvailable,
                                          float & area,
                                          float niceFactor,
                                          uint xformCount,
                                          uint supersample)
{
    size_t trialMemoryAvailable = ULLONG_MAX;
    float trialArea            = area;
    size_t lowMemoryAvailable   = ULLONG_MAX;
    float lowArea              = 1.e20f;
    
    bool success = false;
    // workaround for cant change DeviceUsages while array is being walked
    for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++) {
        SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
        
        trialMemoryAvailable = maxMemAllocSizeForSelectedDevice(selectedDeviceNum);
        trialArea            = area;
        uint deviceNum       = (uint)indexOfDeviceUsage(deviceUsage);
        
        success = checkOclGPUMemAvailable(trialMemoryAvailable,
                                          trialArea,
                                          niceFactor,
                                          warpSizeForDevice(deviceNum),
                                          xformCount,
                                          supersample,
                                          deviceNum);
        if (lowMemoryAvailable > trialMemoryAvailable) {
            lowMemoryAvailable = trialMemoryAvailable;
            lowArea            = trialArea;
        }
    }
    memoryAvailable = lowMemoryAvailable;
    area            = lowArea;
    
    return success;
}


#ifdef __ORIGINAL__

- (NSDictionary *)plistForDevice:(void *)deviceID
{
    int index = -1;
    
    for (int i = 0; i < numDevices; i++) {
        if (deviceIDs[i] == deviceID) {
            index = i;
            break;
        }
    }
    if (index == -1)
        return nil;
    DeviceKind *deviceKind = [deviceKinds objectForKey:[deviceNames objectAtIndex:index]];
    return [deviceKind existingPlist];
}

+ (void)removeAllCachedBinaries
{
    NSError *error              = nil;
    NSFileManager *fm           = [NSFileManager defaultManager];
    NSString *bundleIdentifier  = [[NSBundle mainBundle] bundleIdentifier];
    NSURL *cacheURL             = [DeviceProgram cacheURL];
    NSURL *cacheForBundleDir    = [cacheURL  URLByAppendingPathComponent:bundleIdentifier];
    NSArray *deviceSpecificURLs = [fm contentsOfDirectoryAtURL:cacheForBundleDir
                                    includingPropertiesForKeys:[NSArray array]
                                                       options: NSDirectoryEnumerationSkipsHiddenFiles
                                                         error:&error];
    
    for (NSURL * deviceSpecificURL in deviceSpecificURLs) {
        NSURL *binaryDirectoryURL = [deviceSpecificURL URLByAppendingPathComponent:@"variationSets"];
        [fm removeItemAtURL:binaryDirectoryURL error:&error];
    }
}

+ (void)removeAllOpenCLSourceFiles
{
    NSError *error              = nil;
    NSFileManager *fm           = [NSFileManager defaultManager];
    NSString *bundleIdentifier  = [[NSBundle mainBundle] bundleIdentifier];
    NSURL *cacheURL             = [DeviceProgram cacheURL];
    NSURL *cacheForBundleDir    = [cacheURL  URLByAppendingPathComponent:bundleIdentifier];
    NSArray *deviceSpecificURLs = [fm contentsOfDirectoryAtURL:cacheForBundleDir
                                    includingPropertiesForKeys:[NSArray array]
                                                       options: NSDirectoryEnumerationSkipsHiddenFiles
                                                         error:&error];
    
    for (NSURL * deviceSpecificURL in deviceSpecificURLs) {
        NSURL *binaryDirectoryURL = [deviceSpecificURL URLByAppendingPathComponent:@"variationSets"];
        [fm removeItemAtURL:binaryDirectoryURL error:&error];
    }
}

#endif
