
// Copyright 2008 Steven Brodhead

//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef DefinesOCL_h
#define DefinesOCL_h

#define NUM_ITERATIONS 100

// for 2 warps per block
#define NUM_POINTS_WARPSIZE_32 64

// Single FUSE pass causes the first iteratePointsKernal to be less effective (versus 5 FUSE passes)
// this value is from tested results
#define NUM_ITERATIONS_FUSE_REDUCED 90
#define DENSITY_KERNAL_RADIUS 7
#define NUM_FRAMES 160
#define FRAME_RATE 30
#define BITRATE 54000000

#ifndef SUPERSAMPLE_WIDTH
#define SUPERSAMPLE_WIDTH 0.25f
#endif


#endif /* DefinesOCL_h */
