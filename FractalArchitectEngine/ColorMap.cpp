//
//  ColorMap.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/13/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "ColorMap.hpp"
#include "ElectricSheepStuff.hpp"
#include "ColorNode.hpp"
#include "Flame.hpp"
#include "common.hpp"

ColorMap::ColorMap(Color & _background, ColorNodeVector _colorNodes, enum PaletteMode _paletteMode)
    : ColorGradient(_colorNodes, _paletteMode), background(_background)
{}

ColorMap::ColorMap(rgba & _background, ColorNodeVector _colorNodes, enum PaletteMode _paletteMode)
: ColorGradient(_colorNodes, _paletteMode), background(_background)
{}

ColorMap::ColorMap(const ColorMap & c)
: ColorGradient(c), background(c.background)
{}

bool ColorMap::operator==(const ColorMap & c)
{
    if (background != c.background)
        return false;
    if (ColorGradient::operator!=(c))
        return false;
    return true;
}

std::string ColorMap::info()
{
    static char buf[100];
    size_t count               = colorNodes->size();
    std::string paletteModeStr = ElectricSheepStuff::stringForPaletteMode(paletteMode);
    std::string description;
    
    snprintf(buf, sizeof(buf), "PaletteMode: %s  Background: %s", paletteModeStr.c_str(), background.hexadecimalValueOfAnNSColor().c_str());
    description.append(buf);
    snprintf(buf, sizeof(buf), "\n%zu Color Nodes:\n", count);
    description.append(buf);
    
    const size_t nodesPerLine = 4;
    size_t nodeCount          = 0;
    for (ColorNode & node : *colorNodes) {
        description.append(node.lineDescriptionForColorNodeCount(count));
        
        if (++nodeCount == nodesPerLine) {
            nodeCount = 0;
            description.append("\n");
        }
        else {
            description.append("  ");
        }
    }
    return description;
}

ColorMap * ColorMap::makeColorMapFromFlame(Flame *flame, enum PaletteMode paletteMode)
{
    ColorNodeVector nodes;
    nodes.reserve(256);
    
    for (int i = 0; i < flame->numColors; i++) {
        Color color(flame->colorIndex[i].r,
                    flame->colorIndex[i].g,
                    flame->colorIndex[i].b,
                    flame->colorIndex[i].a);
        float location = flame->colorLocations[i];
        nodes.emplace_back(color, location);
    }
    return new ColorMap(flame->params.background, nodes, paletteMode);
}

#ifdef __ORIGINAL__


- (NSString *)description
{
    return [NSString stringWithFormat:@"%s\n%s", [super description], [self info]];
    
}


@end


#endif
