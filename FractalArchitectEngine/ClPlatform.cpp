//
//  ClPlatform.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/22/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//  Created by Steven Brodhead on 5/17/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "ClPlatform.hpp"
#include "Utilities.hpp"
#include <string.h>

static bool checkErr(cl_int err, const char *name)
{
    if (err != CL_SUCCESS) {
        size_t size = snprintf( nullptr, 0, "ERROR: %s (%u)", name, err) + 1; // Extra space for '\0'
        
        std::unique_ptr<char[]> buf(new char[size]);
        
        snprintf(buf.get(), size, "ERROR: %s (%u)", name, err);
        systemLog(buf.get());
        return true;
    }
    return false;
}

ClPlatform::ClPlatform(cl_platform_id _platformID, cl_device_type filter)
: platformID(_platformID), 
deviceIDs(nullptr), 
version(getPlatformVersion(_platformID)),
numDevices(0),
name(platformName(_platformID)), 
vendor(platformVendor(_platformID))
{
    cl_int errNum = CL_SUCCESS;
    deviceIDs = NULL;
    errNum = clGetDeviceIDs(platformID,
                            filter,
                            0,
                            NULL,
                            &numDevices);
    if (errNum != CL_SUCCESS && errNum != CL_DEVICE_NOT_FOUND)
    {
        if (checkErr(errNum, "clGetDeviceIDs"))
            throw std::runtime_error(("ERROR: No OpenCL device found"));
    }
#ifdef _WIN32
	if (name.substr(0, 12) == "Experimental") // disregard Intel's experimental OpenCL 2.1 platform
		numDevices = 0;
	if (name.substr(0, 3) == "AMD") {  // dont use AMD platform for CPU - crashes on clCreateContext
		if (filter & CL_DEVICE_TYPE_CPU) {
			filter ^= CL_DEVICE_TYPE_CPU;

			errNum = clGetDeviceIDs(platformID,
				filter,
				0,
				NULL,
				&numDevices);
			if (errNum != CL_SUCCESS && errNum != CL_DEVICE_NOT_FOUND)
			{
				if (checkErr(errNum, "clGetDeviceIDs"))
					throw std::runtime_error(("ERROR: No OpenCL device found"));
			}
		}
	}
#endif
    if (numDevices > 0)
    {
        deviceIDs   = (cl_device_id *)malloc(sizeof(cl_device_id) * numDevices);
        
        errNum = clGetDeviceIDs(platformID,
                                filter,
                                numDevices,
                                (cl_device_id *)deviceIDs,
                                NULL);
        if (checkErr(errNum, "clGetDeviceIDs")) {
            throw std::runtime_error(("ERROR: clGetDeviceIDs() failed"));
        }
    }
//	printf("Platform:%s  %s\n", ClPlatform::platformName(_platformID).c_str(), name);
}

ClPlatform::~ClPlatform()
{
    if (deviceIDs) free(deviceIDs);
}

std::string ClPlatform::getPlatformVersion(cl_platform_id platformID)
{
    cl_int errNum;
    size_t paramValueSize;
    
    errNum = clGetPlatformInfo(platformID, CL_PLATFORM_VERSION, 0, NULL,  &paramValueSize);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Platform Version");
        return nullptr;
    }
    
    char *_version = (char *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetPlatformInfo(platformID, CL_PLATFORM_VERSION, paramValueSize, _version, NULL);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL  Platform Version");
        return nullptr;
    }
    return std::string(_version);
}

bool ClPlatform::isApplePlatform(cl_platform_id  id)
{
	cl_int errNum;
	size_t paramValueSize;

	errNum = clGetPlatformInfo(id, CL_PLATFORM_VENDOR, 0, NULL, &paramValueSize);
	if (errNum != CL_SUCCESS)
		return false;

	char *vendor = (char *)alloca(sizeof(char) * paramValueSize);
	errNum = clGetPlatformInfo(id, CL_PLATFORM_VENDOR, paramValueSize, vendor, NULL);
	return strcmp(vendor, "Apple") == 0;
}

std::string ClPlatform::platformVendor(cl_platform_id  id)
{
	cl_int errNum;
	size_t paramValueSize;

	errNum = clGetPlatformInfo(id, CL_PLATFORM_VENDOR, 0, NULL, &paramValueSize);
	if (errNum != CL_SUCCESS)
		return "";

	char *vendor = (char *)alloca(sizeof(char) * paramValueSize);
	errNum = clGetPlatformInfo(id, CL_PLATFORM_VENDOR, paramValueSize, vendor, NULL);

	return std::string(vendor);
}

std::string ClPlatform::platformName(cl_platform_id  id)
{
	cl_int errNum;
	size_t paramValueSize;

	errNum = clGetPlatformInfo(id, CL_PLATFORM_NAME, 0, NULL, &paramValueSize);
	if (errNum != CL_SUCCESS)
		return "";

	char *name = (char *)alloca(sizeof(char) * paramValueSize);
	errNum = clGetPlatformInfo(id, CL_PLATFORM_NAME, paramValueSize, name, NULL);

	return std::string(name);
}
