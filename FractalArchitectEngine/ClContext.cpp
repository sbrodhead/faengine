//
//  ClContext.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/17/16.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.


#include "ClContext.hpp"
#include "Utilities.hpp"
#include "DeviceUsage.hpp"
#include "ClDeviceKind.hpp"
#include "Flam4ClRuntime.hpp"
#include "CpuMemStats.hpp"
#include "Flame.hpp"
#include "ClPlatform.hpp"
#include "common.hpp"

#include <sstream>
#include <algorithm>
#include <unordered_set>
#include <cctype>

#ifdef __linux
#include <limits.h>
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif

using std::string;

static string gpuVendor;
static char buf[1024];

ClContext * ClContext::context = nullptr;

// compiler warns this is unused but it is used by clCreateContext()
static void CL_CALLBACK contextCallback(const char * errInfo,
                                        const void * private_info,
                                        size_t cb,
                                        void * user_data)
{
    snprintf(buf, sizeof(buf), "Error occurred during context use: %s", errInfo);
    systemLog(buf);
}

static bool checkErr(cl_int err, const char *name)
{
    if (err != CL_SUCCESS) {
        size_t size = snprintf( nullptr, 0, "ERROR: %s (%u)", name, err) + 1; // Extra space for '\0'
        
        std::unique_ptr<char[]> buf(new char[size]);
        
        snprintf(buf.get(), size, "ERROR: %s (%u)", name, err);
        systemLog(buf.get());
        return true;
    }
    return false;
}

void ClContext::printDeviceList(cl_device_type devType)
{
    cl_int          errNum;
    cl_uint         numPlatforms   = 0;
    cl_platform_id *platformIDs    = NULL;
    
    // First, select an OpenCL platform to run on.
    errNum = clGetPlatformIDs(0, NULL, &numPlatforms);
    if (checkErr((errNum != CL_SUCCESS) ? errNum : (numPlatforms <= 0 ? -1 : CL_SUCCESS),
                 "clGetPlatformIDs"))
        return;
    
    platformIDs = (cl_platform_id *)alloca(sizeof(cl_platform_id) * numPlatforms);
    
    errNum = clGetPlatformIDs(numPlatforms, platformIDs, NULL);
    if (checkErr((errNum != CL_SUCCESS) ? errNum : (numPlatforms <= 0 ? -1 : CL_SUCCESS),
                 "clGetPlatformIDs"))
        return;
    
    // Iterate through the list of platforms until we find one that supports
    // a CPU device, otherwise fail with an error.
    for (cl_int i = 0; i < numPlatforms; i++) {
        void **deviceIDs = NULL;
        uint numDevices  = 0;
        errNum = clGetDeviceIDs(platformIDs[i],
                                devType,
                                0,
                                NULL,
                                &numDevices);
        if (errNum != CL_SUCCESS && errNum != CL_DEVICE_NOT_FOUND)
        {
            if (checkErr(errNum, "clGetDeviceIDs"))
                return;
        }
        else if (numDevices > 0)
        {
            deviceIDs   = (void**)malloc(sizeof(cl_device_id) * numDevices);
            errNum = clGetDeviceIDs(platformIDs[i],
                                    devType,
                                    numDevices,
                                    (cl_device_id *)deviceIDs,
                                    NULL);
            if (checkErr(errNum, "clGetDeviceIDs")) {
                free(deviceIDs);
                return;
            }
        }
        for (int j = 0; j < numDevices; j++) {
            string deviceName = getDeviceNameForDeviceID((cl_device_id)deviceIDs[j]);
            string deviceVendor = getDeviceVendorForDeviceID((cl_device_id)deviceIDs[j]);
            printf("%s: %s \tPlatform:%s\n", deviceVendor.c_str(), deviceName.c_str(), ClPlatform::platformName(platformIDs[i]).c_str());
        }
        free(deviceIDs);
    }
}

void ClContext::logPlatformInfo (cl_platform_id id, cl_platform_info name, string & str)
{
    cl_int errNum;
    size_t paramValueSize;
    
    errNum = clGetPlatformInfo(id, name, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        snprintf(buf, sizeof(buf), "Failed to find OpenCL platform %s.", str.c_str());
        systemLog(buf);
        return;
    }
    
    char *_info = (char *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetPlatformInfo(id, name, paramValueSize, _info, NULL);
    if (errNum != CL_SUCCESS) {
        snprintf(buf, sizeof(buf), "Failed to find OpenCL platform %s.", str.c_str());
        systemLog(buf);
        return;
    }
    snprintf(buf, sizeof(buf), "\t%s:\t%s", str.c_str(), _info);
    systemLog(buf);
}

string ClContext::getDeviceNameForDeviceID(cl_device_id _id)
{
    cl_int errNum;
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(_id, CL_DEVICE_NAME, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return nullptr;
    }
    
    char *_kind = (char *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetDeviceInfo(_id, CL_DEVICE_NAME, paramValueSize, _kind, NULL);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return nullptr;
    }
    return string(_kind);
}

void ClContext::logDeviceName(int deviceNum, cl_device_id _id)
{
    cl_int errNum;
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(_id, CL_DEVICE_NAME, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return;
    }
    
    char *_kind = (char *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetDeviceInfo(_id, CL_DEVICE_NAME, paramValueSize, _kind, NULL);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return;
    }
    string kind(_kind);
    deviceNames[deviceNum] = kind;
}

string ClContext::getDeviceVendorForDeviceID(cl_device_id _id)
{
    cl_int errNum;
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(_id, CL_DEVICE_VENDOR, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return nullptr;
    }
    
    char *_vendor = (char *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetDeviceInfo(_id, CL_DEVICE_VENDOR, paramValueSize, _vendor, NULL);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return nullptr;
    }
    return _vendor;
}

void ClContext::logDeviceVendor(int deviceNum, cl_device_id _id)
{
    string vendor = getDeviceVendorForDeviceID(_id);
    deviceVendor[deviceNum] = vendor;
}

void ClContext::parseOpenClVersion()
{
    // Platform Info string returned is:
    // OpenCL<space><major_version.minor_version><space><platform-specific information>
    // For example:   OpenCL 1.1 fooness
    // For example:   OpenCL 1.2 (Apr 26 2016 00:05:53)
    if (platformVersion.substr(0, 6) != "OpenCL")
        return;
    
    size_t periodPos = platformVersion.find('.', 7);
    size_t spacePos  = platformVersion.find(' ', 7);
    string _major    = platformVersion.substr(7, periodPos - 7);
    string _minor    = platformVersion.substr(periodPos + 1, spacePos - periodPos - 1);
    int major        = std::stoi(_major);
    int minor        = std::stoi(_minor);
    
    canPartitionCPU = (major > 1) || (minor >= 2);
}

//     DeviceContext(bool forceRebuild, SharedVariationSet defaultVariationSet, bool _singleDeviceAllowed, bool _canUseGPU, bool ignoreQuarantine);

SharedClContext ClContext::makeClContext(cl_device_type filter,
                                         bool forceRebuild,
                                         SharedVariationSet & _defaultVariationSet,
                                         bool _singleDeviceAllowed,
                                         bool _canUseGPU,
                                         bool _ignoreQuarantine)
{
    SharedClContext clContext = std::shared_ptr<ClContext>(new ClContext(filter,
                                                                         forceRebuild,
                                                                         _defaultVariationSet,
                                                                         _singleDeviceAllowed,
                                                                         _canUseGPU,
                                                                         _ignoreQuarantine));
    clContext->phase2Constructor();
    return clContext;
}

ClContext::ClContext(cl_device_type filter,
                     bool forceRebuild,
                     SharedVariationSet & _defaultVariationSet,
                     bool _singleDeviceAllowed,
                     bool _canUseGPU,
                     bool _ignoreQuarantine)
: DeviceContext(forceRebuild, _defaultVariationSet, _singleDeviceAllowed, _canUseGPU, _ignoreQuarantine), platformIDs(nullptr), platforms()
{
    cl_int errNum         = CL_SUCCESS;
    cl_uint platformCount = 0;
    queues                = nullptr;
    subdeviceQueues       = nullptr;
    contexts              = nullptr;
    
    // First, select an OpenCL platform to run on.
    errNum = clGetPlatformIDs(0, NULL, &platformCount);
    if (checkErr((errNum != CL_SUCCESS) ? errNum : (platformCount <= 0 ? -1 : CL_SUCCESS),
                 "clGetPlatformIDs"))
        throw std::runtime_error("Could not get OpenCL Platform ID");
    
    platformIDs = (cl_platform_id *)malloc(sizeof(cl_platform_id) * platformCount);
    
    errNum = clGetPlatformIDs(platformCount, platformIDs, NULL);
    if (checkErr((errNum != CL_SUCCESS) ? errNum : (platformCount <= 0 ? -1 : CL_SUCCESS),
                 "clGetPlatformIDs"))
        throw std::runtime_error("Could not get OpenCL Platform ID");
    
    cl_platform_id applePlatformID = nullptr;
    for (cl_int i = 0; i < platformCount; i++) {
        if (ClPlatform::isApplePlatform(platformIDs[i])) {
            applePlatformID = platformIDs[i];
        }
        platforms.emplace_back(std::make_unique<ClPlatform>(platformIDs[i], filter));
    }
	free(platformIDs);
#ifdef __APPLE__
    if (applePlatformID == nullptr) {
        throw std::runtime_error(("ERROR: No Apple OpenCL Platform"));
    }
    else {
        platformVersion = ClPlatform::getPlatformVersion(applePlatformID);
        parseOpenClVersion();
    }
#endif
    
    // Iterate through the list of platforms until we find one that supports
    // a CPU device, otherwise fail with an error.
    numDevices = 0;
    for (cl_int i = 0; i < platformCount; i++) {
        numDevices += platforms[i]->numDevices;
    }
    if (numDevices == 0)
        throw std::runtime_error(("ERROR: No OpenCL device found"));
    
    deviceIDs   = (void**)malloc(sizeof(cl_device_id) * numDevices);
    platformIDs = (cl_platform_id *)malloc(sizeof(cl_platform_id) * numDevices);
    available   = (bool *)malloc(sizeof(cl_bool) * numDevices);
    quarantined = (bool *)malloc(sizeof(bool) * numDevices);
    contexts    = (cl_context *)malloc(sizeof(cl_context) * numDevices);
    
    uint k = 0;
    for (uint i = 0; i < platformCount; i++) {
        for (uint j = 0; j < platforms[i]->numDevices; j++) {
            deviceIDs[k]   = platforms[i]->deviceIDs[j];
            platformIDs[k] = platforms[i]->platformID;
            k++;
        }
    }
    
    for (uint i = 0; i < numDevices; i++) {
        std::stringstream ss;
        ss << "Device " << i;
        deviceNames.push_back(ss.str());

        ss.str("");
        ss << "Vendor " << i;
        deviceVendor.push_back(ss.str());
    }
    
    for (cl_int i = 0; i < numDevices; i++) {
        logDeviceName(i, (cl_device_id)deviceIDs[i]);
        logDeviceVendor(i, (cl_device_id)deviceIDs[i]);
        available[i]   = checkAvailabilityForDeviceID((cl_device_id)deviceIDs[i]);
        quarantined[i] = _ignoreQuarantine ? false : checkQuarantineForDeviceID((cl_device_id)deviceIDs[i]);
        
        cl_int errNum         = CL_SUCCESS;
        cl_device_id deviceID = (cl_device_id)deviceIDs[i];
        
        // Next, create an OpenCL context on the selected platform.
        cl_context_properties contextProperties[] =
        {
            CL_CONTEXT_PLATFORM,
            (cl_context_properties)platformIDs[i],
            0
        };
        
        contexts[i] = clCreateContext(contextProperties,
                                  1,
                                  &deviceID,
                                  &contextCallback,
                                  this,
                                  &errNum);
    }
}

ClContext::~ClContext()
{
    if (platformIDs)
        free(platformIDs);
    if (subDeviceCount > 0 && subdeviceQueues != NULL) {
        for (int i = 0; i < subDeviceCount; i++) {
            if (subdeviceQueues[i])
                clReleaseCommandQueue((cl_command_queue)subdeviceQueues[i]);
        }
    }
    if (subDeviceCount > 0 && subDeviceIDs != NULL) {
        for (int i = 0; i < subDeviceCount; i++) {
            if (subDeviceIDs[i])
                clReleaseDevice((cl_device_id)subDeviceIDs[i]);
        }
    }
    if (queues) {
        for (int i = 0; i < numDevices; i++) {
            if (queues[i])
                clReleaseCommandQueue((cl_command_queue)queues[i]);
        }
    }
    
    if (subdeviceQueues)
        free(subdeviceQueues);
    if (queues)
        free(queues);
}

void ClContext::phase2Constructor()
{
    cl_int errNum;
    if (! createDeviceKinds()) {
        throw std::runtime_error(("ERROR: Creating Device Kinds failed"));
    }
    
    // now buld the program(s)
    bool haveUsableDevice = false;
    for (int i = 0; i < numDevices; i++) {
        if (available[i] && (! quarantined[i])) {
            haveUsableDevice = true;
            break;
        }
    }
    if (! haveUsableDevice)
        throw std::runtime_error(("ERROR: No useable OpenCL device"));
    
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage & deviceUsage = deviceUsages[i];
        deviceUsage->available          = available[i];
        deviceUsage->quarantined        = quarantined[i];
        deviceUsage->stagedUsed         = available[i] && ! quarantined[i];
        deviceUsage->used               = available[i] && ! quarantined[i];
    }
    //    [self selectDevicesFromPrefs]; // if no prefs, select all
    rearrangeSelectedDevices();
    
    // enforce initial selection when single device limit applies
    if (singleDeviceAllowed) {
        cl_device_id selectedDeviceID = NULL;
        for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDeviceCount(); selectedDeviceNum++)
        {
            SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
            if (deviceUsage->stagedUsed) {
                selectedDeviceID = (cl_device_id)deviceUsage->deviceID;
                break;
            }
            
        }
        for (int i = 0; i < numDevices; i++) {
            SharedDeviceUsage & deviceUsage = selectedDevices[i];
            if (deviceUsage->deviceID != selectedDeviceID)
                deviceUsage->stagedUsed = false;
        }
    }
    rearrangeSelectedDevices();
    
    queues = (cl_command_queue *)malloc(sizeof(cl_command_queue) * numDevices);
    for (int i = 0; i < numDevices; i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        SharedDeviceKind deviceKind    = deviceUsage->deviceKind;
        if (available[i]) {
            ClDeviceKind * clDeviceKind = (ClDeviceKind *)deviceKind.get();
            queues[i] = clCreateCommandQueue(clDeviceKind->context, (cl_device_id)deviceIDs[i], 0, &errNum);
            checkErr(errNum, "clCreateCommandQueue");
            
            SharedDeviceUsage & deviceUsage = selectedDevices[i];
            deviceUsage->queue              = (cl_command_queue)queues[i];
        }
        else
            queues[i] = NULL;
    }
}

bool ClContext::checkAvailabilityForDeviceID(cl_device_id _id)
{
    cl_int errNum;
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(_id, CL_DEVICE_AVAILABLE, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return false;
    }
    
    cl_bool *_available = (cl_bool *)alloca(paramValueSize);
    
    errNum = clGetDeviceInfo(_id, CL_DEVICE_AVAILABLE, paramValueSize, _available, NULL);
    if (errNum != CL_SUCCESS) {
        systemLog("Failed to find OpenCL Device info");
        return false;
    }
    return *_available;
}

bool ClContext::checkAvailabilityOfOneDeviceID(cl_device_id * ids, unsigned count)
{
    for (unsigned i = 0; i < count; i++) {
        if (checkAvailabilityForDeviceID(ids[i]) == CL_TRUE) {
            return true;
        }
    }
    return false;
}

bool ClContext::checkNonQuarantineOfOneDeviceID(cl_device_id * ids, unsigned count)
{
    for (unsigned i = 0; i < count; i++) {
        if (checkQuarantineForDeviceID(ids[i]) == false) {
            return true;
        }
    }
    return false;
}

/// Try to find in the Haystack the Needle - ignore case
bool findStringIC(const std::string & strHaystack, const std::string & strNeedle)
{
    auto it = std::search(
                          strHaystack.begin(), strHaystack.end(),
                          strNeedle.begin(),   strNeedle.end(),
                          [](char ch1, char ch2) { return std::toupper(ch1) == std::toupper(ch2); }
                          );
    return (it != strHaystack.end() );
}

// this is a simple test for OpenCL devices that have defective implementations
// The GeForce GT 650M OpenCL device driver locks up the Mac - its defective
bool ClContext::checkQuarantineForDeviceID(cl_device_id _id)
{
    static string foo1= "HD Graphics 4000";  // causes OpenCL crash due to excessive compilation time
    static string foo2= "HD Graphics 5000";  // causes OpenCL crash due to excessive compilation time
    static string foo3= "HD Graphics 5100";  // causes OpenCL crash due to excessive compilation time
    static string foo4= "HD Graphics 5200";  // causes OpenCL crash due to excessive compilation time
    static string foo5= "HD Graphics 5300";  // causes OpenCL crash due to excessive compilation time
    static string foo6= "HD Graphics 6000";  // causes OpenCL crash due to excessive compilation time
    static string foo7= "HD Graphics 6100";  // causes OpenCL crash due to excessive compilation time
    static string foo8= "Iris";              // causes OpenCL crash due to excessive compilation time
    static string nvidia= "Nvidia";            // causes crash because of El Capitan OpenCL runtime bug
    
    //    static string *foo1 = "Generic"; // -- set to this to turn off quarantine
    
    
    string kind   = getDeviceNameForDeviceID(_id);
    string vendor = getDeviceVendorForDeviceID(_id);
    
    if (findStringIC(kind, foo1))
        return true;
    if (findStringIC(kind, foo2))
        return true;
    if (findStringIC(kind, foo3))
        return true;
    if (findStringIC(kind, foo4))
        return true;
    if (findStringIC(kind, foo5))
        return true;
    if (findStringIC(kind, foo6))
        return true;
    if (findStringIC(kind, foo7))
        return true;
    if (findStringIC(kind, foo8))
        return true;
#ifdef __APPLE__
//    // Nvidia quarantined on El Capitan
//    if (floor(NSAppKitVersionNumber) > NSAppKitVersionNumber10_10_Max) {
//        range  = [vendor rangeOfString:nvidia options:NSCaseInsensitiveSearch];
//        if (range.location != NSNotFound) {
//            return true;
//        }
//    }
#endif
    return false;
}

bool ClContext::createDeviceKinds()
{
    for (int i = 0; i < deviceNames.size(); i++) {
        string & kind                 = deviceNames[i];
        string vendor                 = deviceVendor[i];
        SharedDeviceKind deviceKind   = std::make_shared<ClDeviceKind>(kind, vendor, shared_from_this(), contexts[i]);
        deviceKinds[kind]             = deviceKind;
        
        deviceKind->clDeviceInstances.push_back(deviceIDs[i]);
        
        SharedClDeviceKind clDeviceKind = std::dynamic_pointer_cast<ClDeviceKind>(deviceKind);        
        clDeviceKind->platformInstances.push_back(platformIDs[i]);
        deviceKind->deviceKindProperties();
        
        string deviceType = deviceKind->isCPU() ? "CPU:" : "GPU:";
        if (deviceKind->isCPU())
            cpuDeviceIndex = i;
        string deviceStatus   = (! available[i] || quarantined[i]) ? "Quarantined" : "";
        string & name         = deviceNames[i];
        if (name.find(vendor) != string::npos)
            vendor = "";
        
        snprintf(buf, sizeof(buf), "%s%s%s%s %s",
                deviceType.c_str(),
                vendor.c_str(),
                vendor.length() > 0 ? " " : "",
                deviceNames[i].c_str(),
                deviceStatus.c_str());
        string description = buf;
        
        SharedDeviceUsage deviceUsage = std::make_shared<DeviceUsage>((cl_device_id)deviceIDs[i], platformIDs[i], nullptr, name, description, true);

        if (deviceKind->isCPU()) {
            std::vector<uint> computeUnitCounts;
            uint maxComputUnits   = deviceKind->maxComputeUnits;
            computeUnitCountIndex = maxComputUnits - 1;
            for (uint count = 1; count <= maxComputUnits; count++) {
                computeUnitCounts.push_back(count);
            }
        }
        
        deviceUsage->isGPU          = deviceKind->isGPU();
        deviceUsage->deviceKind     = deviceKind;
        deviceUsage->platformID     = platformIDs[i];
        deviceUsage->enforceLicense(canUseGPU);
        
        deviceUsages.push_back(deviceUsage);
    }
    for (int i = 0; i < deviceNames.size(); i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        SharedDeviceKind deviceKind    = deviceUsage->deviceKind;
        SharedClDeviceKind clDeviceKind = std::dynamic_pointer_cast<ClDeviceKind>(deviceKind);
        
        deviceKind->warpSize  = clDeviceKind->determineWarpSizeForDevice((cl_device_id)deviceIDs[i]);
        deviceUsage->warpSize = deviceKind->warpSize;
    }
    refreshMemoryStats();
    return true;
}

void ClContext::refreshProgramForVariationSet(SharedVariationSet &variationSet, bool rebuild)
{
    size_t selectedDeviceCount = selectedDevices.size();
    std::unordered_set<SharedDeviceKind> deviceKindSet;
    
    for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDeviceCount; selectedDeviceNum++) {
        SharedDeviceKind deviceKind = deviceKindForSelectedDevice(selectedDeviceNum);
        deviceKindSet.insert(deviceKind);
    }
    
    duration<double> buildTime(0.);
    for (const SharedDeviceKind & deviceKind : deviceKindSet) {
        deviceKind->makeProgramFromSource(variationSet, rebuild, buildTime);
    }
}

bool ClContext::makeTestProgramForVariationSet(SharedVariationSet &variationSet)
{
    int firstCPUDevice = -1;
    for (int i = 0; i < numDevices; i++) {
        if (available[i]) {
            SharedDeviceUsage & deviceUsage = selectedDevices[i];
            if (! deviceUsage->isGPU) {
                firstCPUDevice = i;
                break;
            }
        }
    }
    
    duration<double> buildTime(0.);
    const SharedDeviceKind & deviceKind = deviceKindForDevice(firstCPUDevice);
    DeviceProgram * program             = deviceKind->makeProgramFromSource(variationSet, true, buildTime);
    return program != nullptr;
}

bool ClContext::selectedDevicesHasGPU()
{
    for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++)
    {
        SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
        if (deviceUsage->isGPU)
            return true;
    }
    return false;
}

cl_device_id ClContext::firstCPUDeviceID()
{
    for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++)
    {
        SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
        if (!deviceUsage->isGPU)
            return (cl_device_id)deviceUsage->deviceID;
    }
    return NULL;
}

void ClContext::logDeviceTypeInfo (int deviceNum, cl_device_id id)
{
    cl_int errNum;
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(id, CL_DEVICE_TYPE, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        snprintf(buf, sizeof(buf), "Failed to find OpenCL Device info  %s.", "CL_DEVICE_TYPE");
        systemLog(buf);
        return;
    }
    
    cl_device_type *deviceType = (cl_device_type *)alloca(sizeof(char) * paramValueSize);
    errNum = clGetDeviceInfo(id, CL_DEVICE_TYPE, paramValueSize, deviceType, NULL);
    if (errNum != CL_SUCCESS) {
        snprintf(buf, sizeof(buf), "Failed to find OpenCL Device info  %s.", "CL_DEVICE_TYPE");
        systemLog(buf);
        return;
    }
    if (*deviceType & CL_DEVICE_TYPE_CPU)
        systemLog("\tCL_DEVICE_TYPE_CPU\t");
    if (*deviceType & CL_DEVICE_TYPE_GPU)
        systemLog("\tCL_DEVICE_TYPE_GPU\t");
    if (*deviceType & CL_DEVICE_TYPE_DEFAULT)
        systemLog("\tCL_DEVICE_TYPE_DEFAULT\t");
    if (*deviceType & CL_DEVICE_TYPE_ACCELERATOR)
        systemLog("\tCL_DEVICE_TYPE_ACCELERATOR\t");
}

bool ClContext::isCpu(cl_device_id id)
{
    cl_int errNum;
    size_t paramValueSize;
    
    errNum = clGetDeviceInfo(id, CL_DEVICE_TYPE, 0, NULL, &paramValueSize);
    if (errNum != CL_SUCCESS) {
        snprintf(buf, sizeof(buf), "Failed to find OpenCL Device info  %s.", "CL_DEVICE_TYPE");
        systemLog(buf);
        return false;
    }
    
    cl_device_type *deviceType = (cl_device_type *)alloca(sizeof(char) * paramValueSize);
    
    errNum = clGetDeviceInfo(id, CL_DEVICE_TYPE, paramValueSize, deviceType, NULL);
    if (errNum != CL_SUCCESS) {
        snprintf(buf, sizeof(buf), "Failed to find OpenCL Device info  %s.", "CL_DEVICE_TYPE");
        systemLog(buf);
        return false;
    }
    if (*deviceType & CL_DEVICE_TYPE_CPU)
        return true;
    return false;
}

bool ClContext::hasDiscreteGPU()
{
    cl_int          errNum          = CL_SUCCESS;
    cl_uint         numPlatforms    = 0;
    cl_platform_id *platformIDs     = NULL;
    cl_uint         numDevices      = 0;
    
    // First, select an OpenCL platform to run on.
    errNum = clGetPlatformIDs(0, NULL, &numPlatforms);
    if (checkErr((errNum != CL_SUCCESS) ? errNum : (numPlatforms <= 0 ? -1 : CL_SUCCESS),
                 "clGetPlatformIDs"))
        return false;
    
    platformIDs = (cl_platform_id *)alloca(sizeof(cl_platform_id) * numPlatforms);
    
    errNum = clGetPlatformIDs(numPlatforms, platformIDs, NULL);
    if (checkErr((errNum != CL_SUCCESS) ? errNum : (numPlatforms <= 0 ? -1 : CL_SUCCESS),
                 "clGetPlatformIDs"))
        return false;
    
    // Iterate through the list of platforms until we find one that supports
    // a CPU device, otherwise fail with an error.
    for (cl_int i = 0; i < numPlatforms; i++) {
        errNum = clGetDeviceIDs(platformIDs[i],
                                CL_DEVICE_TYPE_GPU,
                                0,
                                NULL,
                                &numDevices);
        if (errNum != CL_SUCCESS && errNum != CL_DEVICE_NOT_FOUND)
        {
            if (checkErr(errNum, "clGetDeviceIDs"))
                return false;
        }
        if (errNum == CL_DEVICE_NOT_FOUND)
            return false;
        if (numDevices > 0) {
            cl_device_id *deviceIDs = (cl_device_id *)alloca(sizeof(cl_device_id) * numDevices);
            errNum = clGetDeviceIDs(platformIDs[i],
                                    CL_DEVICE_TYPE_GPU,
                                    numDevices,
                                    deviceIDs,
                                    NULL);
            if (checkErr(errNum, "clGetDeviceIDs"))
                return false; // cant get GPU deviceiD
            
            if (! checkAvailabilityOfOneDeviceID(deviceIDs, numDevices))
                return false;
            
            gpuVendor = getDeviceVendorForDeviceID(deviceIDs[0]);
            return true;
        }
    }
    return false;
}

bool ClContext::hasGPU(StringsVector &vendors, bool checkLocalMemoryOnDevice, BoolsVector & localMemoryArray, bool ignoreQuarantine)
{
    cl_int          errNum          = CL_SUCCESS;
    cl_uint         numPlatforms    = 0;
    cl_platform_id *platformIDs     = NULL;
    cl_uint         numDevices      = 0;
    
    // First, select an OpenCL platform to run on.
    errNum = clGetPlatformIDs(0, NULL, &numPlatforms);
    if (checkErr((errNum != CL_SUCCESS) ? errNum : (numPlatforms <= 0 ? -1 : CL_SUCCESS),
                 "clGetPlatformIDs"))
        return false;
    
    platformIDs = (cl_platform_id *)alloca(sizeof(cl_platform_id) * numPlatforms);
    
    errNum = clGetPlatformIDs(numPlatforms, platformIDs, NULL);
    if (checkErr((errNum != CL_SUCCESS) ? errNum : (numPlatforms <= 0 ? -1 : CL_SUCCESS),
                 "clGetPlatformIDs"))
        return false;
    
    // Iterate through the list of platforms until we find one that supports
    // a CPU device, otherwise fail with an error.
    for (cl_int i = 0; i < numPlatforms; i++) {
        errNum = clGetDeviceIDs(platformIDs[i],
                                CL_DEVICE_TYPE_GPU,
                                0,
                                NULL,
                                &numDevices);
        if (errNum != CL_SUCCESS && errNum != CL_DEVICE_NOT_FOUND)
        {
            if (checkErr(errNum, "clGetDeviceIDs"))
                return false;
        }
        if (errNum == CL_DEVICE_NOT_FOUND)
            return false;
        if (numDevices > 0) {
            cl_device_id *deviceIDs = (cl_device_id *)alloca(sizeof(cl_device_id) * numDevices);
            errNum = clGetDeviceIDs(platformIDs[i],
                                    CL_DEVICE_TYPE_GPU,
                                    numDevices,
                                    deviceIDs,
                                    NULL);
            if (checkErr(errNum, "clGetDeviceIDs"))
                return false; // cant get GPU deviceiD
            
            if (! checkAvailabilityOfOneDeviceID(deviceIDs, numDevices))
                return false;
            
            if (!ignoreQuarantine && ! checkNonQuarantineOfOneDeviceID(deviceIDs, numDevices))
                return false;
            
            if (vendors.size() > 0) {
                for (int i = 0; i < numDevices; i++) {
                    string vendor = getDeviceVendorForDeviceID(deviceIDs[0]);
                    vendors.push_back(vendor);
                }
            }
            
            // 4XXX generation ATI GPUs dont have local memory and cant be used for rendering
            if (checkLocalMemoryOnDevice) {
                for (int i = 0; i < numDevices; i++) {
                    cl_device_local_mem_type localMemType;
                    errNum = clGetDeviceInfo(deviceIDs[0], CL_DEVICE_LOCAL_MEM_TYPE,
                                             sizeof(cl_device_local_mem_type), &localMemType, NULL);
                    
                    // cant get local mem type info - return false
                    if (checkErr(errNum, "clGetDeviceInfo"))
                        localMemoryArray.push_back(false);
                    else
                        localMemoryArray.push_back(localMemType == CL_LOCAL);
                }
            }
            return true;
        }
    }
    
    return false;
}

std::string ClContext::selectedDevicesDescription()
{
    string s;
    snprintf(buf, sizeof(buf), "%zu OpenCL Devices:\n", selectedDevices.size());
    s.append(buf);
    for (SharedDeviceUsage &deviceUsage : selectedDevices) {
        s.append("\t").append(deviceUsage->description());
    }
    return s;
}

size_t ClContext::deviceNumForDeviceID(void * deviceID)
{
    for (uint i = 0; i < numDevices; i++) {
        if (deviceIDs[i] == deviceID)
            return i;
    }
    return string::npos;
}

size_t ClContext::deviceUsageIndexForDeviceID(void * deviceID)
{
    for (size_t i = 0; i < numDevices; i++) {
        SharedDeviceUsage & deviceUsage = deviceUsages[i];
        if (deviceUsage->deviceID == deviceID)
            return i;
    }
    return string::npos;
}

cl_command_queue ClContext::queueForSelectedDevice(uint selectedDeviceNum)
{
    SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
    return (cl_command_queue)deviceUsage->queue;
}

void * ClContext::deviceIDForDevice(uint i)
{
    if (i < numDevices)
        return deviceIDs[i];
    return NULL;
}

// OpenCL memory available checking ================
// return the largest tile size for this GPU
static bool oclDeviceAllocCheck(ClContext *clContext,
                                int deviceNum,
                                float  &area,
                                size_t & amountAlloced,
                                int warpSize,
                                uint xformCount,
                                uint supersample)
{
    SharedDeviceKind deviceKind = clContext->deviceKindForDevice(deviceNum);
    // variables used to find the largest area we can support
    int numColors     = 256;
    bool success      = false;
    while (!success) {
        if (!(success =
              Flam4ClRuntime::oclMemCheck(clContext,
                                         deviceNum,
                                         area,
                                         numColors,
                                         amountAlloced,
                                         8,
                                         warpSize,
                                         deviceKind->deviceImageSupport,
                                         xformCount,
                                         supersample))) {
                  area = floorf(area * 0.8f);
                  if (area <= 1.0f) // the smallest possible tile size!
                      return false;
              }
    }
    return success;
}

static bool checkOclGPUMemAvailableForCPU(ClContext *clContext,
                                          int deviceNum,
                                          size_t &memoryAvailable,
                                          float & area,
                                          float niceFactor,
                                          int warpSize,
                                          uint xformCount,
                                          uint supersample)
{
    size_t amountAlloced  = UINT_MAX;
    float    areaGlobal = (float)UINT_MAX;
    bool     success    = true;
    
    cl_ulong freeMemory = CpuMemStats::freeMemory();
    const uint minimumFree = 128 * 1048576;
    if (freeMemory < minimumFree)
        freeMemory = minimumFree; // allow VM paging for at least 128 Mb
    
    float deviceMaxArea = ceilf((freeMemory - 1048576)/36.f);
    
    // start with available memory that can be allocated, then reduce it to 1.2X the area if it greater than that
    areaGlobal = deviceMaxArea > area ? area : deviceMaxArea;
    
    success = oclDeviceAllocCheck(clContext, deviceNum, areaGlobal, amountAlloced, warpSize, xformCount, supersample);
    areaGlobal = floorf(niceFactor * areaGlobal);
    
    memoryAvailable = amountAlloced;
    area            = areaGlobal;
    return success;
}

bool ClContext::checkOclGPUMemAvailable(size_t & memoryAvailable,
                                        float & area,
                                        float niceFactor,
                                        uint warpSize,
                                        uint xformCount,
                                        uint supersample,
                                        uint deviceNum)
{
    if (warpSize == 1) // for CPU with virtual memory
        return checkOclGPUMemAvailableForCPU(this, deviceNum, memoryAvailable,
                                             area, niceFactor, warpSize, xformCount, supersample);
    
    unsigned deviceCount = this->numDevices;
    if (deviceCount == 0)
        return false;
    
    size_t amountAlloced = UINT_MAX;
    float    areaGlobal         = (float)UINT_MAX;
    bool     success            = true;
    
    cl_ulong maxMemAllocSize = this->maxMemAllocSizeForDevice(deviceNum);
    //    float deviceMaxArea      = ceilf((maxMemAllocSize - 1048576)/36.f);
    float deviceMaxArea      = floorf(maxMemAllocSize/16.f); // biggest chunk is 16 * area
    
    // start with available memory that can be allocated, then reduce it to 1.2X the area if it greater than that
    areaGlobal = deviceMaxArea > area ? area : deviceMaxArea;
    
    success = oclDeviceAllocCheck(this, deviceNum, areaGlobal, amountAlloced, warpSize, xformCount, supersample);
    areaGlobal = floorf(niceFactor * areaGlobal);
    
    memoryAvailable = amountAlloced;
    area            = areaGlobal;
    return success;
}

bool ClContext::oclDeviceAllocCheckForDeviceNum(size_t deviceNum,
                                                float & area,
                                                size_t & amountAlloced,
                                                uint warpSize,
                                                uint xformCount,
                                                uint supersample)
{
    const SharedDeviceKind & deviceKind = deviceKindForDevice(deviceNum);
    // variables used to find the largest area we can support
    int numColors     = 256;
    bool success      = false;
    while (!success) {
        if (!(success =
              Flam4ClRuntime::oclMemCheck(this,
                                          (uint)deviceNum,
                                          area,
                                          numColors,
                                          amountAlloced,
                                          8,
                                          warpSize,
                                          deviceKind->deviceImageSupport,
                                          xformCount,
                                          supersample))) {
                  area = floorf(area * 0.8f);
                  if (area <= 1.0f) // the smallest possible tile size!
                      return false;
              }
    }
    return success;
}

// 35 Mb minimum => 548,485 pixels or 800X685 pixels
bool ClContext::oclDeviceCheckForEnoughMemoryArea(size_t area, uint xformCount, uint supersample)
{
    int numColors = 256;
    size_t amountAlloced;
    for (uint selectedDeviceNum = 0; selectedDeviceNum < selectedDevices.size(); selectedDeviceNum++) {
        SharedDeviceUsage & deviceUsage = selectedDevices[selectedDeviceNum];
        SharedDeviceKind deviceKind     = deviceKindForSelectedDevice(selectedDeviceNum);
        uint warpSize                   = deviceUsage->warpSize;
        uint deviceNum                  = (uint)indexOfDeviceUsage(deviceUsage);

        bool success = Flam4ClRuntime::oclMemCheck(this,
                                                   (uint)deviceNum,
                                                   area,
                                                   numColors,
                                                   amountAlloced,
                                                   8,
                                                   warpSize,
                                                   deviceKind->deviceImageSupport,
                                                   xformCount,
                                                   supersample);
        if (! success)
            return false;
    }
    return true;
}

void ClContext::refreshMemoryStats()
{
    std::unique_lock<std::mutex> lock(mutex);
    
    for (int i = 0; i < deviceNames.size(); i++) {
        SharedDeviceUsage &deviceUsage = deviceUsages[i];
        SharedDeviceKind deviceKind    = deviceUsage->deviceKind;
        
        size_t memoryAvailable;
        float area = deviceKind->maxMemAllocSize/16.f;
        checkOclGPUMemAvailable(memoryAvailable,
                                area,
                                1.f,
                                deviceKind->warpSize,
                                MAX_XFORMS,
                                1,
                                i);
        size_t maxMemory             = globalMemSizeForDevice(i);
        deviceUsage->maxMemory       = maxMemory;
        deviceUsage->availableMemory = memoryAvailable;
        deviceUsage->empty           = memoryAvailable < 36700160.f; // 35 Mb floor
    }
    rearrangeSelectedDevices();
}

#ifdef __ORIGINAL__

- (NSDictionary *)plistForDevice:(cl_device_id)deviceID
{
    int index = -1;
    
    for (int i = 0; i < numDevices; i++) {
        if (deviceIDs[i] == deviceID) {
            index = i;
            break;
        }
    }
    if (index == -1)
        return nullptr;
    ClDeviceKind *deviceKind = [deviceKinds objectForKey:[deviceNames objectAtIndex:index]];
    return [deviceKind existingPlist];
}

#endif
