//
//  ImageRepTileArray.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/16/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef ImageRepTileArray_hpp
#define ImageRepTileArray_hpp

#include <memory>
#include <vector>
#include <chrono>
#include "Exports.hpp"
#include "Image.hpp"

using namespace std::chrono;

using uchar        = unsigned char;
using uint         = unsigned int;
using SharedImage  = std::shared_ptr<FA::Image>;
using ImagesVector = std::vector<SharedImage>;
using TimesVector  = std::vector<duration<double>>;
using UintsVector  = std::vector<uint>;

class FAENGINE_API ImageRepTileArray : public std::enable_shared_from_this<ImageRepTileArray> {
public:
    ImageRepTileArray(size_t _rows, size_t _cols, bool _flipped);
    
    size_t size()                              { return rows *cols; }    
    SharedImage & tileAtIndex(size_t index)    { return reps[index]; }
    duration<double> timeAtIndex(size_t index) { return renderTimes[index]; }
    uint batchesRenderedAtIndex(size_t index)  { return batchesRendered[index]; }
    
    float overlapTop(size_t row)      { return row > 0             ? overlap : 0.f; }
    float overlapBottom(size_t row)   { return (row < rows - 1)    ? overlap : 0.f; }
    float overlapLeft(size_t column)  { return column > 0          ? overlap : 0.f; }
    float overlapRight(size_t column) { return (column < cols - 1) ? overlap : 0.f; }
    
    void setTileAt(size_t row, size_t col, SharedImage & object, duration<double> time, uint numBatches);

	SharedImage & tileAt(size_t row, size_t col)   { return reps[row * cols + col]; }
    
	SharedImage combinedImageRep(const UniqueBlob & profileData);

private:
    void blendVerticalOverlapRowTop(uint i, uint j, uchar *topTarget);
    void blendVerticalOverlapRowBottom(uint i, uint j, uchar *bottomTarget);
    void blendHorizontalOverlapRow(uint i, uint j, uint k, uchar *rightPixels, uchar *rightTarget);
    
    size_t       rows;
    size_t       cols;
    float        overlap;
    bool         flipped;  // are we rendering for flipped y coords
    ImagesVector reps;
    TimesVector  renderTimes;
    UintsVector  batchesRendered;
};

#endif /* ImageRepTileArray_hpp */
