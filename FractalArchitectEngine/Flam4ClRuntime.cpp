//
//  Flam4ClRuntime.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/21/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifdef __STRICT_ANSI__
#undef __STRICT_ANSI__
#endif

#include "Flam4ClRuntime.hpp"
#include "ClContext.hpp"
#include "Utilities.hpp"
#include "Flame.hpp"
#include "Variation.hpp"
#include "VariationParameter.hpp"
#include "VariationChain.hpp"
#include "VariationGroup.hpp"
#include "VarParInstance.hpp"
#include "VariationSet.hpp"
#include "ClDeviceKind.hpp"
#include "noise.h"
#include "CameraViewProps.hpp"
#include "CameraViewport.hpp"
#include "ClProgram.hpp"
#include "ElectricSheepStuff.hpp"
#include "RenderState.hpp"
#include "RenderInfo.hpp"
#include "NaturalCubicCurve.hpp"
#include "FlameExpanded.hpp"
#include "SpatialFilter.hpp"
#include "Histogram.hpp"

#ifdef __APPLE__
#include <mach/mach_time.h>
#endif

#ifdef _WIN32
#include <Windows.h>
#define ALIGNED_MALLOC(TYPE, target, alignment, size)   target = (TYPE *)_aligned_malloc(size, alignment)
#else
#define ALIGNED_MALLOC(TYPE, target, alignment, size)   posix_memalign((void **)&target, alignment, size)
#endif


#include <iostream>
#include <stdlib.h>
#include <mutex>
#include <assert.h>
#include <string.h>

using std::string;

// these are kernel structures we have to make and pass to the renderer

struct alignas(16) VariationListNode // each xform has a variable length list of active variations and each variation has its own specific variable sized varpar struct
{                        // all of the lists are concatenated into a single buffer - a separate xformUsageIndex has the offset to the xform's first variation in this list
    uint variationID;    // the numeric value identifying the variation from the variation set - NOTE id of zero is used to signify end of list
    uint varparOffset;   // the offset in varpar union list for this variation's specific varpar struct
    uint enterGroup;     // the state transition that handles Pre, Normal, and Post variation groups
    // entering group, last in group, inside group
};

#define MULTI_DEVICE_BATCHES_MIN 10

// ============ Statics =====================
static std::once_flag initialized_flag;

// only need to initialize random starting values once and resuse them over and over
static FAPoint *randomPointPool;
static int     *randomSeeds;
static uint    *initialPointIterations;
static uint    *initialPointIterationsRecharge;
static int     *initialK;
static uint    *pointShuffledBlockWarp16;
static uint    *pointShuffledBlockWarp32;
static uint    *pointShuffledBlockWarp64;
static uint    *pointShuffledBlockWarp128;

static void shuffleByBlock(uint *array, uint numPoints);

void initializeStatics()
{
    if (randomPointPool)                ALIGNED_FREE(randomPointPool);
    if (randomSeeds)                    ALIGNED_FREE(randomSeeds);
    if (initialPointIterations)         ALIGNED_FREE(initialPointIterations);
    if (initialPointIterationsRecharge) ALIGNED_FREE(initialPointIterationsRecharge);
    if (initialK)                       ALIGNED_FREE(initialK);
    
    const size_t singlePointPoolSize = NUM_POINTS_WARPSIZE_32 * BLOCKSX_WARPSIZE_32 * 32;
    const size_t pointPoolSize = MAX_XFORMS * singlePointPoolSize;
	ALIGNED_MALLOC(FAPoint, randomPointPool, 4096, pointPoolSize * sizeof(FAPoint));
    
#ifdef __APPLE__
    srand((unsigned)(0x7FFFFFFF & mach_absolute_time()));
#elif defined(_WIN32)
	DWORD time1 = GetTickCount();
	srand((unsigned)(0x7FFFFFFF & time1));
#else
    timespec time1;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
    srand((unsigned)(0x7FFFFFFF & time1.tv_nsec));
#endif
    for (int n = 0; n < singlePointPoolSize; n++)
    {
        randomPointPool[n].x = 2.f*(float)rand()/(float)RAND_MAX - 1.f;
        randomPointPool[n].y = 2.f*(float)rand()/(float)RAND_MAX - 1.f;
        randomPointPool[n].z = 2.f*(float)rand()/(float)RAND_MAX - 1.f;
        randomPointPool[n].pal = 2.f*(float)rand()/(float)RAND_MAX - 1.f;
    }
    for (int n = 1; n < MAX_XFORMS; n++) {
        for (int i = 0; i < singlePointPoolSize; i++) {
            randomPointPool[i + singlePointPoolSize * n].x   = randomPointPool[i].x;
            randomPointPool[i + singlePointPoolSize * n].y   = randomPointPool[i].y;
            randomPointPool[i + singlePointPoolSize * n].z   = randomPointPool[i].z;
            randomPointPool[i + singlePointPoolSize * n].pal = randomPointPool[i].pal;
        }
    }
    
    // NOTE: BLOCKDIM * BLOCKSX is independent of warpsize
    // NOTE: NUMPOINTS * BLOCKSX is independent of warpsize
    // NOTE: BLOCKSY is independent of warpsize
    const size_t seedPoolSize = BLOCKSX_WARPSIZE_32 * 32 * 32 * warpsPerBlock;
	ALIGNED_MALLOC(int, randomSeeds, 4096, sizeof(int)*seedPoolSize);
    for (int n = 0; n < seedPoolSize; n++)
        randomSeeds[n] = rand();
    
    const size_t kPoolSize = BLOCKSX_WARPSIZE_32 * 32;
	ALIGNED_MALLOC(int, initialK, 4096, sizeof(int)*kPoolSize);

    for (int n = 0; n < kPoolSize; n++)
        initialK[n] = 0;

	ALIGNED_MALLOC(uint, initialPointIterations, 4096, sizeof(uint)*pointPoolSize);
    for (int n = 0; n < pointPoolSize; n++)
        initialPointIterations[n] = 0;
    
	ALIGNED_MALLOC(uint, initialPointIterationsRecharge, 4096, sizeof(uint)*pointPoolSize);
    for (int n = 0; n < pointPoolSize; n++)
        initialPointIterationsRecharge[n] = 15;
    
	ALIGNED_MALLOC(uint, pointShuffledBlockWarp16, 4096, sizeof(uint)*numPointsWarp16*NUM_ITERATIONS);
	ALIGNED_MALLOC(uint, pointShuffledBlockWarp32, 4096, sizeof(uint)*numPointsWarp32*NUM_ITERATIONS);
	ALIGNED_MALLOC(uint, pointShuffledBlockWarp64, 4096, sizeof(uint)*numPointsWarp64*NUM_ITERATIONS);
    ALIGNED_MALLOC(uint, pointShuffledBlockWarp128, 4096, sizeof(uint)*numPointsWarp128*NUM_ITERATIONS);

#ifdef __APPLE__
    srand((unsigned)(0x7FFFFFFF & mach_absolute_time()));
#elif defined(_WIN32)
	time1 = GetTickCount();
	srand((unsigned)(0x7FFFFFFF & time1));
#else
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
    srand((unsigned)(0x7FFFFFFF & time1.tv_nsec));
#endif
    for (uint n = 0; n < NUM_ITERATIONS; n++) {
        shuffleByBlock(&pointShuffledBlockWarp16[n * numPointsWarp16], numPointsWarp16);
        shuffleByBlock(&pointShuffledBlockWarp32[n * numPointsWarp32], numPointsWarp32);
        shuffleByBlock(&pointShuffledBlockWarp64[n * numPointsWarp64], numPointsWarp64);
        shuffleByBlock(&pointShuffledBlockWarp128[n * numPointsWarp128], numPointsWarp128);
    }
}


Flam4ClRuntime::Flam4ClRuntime(SharedClContext _clContext, uint _numBatches)
    : DeviceRuntime(_numBatches,
                    _numBatches < MULTI_DEVICE_BATCHES_MIN ? 1 : (uint)_clContext->selectedDeviceCount()),
    clContext(_clContext),
    d_g_Palette(nullptr),
    d_g_PaletteTexture(nullptr),
    d_g_Flame(nullptr),
    d_g_Camera(nullptr),
    d_g_Xforms(nullptr),
    d_g_varUsages(nullptr),
    varpars(nullptr),
    d_g_varUsageIndexes(nullptr),
    d_g_accumBuffer(nullptr),
    d_g_resultStaging(nullptr),
    d_g_startingXform(nullptr),
    d_g_markCounts(nullptr),
    d_g_pixelCounts(nullptr),
    d_g_reduction(nullptr),
    d_g_pointList(nullptr),
    d_g_pointIterations(nullptr),
    d_g_randSeeds(nullptr),
    d_g_randK(nullptr),
    d_g_renderTarget(nullptr),
    d_g_switchMatrix(nullptr),
    pin_mergeStaging(nullptr),
    mergeStagingBuffer(nullptr),
    isMergePinned(true),
    permutations(nullptr),
    gradients(nullptr),
    shuffle(nullptr),
    iterationCount(nullptr),
    buf(nullptr)
{
    std::call_once(initialized_flag, initializeStatics); // guaranteed to be called just once
    
    _fuseIterations = new uint[numDevices];
	buf             = new char *[numDevices];
    
    // NOTE: BLOCKDIM * BLOCKSX is independent of warpsize
    // NOTE: NUMPOINTS * BLOCKSX is independent of warpsize
    // NOTE: BLOCKSY is independent of warpsize
    for (uint selectedDeviceNum = 0; selectedDeviceNum < numDevices; selectedDeviceNum++) {
        uint warpSize = clContext->warpSizeForSelectedDevice(selectedDeviceNum);
        
        if (warpSize == 1 ) { // CPU - use pseudo Warpsize = 32
            BLOCKDIM[selectedDeviceNum]  = 32*warpsPerBlock;
            BLOCKSX[selectedDeviceNum]   = BLOCKSX_WARPSIZE_32;
            NUMPOINTS[selectedDeviceNum] = NUM_POINTS_WARPSIZE_32;
        }
        else {
            BLOCKDIM[selectedDeviceNum]  = warpSize*warpsPerBlock;
            BLOCKSX[selectedDeviceNum]   = BLOCKSX_WARPSIZE_32*32/warpSize;
            NUMPOINTS[selectedDeviceNum] = NUM_POINTS_WARPSIZE_32/32*warpSize;
        }
        BLOCKSY[selectedDeviceNum] = 32;
        _fuseIterations[selectedDeviceNum] = clContext->fuseIterationsForSelectedDevice(selectedDeviceNum);
    }
    initGlobalBuffers();
}

Flam4ClRuntime::~Flam4ClRuntime()
{
    delete _fuseIterations;
    
    if (d_g_Palette)			delete [] d_g_Palette;
    if (d_g_PaletteTexture)     delete [] d_g_PaletteTexture;
    if (d_g_varUsages)			delete [] d_g_varUsages;
    if (d_g_varUsageIndexes)	delete [] d_g_varUsageIndexes;
    if (varpars)                delete [] varpars;
    
    if (d_g_Flame)              delete [] d_g_Flame;
    if (d_g_Camera)             delete [] d_g_Camera;
    if (d_g_Xforms)             delete [] d_g_Xforms;
    if (d_g_accumBuffer)        delete [] d_g_accumBuffer;
    if (d_g_resultStaging)      delete [] d_g_resultStaging;
    if (d_g_startingXform)      delete [] d_g_startingXform;
    if (d_g_markCounts)         delete [] d_g_markCounts;
    if (d_g_pixelCounts)        delete [] d_g_pixelCounts;
    if (d_g_reduction)          delete [] d_g_reduction;
    if (d_g_pointList)          delete [] d_g_pointList;
    if (d_g_pointIterations)    delete [] d_g_pointIterations;
    if (d_g_randSeeds)          delete [] d_g_randSeeds;
    if (d_g_renderTarget)       delete [] d_g_renderTarget;
    if (d_g_randK)              delete [] d_g_randK;
    if (d_g_switchMatrix)       delete [] d_g_switchMatrix;
    if (permutations)           delete [] permutations;
    if (gradients)              delete [] gradients;
    if (shuffle)                delete [] shuffle;
    if (iterationCount)         delete [] iterationCount;
    
    for (int n = 0; n < numDevices; n++)
        delete buf[n];
    
    if (buf)                    delete [] buf;
}

// initialize global host buffers
void Flam4ClRuntime::initGlobalBuffers()
{
    if (!d_g_Palette)			d_g_Palette         = new cl_mem[numDevices];
    if (!d_g_PaletteTexture)	d_g_PaletteTexture  = new cl_mem[numDevices];
    if (!d_g_varUsages)         d_g_varUsages       = new cl_mem[numDevices];
    if (!varpars)               varpars             = new cl_mem[numDevices];
    if (!d_g_varUsageIndexes)   d_g_varUsageIndexes = new cl_mem[numDevices];
    if (!d_g_Flame)             d_g_Flame           = new cl_mem[numDevices];
    if (!d_g_Camera)            d_g_Camera          = new cl_mem[numDevices];
    if (!d_g_Xforms)            d_g_Xforms          = new cl_mem[numDevices];
    if (!d_g_accumBuffer)       d_g_accumBuffer     = new cl_mem[numDevices];
    if (!d_g_resultStaging)     d_g_resultStaging   = new cl_mem[numDevices];
    if (!d_g_startingXform)     d_g_startingXform   = new cl_mem[numDevices];
    if (!d_g_markCounts)        d_g_markCounts      = new cl_mem[numDevices];
    if (!d_g_pixelCounts)       d_g_pixelCounts     = new cl_mem[numDevices];
    if (!d_g_reduction)         d_g_reduction       = new cl_mem[numDevices];
    if (!d_g_pointList)         d_g_pointList       = new cl_mem[numDevices];
    if (!d_g_pointIterations)   d_g_pointIterations = new cl_mem[numDevices];
    if (!d_g_randSeeds)         d_g_randSeeds       = new cl_mem[numDevices];
    if (!d_g_renderTarget)      d_g_renderTarget    = new cl_mem[numDevices];
    if (!d_g_randK)             d_g_randK           = new cl_mem[numDevices];
    if (!d_g_switchMatrix)      d_g_switchMatrix    = new cl_mem[numDevices];
    if (!permutations)			permutations        = new cl_mem[numDevices];
    if (!gradients)			    gradients           = new cl_mem[numDevices];
    if (!shuffle)			    shuffle             = new cl_mem[numDevices];
    if (!iterationCount)        iterationCount      = new cl_mem[numDevices];
    if (!buf)                   buf                 = new char *[numDevices];
    
    for (int n = 0; n < numDevices; n++)
    {
        d_g_Palette[n]         = nullptr;
        d_g_PaletteTexture[n]  = nullptr;
        d_g_varUsages[n]       = nullptr;
        varpars[n]             = nullptr;
        d_g_varUsageIndexes[n] = nullptr;
        d_g_Flame[n]           = nullptr;
        d_g_Camera[n]          = nullptr;
        d_g_Xforms[n]          = nullptr;
        d_g_accumBuffer[n]     = nullptr;
        d_g_resultStaging[n]   = nullptr;
        d_g_markCounts[n]      = nullptr;
        d_g_pixelCounts[n]     = nullptr;
        d_g_reduction[n]       = nullptr;
        d_g_startingXform[n]   = nullptr;
        d_g_pointList[n]       = nullptr;
        d_g_pointIterations[n] = nullptr;
        d_g_randSeeds[n]       = nullptr;
        d_g_renderTarget[n]    = nullptr;
        d_g_randK[n]           = nullptr;
        d_g_switchMatrix[n]    = nullptr;
        permutations[n]        = nullptr;
        gradients[n]           = nullptr;
        shuffle[n]             = nullptr;
        iterationCount[n]      = nullptr;
        buf[n]                 = new char[bufsize];
    }
}

//static void logArray(uint *array, uint numPoints)
//{
//    uint warpSize = numPoints/warpsPerBlock;
//    
//    std::cout << "WarpSize: " << warpSize;
//    
//    string s;
//    s.append("FirstWarp:[");
//    
//    for (uint i = 0; i < warpSize; i ++)
//        s.append(itoa(array[i])).append(", ");
//    
//    s.append("]\n");
//    s.append("  2ndWarp:[");
//    
//    for (uint i = warpSize; i < numPoints; i ++)
//        s.append("%u, ", array[i]);
//    
//    s.append("]\n");
//    std::cout << s;
//}

// To initialize an array a of n elements to a randomly shuffled copy of source, both 0-based:
//    a[0] ← source[0]
//    for i from 1 to n − 1 do
//      j ← random integer with 0 ≤ j ≤ i
//      if j ≠ i
//          a[i] ← a[j]
//      a[j] ← source[i]
static void shuffleByBlock(uint *array, uint numPoints)
{
    array[0] = 0;
    for (uint i = 1; i < numPoints; i ++) {
        uint j = rand() % (i + 1);
        if (j != i)
            array[i] = array[j];
        array[j] = i;
    }
    //    logArray(array, numPoints);
}

//static void shuffleByWarp(uint *array, uint numPoints)
//{
//    uint warpSize = numPoints/warpsPerBlock;
//    array[0] = 0;
//    for (uint i = 1; i < warpSize; i ++) {
//        uint j = random() % (i + 1);
//        if (j != i)
//            array[i] = array[j];
//        array[j] = i;
//    }
//    
//    array[warpSize] = warpSize;
//    for (uint i = 1; i < warpSize; i ++) {
//        uint j = random() % (i + 1);
//        if (j != i)
//            array[warpSize + i] = array[warpSize + j];
//        array[warpSize + j] = i + warpSize;
//    }
//    //    logArray(array, numPoints);
//}

void Flam4ClRuntime::logVarlist(Flame & flame,
                                uint selectedDeviceNum,
                                unsigned int *xformVarUsageIndexes,
                                struct VariationListNode *varList,
                                float *varparInstances,
                                uint total,
                                uint totalVarPar,
                                const SharedVariationSet & variationSet)
{
    systemLog("\nXform VarList Offsets =======");
    for (int xform = 0; xform < flame.params.numTrans; xform++) {
        snprintf(buf[selectedDeviceNum], bufsize,
                 "   Xform#%u VarList Offset:%u", xform, xformVarUsageIndexes[xform]);
        systemLog(buf[selectedDeviceNum]);
    }
    systemLog("\nFinal Xform VarList Offsets =======");
    for (int xform = 0; xform < flame.params.numFinal; xform++) {
        snprintf(buf[selectedDeviceNum], bufsize,
                 "   FinalXform#%u VarList Offset:%u", xform, xformVarUsageIndexes[flame.params.numTrans + xform]);
        systemLog(buf[selectedDeviceNum]);
    }
    
    systemLog("\nVarList =======");
    for (int i = 0; i < total; i++) {
        struct VariationListNode list = varList[i];
        
        Variation * variation = variationSet->variationForXformIndex(list.variationID);
        const char * name = variation ? variation->name.c_str() : "";

        snprintf(buf[selectedDeviceNum], bufsize,
                 "   Index:%u  VarPar Offset:%u xformIndex:%u name:%s enterGroup:%u", i, list.varparOffset, list.variationID, name, list.enterGroup);
        systemLog(buf[selectedDeviceNum]);
    }
    
    systemLog("\nVarPars =======");
    for (int i = 0; i < totalVarPar; i++) {
        snprintf(buf[selectedDeviceNum], bufsize, "   Index:%u  VarPar %f", i, varparInstances[i]);
        systemLog(buf[selectedDeviceNum]);
    }
}

// Each xform has a variable length list of variation instances
// Each variation instance has its own variable sized varpar cluster struct (struct has weight + any parameter values)

// all of the lists are concatenated into a single buffer (one buffer per list type)

// kernel receives three lists
//    d_g_varUsages       - list of VariationListNode structs (one per variation instance)
//                              uint variationID;   - value identifying the variation from the variation set
//                                                  - NOTE: id of zero is used to signify end of list
//                              uint varparOffset;  - offset in varpars list for this variation's specific varpar struct
//                              uint enterGroup;    - the state transition that handles entering Pre, Normal, and Post variation groups

//    d_g_varUsageIndexes - list of indexes (one per xform) - has the offset to each xform's first variation in varUsages list
//    varpars             - list of varpar clusters

// so kernel takes current xform index - it finds the sub-list of variation indexes:  varUsagesIndex = d_g_varUsageIndexes[xformIndex]
//          its first VariationListNode is:            node = d_g_varUsages[varUsagesIndex]
//          the varpar struct for that instance is at: varpars[node.varparOffset]
//    kernel walks an xform's d_g_varUsages sub-list - rendering each variation instance until node wth variationID of zero is reached

void Flam4ClRuntime::makeVarUsageListsForSelectedDeviceNum(uint selectedDeviceNum, Flame & flame, const SharedVariationSet & variationSet)
{
    cl_command_queue queue = clContext->queueForSelectedDevice(selectedDeviceNum);
    
#ifdef SHOW_VARLIST
    bool showVarlist = true;
#else
    bool showVarlist = false;
#endif
    unsigned int xformVarUsageCounts      [MAX_XFORMS + MAX_XFORMS];
    unsigned int xformVarParUsageCounts   [MAX_XFORMS + MAX_XFORMS];
    unsigned int xformVarParOffsetIndexes [MAX_XFORMS + MAX_XFORMS];
    unsigned int xformVarUsageIndexes     [MAX_XFORMS + MAX_XFORMS];
    
    for (uint i = 0; i < MAX_XFORMS + MAX_XFORMS; i++) {
        xformVarUsageCounts[i]       = 0;
        xformVarParUsageCounts[i]    = 0;
        xformVarParOffsetIndexes[i]  = 0;
        xformVarUsageIndexes[i]      = 0;
    }
    
    // count number of variation instance slots used by all xforms AND number of varpar slots
    uint total = 0, totalVarPar = 0;
    for (int xform = 0; xform < flame.params.numTrans; xform++)
    {
        xformVarUsageCounts[xform] = 1; // for the trailing NULL slot
        xformVarParUsageCounts[xform] = 1;
        
        SharedVariationChain & chain = flame.xformVarChains[xform];
        for (uint k = 0; k < chain->size(); k++) {
            SharedVariationGroup & group = chain->operator[](k);
            for (string  & key : group->allKeys()) {
                if (VarParInstance::instanceNumOfKey(key) == string::npos)
                    continue;
                
                const Variation * variation = variationSet->variationForKey(key);
                if (variation) {
                    SharedVarParInstance & varpar = group->operator[](key);
                    if (varpar->floatValue == 0.f)
                        continue;
                    
                    xformVarUsageCounts[xform]++;
                    xformVarParUsageCounts[xform] += 1 + variation->parameters.size();
                }
            }
        }
        total       += xformVarUsageCounts[xform];
        totalVarPar += xformVarParUsageCounts[xform];
    }
    
    for (int xform = 0; xform < flame.params.numFinal; xform++)
    {
        xformVarUsageCounts[flame.params.numTrans + xform] = 1; // for the trailing NULL slot
        xformVarParUsageCounts[flame.params.numTrans + xform] = 1;
        
        SharedVariationChain & chain = flame.finalVarChains[xform];
        for (uint k = 0; k < chain->size(); k++) {
            SharedVariationGroup & group = chain->operator[](k);
            for (string  & key : group->allKeys()) {
                if (VarParInstance::instanceNumOfKey(key) == string::npos)
                    continue;
                
                const Variation * variation = variationSet->variationForKey(key);
                if (variation) {
                    SharedVarParInstance & varpar = group->operator[](key);
                    if (varpar->floatValue == 0.f)
                        continue;
                    
                    xformVarUsageCounts[flame.params.numTrans + xform]++;
                    xformVarParUsageCounts[flame.params.numTrans + xform] += 1 + variation->parameters.size();
                }
            }
        }
        total       += xformVarUsageCounts[flame.params.numTrans + xform];
        totalVarPar += xformVarParUsageCounts[flame.params.numTrans + xform];
    }
    
    size_t totalUsageSize  = sizeof(struct VariationListNode)*total + 8; // <=== pad by 8 bytes
    size_t totalVarparSize = sizeof(float)*totalVarPar + 8;
    
    struct VariationListNode *varList = (struct VariationListNode *)malloc(totalUsageSize);
    float *varparInstances            = (float *)malloc(totalVarparSize);
    
    xformVarUsageIndexes[0] = 0;
    for (int xform = 1; xform < flame.params.numTrans; xform++)
        xformVarUsageIndexes[xform] = xformVarUsageIndexes[xform-1] + xformVarUsageCounts[xform-1];
    
    for (int xform = flame.params.numTrans; xform < flame.params.numTrans + flame.params.numFinal; xform++)
        xformVarUsageIndexes[xform] = xformVarUsageIndexes[xform-1] + xformVarUsageCounts[xform-1];
    
    xformVarParOffsetIndexes[0] = 0;
    for (int xform = 1; xform < flame.params.numTrans; xform++)
        xformVarParOffsetIndexes[xform] = xformVarParOffsetIndexes[xform-1] + xformVarParUsageCounts[xform-1];

    for (int xform = flame.params.numTrans; xform < flame.params.numTrans + flame.params.numFinal; xform++)
        xformVarParOffsetIndexes[xform] = xformVarParOffsetIndexes[xform-1] + xformVarParUsageCounts[xform-1];
    
    
    bool enterGroup = true;
    uint index = 0, offset = 0;
    for (int xform = 0; xform < flame.params.numTrans; xform++)
    {
        if (showVarlist)
            { snprintf(buf[selectedDeviceNum], bufsize, "Xform #%i =======\n", xform); systemLog(buf[selectedDeviceNum]); }
        index  = xformVarUsageIndexes[xform];
        offset = xformVarParOffsetIndexes[xform];
        enterGroup = true;
        
        SharedVariationChain & chain = flame.xformVarChains[xform];
        for (uint k = 0; k < chain->size(); k++) {
            SharedVariationGroup & group = chain->operator[](k);
            enterGroup = group->size() > 0;  // group might be empty
            
            if (group->amPostMatrix(variationSet)) {
                enterGroup = false; // dont wast time doing precalcs for this specialized variation group
            }
            for (std::string & key : keysSortedByValue(group->getDictionary(), VarParInstance::xformIndexPairCompare)) {
                if (VarParInstance::instanceNumOfKey(key) == string::npos)
                    continue;
                Variation * variation = variationSet->variationForKey(key);
                if (variation) {
                    SharedVarParInstance & varpar = group->operator[](key);
                    if (varpar->floatValue == 0.f)
                        continue;
                    
                    size_t instanceNum          = varpar->instanceNum;
                    varList[index].varparOffset = offset;
                    varList[index].variationID  = (uint)variation->xformIndex;
                    varList[index].enterGroup   = enterGroup;
                    enterGroup = false;
                    index++;
                    varparInstances[offset++]   = varpar->floatValue;
                    if (showVarlist)
                        { snprintf(buf[selectedDeviceNum], bufsize, "%s\n", varpar->description().c_str()); systemLog(buf[selectedDeviceNum]); }
                    
                    for (std::string & parameterName :  keysSortedByValue(variation->parameters, VariationParameter::xformIndexPairCompare)) {
                        SharedParameter & parameter     = variation->parameters.operator[](parameterName);
                        string instancedParamKey        = VarParInstance::makeInstancedKey(parameter->key, instanceNum);
                        SharedVarParInstance & varparam = group->operator[](instancedParamKey);
                        varparInstances[offset++]       = varparam->floatValue;
                        if (showVarlist)
                            { snprintf(buf[selectedDeviceNum], bufsize, "%s\n", varparam->description().c_str()); systemLog(buf[selectedDeviceNum]); }
                    }
                }
            }
        }
        varList[index].varparOffset = 0;
        varList[index].variationID  = 0;
        varList[index].enterGroup   = enterGroup;
        index++;
    }
    
    for (int xform = 0; xform < flame.params.numFinal; xform++)
    {
        if (showVarlist)
            { snprintf(buf[selectedDeviceNum], bufsize, "Final Xform #%i =======\n", xform); systemLog(buf[selectedDeviceNum]); }
        index  = xformVarUsageIndexes[flame.params.numTrans + xform];
        offset = xformVarParOffsetIndexes[flame.params.numTrans + xform];
        enterGroup = true;
        
        SharedVariationChain & chain = flame.finalVarChains[xform];
        for (uint k = 0; k < chain->size(); k++) {
            SharedVariationGroup & group = chain->operator[](k);
            enterGroup = group->size() > 0;  // group might be empty
            
            if (group->amPostMatrix(variationSet)) {
                enterGroup = false; // dont wast time doing precalcs for this specialized variation group
            }
            for (std::string & key : keysSortedByValue(group->getDictionary(), VarParInstance::xformIndexPairCompare)) {
                if (VarParInstance::instanceNumOfKey(key) == string::npos)
                    continue;
                Variation * variation = variationSet->variationForKey(key);
                if (variation) {
                    SharedVarParInstance & varpar = group->operator[](key);
                    if (varpar->floatValue == 0.f)
                        continue;
                    
                    size_t instanceNum          = varpar->instanceNum;
                    varList[index].varparOffset = offset;
                    varList[index].variationID  = (uint)variation->xformIndex;
                    varList[index].enterGroup   = enterGroup;
                    enterGroup = false;
                    index++;
                    varparInstances[offset++]   = varpar->floatValue;
                    if (showVarlist)
                        { snprintf(buf[selectedDeviceNum], bufsize, "%s\n", varpar->description().c_str()); systemLog(buf[selectedDeviceNum]); }
                    
                    for (std::string & parameterName :  keysSortedByValue(variation->parameters, VariationParameter::xformIndexPairCompare)) {
                        SharedParameter & parameter     = variation->parameters.operator[](parameterName);
                        string instancedParamKey        = VarParInstance::makeInstancedKey(parameter->key, instanceNum);
                        SharedVarParInstance & varparam = group->operator[](instancedParamKey);
                        varparInstances[offset++]       = varparam->floatValue;
                        if (showVarlist)
                            { snprintf(buf[selectedDeviceNum], bufsize, "%s\n", varparam->description().c_str()); systemLog(buf[selectedDeviceNum]); }
                    }
                }
            }
        }
        varList[index].varparOffset = 0;
        varList[index].variationID  = 0;
        varList[index].enterGroup   = enterGroup;
        index++;
    }
    
    if (showVarlist) {
        logVarlist(flame, selectedDeviceNum, xformVarUsageIndexes, varList, varparInstances, total, totalVarPar, variationSet);
    }
    
    cl_int errNum = CL_SUCCESS;
    cl_context context;
    clGetCommandQueueInfo(queue, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, NULL);
    
    if (d_g_varUsages[selectedDeviceNum] != NULL) {
        size_t length = 0;
        clGetMemObjectInfo(d_g_varUsages[selectedDeviceNum], CL_MEM_SIZE, sizeof(size_t), &length, NULL);
        if (length < totalUsageSize) {
            clReleaseMemObject(d_g_varUsages[selectedDeviceNum]);
            d_g_varUsages[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_ONLY, totalUsageSize, NULL, &errNum);
        }
    }
    else {
        d_g_varUsages[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_ONLY, totalUsageSize, NULL, &errNum);
        if (errNum != CL_SUCCESS)
            d_g_varUsages[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE, totalUsageSize, NULL, &errNum);
    }
    
    if (varpars[selectedDeviceNum] != NULL) {
        size_t length = 0;
        clGetMemObjectInfo(varpars[selectedDeviceNum], CL_MEM_SIZE, sizeof(size_t), &length, NULL);
        if (length < totalVarparSize) {
            clReleaseMemObject(varpars[selectedDeviceNum]);
            varpars[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_ONLY, totalVarparSize, NULL, &errNum);
        }
    }
    else {
        varpars[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_ONLY, totalVarparSize, NULL, &errNum);
        if (errNum != CL_SUCCESS)
            varpars[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE, totalVarparSize, NULL, &errNum);
    }
    
    clEnqueueWriteBuffer(queue, d_g_varUsages[selectedDeviceNum], true, 0, totalUsageSize,  varList,   0, NULL, NULL);
    clEnqueueWriteBuffer(queue, varpars[selectedDeviceNum],       true, 0, totalVarparSize, varparInstances, 0, NULL, NULL);
    
    clEnqueueWriteBuffer(queue, d_g_varUsageIndexes[selectedDeviceNum], true, 0,
                         sizeof(uint)*(MAX_XFORMS+MAX_XFORMS), xformVarUsageIndexes, 0, NULL, NULL);
    
    free(varList);
    free(varparInstances);
}

void Flam4ClRuntime::optBlockYforQuality(int quality, uint selectedDeviceNum)
{
    size_t singlePointPoolSize   = NUM_POINTS_WARPSIZE_32 * BLOCKSX_WARPSIZE_32 * 32;
    double requiredIterationsPerTile = ((double)xDim*yDim*quality);
    size_t fuseIterations       = singlePointPoolSize * _fuseIterations[selectedDeviceNum];
    int optBlockY                    = ceil((requiredIterationsPerTile +  fuseIterations) /
                                            (double)(BLOCKSX[selectedDeviceNum]*BLOCKDIM[selectedDeviceNum]*NUM_ITERATIONS_FUSE_REDUCED));
    
    // Note: BLOCKSX*BLOCKSY*BLOCKDIM*NUM_ITERATIONS = xDim*yDim*actualQuality
    BLOCKSY[selectedDeviceNum] = optBlockY > 32 ? 32 : optBlockY; // Cap at 32
}

void Flam4ClRuntime::saveRenderTarget(cl_command_queue queue, uint selectedDeviceNum)
{
    clEnqueueReadBuffer(queue, d_g_renderTarget[selectedDeviceNum], true, 0, accumBufferSize(), hostAccumBuffer, 0, NULL, NULL);
}

#pragma mark Buffer Retain & Release

uint Flam4ClRuntime::referenceCountfor(cl_mem memobj)
{
    cl_uint refCount = 0;
    clGetMemObjectInfo(memobj, CL_MEM_REFERENCE_COUNT, sizeof(refCount), &refCount, NULL);
    return refCount;
}

void Flam4ClRuntime::deleteCUDAbuffers(cl_command_queue queue, uint selectedDeviceNum)
{
    if (selectedDeviceNum == 0 && pin_mergeStaging != NULL && numDevices > 1)
    {
        unpinBuffers(queue, selectedDeviceNum);
    }
    if (d_g_Palette[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_Palette[selectedDeviceNum]);
        d_g_Palette[selectedDeviceNum] = NULL;
    }
    if (d_g_PaletteTexture[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_PaletteTexture[selectedDeviceNum]);
        d_g_PaletteTexture[selectedDeviceNum] = NULL;
    }
    if (d_g_varUsages[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_varUsages[selectedDeviceNum]);
        d_g_varUsages[selectedDeviceNum] = NULL;
    }
    if (varpars[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(varpars[selectedDeviceNum]);
        varpars[selectedDeviceNum] = NULL;
    }
    
    if (d_g_varUsageIndexes[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_varUsageIndexes[selectedDeviceNum]);
        d_g_varUsageIndexes[selectedDeviceNum] = NULL;
    }
    if (d_g_Flame[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_Flame[selectedDeviceNum]);
        d_g_Flame[selectedDeviceNum] = NULL;
    }
    if (d_g_Camera[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_Camera[selectedDeviceNum]);
        d_g_Camera[selectedDeviceNum] = NULL;
    }
    if (d_g_Xforms[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_Xforms[selectedDeviceNum]);
        d_g_Xforms[selectedDeviceNum] = NULL;
    }
    if (d_g_switchMatrix[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_switchMatrix[selectedDeviceNum]);
        d_g_switchMatrix[selectedDeviceNum] = NULL;
    }
    
    if (d_g_accumBuffer[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_accumBuffer[selectedDeviceNum]);
        d_g_accumBuffer[selectedDeviceNum] = NULL;
    }
    if (d_g_resultStaging[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_resultStaging[selectedDeviceNum]);
        d_g_resultStaging[selectedDeviceNum] = NULL;
    }
    if (d_g_startingXform[selectedDeviceNum] != NULL)
    {
        bool setNULL = referenceCountfor(d_g_startingXform[selectedDeviceNum]) == 1 ? true : false;
        clReleaseMemObject(d_g_startingXform[selectedDeviceNum]);
        if (setNULL)
            d_g_startingXform[selectedDeviceNum] = NULL;
            }
    if (d_g_markCounts[selectedDeviceNum] != NULL)
    {
        bool setNULL = referenceCountfor(d_g_markCounts[selectedDeviceNum]) == 1 ? true : false;
        clReleaseMemObject(d_g_markCounts[selectedDeviceNum]);
        if (setNULL)
            d_g_markCounts[selectedDeviceNum] = NULL;
            }
    if (d_g_pixelCounts[selectedDeviceNum] != NULL)
    {
        bool setNULL = referenceCountfor(d_g_pixelCounts[selectedDeviceNum]) == 1 ? true : false;
        clReleaseMemObject(d_g_pixelCounts[selectedDeviceNum]);
        if (setNULL)
            d_g_pixelCounts[selectedDeviceNum] = NULL;
            }
    if (d_g_reduction[selectedDeviceNum] != NULL)
    {
        bool setNULL = referenceCountfor(d_g_reduction[selectedDeviceNum]) == 1 ? true : false;
        clReleaseMemObject(d_g_reduction[selectedDeviceNum]);
        if (setNULL)
            d_g_reduction[selectedDeviceNum] = NULL;
            }
    if (d_g_pointList[selectedDeviceNum] != NULL)
    {
        bool setNULL = referenceCountfor(d_g_pointList[selectedDeviceNum]) == 1 ? true : false;
        clReleaseMemObject(d_g_pointList[selectedDeviceNum]);
        if (setNULL)
            d_g_pointList[selectedDeviceNum] = NULL;
            }
    if (d_g_pointIterations[selectedDeviceNum] != NULL)
    {
        bool setNULL = referenceCountfor(d_g_pointIterations[selectedDeviceNum]) == 1 ? true : false;
        clReleaseMemObject(d_g_pointIterations[selectedDeviceNum]);
        if (setNULL)
            d_g_pointIterations[selectedDeviceNum] = NULL;
            }
    if (d_g_randSeeds[selectedDeviceNum] != NULL)
    {
        bool setNULL = referenceCountfor(d_g_randSeeds[selectedDeviceNum]) == 1 ? true : false;
        clReleaseMemObject(d_g_randSeeds[selectedDeviceNum]);
        if (setNULL)
            d_g_randSeeds[selectedDeviceNum] = NULL;
            }
    if (d_g_renderTarget[selectedDeviceNum] != NULL)
    {
        bool setNULL = referenceCountfor(d_g_renderTarget[selectedDeviceNum]) == 1 ? true : false;
        clReleaseMemObject(d_g_renderTarget[selectedDeviceNum]);
        if (setNULL)
            d_g_renderTarget[selectedDeviceNum] = NULL;
            }
    if (d_g_randK[selectedDeviceNum] != NULL)
    {
        bool setNULL = referenceCountfor(d_g_randK[selectedDeviceNum]) == 1 ? true : false;
        clReleaseMemObject(d_g_randK[selectedDeviceNum]);
        if (setNULL)
            d_g_randK[selectedDeviceNum] = NULL;
            }
    if (permutations[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(permutations[selectedDeviceNum]);
        permutations[selectedDeviceNum] = NULL;
    }
    if (gradients[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(gradients[selectedDeviceNum]);
        gradients[selectedDeviceNum] = NULL;
    }
    if (shuffle[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(shuffle[selectedDeviceNum]);
        shuffle[selectedDeviceNum] = NULL;
    }
    if (iterationCount[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(iterationCount[selectedDeviceNum]);
        iterationCount[selectedDeviceNum] = NULL;
    }
}

// when suspended we need to retain these buffers so we have the opportunity to save or discard state
void Flam4ClRuntime::retainCUDAbuffers(cl_command_queue queue, uint selectedDeviceNum)
{
    if (d_g_startingXform[selectedDeviceNum] != NULL)
        clRetainMemObject(d_g_startingXform[selectedDeviceNum]);
    if (d_g_markCounts[selectedDeviceNum] != NULL)
        clRetainMemObject(d_g_markCounts[selectedDeviceNum]);
    if (d_g_pixelCounts[selectedDeviceNum] != NULL)
        clRetainMemObject(d_g_pixelCounts[selectedDeviceNum]);
    if (d_g_reduction[selectedDeviceNum] != NULL)
        clRetainMemObject(d_g_reduction[selectedDeviceNum]);
    if (d_g_pointList[selectedDeviceNum] != NULL)
        clRetainMemObject(d_g_pointList[selectedDeviceNum]);
    if (d_g_pointIterations[selectedDeviceNum] != NULL)
        clRetainMemObject(d_g_pointIterations[selectedDeviceNum]);
    if (d_g_randSeeds[selectedDeviceNum] != NULL)
        clRetainMemObject(d_g_randSeeds[selectedDeviceNum]);
    if (d_g_renderTarget[selectedDeviceNum] != NULL)
        clRetainMemObject(d_g_renderTarget[selectedDeviceNum]);
    if (d_g_randK[selectedDeviceNum] != NULL)
        clRetainMemObject(d_g_randK[selectedDeviceNum]);
}

void Flam4ClRuntime::unpinBuffers(cl_command_queue queue, uint selectedDeviceNum)
{
    if (mergeStagingBuffer == NULL)
        return;
    if (isMergePinned) {
        clEnqueueUnmapMemObject(queue, pin_mergeStaging, mergeStagingBuffer, 0, NULL, NULL);
        mergeStagingBuffer = NULL;
        clReleaseMemObject(pin_mergeStaging);
        pin_mergeStaging = NULL;
    }
    else {
        delete[] mergeStagingBuffer;
        mergeStagingBuffer = NULL;
    }
}

void Flam4ClRuntime::addToAccumBufferForSelectedDeviceNum(uint selectedDeviceNum)
{
    cl_command_queue queue = clContext->queueForSelectedDevice(selectedDeviceNum);
    cl_ulong length = accumBufferSize();
    Point *buf      = NULL;
	ALIGNED_MALLOC(Point, buf, 4096, length);
    clEnqueueReadBuffer(queue, d_g_renderTarget[selectedDeviceNum], true, 0, length, buf, 0, NULL, NULL);
    
    length     /= sizeof(float);
    float *from = (float *)buf;
    float *to   = (float *)hostAccumBuffer;
    for (cl_ulong  i = 0; i < length; i++) {
        *to++ += *from++;
    }
	ALIGNED_FREE(buf);
}

#pragma mark Create Buffers

void Flam4ClRuntime::createCUDAbuffers(ClContext *_clContext,
                                       uint selectedDeviceNum,
                                       uint bitDepth,
                                       uint warpSize,
                                       uint desiredQuality,
                                       bool saveRenderState,
                                       size_t maxXformCount,
                                       size_t maxPointPoolCount,
                                       size_t xformCount)
{
    const SharedDeviceKind & deviceKind = clContext->deviceKindForSelectedDevice(selectedDeviceNum);
    ClDeviceKind * clDeviceKind         = (ClDeviceKind *)deviceKind.get();
    cl_context context     = clDeviceKind->context;
    cl_command_queue queue = clContext->queueForSelectedDevice(selectedDeviceNum);
    optBlockYforQuality(desiredQuality, selectedDeviceNum);
    cl_int errNum;
    
    permutations[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                     sizeof(permutation), permutation, &errNum);
    gradients[selectedDeviceNum]    = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                     sizeof(grad4), grad4, &errNum);
    
    uint* pointShuffled;
    uint  sizeShuffled;
    switch (warpSize) {
        case 16:
            pointShuffled = pointShuffledBlockWarp16;
            sizeShuffled  = sizeof(uint)*numPointsWarp16*NUM_ITERATIONS;
            break;
        case 32:
        default:
            pointShuffled = pointShuffledBlockWarp32;
            sizeShuffled  = sizeof(uint)*numPointsWarp32*NUM_ITERATIONS;
            break;
        case 64:
            pointShuffled = pointShuffledBlockWarp64;
            sizeShuffled  = sizeof(uint)*numPointsWarp64*NUM_ITERATIONS;
            break;
        case 128:
            pointShuffled = pointShuffledBlockWarp128;
            sizeShuffled  = sizeof(uint)*numPointsWarp128*NUM_ITERATIONS;
            break;
    }
    shuffle[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                sizeShuffled, pointShuffled, &errNum);
    
    if (selectedDeviceNum == 0 && numDevices > 1) {
        pin_mergeStaging = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR,
                                          sizeof(cl_float4)*xDim*yDim, NULL, &errNum);
        if (errNum == CL_SUCCESS) {
            isMergePinned = true;
            mergeStagingBuffer = (cl_float4 *)clEnqueueMapBuffer(queue, pin_mergeStaging, CL_TRUE, CL_MAP_READ,
                                                                 0, sizeof(cl_float4)*xDim*yDim, 0, NULL, NULL, &errNum);
        }
        else {
            isMergePinned = false;
            mergeStagingBuffer = new cl_float4[xDim*yDim];;
        }
    }
    uint _iterationCount = 0;
    iterationCount[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                       sizeof(uint), &_iterationCount, &errNum);
    d_g_varUsageIndexes[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_ONLY,
                                                            sizeof(unsigned int)*(MAX_XFORMS+MAX_XFORMS), NULL, &errNum);
    d_g_Flame[selectedDeviceNum]    = clCreateBuffer(context, CL_MEM_READ_ONLY,
                                                     sizeof(struct FlameParams), NULL, &errNum);
    d_g_Camera[selectedDeviceNum]    = clCreateBuffer(context, CL_MEM_READ_ONLY,
                                                      sizeof(struct CameraViewProperties), NULL, &errNum);
    d_g_Xforms[selectedDeviceNum]   = clCreateBuffer(context,
                                                     CL_MEM_READ_ONLY,
                                                     sizeof(struct xForm)*maxXformCount,
                                                     NULL, &errNum);
    d_g_switchMatrix[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_ONLY,
                                                         sizeof(float)*xformCount*xformCount, NULL, &errNum);
    
    d_g_accumBuffer[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                                        sizeof(cl_float4)*xDim*yDim,
                                                        NULL, &errNum);
    d_g_renderTarget[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                                         sizeof(cl_float4)*xDim*yDim,
                                                         NULL, &errNum);
    // if we need an accum Buffer and we have not read it from file, create one
    // after blade batch allocation
    
    if ((! hostAccumBuffer) && (saveRenderState || numBatches > CLEAR_FREQUENCY)) {
        void *buf = nullptr;
		ALIGNED_MALLOC(cl_uchar4, buf, 4096, xDim*yDim * sizeof(cl_float4));
		memset(buf, '\0', xDim*yDim * sizeof(cl_float4));
        hostAccumBuffer = (char *)buf;
    }
    
    if (bitDepth == 8)
        d_g_resultStaging[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                                              sizeof(cl_uchar4)*resampledXdim*resampledYdim,
                                                              NULL, &errNum);
    else if (bitDepth == 16)
        d_g_resultStaging[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                                              sizeof(cl_ushort4)*resampledXdim*resampledYdim,
                                                              NULL, &errNum);
    else
        d_g_resultStaging[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE,
                                                              sizeof(cl_float4)*resampledXdim*resampledYdim,
                                                              NULL, &errNum);
    
    uint *markCounts = NULL;

	ALIGNED_MALLOC(uint, markCounts, 4096, BLOCKDIM[selectedDeviceNum] * BLOCKSX[selectedDeviceNum] * BLOCKSY[selectedDeviceNum] * sizeof(uint));
	memset(markCounts, '\0', BLOCKDIM[selectedDeviceNum] * BLOCKSX[selectedDeviceNum] * BLOCKSY[selectedDeviceNum] * sizeof(uint));
    
    // on GPU's each block shares a startingXform, on CPU each thread has its own distinct startingXform
    d_g_startingXform[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                          sizeof(uint)*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum],
                                                          markCounts, &errNum); // markCounts buffer is bigger than we need to copy from
    d_g_markCounts[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                       sizeof(uint)*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum],
                                                       markCounts, &errNum);
    ALIGNED_FREE(markCounts);
    
    cl_ulong *reducedCounts = NULL;
    
    uint blocksTarget = ((xDim * yDim) + (BLOCKDIM[selectedDeviceNum] - 1))/BLOCKDIM[selectedDeviceNum];
	ALIGNED_MALLOC(cl_ulong, reducedCounts, 4096, sizeof(cl_ulong)*blocksTarget);
	memset(reducedCounts, '\0', sizeof(cl_ulong)*blocksTarget);
    
    // warpsize dependent
    d_g_reduction[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                      sizeof(cl_ulong)*blocksTarget,
                                                      reducedCounts, &errNum);
	ALIGNED_FREE(reducedCounts);
    
    uint *pixelCounts = NULL;
	ALIGNED_MALLOC(uint, pixelCounts, 4096, xDim*yDim * sizeof(uint));
	memset(pixelCounts, '\0', xDim*yDim * sizeof(uint));
    d_g_pixelCounts[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                        sizeof(uint)*xDim*yDim,
                                                        pixelCounts, &errNum);
	ALIGNED_FREE(pixelCounts);
    
    d_g_pointList[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                      sizeof(FAPoint)*(maxPointPoolCount*NUMPOINTS[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]),
                                                      randomPointPool, &errNum);
    d_g_pointIterations[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                                                            sizeof(uint)*(maxPointPoolCount*NUMPOINTS[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]),
                                                            initialPointIterations, &errNum);
    if (_clContext->warpSizeForSelectedDevice(selectedDeviceNum) == 1) {
        d_g_randSeeds[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                                          sizeof(int)*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]*16,
                                                          randomSeeds, &errNum);
        d_g_randK[selectedDeviceNum]     = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                                          sizeof(int)*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum],
                                                          initialK, &errNum);
    }
    else {
        d_g_randSeeds[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                                          sizeof(int)*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]*BLOCKDIM[selectedDeviceNum],
                                                          randomSeeds, &errNum);
        d_g_randK[selectedDeviceNum]     = NULL;
    }
    
    //    [Flam4ClRuntime logMD5sumFor:flame.colorIndex length:flame.numColors*sizeof(cl_float4) prefix:"Palette Digest: "];
}

#pragma mark Fuse

void Flam4ClRuntime::runFuseForSelectedDeviceNum(uint selectedDeviceNum,
                                                 float epsilon,
                                                 uint maxWorkGroupSize,
                                                 const SharedVariationSet & variationSet)
{
    const SharedDeviceKind & deviceKind = clContext->deviceKindForSelectedDevice(selectedDeviceNum);
    cl_command_queue queue              = clContext->queueForSelectedDevice(selectedDeviceNum);
    Flame & flame                       = *g_pFlame[selectedDeviceNum];
    cl_int errNum;
    
    //deep copy flm to the device
    errNum = clEnqueueWriteBuffer(queue, d_g_Flame[selectedDeviceNum], true, 0, sizeof(FlameParams), &(flame.params), 0, NULL, NULL);
    CameraViewport cv(&flame);
    clEnqueueWriteBuffer(queue, d_g_Camera[selectedDeviceNum], true, 0,
                         sizeof(CameraViewProperties), cv.pProperties(), 0, NULL, NULL);
    for (int n = 0; n < flame.params.numTrans; n++)
    {
        size_t sizeXform = sizeof(struct xForm);
        clEnqueueWriteBuffer(queue, d_g_Xforms[selectedDeviceNum], true, sizeXform * n, sizeXform, &(flame.trans[n]), 0, NULL, NULL);
    }
    for (int n = 0; n < flame.params.numFinal; n++)
    {
        size_t sizeXform = sizeof(struct xForm);
        clEnqueueWriteBuffer(queue, d_g_Xforms[selectedDeviceNum], true, sizeXform * (n + flame.params.numTrans), sizeXform, &(flame.finals[n]), 0, NULL, NULL);
    }
    clEnqueueWriteBuffer(queue, d_g_switchMatrix[selectedDeviceNum], true,
                         0, sizeof(float) * flame.params.numTrans * flame.params.numTrans, flame.switchMatrix, 0, NULL, NULL);
    
    makeVarUsageListsForSelectedDeviceNum(selectedDeviceNum, flame, variationSet);
    
    //Render it!
#ifdef __APPLE__
    srand((unsigned)(0x7FFFFFFF & mach_absolute_time()));
#elif defined(_WIN32)
	DWORD time1 = GetTickCount();
	srand((unsigned)(0x7FFFFFFF & time1));
#else
    timespec time1;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
    srand((unsigned)(0x7FFFFFFF & time1.tv_nsec));
#endif
    if (deviceKind->warpSize == 1) {
        int *seeds  = NULL;
		ALIGNED_MALLOC(int, seeds, 4096, sizeof(int)*BLOCKSX[selectedDeviceNum] * BLOCKSY[selectedDeviceNum] * 16);
        for (int n = 0; n < BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]*16; n++)
            seeds[n] = rand();
        clEnqueueWriteBuffer(queue, d_g_randSeeds[selectedDeviceNum], true, 0, BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]*16*sizeof(int), seeds, 0, NULL, NULL);
        ALIGNED_FREE(seeds);
    }
    else {
        int *seeds  = NULL;
		ALIGNED_MALLOC(int, seeds, 4096, sizeof(int)*BLOCKSX[selectedDeviceNum] * BLOCKSY[selectedDeviceNum] * BLOCKDIM[selectedDeviceNum]);
        for (int n = 0; n < BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]*BLOCKDIM[selectedDeviceNum]; n++)
            seeds[n] = rand();
        clEnqueueWriteBuffer(queue, d_g_randSeeds[selectedDeviceNum], true, 0, BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]*BLOCKDIM[selectedDeviceNum]*sizeof(int), seeds, 0, NULL, NULL);
		ALIGNED_FREE(seeds);
   }
    
    // no fuse is done now - done as part of renderBatches
}

void Flam4ClRuntime::readPointIterations(cl_command_queue queue, uint selectedDeviceNum, uint perXformPointCount, uint xformCount)
{
    const uint FUSE_ITERATIONS = 30;
    cl_ulong length = xformCount * perXformPointCount * sizeof(uint);
    uint *buffer  = NULL;
	ALIGNED_MALLOC(uint, buffer, 4096, length);
    
    clEnqueueReadBuffer(queue, d_g_pointIterations[selectedDeviceNum], true, 0, length, buffer, 0, NULL, NULL);
    
    //    size_t totalIterations = BLOCKSX[selectedDeviceNum] * BLOCKSY[selectedDeviceNum] * BLOCKDIM[selectedDeviceNum] * NUM_ITERATIONS;
    for (volatile uint j = 0; j < xformCount; j++) {
        int min = 1000000;
        int max = 0;
        uint sum = 0.f;
        size_t maxLocation = string::npos;
        size_t count = perXformPointCount;
        size_t unfusedCount = 0;
        
        for (size_t i = 0; i < count; i++) {
            uint value = buffer[j * perXformPointCount + i];
            min = min >= value ? value : min;
            maxLocation = max <= value ? i : maxLocation;
            max = max <= value ? value : max;
            sum += value;
            if (value < FUSE_ITERATIONS)
                unfusedCount++;
        }
        float mean = (float)sum / count;
        //        float markEfficiency = (float)sum /totalIterations;
        float unfusedFraction = (float)unfusedCount/count;
        float sum2 = 0.f;
        for (size_t i = 0; i < count; i++) {
            uint value = buffer[j * perXformPointCount + i];
            sum2 += (value - mean)*(value - mean);
        }
        float stddev     = sqrtf(sum2/count);
        if (j == 0) {
            snprintf(buf[selectedDeviceNum], bufsize,
                     "Xform #%u Point iterations min:%u mean:%f max:%u stdDev:%f unfused:%f%%", j + 1, min, mean, max, stddev, unfusedFraction*100.f);
            systemLog(buf[selectedDeviceNum]);
        }
        else {
            snprintf(buf[selectedDeviceNum], bufsize,
                     "      #%u Point iterations min:%u mean:%f max:%u stdDev:%f unfused:%f%%", j + 1, min, mean, max, stddev, unfusedFraction*100.f);
            systemLog(buf[selectedDeviceNum]);
        }
    }
	ALIGNED_FREE(buffer);
}

#pragma mark StartFrame

void Flam4ClRuntime::startFrameForSelectedDeviceNum(uint selectedDeviceNum,
                                                    uint maxWorkGroupSize,
                                                    const SharedVariationSet & variationSet,
                                                    duration<double> & buildTime)

{
    const SharedDeviceKind & deviceKind = clContext->deviceKindForSelectedDevice(selectedDeviceNum);
    ClDeviceKind * clDeviceKind         = (ClDeviceKind *)deviceKind.get();
    cl_context context                  = clDeviceKind->context;
    ClProgram *program                  = clDeviceKind->programForVariationSet(variationSet, buildTime);
    cl_command_queue queue              = clContext->queueForSelectedDevice(selectedDeviceNum);
    Flame & flame                       = *g_pFlame[selectedDeviceNum];
    cl_int errNum;
    
    //deep copy flm to the device
    clEnqueueWriteBuffer(queue, d_g_Flame[selectedDeviceNum], true, 0, sizeof(FlameParams), &(flame.params), 0, NULL, NULL);
    CameraViewport cv(&flame);
    clEnqueueWriteBuffer(queue, d_g_Camera[selectedDeviceNum], true, 0,
                         sizeof(CameraViewProperties), cv.pProperties(), 0, NULL, NULL);

    for (int n = 0; n < flame.params.numTrans; n++)
        clEnqueueWriteBuffer(queue, d_g_Xforms[selectedDeviceNum], true, sizeof(xForm)*n, sizeof(xForm), &(flame.trans[n]), 0, NULL, NULL);
    for (int n = 0; n < flame.params.numFinal; n++)
        clEnqueueWriteBuffer(queue, d_g_Xforms[selectedDeviceNum], true, sizeof(xForm)*(n + flame.params.numTrans), sizeof(xForm), &(flame.finals[n]), 0, NULL, NULL);
    clEnqueueWriteBuffer(queue, d_g_switchMatrix[selectedDeviceNum], true,
                         0, sizeof(float) * flame.params.numTrans * flame.params.numTrans, flame.switchMatrix, 0, NULL, NULL);
        
        if (deviceKind->deviceImageSupport) {
            if (d_g_PaletteTexture[selectedDeviceNum] != NULL)
            {
                clReleaseMemObject(d_g_PaletteTexture[selectedDeviceNum]);
                d_g_PaletteTexture[selectedDeviceNum] = NULL;
            }
            
            cl_image_format image_format;
            image_format.image_channel_order     = CL_RGBA;
            image_format.image_channel_data_type = CL_FLOAT;
            
            _cl_image_desc image_desc;
            image_desc.image_type        = CL_MEM_OBJECT_IMAGE2D;
            image_desc.image_width       = flame.numColors;
            image_desc.image_height      = 1;
            image_desc.image_depth       = 0;
            image_desc.image_array_size  = 0;
            image_desc.image_row_pitch   = 0;
            image_desc.image_slice_pitch = 0;
            image_desc.num_mip_levels    = 0;
            image_desc.num_samples       = 0;
            image_desc.buffer            = NULL;
            
            d_g_PaletteTexture[selectedDeviceNum] = clCreateImage(context,
                                                                  CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                                                  &image_format,
                                                                  &image_desc,
                                                                  flame.colorIndex,
                                                                  &errNum);
// OpenCl 1.1 code:
//            d_g_PaletteTexture[selectedDeviceNum] = clCreateImage2D(context,
//                                                                    CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
//                                                                    &image_format,
//                                                                    flame.numColors,1,
//                                                                    0, flame.colorIndex,
//                                                                    &errNum);
        }
    if (d_g_Palette[selectedDeviceNum] != NULL)
    {
        clReleaseMemObject(d_g_Palette[selectedDeviceNum]);
        d_g_Palette[selectedDeviceNum] = NULL;
    }
    
    d_g_Palette[selectedDeviceNum] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                                    sizeof(cl_float4)*flame.numColors,
                                                    flame.colorIndex, &errNum);
    //Render it!
    //    size_t globalWorkSize[2] = { ((xDim + 15)/16)*16, ((yDim + 15)/16)*16 };
    //    size_t localWorkSize[2]  = { 16, 16 };
    size_t globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    size_t localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    cl_kernel setBufferKernal = program->setBufferKernal;
    
    cl_float4 black = { 0.f, 0.f, 0.f, 0.f };
    
    errNum = clSetKernelArg(setBufferKernal, 0, sizeof(cl_mem),    &d_g_accumBuffer[selectedDeviceNum]);
    errNum = clSetKernelArg(setBufferKernal, 1, sizeof(cl_float4), &black);
    errNum = clSetKernelArg(setBufferKernal, 2, sizeof(cl_uint),   &xDim);
    errNum = clSetKernelArg(setBufferKernal, 3, sizeof(cl_uint),   &yDim);
    
    errNum = clEnqueueNDRangeKernel(queue, setBufferKernal, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, NULL);
    
    errNum = clSetKernelArg(setBufferKernal, 0, sizeof(cl_mem),    &d_g_renderTarget[selectedDeviceNum]);
    errNum = clSetKernelArg(setBufferKernal, 1, sizeof(cl_float4), &black);
    errNum = clSetKernelArg(setBufferKernal, 2, sizeof(cl_uint),   &xDim);
    errNum = clSetKernelArg(setBufferKernal, 3, sizeof(cl_uint),   &yDim);
    
    errNum = clEnqueueNDRangeKernel(queue, setBufferKernal, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, NULL);
    clFlush(queue);
}

void Flam4ClRuntime::clearRenderTargetForSelectedDeviceNum(unsigned selectedDeviceNum,
                                                           uint maxWorkGroupSize,
                                                           const SharedVariationSet & variationSet)
{
    duration<double> buildTime(0.);
    const SharedDeviceKind & deviceKind = clContext->deviceKindForSelectedDevice(selectedDeviceNum);
    ClDeviceKind * clDeviceKind         = (ClDeviceKind *)deviceKind.get();
    ClProgram *program                  = clDeviceKind->programForVariationSet(variationSet, buildTime);
    cl_command_queue queue              = clContext->queueForSelectedDevice(selectedDeviceNum);
    cl_kernel setBufferKernal           = program->setBufferKernal;
    
    cl_int errNum;
    //    size_t globalWorkSize[2] = { ((xDim + 15)/16)*16, ((yDim + 15)/16)*16 };
    //    size_t localWorkSize[2]  = { 16, 16 };
    size_t globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    size_t localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    
    cl_float4 black = { 0.f, 0.f, 0.f, 0.f };
    
    errNum = clSetKernelArg(setBufferKernal, 0, sizeof(cl_mem),    &d_g_renderTarget[selectedDeviceNum]);
    errNum = clSetKernelArg(setBufferKernal, 1, sizeof(cl_float4), &black);
    errNum = clSetKernelArg(setBufferKernal, 2, sizeof(cl_uint),   &xDim);
    errNum = clSetKernelArg(setBufferKernal, 3, sizeof(cl_uint),   &yDim);
    
    errNum = clEnqueueNDRangeKernel(queue, setBufferKernal, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, NULL);
    clFlush(queue);
}

#pragma mark Render Batch

void Flam4ClRuntime::renderBatchForSelectedDeviceNum(unsigned selectedDeviceNum,
                                                     SharedElc & elc,
                                                     float epsilon,
                                                     uint maxWorkGroupSize,
                                                     const SharedVariationSet & variationSet,
                                                     uint batchNum)
{
    //    [self logAllBuffersForDeviceNum:selectedDeviceNum];
    
    duration<double> buildTime(0.);
    const SharedDeviceKind & deviceKind = clContext->deviceKindForSelectedDevice(selectedDeviceNum);
    ClDeviceKind * clDeviceKind         = (ClDeviceKind *)deviceKind.get();
    ClProgram *program                  = clDeviceKind->programForVariationSet(variationSet, buildTime);
    cl_kernel iteratePointsKernal       = program->iteratePointsKernal;
    
    cl_command_queue queue              = clContext->queueForSelectedDevice(selectedDeviceNum);
    Flame & flame                       = *g_pFlame[selectedDeviceNum];
    
    
    cl_int errNum;
    int sXdim = xDim;
    int sYdim = yDim;
    cl_uint numColors       = flame.numColors;
    cl_int paletteStepMode  = (cl_int)(elc->paletteMode == modeStep);
    uint perXformPointPoolSize = NUMPOINTS[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    
    //deep copy flm to the device
    clEnqueueWriteBuffer(queue, d_g_Flame[selectedDeviceNum],  true, 0, sizeof(FlameParams), &(flame.params), 0, NULL, NULL);
    CameraViewport cv(&flame);
    clEnqueueWriteBuffer(queue, d_g_Camera[selectedDeviceNum], true, 0,
                         sizeof(CameraViewProperties), cv.pProperties(), 0, NULL, NULL);
    
    for (int n = 0; n < flame.params.numTrans; n++)
        clEnqueueWriteBuffer(queue, d_g_Xforms[selectedDeviceNum], true, sizeof(xForm)*n, sizeof(xForm), &(flame.trans[n]), 0, NULL, NULL);
    for (int n = 0; n < flame.params.numFinal; n++)
        clEnqueueWriteBuffer(queue, d_g_Xforms[selectedDeviceNum], true, sizeof(xForm)*(n + flame.params.numTrans), sizeof(xForm), &(flame.finals[n]), 0, NULL, NULL);
    clEnqueueWriteBuffer(queue, d_g_switchMatrix[selectedDeviceNum], true,
                         0, sizeof(float) * flame.params.numTrans * flame.params.numTrans, flame.switchMatrix, 0, NULL, NULL);
    
    //Render it!
    //    size_t globalWorkSize[2] = { BLOCKSX[selectedDeviceNum]*BLOCKDIM[selectedDeviceNum], BLOCKSY[selectedDeviceNum] };
    //    size_t localWorkSize[2]  = { BLOCKDIM[selectedDeviceNum], 1 };
    size_t globalWorkSize[2] = { BLOCKSX[selectedDeviceNum], BLOCKSY[selectedDeviceNum] };
    size_t localWorkSize[2]  = { 1, 1 };
    
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= BLOCKDIM[selectedDeviceNum];
        localWorkSize[0]  *= BLOCKDIM[selectedDeviceNum];
    }
    
    int i = 0;
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_varUsages[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_varUsageIndexes[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_Xforms[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &varpars[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_switchMatrix[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_Flame[selectedDeviceNum]);
    if (variationSet->is3DCapable)
        errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_Camera[selectedDeviceNum]);
    
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_pointList[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_pointIterations[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_randSeeds[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   deviceKind->warpSize == 1 ? &d_g_randK[selectedDeviceNum] : NULL);
    if (deviceKind->deviceImageSupport)
        errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),  &d_g_PaletteTexture[selectedDeviceNum]);
    
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_Palette[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_uint),  &numColors);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_int),   &paletteStepMode);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_float), &epsilon);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_uint),  &_fuseIterations[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_int),   &sXdim);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_int),   &sYdim);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_startingXform[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_markCounts[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &d_g_pixelCounts[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_uint),  &perXformPointPoolSize);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &permutations[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &gradients[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &shuffle[selectedDeviceNum]);
    errNum = clSetKernelArg(iteratePointsKernal, i++, sizeof(cl_mem),   &iterationCount[selectedDeviceNum]);
    
    //        NSLog("fuseIterations: %u", _fuseIterations[selectedDeviceNum]);
    // Queue the kernel up for execution across the array
    errNum = clEnqueueNDRangeKernel(queue, iteratePointsKernal, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, NULL);

	// NOTE: uncomment this to get histogram logging for testing
    //errNum = clFinish(queue);
    //logHistogramStatsFromQueue(queue, selectedDeviceNum, "Test", batchNum + 1);
}

void Flam4ClRuntime::grabFrameFromSelectedDeviceNum(uint selectedDeviceNum)
{
    cl_command_queue queue = clContext->queueForSelectedDevice(selectedDeviceNum);
    clEnqueueReadBuffer(queue, d_g_renderTarget[selectedDeviceNum], true, 0, xDim*yDim*sizeof(cl_float4), mergeStagingBuffer, 0, NULL, NULL);
}

void Flam4ClRuntime::synchronizeQueueForSelectedDeviceNum(uint selectedDeviceNum)
{
    cl_command_queue queue = clContext->queueForSelectedDevice(selectedDeviceNum);
    clFinish(queue);
}

void Flam4ClRuntime::mergeFramesToSelectedDeviceNum(uint selectedDeviceNum,
                                                    uint fromSelectedDeviceNum,
                                                    uint maxWorkGroupSize,
                                                    const SharedVariationSet & variationSet,
                                                    SharedRenderState & renderState)
{
    duration<double> buildTime(0.);
    const SharedDeviceKind & deviceKind = clContext->deviceKindForSelectedDevice(selectedDeviceNum);
    ClDeviceKind * clDeviceKind         = (ClDeviceKind *)deviceKind.get();
    ClProgram *program                  = clDeviceKind->programForVariationSet(variationSet, buildTime);
    cl_command_queue queue              = clContext->queueForSelectedDevice(selectedDeviceNum);
    cl_kernel mergeKernal               = program->MergeKernal;
    
    uint fromBatches = 0;
    cl_command_queue fromQueue          = clContext->queueForSelectedDevice(fromSelectedDeviceNum);
    clEnqueueReadBuffer(fromQueue, iterationCount[fromSelectedDeviceNum], true, 0, sizeof(uint), &fromBatches, 0, NULL, NULL);
    
    uint toBatches = 0;
    clEnqueueReadBuffer(queue, iterationCount[selectedDeviceNum], true, 0, sizeof(uint), &toBatches, 0, NULL, NULL);
    
    if (fromBatches != renderedBatches[fromSelectedDeviceNum]) {
        snprintf(buf[selectedDeviceNum], bufsize, "Frame:%u Device:%u Expected:%u  Actual Count:%u", renderState->renderInfo->frameNumber,
              fromSelectedDeviceNum, renderedBatches[fromSelectedDeviceNum], fromBatches);
        systemLog(buf[selectedDeviceNum]);
    }
    
    if (toBatches != renderedBatches[selectedDeviceNum]) {
        snprintf(buf[selectedDeviceNum], bufsize, "Frame:%u Device:%u Expected:%u  Actual Count:%u", renderState->renderInfo->frameNumber,
              selectedDeviceNum, renderedBatches[selectedDeviceNum], toBatches);
        systemLog(buf[selectedDeviceNum]);
    }
    
    toBatches += fromBatches;
    clEnqueueWriteBuffer(queue, iterationCount[selectedDeviceNum], true, 0, sizeof(uint), &toBatches, 0, NULL, NULL);
    
    
    //    [self logHistogramStatsFromQueue:fromQueue selectedDeviceNum:fromSelectedDeviceNum prefix:[NSString stringWithFormat:"RT Before Merge Device %u", fromSelectedDeviceNum] batches:renderedBatches[fromSelectedDeviceNum]];
    //    [self logHistogramStatsFromQueue:queue selectedDeviceNum:selectedDeviceNum prefix:[NSString stringWithFormat:"RT Before Merge Device %u", selectedDeviceNum] batches:renderedBatches[selectedDeviceNum]];
    
    clEnqueueWriteBuffer(queue, d_g_accumBuffer[selectedDeviceNum], true, 0, xDim*yDim*sizeof(cl_float4), mergeStagingBuffer, 0, NULL, NULL);
    
    //    size_t globalWorkSize[2] = { ((xDim + 15)/16)*16, ((yDim + 15)/16)*16 };
    //    size_t localWorkSize[2]  = { 16, 16 };
    size_t globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    size_t localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    
    cl_int errNum;
    errNum = clSetKernelArg(mergeKernal, 0, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
    errNum = clSetKernelArg(mergeKernal, 1, sizeof(cl_mem),   &d_g_accumBuffer[selectedDeviceNum]);
    errNum = clSetKernelArg(mergeKernal, 2, sizeof(cl_uint),  &xDim);
    errNum = clSetKernelArg(mergeKernal, 3, sizeof(cl_uint),  &yDim);
    
    errNum = clEnqueueNDRangeKernel(queue, mergeKernal, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, NULL);
    clFinish(queue);
    
    //    size_t batches = [self numDevices] == 1 ? renderedBatches[selectedDeviceNum] : (renderedBatches[0] + renderedBatches[1]);
    //    [self logHistogramStatsFromQueue:queue selectedDeviceNum:selectedDeviceNum prefix:[NSString stringWithFormat:"RT After Merge Device %u", selectedDeviceNum] batches:batches];
}

#pragma mark Finish Frame

void Flam4ClRuntime::rgbCurveAdjust(SharedClContext         & _clContext,
                                    const SharedPointVector & curveData,
                                    uint                      selectedDeviceNum,
                                    cl_kernel                 colorCurveRGB3ChannelsKernal,
                                    size_t                  * globalWorkSize,
                                    size_t                  * localWorkSize)
{
    NaturalCubicCurve cubicCurve(curveData, false, false);
    size_t cpCount = cubicCurve.cpCount();
    cl_int errNum;
    cl_command_queue queue = _clContext->queueForSelectedDevice(selectedDeviceNum);
    cl_context context;
    
    clGetCommandQueueInfo(queue, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, NULL);
    
    cl_mem XsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*cpCount,     cubicCurve.Xs->data(), &errNum);
    cl_mem AsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*cpCount,     cubicCurve.As->data(), &errNum);
    cl_mem BsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*(cpCount-1), cubicCurve.Bs->data(), &errNum);
    cl_mem CsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*cpCount,     cubicCurve.Cs->data(), &errNum);
    cl_mem DsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*(cpCount-1), cubicCurve.Ds->data(), &errNum);
    
    errNum = clSetKernelArg(colorCurveRGB3ChannelsKernal, 0, sizeof(cl_mem),   &d_g_accumBuffer[selectedDeviceNum]);
    errNum = clSetKernelArg(colorCurveRGB3ChannelsKernal, 1, sizeof(cl_mem),   &XsBuffer);
    errNum = clSetKernelArg(colorCurveRGB3ChannelsKernal, 2, sizeof(cl_mem),   &AsBuffer);
    errNum = clSetKernelArg(colorCurveRGB3ChannelsKernal, 3, sizeof(cl_mem),   &BsBuffer);
    errNum = clSetKernelArg(colorCurveRGB3ChannelsKernal, 4, sizeof(cl_mem),   &CsBuffer);
    errNum = clSetKernelArg(colorCurveRGB3ChannelsKernal, 5, sizeof(cl_mem),   &DsBuffer);
    errNum = clSetKernelArg(colorCurveRGB3ChannelsKernal, 6, sizeof(cl_uint),  &xDim);
    errNum = clSetKernelArg(colorCurveRGB3ChannelsKernal, 7, sizeof(cl_uint),  &yDim);
    errNum = clSetKernelArg(colorCurveRGB3ChannelsKernal, 8, sizeof(cl_uint),  &cpCount);
    
    errNum = clEnqueueNDRangeKernel(queue, colorCurveRGB3ChannelsKernal, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, NULL);
    
    clReleaseMemObject(XsBuffer);
    clReleaseMemObject(AsBuffer);
    clReleaseMemObject(BsBuffer);
    clReleaseMemObject(CsBuffer);
    clReleaseMemObject(DsBuffer);
}

void Flam4ClRuntime::curveAdjust(SharedClContext         & _clContext,
                                 const SharedPointVector & curveData,
                                 uint                      channel,
                                 uint                      selectedDeviceNum,
                                 cl_kernel                 colorCurveRGBChannelKernal,
                                 size_t                  * globalWorkSize,
                                 size_t                  * localWorkSize)
{
    NaturalCubicCurve cubicCurve(curveData, false, false);
    size_t cpCount = cubicCurve.cpCount();
    cl_int errNum;
    cl_command_queue queue = _clContext->queueForSelectedDevice(selectedDeviceNum);
    cl_context context;
    
    clGetCommandQueueInfo(queue, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, NULL);
    
    cl_mem XsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*cpCount,     cubicCurve.Xs->data(), &errNum);
    cl_mem AsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*cpCount,     cubicCurve.As->data(), &errNum);
    cl_mem BsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*(cpCount-1), cubicCurve.Bs->data(), &errNum);
    cl_mem CsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*cpCount,     cubicCurve.Cs->data(), &errNum);
    cl_mem DsBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(cl_float)*(cpCount-1), cubicCurve.Ds->data(), &errNum);
    
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 0, sizeof(cl_mem),   &d_g_accumBuffer[selectedDeviceNum]);
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 1, sizeof(cl_mem),   &XsBuffer);
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 2, sizeof(cl_mem),   &AsBuffer);
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 3, sizeof(cl_mem),   &BsBuffer);
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 4, sizeof(cl_mem),   &CsBuffer);
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 5, sizeof(cl_mem),   &DsBuffer);
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 6, sizeof(cl_uint),  &xDim);
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 7, sizeof(cl_uint),  &yDim);
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 8, sizeof(cl_uint),  &cpCount);
    errNum = clSetKernelArg(colorCurveRGBChannelKernal, 9, sizeof(cl_uint),  &channel);
    
    errNum = clEnqueueNDRangeKernel(queue, colorCurveRGBChannelKernal, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, NULL);
    
    clReleaseMemObject(XsBuffer);
    clReleaseMemObject(AsBuffer);
    clReleaseMemObject(BsBuffer);
    clReleaseMemObject(CsBuffer);
    clReleaseMemObject(DsBuffer);
}

size_t Flam4ClRuntime::reduceMarkCountsForSelectedDeviceNum(uint selectedDeviceNum,
                                                            uint maxWorkGroupSize,
                                                            cl_kernel reductionKernal,
                                                            cl_command_queue queue)
{
    if (d_g_reduction[selectedDeviceNum] == NULL)
        return 0;
    
    // reduce fused fractions down to single value
    uint blockDim = BLOCKDIM[selectedDeviceNum];
    cl_int errNum = CL_SUCCESS;
    uint countsLength = BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    if (maxWorkGroupSize == 1) {
        size_t globalWorkSizeR[1] = { BLOCKSX[selectedDeviceNum] * BLOCKSY[selectedDeviceNum] };
        size_t localWorkSizeR[1]  = { 1 };
        
        errNum = clSetKernelArg(reductionKernal, 0, sizeof(cl_mem),    &d_g_markCounts[selectedDeviceNum]);
        errNum = clSetKernelArg(reductionKernal, 1, sizeof(cl_uint),   &blockDim);
        errNum = clSetKernelArg(reductionKernal, 2, sizeof(cl_uint),   &countsLength);
        errNum = clSetKernelArg(reductionKernal, 3, sizeof(cl_mem),    &d_g_reduction[selectedDeviceNum]);
        
        errNum = clEnqueueNDRangeKernel(queue, reductionKernal, 1, NULL,
                                        globalWorkSizeR, localWorkSizeR,
                                        0, NULL, NULL);
    }
    else {
        size_t globalWorkSizeR[1] = { BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum] };
        size_t localWorkSizeR[1]  = { BLOCKDIM[selectedDeviceNum] };
        
        errNum = clSetKernelArg(reductionKernal, 0, sizeof(cl_mem),    &d_g_markCounts[selectedDeviceNum]);
        errNum = clSetKernelArg(reductionKernal, 1, sizeof(cl_uint),   &countsLength);
        errNum = clSetKernelArg(reductionKernal, 2, sizeof(cl_mem),    &d_g_reduction[selectedDeviceNum]);
        
        errNum = clEnqueueNDRangeKernel(queue, reductionKernal, 1, NULL,
                                        globalWorkSizeR, localWorkSizeR,
                                        0, NULL, NULL);
    }
    cl_ulong *reducedResults  = NULL;
    uint blocksTarget = ((xDim * yDim) + (BLOCKDIM[selectedDeviceNum] - 1))/BLOCKDIM[selectedDeviceNum];
    
	ALIGNED_MALLOC(cl_ulong, reducedResults, 4096, blocksTarget * sizeof(cl_ulong));
	memset(reducedResults, '\0', blocksTarget * sizeof(cl_ulong));
    clEnqueueReadBuffer(queue, d_g_reduction[selectedDeviceNum], true, 0,
                        blocksTarget*sizeof(cl_ulong), reducedResults, 0, NULL, NULL);
    
    cl_ulong totalIterationCount = 0;
    for (uint i = 0; i < blocksTarget; i++)
        totalIterationCount += reducedResults[i];
        
	ALIGNED_FREE(reducedResults);
    return totalIterationCount;
}

size_t Flam4ClRuntime::reduceMarkCountsForAllDevices(uint maxWorkGroupSize, const SharedVariationSet & variationSet)
{
    size_t count = 0;
    duration<double> buildTime(0.);
    for (uint i = 0; i < numDevices; i++) {
        // reduce fused fractions down to single value
        cl_command_queue queue              = clContext->queueForSelectedDevice(i);
        const SharedDeviceKind & deviceKind = clContext->deviceKindForSelectedDevice(i);
        ClDeviceKind * clDeviceKind         = (ClDeviceKind *)deviceKind.get();
        ClProgram *program                  = clDeviceKind->programForVariationSet(variationSet, buildTime);
        
        count += reduceMarkCountsForSelectedDeviceNum(i, maxWorkGroupSize, program->reductionKernal, queue);
    }
    return count;
}

size_t Flam4ClRuntime::reducePixelCountsForSelectedDeviceNum(uint selectedDeviceNum,
                                                             uint maxWorkGroupSize,
                                                             cl_kernel reductionKernal,
                                                             cl_command_queue queue)
{
    if (d_g_reduction[selectedDeviceNum] == NULL)
        return 0;
    
    // reduce per pixel mark counts down to single value
    cl_int errNum = CL_SUCCESS;
    if (maxWorkGroupSize == 1) {
        size_t globalWorkSizeR2[1] = { ((xDim * yDim) + (BLOCKDIM[selectedDeviceNum] - 1))/BLOCKDIM[selectedDeviceNum] };
        size_t localWorkSizeR2[1]  = { 1 };
        
        uint countsLength = xDim*yDim;
        uint blockDim     = BLOCKDIM[selectedDeviceNum];
        errNum = clSetKernelArg(reductionKernal, 0, sizeof(cl_mem),    &d_g_pixelCounts[selectedDeviceNum]);
        errNum = clSetKernelArg(reductionKernal, 1, sizeof(cl_uint),   &blockDim);
        errNum = clSetKernelArg(reductionKernal, 2, sizeof(cl_uint),   &countsLength);
        errNum = clSetKernelArg(reductionKernal, 3, sizeof(cl_mem),    &d_g_reduction[selectedDeviceNum]);
        
        errNum = clEnqueueNDRangeKernel(queue, reductionKernal, 1, NULL,
                                        globalWorkSizeR2, localWorkSizeR2,
                                        0, NULL, NULL);
    }
    else {
        size_t globalWorkSizeR2[1] = { (((xDim * yDim) + (BLOCKDIM[selectedDeviceNum] - 1))/BLOCKDIM[selectedDeviceNum])*BLOCKDIM[selectedDeviceNum] };
        size_t localWorkSizeR2[1]  = { BLOCKDIM[selectedDeviceNum] };
        
        uint countsLength = xDim*yDim;
        errNum = clSetKernelArg(reductionKernal, 0, sizeof(cl_mem),    &d_g_pixelCounts[selectedDeviceNum]);
        errNum = clSetKernelArg(reductionKernal, 1, sizeof(cl_uint),   &countsLength);
        errNum = clSetKernelArg(reductionKernal, 2, sizeof(cl_mem),    &d_g_reduction[selectedDeviceNum]);
        
        errNum = clEnqueueNDRangeKernel(queue, reductionKernal, 1, NULL,
                                        globalWorkSizeR2, localWorkSizeR2,
                                        0, NULL, NULL);
    }
    cl_ulong *reducedPixelResults = NULL;
    size_t _BLOCKSX = ((xDim * yDim) + (BLOCKDIM[selectedDeviceNum] - 1))/BLOCKDIM[selectedDeviceNum];
	ALIGNED_MALLOC(cl_ulong, reducedPixelResults, 4096, _BLOCKSX * sizeof(cl_ulong));
	memset(reducedPixelResults, '\0', _BLOCKSX * sizeof(cl_ulong));

//	size_t bufSize = 0;
//	clGetMemObjectInfo(d_g_reduction[selectedDeviceNum], CL_MEM_SIZE, sizeof(bufSize), &bufSize, NULL);

    clEnqueueReadBuffer(queue, d_g_reduction[selectedDeviceNum], true, 0,
                        _BLOCKSX*sizeof(cl_ulong), reducedPixelResults, 0, NULL, NULL);
    
    cl_ulong totalPixelCount = 0;
    for (uint i = 0; i < _BLOCKSX; i++)
        totalPixelCount += reducedPixelResults[i];
        
	ALIGNED_FREE(reducedPixelResults);
    return totalPixelCount;
}

size_t Flam4ClRuntime::reducePixelCountsForAllDevices(uint maxWorkGroupSize, const SharedVariationSet & variationSet)
{
    size_t count = 0;
    duration<double> buildTime(0.);
     for (uint i = 0; i < numDevices; i++) {
        // reduce fused fractions down to single value
        cl_command_queue queue = clContext->queueForSelectedDevice(i);
        const SharedDeviceKind & deviceKind = clContext->deviceKindForSelectedDevice(i);
        ClDeviceKind * clDeviceKind         = (ClDeviceKind *)deviceKind.get();
        ClProgram *program                  = clDeviceKind->programForVariationSet(variationSet, buildTime);
        count += reducePixelCountsForSelectedDeviceNum(i, maxWorkGroupSize, program->reductionKernal, queue);
    }
    return count;
}

void Flam4ClRuntime::resample(SharedClContext &_clContext,
                              cl_kernel readChannelKernal,
                              cl_kernel writeChannelKernal,
                              cl_kernel writeChannelStripedKernal,
                              cl_kernel convolveRowKernal,
                              cl_kernel convolveColKernal,
                              uint selectedDeviceNum,
                              uint maxWorkGroupSize,
                              SharedElc & elc)
{
    uint supersample = g_pFlame[selectedDeviceNum]->params.oversample;
    
    const uint KERNEL_RADIUS_ALIGNED = 16;
    const uint ROW_TILE_W            = 128;
    const uint COLUMN_TILE_W         = 16;
    const uint COLUMN_TILE_H         = 48;
    const uint BLOCKDIM_ROW_X        = KERNEL_RADIUS_ALIGNED + ROW_TILE_W + KERNEL_RADIUS_ALIGNED;
    
    size_t globalWorkSizeRow[2] = { (xDim + ROW_TILE_W - 1)/ROW_TILE_W, (yDim + 15)/16*16 };
    size_t localWorkSizeRow[2]  = { 1, 1 };
    
    size_t globalWorkSizeCol[2] = { (xDim + COLUMN_TILE_W - 1)/COLUMN_TILE_W, (yDim + COLUMN_TILE_H - 1)/COLUMN_TILE_H };
    size_t localWorkSizeCol[2]  = { 1, 1 };
    
    const uint COLUMN_TILE_BLOCK_YDIM = 8;
    
    if (maxWorkGroupSize > 1) {
        globalWorkSizeRow[0] *= BLOCKDIM_ROW_X;
        globalWorkSizeRow[1] *= 1;
        localWorkSizeRow[0]  *= BLOCKDIM_ROW_X;
        localWorkSizeRow[1]  *= 1;
        
        globalWorkSizeCol[0] *= COLUMN_TILE_W;
        globalWorkSizeCol[1] *= COLUMN_TILE_BLOCK_YDIM;
        localWorkSizeCol[0]  *= COLUMN_TILE_W;
        localWorkSizeCol[1]  *= COLUMN_TILE_BLOCK_YDIM;
    }
    
    // calculate buffer padding needed
    uint tempWidth1  = (xDim + ROW_TILE_W - 1)/ROW_TILE_W*ROW_TILE_W;
    uint tempHeigth1 = (yDim + 15)/16*16;
    uint size1       = tempWidth1 * tempHeigth1;
    uint tempWidth2  = (xDim + COLUMN_TILE_W - 1)/COLUMN_TILE_W*COLUMN_TILE_W;
    uint tempHeigth2 = (yDim + COLUMN_TILE_H - 1)/COLUMN_TILE_H*COLUMN_TILE_H;
    uint size2       = tempWidth2 * tempHeigth2;
    
    uint bufferSize = size1 > size2 ? size1 : size2;
    
    size_t globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    size_t localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    
    uint filterWidth = 0;
    float *filter    = SpatialFilter::createFilter(*elc, &filterWidth, 2*KERNEL_RADIUS_ALIGNED + 1);
    
    uint filterRadius = filterWidth/2;
    if (filterRadius > KERNEL_RADIUS_ALIGNED) {
        if (filter) free(filter);
        return;
    }
    if (!filter && supersample == 1)
        return;
    
    cl_int errNum;
    
    cl_command_queue queue = _clContext->queueForSelectedDevice(selectedDeviceNum);
    cl_context context;
    
    clGetCommandQueueInfo(queue, CL_QUEUE_CONTEXT, sizeof(cl_context), &context, NULL);
    cl_mem tempBuffer   = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*bufferSize, NULL, &errNum);
    cl_mem temp2Buffer  = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_float)*bufferSize, NULL, &errNum);
    cl_mem filterBuffer = NULL;
    if (filter) {
        filterBuffer = clCreateBuffer(context, CL_MEM_READ_ONLY, filterWidth * sizeof(float), NULL, &errNum);
        errNum = clEnqueueWriteBuffer(queue, filterBuffer, true, 0, filterWidth * sizeof(float), filter, 0, NULL, NULL);
    }
    
    for (uint channel = 0; channel < 4; channel++) {
        if (filter || supersample > 1) {
            errNum = clSetKernelArg(readChannelKernal, 0, sizeof(cl_mem),   &tempBuffer);
            errNum = clSetKernelArg(readChannelKernal, 1, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
            errNum = clSetKernelArg(readChannelKernal, 2, sizeof(cl_uint),  &xDim);
            errNum = clSetKernelArg(readChannelKernal, 3, sizeof(cl_uint),  &yDim);
            errNum = clSetKernelArg(readChannelKernal, 4, sizeof(cl_uint),  &channel);
            errNum = clEnqueueNDRangeKernel(queue, readChannelKernal, 2, NULL,
                                            globalWorkSize, localWorkSize,
                                            0, NULL, NULL);
        }
        if (filter) {
            errNum = clSetKernelArg(convolveRowKernal, 0, sizeof(cl_mem),   &filterBuffer);
            errNum = clSetKernelArg(convolveRowKernal, 1, sizeof(cl_mem),   &temp2Buffer);
            errNum = clSetKernelArg(convolveRowKernal, 2, sizeof(cl_mem),   &tempBuffer);
            errNum = clSetKernelArg(convolveRowKernal, 3, sizeof(cl_uint),  &xDim);
            errNum = clSetKernelArg(convolveRowKernal, 4, sizeof(cl_uint),  &yDim);
            errNum = clSetKernelArg(convolveRowKernal, 5, sizeof(cl_uint),  &filterRadius);
            errNum = clEnqueueNDRangeKernel(queue, convolveRowKernal, 2, NULL,
                                            globalWorkSizeRow, localWorkSizeRow,
                                            0, NULL, NULL);
            
            uint smemstride = COLUMN_TILE_W * COLUMN_TILE_BLOCK_YDIM;
            uint gmemstride = xDim * COLUMN_TILE_BLOCK_YDIM;
            
            errNum = clSetKernelArg(convolveColKernal, 0, sizeof(cl_mem),   &filterBuffer);
            errNum = clSetKernelArg(convolveColKernal, 1, sizeof(cl_mem),   &tempBuffer);
            errNum = clSetKernelArg(convolveColKernal, 2, sizeof(cl_mem),   &temp2Buffer);
            errNum = clSetKernelArg(convolveColKernal, 3, sizeof(cl_uint),  &xDim);
            errNum = clSetKernelArg(convolveColKernal, 4, sizeof(cl_uint),  &yDim);
            errNum = clSetKernelArg(convolveColKernal, 5, sizeof(cl_uint),  &smemstride);
            errNum = clSetKernelArg(convolveColKernal, 6, sizeof(cl_uint),  &gmemstride);
            errNum = clSetKernelArg(convolveColKernal, 7, sizeof(cl_uint),  &filterRadius);
            errNum = clEnqueueNDRangeKernel(queue, convolveColKernal, 2, NULL,
                                            globalWorkSizeCol, localWorkSizeCol,
                                            0, NULL, NULL);
        }
        
        if (supersample > 1) {
            errNum = clSetKernelArg(writeChannelStripedKernal, 0, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
            errNum = clSetKernelArg(writeChannelStripedKernal, 1, sizeof(cl_mem),   &tempBuffer);
            errNum = clSetKernelArg(writeChannelStripedKernal, 2, sizeof(cl_uint),  &xDim);
            errNum = clSetKernelArg(writeChannelStripedKernal, 3, sizeof(cl_uint),  &yDim);
            errNum = clSetKernelArg(writeChannelStripedKernal, 4, sizeof(cl_uint),  &channel);
            errNum = clSetKernelArg(writeChannelStripedKernal, 5, sizeof(cl_uint),  &supersample);
            errNum = clEnqueueNDRangeKernel(queue, writeChannelStripedKernal, 2, NULL,
                                            globalWorkSize, localWorkSize,
                                            0, NULL, NULL);
        }
        else if (filter){
            
            errNum = clSetKernelArg(writeChannelKernal, 0, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
            errNum = clSetKernelArg(writeChannelKernal, 1, sizeof(cl_mem),   &tempBuffer);
            errNum = clSetKernelArg(writeChannelKernal, 2, sizeof(cl_uint),  &xDim);
            errNum = clSetKernelArg(writeChannelKernal, 3, sizeof(cl_uint),  &yDim);
            errNum = clSetKernelArg(writeChannelKernal, 4, sizeof(cl_uint),  &channel);
            errNum = clEnqueueNDRangeKernel(queue, writeChannelKernal, 2, NULL,
                                            globalWorkSize, localWorkSize,
                                            0, NULL, NULL);
        }
    }
    if (filter) {
        free(filter);
        clReleaseMemObject(filterBuffer);
    }
    clReleaseMemObject(tempBuffer);
    clReleaseMemObject(temp2Buffer);
}

Point Flam4ClRuntime::finishFrame(SharedRenderState  & renderState,
                                  void               * outputBuffer,
                                  float              * backgroundImage,
                                  size_t               renderedBatches,
                                  const SharedVariationSet & variationSet,
                                  SharedElc          & elc,
                                  SharedFE           & flameExpanded,
                                  duration<double>   & deDuration)
{
    deDuration = duration<double>(0.);
    
    duration<double> buildTime(0.);
    const SharedDeviceKind & deviceKind = renderState->deviceContext->deviceKindForSelectedDevice(renderState->selectedDeviceNum);
    ClDeviceKind * clDeviceKind         = (ClDeviceKind *)deviceKind.get();
    ClProgram *program                  = clDeviceKind->programForVariationSet(variationSet, buildTime);
    SharedClContext & _clContext        = (SharedClContext &)renderState->deviceContext;
    cl_command_queue queue              = _clContext->queueForSelectedDevice(renderState->selectedDeviceNum);
    uint selectedDeviceNum              = renderState->selectedDeviceNum;
    bool useAlpha                       = renderState->renderInfo->useAlpha;
    uint bitDepth                       = renderState->renderInfo->bitDepth;
    uint maxWorkGroupSize               = renderState->maxWorkGroupSize;
    std::string pixelFormat             = renderState->renderInfo->pixelFormat;
    PriorUse usePriorFuseFraction       = renderState->usePriorFuseFraction;
    float priorFraction                 = renderState->priorFraction;
    Flame & flame                       = *g_pFlame[renderState->selectedDeviceNum];
    
    cl_kernel FlexibleDensityEstimationKernal = program->FlexibleDensityEstimationKernal;
    cl_kernel setBufferKernal                 = program->setBufferKernal;
    cl_kernel mergeKernal                     = program->MergeKernal;
    cl_kernel postProcessStep1Kernal          = program->postProcessStep1Kernal;
    cl_kernel postProcessStep2Kernal          = program->postProcessStep2Kernal;
    cl_kernel colorCurveRGB3ChannelsKernal    = program->colorCurveRGB3ChannelsKernal;
    cl_kernel colorCurveRGBChannelKernal      = program->colorCurveRGBChannelKernal;
    cl_kernel RGBA128FtoRGBA32UKernal         = program->RGBA128FtoRGBA32UKernal;
    cl_kernel RGBA128FtoBGRA32UKernal         = program->RGBA128FtoBGRA32UKernal;
    cl_kernel RGBA128FtoRGBA64UKernal         = program->RGBA128FtoRGBA64UKernal;
    cl_kernel RGBA128FtoRGBA128FKernal        = program->RGBA128FtoRGBA128FKernal;
    cl_kernel readChannelKernal               = program->readChannelKernal;
    cl_kernel writeChannelKernal              = program->writeChannelKernal;
    cl_kernel writeChannelStripedKernal       = program->writeChannelStripedKernal;
    cl_kernel convolveRowKernal               = program->convolveRowKernal;
    cl_kernel convolveColKernal               = program->convolveColKernal;

    cl_int errNum;
    
    //    size_t globalWorkSize[2] = { ((xDim + 15)/16)*16, ((yDim + 15)/16)*16 };
    //    size_t localWorkSize[2]  = { 16, 16 };
    size_t globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    size_t localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    
    if (hostAccumBuffer) { // add current acccum buffer to current render state
        clEnqueueWriteBuffer(queue, d_g_accumBuffer[selectedDeviceNum], true, 0, accumBufferSize(), hostAccumBuffer, 0, NULL, NULL);
        
        errNum = clSetKernelArg(mergeKernal, 0, sizeof(cl_mem),   &d_g_accumBuffer[selectedDeviceNum]);
        errNum = clSetKernelArg(mergeKernal, 1, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
        errNum = clSetKernelArg(mergeKernal, 2, sizeof(cl_uint),  &xDim);
        errNum = clSetKernelArg(mergeKernal, 3, sizeof(cl_uint),  &yDim);
        errNum = clEnqueueNDRangeKernel(queue, mergeKernal, 2, NULL,
                                        globalWorkSize, localWorkSize,
                                        0, NULL, NULL);
        // update the host accumulation buffer with the merge results
        clEnqueueReadBuffer(queue, d_g_accumBuffer[selectedDeviceNum], true, 0, accumBufferSize(), hostAccumBuffer, 0, NULL, NULL);
        //        [self logHistogramStatsFromData:hostAccumBuffer progress:0 prefix:"hostAccumBufferRead"];
        
        
        // copy back to render target in prep for Density estimation
        if (flame.params.estimatorRadius > 0)
            clEnqueueCopyBuffer(queue, d_g_accumBuffer[selectedDeviceNum], d_g_renderTarget[selectedDeviceNum], 0, 0, xDim*yDim*sizeof(cl_float4), 0, NULL, NULL);
            }
    
    // reduce fused fractions down to single value
    size_t totalIterationCount = reduceMarkCountsForAllDevices(maxWorkGroupSize, variationSet);
    
    size_t maxTheoretical       = renderedBatches * NUM_ITERATIONS*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    size_t maxFusedTheoretical  = maxTheoretical - _fuseIterations[selectedDeviceNum] *BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    float fusedFraction = (float)((double)totalIterationCount/maxFusedTheoretical);
    
    // reduce per pixel mark counts down to single value
    size_t totalPixelCount = reducePixelCountsForAllDevices(maxWorkGroupSize, variationSet);
    
    // for multiple tile images we must use the same fuse fraction for all tiles - we use that from the first tile
    float retainedFraction = (float)((totalIterationCount > 0) ? (double)totalPixelCount/totalIterationCount : (double)totalPixelCount/maxFusedTheoretical);
    float bothFraction     = priorFraction; // usePrior
    
    if (usePriorFuseFraction == noPrior)
        bothFraction = fusedFraction * retainedFraction;
        else if (usePriorFuseFraction == nonAdaptive)
            bothFraction = 1.f;
            
            //    NSLog("Batches:%u fusedFraction=%f retainedFraction=%f bothFraction=%f", _renderedBatches, fusedFraction, retainedFraction, bothFraction);
            if (flame.params.estimatorRadius > 0)
            {
                time_point<high_resolution_clock> now = high_resolution_clock::now();

                uint maxEstimatorRadius      = _clContext->maxEstimatorRadiusForSelectedDevices();
                flame.params.estimatorRadius = flame.params.estimatorRadius > maxEstimatorRadius ? maxEstimatorRadius : flame.params.estimatorRadius;
                
                float baseThreshold = 0.9f;
                errNum = clSetKernelArg(FlexibleDensityEstimationKernal, 0, sizeof(cl_mem),   &d_g_accumBuffer[selectedDeviceNum]);
                errNum = clSetKernelArg(FlexibleDensityEstimationKernal, 1, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
                errNum = clSetKernelArg(FlexibleDensityEstimationKernal, 2, sizeof(cl_uint),  &xDim);
                errNum = clSetKernelArg(FlexibleDensityEstimationKernal, 3, sizeof(cl_uint),  &yDim);
                errNum = clSetKernelArg(FlexibleDensityEstimationKernal, 4, sizeof(cl_float), &baseThreshold);
                errNum = clSetKernelArg(FlexibleDensityEstimationKernal, 5, sizeof(cl_uint),  &flame.params.estimatorRadius);
                errNum = clEnqueueNDRangeKernel(queue, FlexibleDensityEstimationKernal, 2, NULL,
                                                globalWorkSize, localWorkSize,
                                                0, NULL, NULL);
                errNum = clFinish(queue);
                deDuration = duration_cast<duration<double>>(high_resolution_clock::now() - now);
            }
            else if (!hostAccumBuffer) // accumbuffer already has the data when hostAccumBuffer exists - dont overwrite it
            {
                // copy from render target to accumBuffer
                clEnqueueCopyBuffer(queue, d_g_renderTarget[selectedDeviceNum], d_g_accumBuffer[selectedDeviceNum], 0, 0, xDim*yDim*sizeof(cl_float4), 0, NULL, NULL);
            }
            else {
                systemLog("Should not be here!!");
            }
    
    // From here on we expect histogram to be in accumBuffer - image output will be put into render target
    
    if (backgroundImage) {
        clEnqueueWriteBuffer(queue, d_g_renderTarget[selectedDeviceNum], true, 0, xDim*yDim*sizeof(cl_float4), backgroundImage, 0, NULL, NULL);
    }
    else {
        cl_float4 background = { flame.params.background.r,flame.params.background.g,flame.params.background.b,flame.params.background.a };
        errNum = clSetKernelArg(setBufferKernal, 0, sizeof(cl_mem),    &d_g_renderTarget[selectedDeviceNum]);
        errNum = clSetKernelArg(setBufferKernal, 1, sizeof(cl_float4), &background);
        errNum = clSetKernelArg(setBufferKernal, 2, sizeof(cl_uint),   &xDim);
        errNum = clSetKernelArg(setBufferKernal, 3, sizeof(cl_uint),   &yDim);
        
        errNum = clEnqueueNDRangeKernel(queue, setBufferKernal, 2, NULL,
                                        globalWorkSize, localWorkSize,
                                        0, NULL, NULL);
    }
    // read the actual iteration count
    uint _actualBatches = 0;
    clEnqueueReadBuffer(queue, iterationCount[selectedDeviceNum], true, 0, sizeof(uint), &_actualBatches, 0, NULL, NULL);
    
    // correct for fan blades having set the numBatches to another value
    flame.params.numBatches = _actualBatches > 0 ? _actualBatches : renderedBatches;
    clEnqueueWriteBuffer(queue, d_g_Flame[selectedDeviceNum], true, 0, sizeof(FlameParams), &(flame.params), 0, NULL, NULL);
    
    clFlush(queue);
    cl_event event;
    clEnqueueBarrierWithWaitList(queue, 0, NULL, &event);
    
    if (flameExpanded->rgbCurve) {
        rgbCurveAdjust(_clContext, flameExpanded->rgbCurve, selectedDeviceNum, colorCurveRGB3ChannelsKernal, globalWorkSize, localWorkSize);
        clEnqueueBarrierWithWaitList(queue, 0, NULL, &event);
    }
    if (flameExpanded->redCurve) {
        curveAdjust(_clContext, flameExpanded->redCurve, 0, selectedDeviceNum, colorCurveRGBChannelKernal, globalWorkSize, localWorkSize);
        clEnqueueBarrierWithWaitList(queue, 0, NULL, &event);
    }
    if (flameExpanded->greenCurve) {
        curveAdjust(_clContext, flameExpanded->greenCurve, 1, selectedDeviceNum, colorCurveRGBChannelKernal, globalWorkSize, localWorkSize);
        clEnqueueBarrierWithWaitList(queue, 0, NULL, &event);
    }
    if (flameExpanded->blueCurve) {
        curveAdjust(_clContext, flameExpanded->blueCurve, 2, selectedDeviceNum, colorCurveRGBChannelKernal, globalWorkSize, localWorkSize);
        clEnqueueBarrierWithWaitList(queue, 0, NULL, &event);
    }
    
    //    [self logPostProcessConstants:selectedDeviceNum flame:flm];
    //    [self logAccumHistAC Before Post Process DevogramStatsFromQueue:queue selectedDeviceNum:selectedDeviceNum prefix:[NSString stringWithFormat:"AC Before Post Process Device %u", selectedDeviceNum] batches:renderedBatches[selectedDeviceNum]];
    
    errNum = clSetKernelArg(postProcessStep1Kernal, 0, sizeof(cl_mem),   &d_g_Flame[selectedDeviceNum]);
    errNum = clSetKernelArg(postProcessStep1Kernal, 1, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
    errNum = clSetKernelArg(postProcessStep1Kernal, 2, sizeof(cl_mem),   &d_g_accumBuffer[selectedDeviceNum]);
    errNum = clSetKernelArg(postProcessStep1Kernal, 3, sizeof(cl_uint),  &xDim);
    errNum = clSetKernelArg(postProcessStep1Kernal, 4, sizeof(cl_uint),  &yDim);
    errNum = clSetKernelArg(postProcessStep1Kernal, 5, sizeof(cl_int),   &BLOCKSY[selectedDeviceNum]);
    errNum = clSetKernelArg(postProcessStep1Kernal, 6, sizeof(cl_float), &bothFraction);
    errNum = clEnqueueNDRangeKernel(queue, postProcessStep1Kernal, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, NULL);
    
    clEnqueueBarrierWithWaitList(queue, 0, NULL, &event);
    
    cl_float4 k2Adjust = { 1.f, 1.f, 1.f, 1.f };
    NaturalCubicCurve::k2AdjustForData(flameExpanded->rgbCurve,
                                       flameExpanded->redCurve,
                                       flameExpanded->greenCurve,
                                       flameExpanded->blueCurve,
                                       (float *)&k2Adjust);
    
    errNum = clSetKernelArg(postProcessStep2Kernal, 0, sizeof(cl_mem),   &d_g_Flame[selectedDeviceNum]);
    errNum = clSetKernelArg(postProcessStep2Kernal, 1, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
    errNum = clSetKernelArg(postProcessStep2Kernal, 2, sizeof(cl_mem),   &d_g_accumBuffer[selectedDeviceNum]);
    errNum = clSetKernelArg(postProcessStep2Kernal, 3, sizeof(cl_uint),  &xDim);
    errNum = clSetKernelArg(postProcessStep2Kernal, 4, sizeof(cl_uint),  &yDim);
    errNum = clSetKernelArg(postProcessStep2Kernal, 5, sizeof(cl_int),   &BLOCKSY[selectedDeviceNum]);
    errNum = clSetKernelArg(postProcessStep2Kernal, 6, sizeof(cl_float), &bothFraction);
    errNum = clSetKernelArg(postProcessStep2Kernal, 7, sizeof(cl_float4),&k2Adjust);
    errNum = clEnqueueNDRangeKernel(queue, postProcessStep2Kernal, 2, NULL,
                                    globalWorkSize, localWorkSize,
                                    0, NULL, NULL);
    
    // do optional filtering
    if (flame.params.oversample > 1 || elc->filter > 0.f)
    {
        resample(_clContext,
                 readChannelKernal,
                 writeChannelKernal,
                 writeChannelStripedKernal,
                 convolveRowKernal,
                 convolveColKernal,
                 selectedDeviceNum,
                 maxWorkGroupSize,
                 elc);
    }
    
    if (outputBuffer != NULL)
    {
        if (bitDepth == 8 && pixelFormat != "BGRA") {
            errNum = clSetKernelArg(RGBA128FtoRGBA32UKernal, 0, sizeof(cl_mem),   &d_g_resultStaging[selectedDeviceNum]);
            errNum = clSetKernelArg(RGBA128FtoRGBA32UKernal, 1, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
            errNum = clSetKernelArg(RGBA128FtoRGBA32UKernal, 2, sizeof(cl_uint),  &resampledXdim);
            errNum = clSetKernelArg(RGBA128FtoRGBA32UKernal, 3, sizeof(cl_uint),  &resampledYdim);
            errNum = clSetKernelArg(RGBA128FtoRGBA32UKernal, 4, sizeof(cl_int),   &useAlpha);
            errNum = clEnqueueNDRangeKernel(queue, RGBA128FtoRGBA32UKernal, 2, NULL,
                                            globalWorkSize, localWorkSize,
                                            0, NULL, NULL);
            
            clEnqueueReadBuffer(queue, d_g_resultStaging[selectedDeviceNum], true, 0, resampledXdim*resampledYdim*sizeof(cl_uchar4), outputBuffer, 0, NULL, NULL);
        }
        else if (bitDepth == 8 && pixelFormat == "BGRA") {
            errNum = clSetKernelArg(RGBA128FtoBGRA32UKernal, 0, sizeof(cl_mem),   &d_g_resultStaging[selectedDeviceNum]);
            errNum = clSetKernelArg(RGBA128FtoBGRA32UKernal, 1, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
            errNum = clSetKernelArg(RGBA128FtoBGRA32UKernal, 2, sizeof(cl_uint),  &resampledXdim);
            errNum = clSetKernelArg(RGBA128FtoBGRA32UKernal, 3, sizeof(cl_uint),  &resampledYdim);
            errNum = clEnqueueNDRangeKernel(queue, RGBA128FtoBGRA32UKernal, 2, NULL,
                                            globalWorkSize, localWorkSize,
                                            0, NULL, NULL);
            
            clEnqueueReadBuffer(queue, d_g_resultStaging[selectedDeviceNum], true, 0, resampledXdim*resampledYdim*sizeof(cl_uchar4), outputBuffer, 0, NULL, NULL);
        }
        else if (bitDepth == 16) {
            errNum = clSetKernelArg(RGBA128FtoRGBA64UKernal, 0, sizeof(cl_mem),   &d_g_resultStaging[selectedDeviceNum]);
            errNum = clSetKernelArg(RGBA128FtoRGBA64UKernal, 1, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
            errNum = clSetKernelArg(RGBA128FtoRGBA64UKernal, 2, sizeof(cl_uint),  &resampledXdim);
            errNum = clSetKernelArg(RGBA128FtoRGBA64UKernal, 3, sizeof(cl_uint),  &resampledYdim);
            errNum = clSetKernelArg(RGBA128FtoRGBA64UKernal, 4, sizeof(cl_int),   &useAlpha);
            errNum = clEnqueueNDRangeKernel(queue, RGBA128FtoRGBA64UKernal, 2, NULL,
                                            globalWorkSize, localWorkSize,
                                            0, NULL, NULL);
            
            clEnqueueReadBuffer(queue, d_g_resultStaging[selectedDeviceNum], true, 0, resampledXdim*resampledYdim*sizeof(cl_ushort4), outputBuffer, 0, NULL, NULL);
        }
        else {
            errNum = clSetKernelArg(RGBA128FtoRGBA128FKernal, 0, sizeof(cl_mem),   &d_g_resultStaging[selectedDeviceNum]);
            errNum = clSetKernelArg(RGBA128FtoRGBA128FKernal, 1, sizeof(cl_mem),   &d_g_renderTarget[selectedDeviceNum]);
            errNum = clSetKernelArg(RGBA128FtoRGBA128FKernal, 2, sizeof(cl_uint),  &resampledXdim);
            errNum = clSetKernelArg(RGBA128FtoRGBA128FKernal, 3, sizeof(cl_uint),  &resampledYdim);
            errNum = clSetKernelArg(RGBA128FtoRGBA128FKernal, 4, sizeof(cl_int),   &useAlpha);
            errNum = clEnqueueNDRangeKernel(queue, RGBA128FtoRGBA128FKernal, 2, NULL,
                                            globalWorkSize, localWorkSize,
                                            0, NULL, NULL);
            
            clEnqueueReadBuffer(queue, d_g_resultStaging[selectedDeviceNum], true, 0, resampledXdim*resampledYdim*sizeof(cl_float4), outputBuffer, 0, NULL, NULL);
        }
    }
    
    return Point(fusedFraction, retainedFraction);
}

#pragma mark Quality to Batches Conversion

size_t Flam4ClRuntime::batchesPerTileForActualAdaptive(double actualQuality,
                                                       double width,
                                                       double height,
                                                       uint tiles,
                                                       uint selectedDeviceNum)
{
    double requiredIterationsPerTile = width * height /tiles * actualQuality;
    double optBlockY                 = ceil((requiredIterationsPerTile + _fuseIterations[selectedDeviceNum])/
                                            (BLOCKSX[selectedDeviceNum] * BLOCKDIM[selectedDeviceNum] * NUM_ITERATIONS));
    if (optBlockY > 32)  // Cap at 32
        optBlockY = 32;
        if (optBlockY < 32)
            return 1;
    
    size_t iterationsPerBatch = BLOCKSX[selectedDeviceNum] * BLOCKSY[selectedDeviceNum] * BLOCKDIM[selectedDeviceNum] * NUM_ITERATIONS;
    size_t batches            = ceil((requiredIterationsPerTile + _fuseIterations[selectedDeviceNum]) / iterationsPerBatch);
    
    return batches;
}

#pragma mark OCL Wrappers

// check and see if we can allocate a the CUDA device buffers
// return success/failure and the total amount that the test was able to allocate
bool Flam4ClRuntime::oclMemCheck(ClContext *clContext,
                                 uint    deviceNumber,
                                 size_t  area,
                                 uint    numColors,
                                 size_t & amountAlloced,
                                 int     bitDepth,
                                 int     warpSize,
                                 bool    deviceImageSupport,
                                 uint    xformCount,
                                 uint    supersample)
{
    if (warpSize == 1) // CPU's use virtual memory making this check unnecessart
        return true;
    // Note: formula for amount of memory is: 1 Mb  +  (36 * area / 1048576) Mb
    // or  max area is: (X - 1)*1048576/36 with X in Mb units
    //
    // so for 35 Mb total, the max area is: 990,322 pixels squared or about 1000x990 pixels
    // so for 45 Mb total, the max area is: 1,273,271 or about 1280x994  pixels
    // so for 90 Mb total, the max area is: 2,546,542 or about 2560x994 pixels
    // 1440x900 => 45.49 Mb, 1366x768 = 37.02 Mb, 1920x1280 = 85.375 Mb
    bool succeeded = false;
    
    float resampleArea = area/(supersample * supersample);
    
    cl_mem pointList           = NULL;
    cl_mem pointIterations     = NULL;
    cl_mem lastPointIndexes    = NULL;
    cl_mem accumBuffer         = NULL;
    cl_mem renderTarget        = NULL;
    cl_mem resultStagingBuffer = NULL;
    cl_mem perThreadRandSeeds  = NULL;
    cl_mem palette             = NULL;
    cl_mem paletteTexture      = NULL;
    cl_mem startingXforms      = NULL;
    cl_mem markCounts          = NULL;
    cl_mem pixelCounts         = NULL;
    
    int BLOCKSX;
    int BLOCKSY = 32;
    int NUMPOINTS;
    int BLOCKDIM;
    if (warpSize == 1 ) { // CPU - use pseudo Warpsize = 32
        BLOCKDIM   = 32*warpsPerBlock;
        BLOCKSX    = BLOCKSX_WARPSIZE_32;
        NUMPOINTS = NUM_POINTS_WARPSIZE_32;
    }
    else {
        BLOCKDIM   = warpSize*warpsPerBlock;
        BLOCKSX    = BLOCKSX_WARPSIZE_32*32/warpSize;
        NUMPOINTS = NUM_POINTS_WARPSIZE_32/32*warpSize;
    }
    
    amountAlloced               = 0;
    cl_int flag                 = CL_SUCCESS;
    SharedDeviceKind deviceKind = clContext->deviceKindForDevice(deviceNumber);
    ClDeviceKind * clDeviceKind = (ClDeviceKind *)deviceKind.get();
    cl_context context          = clDeviceKind->context;
    
    size_t size = sizeof(cl_float4)*numColors; // 256 colors -> 4k
    if (deviceImageSupport) {
        cl_image_format image_format;
        image_format.image_channel_order     = CL_RGBA;
        image_format.image_channel_data_type = CL_FLOAT;
        
        _cl_image_desc image_desc;
        image_desc.image_type        = CL_MEM_OBJECT_IMAGE2D;
        image_desc.image_width       = numColors;
        image_desc.image_height      = 1;
        image_desc.image_depth       = 0;
        image_desc.image_array_size  = 0;
        image_desc.image_row_pitch   = 0;
        image_desc.image_slice_pitch = 0;
        image_desc.num_mip_levels    = 0;
        image_desc.num_samples       = 0;
        image_desc.buffer            = NULL;
        
        paletteTexture = clCreateImage(context,
                                       CL_MEM_READ_ONLY,
                                       &image_format,
                                       &image_desc,
                                       NULL,
                                       &flag);
//        paletteTexture = clCreateImage2D(context, CL_MEM_READ_ONLY,   // OpenCL 1.1 code
//                                         &image_format,
//                                         numColors,1,
//                                         0, NULL,
//                                         &flag);
        if (flag != CL_SUCCESS)
            goto freeThem;
        else
            amountAlloced += size;
    }
    
    palette = clCreateBuffer(context, CL_MEM_READ_ONLY,
                             size,
                             NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(cl_float4)*xformCount*NUMPOINTS*BLOCKSX*BLOCKSY; // 0.78 Mb
    pointList = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(int)*xformCount*NUMPOINTS*BLOCKSX*BLOCKSY; // 0.78 Mb
    pointIterations = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(uint)*BLOCKDIM*BLOCKSX*BLOCKSY; // 0.125 Mb
    lastPointIndexes = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(cl_float4)*area;  // 16 * area /(1024*1024) Mb
    accumBuffer = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(cl_float4)*area;  // 16 * area /(1024*1024) Mb
    renderTarget = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(cl_float)*area;  // 4 * area /(1024*1024) Mb
    pixelCounts = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    if (bitDepth == 8)
        size = sizeof(cl_uchar4)*resampleArea; // 4 * resampleArea /(1024*1024) Mb
    else if (bitDepth == 16)
        size = sizeof(cl_ushort4)*resampleArea;
    else
        size = sizeof(cl_float4)*resampleArea;
    
    resultStagingBuffer = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(cl_int)*BLOCKSX*BLOCKSY*BLOCKDIM; // 0.125 Mb
    perThreadRandSeeds = clCreateBuffer(context, CL_MEM_READ_ONLY, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(cl_int)*BLOCKSX*BLOCKSY*BLOCKDIM; // 0.125 Mb
    startingXforms = clCreateBuffer(context, CL_MEM_READ_ONLY, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(cl_int)*BLOCKSX*BLOCKSY*BLOCKDIM; // 0.125 Mb
    markCounts = clCreateBuffer(context, CL_MEM_READ_WRITE, size, NULL, &flag);
    if (flag != CL_SUCCESS)
        goto freeThem;
    else
        amountAlloced += size;
    
    
    succeeded = true;
    
freeThem:
    if (pointList)				clReleaseMemObject(pointList);
    if (pointIterations)	    clReleaseMemObject(pointIterations);
    if (lastPointIndexes)	    clReleaseMemObject(lastPointIndexes);
    if (accumBuffer)			clReleaseMemObject(accumBuffer);
    if (renderTarget)			clReleaseMemObject(renderTarget);
    if (resultStagingBuffer)	clReleaseMemObject(resultStagingBuffer);
    if (perThreadRandSeeds)		clReleaseMemObject(perThreadRandSeeds);
    if (palette)                clReleaseMemObject(palette);
    if (paletteTexture)         clReleaseMemObject(paletteTexture);
    if (startingXforms)         clReleaseMemObject(startingXforms);
    if (markCounts)             clReleaseMemObject(markCounts);
    if (pixelCounts)            clReleaseMemObject(pixelCounts);
    
    return succeeded;
}

size_t Flam4ClRuntime::maxXformCount(FlamesVector & blades)
{
    int numXforms = 0;
    for (SharedFlameExpanded &fe : blades)
        if (fe->flame->params.numTrans + fe->flame->params.numFinal > numXforms)
            numXforms = fe->flame->params.numTrans + fe->flame->params.numFinal;
    return numXforms;
}

// final xforms dont have their own point pool
size_t Flam4ClRuntime::maxPointPoolCount(FlamesVector & blades)
{
    int numXforms = 0;
    for (SharedFlameExpanded &fe : blades)
        if (fe->flame->params.numTrans > numXforms)
            numXforms = fe->flame->params.numTrans;
    return numXforms;
}

void Flam4ClRuntime::startCuda(SharedRenderState & renderState, uint selectedDeviceNum)
{
    Flame *flame  = g_pFlame[selectedDeviceNum];
    xDim          = renderState->renderInfo->width;
    yDim          = renderState->renderInfo->height;
    resampledXdim = xDim / flame->params.oversample;
    resampledYdim = yDim / flame->params.oversample;
    
    SharedClContext & _clContext = (SharedClContext &)renderState->deviceContext;
    
    createCUDAbuffers(_clContext.get(),
                      selectedDeviceNum,
                      renderState->renderInfo->bitDepth,
                      renderState->deviceContext->warpSizeForSelectedDevice(selectedDeviceNum),
                      renderState->renderInfo->quality,
                      renderState->renderInfo->saveRenderState,
                      maxXformCount(*renderState->renderInfo->blades),
                      maxPointPoolCount(*renderState->renderInfo->blades),
                      flame->params.numTrans);
}

void Flam4ClRuntime::stopCudaForSelectedDeviceNum(uint selectedDeviceNum)
{
    if (g_pFlame[selectedDeviceNum])
        delete g_pFlame[selectedDeviceNum];
    g_pFlame[selectedDeviceNum] = NULL;
    
    cl_command_queue queue = clContext->queueForSelectedDevice(selectedDeviceNum);
    deleteCUDAbuffers(queue, selectedDeviceNum);
}

void Flam4ClRuntime::fuseIterations(uint fuseIterations, uint selectedDeviceNum)
{
    _fuseIterations[selectedDeviceNum] = fuseIterations;
}

float Flam4ClRuntime::logHistogramStatsFromData(ColorVector &data,
                                                const char * prefix,
                                                uint batches,
                                                size_t maxTheoretical,
                                                uint selectedDeviceNum)
{
    
    Histogram<cl_float4> histogram(data, xDim, yDim, g_pFlame[selectedDeviceNum], batches);

    double sumW = histogram.getSumW();
    if ((size_t)sumW > maxTheoretical) {
        snprintf(buf[selectedDeviceNum], bufsize, "<============= Too Many Hits ==================> MaxTheoretical: %zu SumW:%zu", maxTheoretical, (size_t)sumW);
        systemLog(buf[selectedDeviceNum]);
        checkRenderedBatchCounts();
    }
    
    cl_float4 point  = histogram.pointAtPointOffset(Point(xDim*0.5f, yDim*0.5f));
    
    snprintf(buf[selectedDeviceNum], bufsize, "%s %s sumLimit=%zu w@Center=%f w/batches=%f Sdm:%u",
             prefix, histogram.description().c_str(), maxTheoretical, point.w, point.w/batches, selectedDeviceNum);
    systemLog(buf[selectedDeviceNum]);
    
    float avgHitsPerBatch = histogram.avgHitsPerBatch();
    return avgHitsPerBatch;
}

float Flam4ClRuntime::logHistogramStatsFromQueue(cl_command_queue queue,
                                                 uint selectedDeviceNum,
                                                 const char * prefix,
                                                 uint batches)
{
    cl_ulong length = xDim*yDim*sizeof(cl_float4);
    cl_float4 *buf  = NULL;

	ALIGNED_MALLOC(cl_float4, buf, 4096, length);
    clEnqueueReadBuffer(queue, d_g_renderTarget[selectedDeviceNum], true, 0, length, buf, 0, NULL, NULL);
    
    std::vector<cl_float4> data(buf, buf + xDim*yDim);
	ALIGNED_FREE(buf);
    size_t maxTheoretical  =
    batches * NUM_ITERATIONS*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    
    return logHistogramStatsFromData(data, prefix, batches, maxTheoretical, selectedDeviceNum);
}



#ifdef __ORIGINAL__

// define DIGEST_VERIFY to do runtime check of unarchive success
// #define DIGEST_VERIFY

#undef SHOW_VARLIST


- (float)logAccumHistogramStatsFromQueue:(cl_command_queue)queue selectedDeviceNum:(int)selectedDeviceNum prefix:(NSString *)prefix batches:(uint)batches

{
    size_t maxTheoretical       = batches * NUM_ITERATIONS*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    size_t maxFusedTheoretical  = maxTheoretical - _fuseIterations[selectedDeviceNum] *BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    
    cl_ulong length = xDim*yDim*sizeof(cl_float4);
    cl_float4 *buf  = NULL;
    posix_memalign((void **)&buf, 4096, length);
    clEnqueueReadBuffer(queue, d_g_accumBuffer[selectedDeviceNum], true, 0, length, buf, 0, NULL, NULL);
    return [self logHistogramStatsFromData:[NSData dataWithBytesNoCopy:buf length:length]
                                    prefix:prefix
                                   batches:batches
                            maxTheoretical:maxFusedTheoretical
                         selectedDeviceNum:selectedDeviceNum];
}

- (float)logHostAccumHistogramStatsFromSelectedDeviceNum:(int)selectedDeviceNum prefix:(NSString *)prefix batches:(uint)batches maxTheoretical:(uint)maxTheoretical
{
    return [self logHistogramStatsFromData:hostAccumBuffer
                                    prefix:prefix
                                   batches:batches
                            maxTheoretical:maxTheoretical
                         selectedDeviceNum:selectedDeviceNum];
}

- (void)logBufferInfoForBufferName:(NSString *)name selectedDeviceNum:(int)selectedDeviceNum buffer:(cl_mem)buffer
{
    size_t length = 0;
    void *hostPtr = nil;
    
    if (buffer != 0) {
        clGetMemObjectInfo(buffer, CL_MEM_SIZE, sizeof(size_t), &length, NULL);
        clGetMemObjectInfo(buffer, CL_MEM_HOST_PTR, sizeof(void *), &hostPtr, NULL);
        NSLog("Device:%i Buffer %s: len:%lu hostPtr:%p", selectedDeviceNum, name, length, hostPtr);
    }
    else
        NSLog("Device:%i Buffer %s is zero", selectedDeviceNum, name);
        }

- (void)logAllBuffersForDeviceNum:(int)selectedDeviceNum
{
    [self logBufferInfoForBufferName:"palette" selectedDeviceNum:selectedDeviceNum buffer:d_g_Palette[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"paletteTexture" selectedDeviceNum:selectedDeviceNum buffer:d_g_PaletteTexture[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"varUsages" selectedDeviceNum:selectedDeviceNum buffer:d_g_varUsages[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"varUsageIndexes" selectedDeviceNum:selectedDeviceNum buffer:d_g_varUsageIndexes[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"varpars" selectedDeviceNum:selectedDeviceNum buffer:varpars[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"flame" selectedDeviceNum:selectedDeviceNum buffer:d_g_Flame[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"camera" selectedDeviceNum:selectedDeviceNum buffer:d_g_Camera[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"xforms" selectedDeviceNum:selectedDeviceNum buffer:d_g_Xforms[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"accum" selectedDeviceNum:selectedDeviceNum buffer:d_g_accumBuffer[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"resultStaging" selectedDeviceNum:selectedDeviceNum buffer:d_g_markCounts[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"pixelCounts" selectedDeviceNum:selectedDeviceNum buffer:d_g_pixelCounts[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"reduction" selectedDeviceNum:selectedDeviceNum buffer:d_g_reduction[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"startingXform" selectedDeviceNum:selectedDeviceNum buffer:d_g_startingXform[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"pointList" selectedDeviceNum:selectedDeviceNum buffer:d_g_pointList[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"pointIterations" selectedDeviceNum:selectedDeviceNum buffer:d_g_pointIterations[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"randSeeds" selectedDeviceNum:selectedDeviceNum buffer:d_g_randSeeds[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"randK" selectedDeviceNum:selectedDeviceNum buffer:d_g_randK[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"renderTarget" selectedDeviceNum:selectedDeviceNum buffer:d_g_renderTarget[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"switchMatrix" selectedDeviceNum:selectedDeviceNum buffer:d_g_switchMatrix[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"permutations" selectedDeviceNum:selectedDeviceNum buffer:permutations[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"gradients" selectedDeviceNum:selectedDeviceNum buffer:gradients[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"shuffle" selectedDeviceNum:selectedDeviceNum buffer:shuffle[selectedDeviceNum]];
    [self logBufferInfoForBufferName:"iterationCount" selectedDeviceNum:selectedDeviceNum buffer:iterationCount[selectedDeviceNum]];
}

- (void)logPostProcessConstants:(uint)selectedDeviceNum flame:(struct Flame *)flm
{
    float k1   = (flame.params.brightness*268.0f)/255.0f;
    float area = fabs(flame.params.size[0]*flame.params.size[1]);
    float k2   = ((float)(xDim*yDim))/(area*((float)(NUM_ITERATIONS))*flame.params.numBatches*32.f*1024.0f*((float)BLOCKSY[selectedDeviceNum]/32.f));
    NSLog("k1=%f k2=%f batches=%u flame.params.numBatches=%f", k1, k2, numBatches, flame.params.numBatches);
}

- (size_t)sumMarkCountsWithQueue:(cl_command_queue)queue
selectedDeviceNum:(int)selectedDeviceNum
{
    // counting double check
    uint countsLength          = BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    size_t markCountsSize  = (size_t)countsLength * sizeof(uint);
    size_t totalMarkCounts = 0;
    size_t *markCounts     = NULL;
    posix_memalign((void **)&markCounts, 4096, markCountsSize);
    bzero(markCounts, markCountsSize);
    clEnqueueReadBuffer(queue, d_g_markCounts[selectedDeviceNum], true, 0, markCountsSize, markCounts, 0, NULL, NULL);
    for (uint i = 0; i < countsLength; i++)
        totalMarkCounts += markCounts[i];
        return totalMarkCounts;
}

- (size_t)sumHistogramCountsWithQueue:(cl_command_queue)queue
selectedDeviceNum:(int)selectedDeviceNum
{
    // counting double check
    uint countsLength           = xDim * yDim;
    size_t pixelCountsSize  = (size_t)countsLength * sizeof(uint);
    size_t totalPixelCounts = 0;
    uint *pixelCounts           = NULL;
    posix_memalign((void **)&pixelCounts, 4096, pixelCountsSize);
    bzero(pixelCounts, pixelCountsSize);
    clEnqueueReadBuffer(queue, d_g_pixelCounts[selectedDeviceNum], true, 0, pixelCountsSize, pixelCounts, 0, NULL, NULL);
    for (uint i = 0; i < countsLength; i++)
        totalPixelCounts += pixelCounts[i];
        return totalPixelCounts;
}

- (void)logMarkCountsWithTotalIterCount:(size_t)totalIterationCount
queue:(cl_command_queue)queue
selectedDeviceNum:(int)selectedDeviceNum
{
    size_t totalMarkCounts = [self sumMarkCountsWithQueue:queue selectedDeviceNum:selectedDeviceNum];
    NSLog("TotalMarkCounts:%lu TotalIterationCount:%lu ", totalMarkCounts, totalIterationCount);
}

- (void)logPixelCountsWithTotalIterCount:(size_t)totalIterationCount
queue:(cl_command_queue)queue
selectedDeviceNum:(int)selectedDeviceNum
{
    size_t totalPixelCounts = [self sumHistogramCountsWithQueue:queue selectedDeviceNum:selectedDeviceNum];
    NSLog("TotalPixelCounts:%lu TotalIterationCount:%lu ", totalPixelCounts, totalIterationCount);
}

#endif
