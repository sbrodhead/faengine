//
//  AffineMatrix.h
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/29/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef AffineMatrix_h
#define AffineMatrix_h

#include "Exports.hpp"
#include "common.hpp"

using namespace FA;

struct xForm;

class FAENGINE_API AffineMatrix
{
public:
    AffineMatrix();
    AffineMatrix(float _a, float _b, float _c, float _d, float _e, float _f);

    AffineMatrix(const AffineMatrix &) = default;

    AffineMatrix(const struct xForm &xf);
    AffineMatrix(const struct xForm &xf, bool postXform = true);
    
    AffineMatrix & operator = (const AffineMatrix &) = default;
    
    bool operator==(const AffineMatrix &other) const;
    bool operator!=(const AffineMatrix &other) const;
    
    static AffineMatrix *matrixWithRotation(float theta);
    static AffineMatrix *matrixWithScale(float x);
    static AffineMatrix *matrixWithTranslate(const Point &p);
    static AffineMatrix *matrixWithInverseOfXform(const struct xForm &xf);
    static AffineMatrix *matrixWithInverseOfPostXform(const struct xForm &xf);
    static AffineMatrix *matrixWithMatrix(const AffineMatrix &m);
    static AffineMatrix *matrixWithCoefs(float _a, float _b, float _c, float _d, float _e, float _f);
    static AffineMatrix *matrixWithXform(const struct xForm &xf);
    static AffineMatrix *matrixWithPostXform(const struct xForm &xf);
    
    void setIdentity();
    void fillWith(float _a, float _b, float _c, float _d, float _e, float _f);

    void rotateBy(float theta);
    void scaleBy(float x);
    void translateBy(const Point &t);
    
    void invert();
    void horizReflect();
    void vertReflect();
    void horizVertReflect();
    void fillXform(struct xForm &xf);
    void fillPostXform(struct xForm &xf);

    void transformBy(const AffineMatrix &m);
    Point transformPoint(const Point &p);
    
    static bool isIdentityXform(const struct xForm &xf);
    static bool isIdentityPostXform(const struct xForm &xf);
    
    static void translateXform(struct xForm &xf, const Point &t);
    static void translatePostXform(struct xForm &xf, const Point &t);
    static void setXformToIdentity(struct xForm &xf);
    static void setPostXformToIdentity(struct xForm &xf);
    static void rotateXform(struct xForm &xf, float theta);
    static void rotatePostXform(struct xForm &xf, float theta);
    static void scaleXform(struct xForm &xf, float x);
    static void scalePostXform(struct xForm &xf, float x);
    static void transformXform(struct xForm &xf, const AffineMatrix &m);
    static void transformPostXform(struct xForm &xf, const AffineMatrix &m);
    static void invertXform(struct xForm &xf);
    static void invertPostXform(struct xForm &xf);
    static AffineMatrix * randomPreMatrix();
    static AffineMatrix * randomPostMatrix();
    
    static AffineMatrix * interpolateFrom(const AffineMatrix &fromMatrix,
                                          const AffineMatrix &toMatrix,
                                          float amount,
                                          bool polarInterpolation);
    
    // ===== Members =====
    float a;
    float b;
    float c;
    float d;
    float e;
    float f;
};

#endif /* AffineMatrix_h */
