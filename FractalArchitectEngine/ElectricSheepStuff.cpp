//
//  ElectricSheepStuff.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/5/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "ElectricSheepStuff.hpp"

/*
 *  ElectricSheepStuff.cpp
 *  FractalArchitect
 *
 *  Created by Steven Brodhead on 2/23/11.
 *  Copyright 2011 Centcom Inc. All rights reserved.
 *
 */
#include  "FlameMetaData.hpp"
#include "ElectricSheepStuff.hpp"
#include "XaosMatrixVector.hpp"
#include "Utilities.hpp"
#include <assert.h>
#include <string.h>

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif


std::unordered_map<std::string, int> shapes {
    { "gaussian", shapeGaussian },
    { "hermite",  shapeHermite },
    { "box",      shapeBox },
    { "triangle", shapeTriangle },
    { "bspline",  shapeBspline },
    { "mitchell", shapeMitchell },
    { "blackman", shapeBlackman },
    { "hanning",  shapeHanning },
    { "hamming",  shapeHamming },
    { "quadratic", shapeQuadratic },
};

std::unordered_map<std::string, int> temporalTypes {
    { "gaussian", temporalGaussian },
    { "ga",       temporalGaussian },
    { "exp",      temporalExp },
    { "box",      temporalBox },
};

std::unordered_map<int, std::string> reverseShapes {
    { shapeGaussian, "gaussian" },
    { shapeHermite, "hermite" },
    { shapeBox, "box" },
    { shapeTriangle, "triangle" },
    { shapeBspline, "bspline" },
    { shapeMitchell, "mitchell" },
    { shapeBlackman, "blackman" },
    { shapeHanning, "hanning" },
    { shapeHamming, "hamming" },
    { shapeQuadratic, "quadratic" },    
};

std::unordered_map<int, std::string> reverseTemporalTypes {
    { temporalGaussian, "gaussian" },
    { temporalGaussian, "ga" },
    { temporalExp, "exp" },
    { temporalBox, "box" },
};

std::unordered_map<std::string, int> paletteModes {
    { "step",   modeStep },
    { "linear", modeLinear },
    { "step",   modeSmooth },
};

std::unordered_map<int, std::string> reversePaletteModes {
    { modeStep,   "step"   },
    { modeLinear, "linear" },
    { modeSmooth, "step"   },
};

std::unordered_map<std::string, int> interpolations {
    { "smooth", interpolationSmooth },
    { "linear", interpolationLinear },
};

std::unordered_map<int, std::string> reverseInterpolations {
    { interpolationSmooth, "smooth" },
    { interpolationLinear, "linear" },
};

std::unordered_map<std::string, int> paletteInterpolations {
    { "hsv",   paletteHSV },
    { "sweep", paletteSweep },
};

std::unordered_map<int, std::string> reversePaletteInterpolations {
    { paletteHSV,   "hsv" },
    { paletteSweep, "sweep" },
};

std::unordered_map<std::string, int> interpolationTypes {
    { "linear", interpolationTypeLinear },
    { "log",    interpolationTypeLog },
    { "old",    interpolationTypeOld },
};

std::unordered_map<int, std::string> reverseInterpolationTypes {
    { interpolationTypeLinear, "linear" },
    { interpolationTypeLog,    "log" },
    { interpolationTypeOld,    "old" },
};


ElectricSheepStuff::ElectricSheepStuff()
{
    filterShape          = shapeGaussian;
    temporalFilterType   = temporalBox;
    interpolation        = interpolationSmooth;
    paletteInterpolation = paletteHSV;
    paletteMode          = modeLinear;
    interpolationType    = interpolationTypeLinear;
    
    filter               = 0.2f;
    hasMask              = 0;
    temporalFilterWidth  = 1.f;
    temporalFilterExp    = 0.f;
    gammaThreshold       = 0.0f;
    supersample          = 1;
    temporalSamples      = 1000;
    passes               = 1;
    estimatorRadius      = 9;
    estimatorMinimum     = 0;
    estimatorCurve       = 0.4f;
    palette              = 0.f;
    highlightPower       = 1.f;
    defaultFuseIters     = true;
    cpuFuseIterations    = 30;
    gpuFuseIterations    = 60;
    xaosMatrix           = std::unique_ptr<XaosForXformsVector>();
}

ElectricSheepStuff::ElectricSheepStuff(const ElectricSheepStuff & other)
{
    filterShape          = other.filterShape;
    temporalFilterType   = other.temporalFilterType;
    interpolation        = other.interpolation;
    paletteInterpolation = other.paletteInterpolation;
    paletteMode          = other.paletteMode;
    interpolationType    = other.interpolationType;
    
    filter               = other.filter;
    hasMask              = other.hasMask;
    temporalFilterWidth  = other.temporalFilterWidth;
    temporalFilterExp    = other.temporalFilterExp;
    gammaThreshold       = other.gammaThreshold;
    supersample          = other.supersample;
    temporalSamples      = other.temporalSamples;
    passes               = other.passes;
    estimatorRadius      = other.estimatorRadius;
    estimatorMinimum     = other.estimatorMinimum;
    estimatorCurve       = other.estimatorCurve;
    palette              = other.palette;
    highlightPower       = other.highlightPower;
    defaultFuseIters     = other.defaultFuseIters;
    cpuFuseIterations    = other.cpuFuseIterations;
    gpuFuseIterations    = other.gpuFuseIterations;
    
    if (other.xaosMatrix)
        xaosMatrix = std::make_unique<XaosForXformsVector>(*other.xaosMatrix);
    else
        xaosMatrix = std::unique_ptr<XaosForXformsVector>();
}

void ElectricSheepStuff::deleteChildren()
{
    xaosMatrix = std::unique_ptr<XaosForXformsVector>();
}

bool ElectricSheepStuff::usesXaos()
{
    if (! xaosMatrix)
        return false;
    return xaosMatrix->usesXaos();
}

ElectricSheepStuff::ElectricSheepStuff(const std::vector<char> &data)
{
    const void * buf = data.data();
    int num    = *(int *)buf;
    
    int sizeXaos = sizeof(float) * num * num;
    
    memcpy(this, (char *)buf + sizeXaos + sizeof(int), sizeof(struct ElectricSheepStuff));
    
    xaosMatrix = std::make_unique<XaosForXformsVector>(num, num, (void *)((char *)buf + sizeof(int)));
    assert(num < 100);
    assert(xaosMatrix->num < 100);
}

std::string ElectricSheepStuff::stringForFilterShape(enum FilterShape shape)
{
    if (reverseShapes.find(shape) == reverseShapes.end())
        return "gaussian";
    return reverseShapes[shape];
}

std::string ElectricSheepStuff::stringForTemporalType(enum TemporalFilterType tft)
{
    if (reverseTemporalTypes.find(tft) == reverseTemporalTypes.end())
        return "gaussian";
    return reverseTemporalTypes[tft];
}

std::string ElectricSheepStuff::stringForPaletteMode(enum PaletteMode mode)
{
    if (reversePaletteModes.find(mode) == reversePaletteModes.end())
        return "linear";
    return reversePaletteModes[mode];
}

std::string ElectricSheepStuff::stringForPaletteInterpolation(enum PaletteInterpolation interpolation)
{
    if (reversePaletteInterpolations.find(interpolation) == reversePaletteInterpolations.end())
        return "sweep";
    return reversePaletteInterpolations[interpolation];
}

std::string ElectricSheepStuff::stringForFlameInterpolation(enum FlameInterpolation interpolation)
{
    if (reverseInterpolations.find(interpolation) == reverseInterpolations.end())
        return "linear";
    return reverseInterpolations[interpolation];
}

std::string ElectricSheepStuff::stringForInterpolationType(enum InterpolationType type)
{
    if (reverseInterpolationTypes.find(type) == reverseInterpolationTypes.end())
        return "old";
    return reverseInterpolationTypes[type];
}

enum FilterShape ElectricSheepStuff::getFilterShapeFor(const std::string & choice)
{
    if (shapes.find(choice) == shapes.end())
        return shapeGaussian;
    return (enum FilterShape)shapes[choice];
}

enum TemporalFilterType ElectricSheepStuff::getTemporalTypeFor(const std::string & choice)
{
    if (temporalTypes.find(choice) == temporalTypes.end())
        return temporalGaussian;
    return (enum TemporalFilterType)temporalTypes[choice];
}

enum PaletteMode ElectricSheepStuff::getPaletteModeFor(const std::string & choice)
{
    if (paletteModes.find(choice) == paletteModes.end())
        return modeLinear;
    return (enum PaletteMode)paletteModes[choice];
}

enum FlameInterpolation ElectricSheepStuff::getFlameInterpolationFor(const std::string & choice)
{
    if (interpolations.find(choice) == interpolations.end())
        return interpolationLinear;
    return (enum FlameInterpolation)interpolations[choice];
}

enum PaletteInterpolation ElectricSheepStuff::getPaletteInterpolationFor(const std::string & choice)
{
    if (paletteInterpolations.find(choice) == paletteInterpolations.end())
        return paletteSweep;
    return (PaletteInterpolation)paletteInterpolations[choice];
}

enum InterpolationType ElectricSheepStuff::getInterpolationTypeFor(const std::string &choice)
{
    if (interpolationTypes.find(choice) == interpolationTypes.end())
        return interpolationTypeOld;
    return (InterpolationType)interpolationTypes[choice];
}

