// Copyright 2008 Steven Brodhead, Jr.
// Copyright 2011-2014 Steven Brodhead, Sr., Centcom Inc.
// Alll rights reserved.
 
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#define NUM_ITERATIONS 100
//#define DENSITY_KERNAL_RADIUS 7
#define DENSITY_KERNAL_RADIUS_16KB 7
#define DENSITY_KERNAL_RADIUS_32KB 14
#define DENSITY_KERNAL_RADIUS_48KB 19
#define NUM_FRAMES 160
#define FRAME_RATE 30
#define BITRATE 54000000

#ifndef SUPERSAMPLE_WIDTH
#define SUPERSAMPLE_WIDTH 0.25f
#endif

#ifndef FLAMEDATA_H
#define FLAMEDATA_H

#define MAX_XFORMS 58 // We're limited to 64KB constant memory for compute capacity 1.0.
                      // All xForms must fit in this.

#define NO_RGBA_CONSTRUCTOR

#ifndef RGBA_H
#define RGBA_H

struct rgba
{
    float r;
    float g;
    float b;
    float a;
} __attribute__ ((aligned (16)));

#endif

struct xForm
{
    float a;
    float b;
    float c;
    float d;
    float e;
    float f;
    float pa;
    float pb;
    float pc;
    float pd;
    float pe;
    float pf;
    float color;
    float symmetry;
    float weight;
    float opacity;
    float var_color;
    int rotates;
} __attribute__ ((aligned (8)));

struct VariationListNode // each xform has a variable length list of active variations and each variation has its own specific variable sized varpar struct
{                        // all of the lists are concatenated into a single buffer - a separate xformUsageIndex has the offset to the xform's first variation in this list
    uint variationID;    // the numeric value identifying the variation from the variation set - NOTE id of zero is used to signify end of list
    uint varparOffset;   // the offset in varpar union list for this variation's specific varpar struct
    uint enterGroup;     // the state transition that handles entering Pre, Normal, and Post variation groups
}  __attribute__ ((aligned (16)));

struct unAnimatedxForm
{
    float a;
    float b;
    float d;
    float e;
    int rotates;
} __attribute__ ((aligned (8)));

struct FlameParams
{
    struct rgba background;
    float center[2];				//{x,y}
    float size[2];					//size/(scale*zoom)
    float scale;
    float zoom;
    float cam_yaw;
    float cam_pitch;
    float cam_perspective;
    int   clipToNDC;
    float cam_dof;
    float cam_zpos;
    float cam_x;
    float cam_y;
    float cam_z;
    float cam_fov;
    float cam_near;
    float cam_orthowide;
    float hue;
    float numBatches;
    float quality;
    float desiredQuality;
    float rotation;
    float symmetryKind;
    float brightness;
    float gamma;
    float gammaThreshold;
    float alphaGamma;
    float vibrancy;
    unsigned int   numTrans;
    unsigned int   numFinal;
    float supersampleWidth;
    int   frame;
    int   useXaos;
    int   oversample;
    float   highlightPower;
    int    estimatorRadius;			// default 7
    float  estimatorCurve;			// default 0.4
} __attribute__ ((aligned (16)));

struct Flame //  : public std::enable_shared_from_this<Flame>
{
    struct FlameParams params;
    int                numColors;
    struct xForm      *trans;
    struct xForm      *finals;
    struct rgba       *colorIndex;
    float             *colorLocations;
    float             *switchMatrix;
    //  std::vector<SharedVariationChain> xformVarChains;
    //  std::vector<SharedVariationChain> finalVarChains;
#ifdef __cplusplus
    Flame();
    Flame(int numTrans,int paletteSize, int numFinal);
    Flame(int numTrans,int paletteSize, int numFinal, int alignment);
    Flame(const Flame &other);
    Flame(const Flame &other, int alignment);
    
    void Clone(Flame** target);
    void CloneAligned(Flame** target, int alignment);
    void deleteChildren();
    void prepareSwitchMatrix (float *brick);
    ~Flame();
#endif
} __attribute__ ((aligned (16)));
#endif

__VARPAR_STRUCT_DECLS__


#define PI 3.141592653589793f

#ifndef FLAM4_KERNAL_CUH
#define FLAM4_KERNAL_CUH

struct point
{
    float x;
    float y;
    float z;
    float pal;
}  __attribute__ ((aligned (16)));
#endif

#ifndef FOR_2D
struct CameraViewProperties {
    float matrix[16];
    float yaw;
    float pitch;
    float roll;
    float perspective;
    float dof;
    float zpos;
    float cosRoll;
    float sinRoll;
    float camWidth;
    float camHeight;
    float centerX;
    float centerY;
    int   clipToNDC;
    float rotatedViewOffsetx;
    float rotatedViewOffsety;
} __attribute__ ((aligned (16)));
#endif

// so it can be precompiled as part of the build for syntax checking
#ifndef KERNEL_RUNTIME
#define WARP_SIZE 32
#define NUM_POINTS 64
#endif

#define ADD_EPSILON +epsilon
//#define ADD_EPSILON +1.e-7f
//#define ADD_EPSILON +1.e-10f
//#define ADD_EPSILON

#define WARPS_PER_BLOCK 2
#define BLOCKDIM  WARPS_PER_BLOCK*WARP_SIZE

#define RANDFLOAT() randFloat(randStates, km)
#define RANDINT()   randInt(randStates, km)

__VARIATION_INDEX_DEFINES__

unsigned int randInt(__private unsigned int *randStates, __global unsigned int *km);
float randFloat(__private unsigned int *randStates, __global unsigned int *km);

int binary_range_search(__global float* X, float key, int imin, int imax);
float curveAdjust(float x,
                  __global float* X,
                  __global float* A,
                  __global float* B,
                  __global float* C,
                  __global float* D,
                  uint cpCount);
float4 RGBtoHSV(float4 color);
float4 RGBtoHSVHueAdjusted(float4 color);
float4 HSVtoRGB(float4 color);
void iteratePoint(__constant struct VariationListNode *varUsageList,
                  __constant float *varpars,
                  __constant struct FlameParams *d_g_Flame,
                  unsigned int index,
                  __constant struct xForm* xForm,
                  uint xformIndex,
                  float epsilon,
                  __private struct point *fromPoint,
                  __private struct point *activePoint,
                  __private unsigned int *randStates,
                  __global unsigned int *km,
                  __global uint *permutations,
                  __global float4 *gradients);

unsigned int TausStep(unsigned int z, int S1, int S2, int S3, unsigned int M);
float4 read_image(__global float4 * image, int length, float index);
float4 read_imageStepMode(__global float4 * image, int length, float index);
float sinhcosh(float theta, __private float* ch);

#ifndef FOR_2D
void projectJWF(__private struct point *p, __constant struct CameraViewProperties *properties,
                     float rnd1, float rnd2);
void applyRotation(__constant struct FlameParams *d_g_Flame, __private struct point* point, float rotatedViewOffsetx, float rotatedViewOffsety);
#else
void applyRotation(__constant struct FlameParams *d_g_Flame, __private struct point* Point,
                   float cosRotation, float sinRotation);
#endif

__VARIATION_FUNCTION_PROTOTYPES__

unsigned int randInt (__private unsigned int*x, __global unsigned int *km) {
    unsigned int y, t;
    unsigned int k = *km;
    t = x[(k+7) & 0x7U];    t=t^(t<<13); y=t^(t<<9);
    t = x[(k+4) & 0x7U];    y^= t ^ (t>>7);
    t = x[(k+3) & 0x7U];    y^= t ^ (t<<3);
    t = x[(k+1) & 0x7U];    y^= t ^ (t>>10);
    t = x[k];               t = t ^ (t>>7);    y^= t ^ (t<<24);
    x[k] = y;               *km = (k+1) & 0x7U;
    return y;
}

float randFloat(__private unsigned int *randStates, __global unsigned int *km)
//This function returns a random float in [0,1] and updates seed
{
    unsigned int y = randInt(randStates, km);
    return as_float((y&0x007FFFFF)|0x3F800000)-1.0f;
}

//float curveAdjust(float x,
//                  __global float* X,
//                  __global float* A,
//                  __global float* B,
//                  __global float* C,
//                  __global float* D,
//                  uint cpCount)
//{
//    if (cpCount < 2)
//        return 1.f;
//    
//    if (x < X[0])
//        return A[0];    // return first
//    uint n = cpCount - 1;
//    for (uint j = 0; j < n; j++) {
//        if (x <= X[j+1]) {
//            float t = x - X[j];
//            return A[j] + B[j]*t + C[j]*t*t + D[j]*t*t*t;
//        }
//    }
//    return A[n]; // return last;
//}

// find which range of X holds the key, returns the left index of the containing range
int binary_range_search(__global float* X, float key, int imin, int imax)
{
    int omax = imax;
    
    // continue searching while [imin,imax] is not empty
    while (imax >= imin)
    {
        // calculate the midpoint for roughly equal partition
        int imid = (imin + imax)/2;
        if (X[imid] == key)
            // key found at index imid
            return imid;
        // determine which subarray to search
        else if (X[imid] < key) {
            // check for enclosing range  Xi < key < Xi+1 (if == binary search will find that one)
            if (imid + 1 <= omax && X[imid+1] > key)
                return imid;
            // change min index to search upper subarray
            imin = imid + 1;
        }
        else {  //   Xi > key
            // check for enclosing range  Xi-1 < key < Xi  (if == binary search will find that one)
            if (imid - 1 >= 0 && X[imid-1] < key)
                return imid - 1;
            // change max index to search lower subarray
            imax = imid - 1;
        }
    }
    // key was not found
    return -1;
}

float curveAdjust(float x,
                  __global float* X,
                  __global float* A,
                  __global float* B,
                  __global float* C,
                  __global float* D,
                  uint cpCount)
{
    if (x <= X[0])
        return A[0];    // return first

    int index = binary_range_search(X, x, 0, cpCount - 1);
    
    if (index >= 0 && index < (int)cpCount - 1) {
        float t = x - X[index];
        return A[index] + B[index]*t + C[index]*t*t + D[index]*t*t*t;
    }
    return A[cpCount - 1]; // return last;
}

float4 RGBtoHSV(float4 color)
{
    float r = color.x;
    float g = color.y;
    float b = color.z;
    float mx = fmax(fmax(r,g),b);
    float mn = fmin(fmin(r,g),b);
    float h,s,v;
    if (mx == mn)
        h = 0.0f;
    else if (mx == r)
        h = .16666666667f*(g-b)/(mx-mn);
    else if (mx == g)
        h = .16666666667f*(b-r)/(mx-mn)+.33333333f;
    else
        h = .16666666667f*(r-g)/(mx-mn)+.66666667f;
    h = h-floor(h);
    if (mx == 0.0f)
        s = 0.0f;
    else
        s = (mx-mn)/(mx);
    v = mx;
    if (v > 1.0f) // clamp to 1.f if to high value
        v = 1.0f;
    return (float4)(h,s,v,color.w);
}

float4 RGBtoHSVHueAdjusted(float4 color)
{
    float r = color.x;
    float g = color.y;
    float b = color.z;
    float mx = fmax(fmax(r,g),b);
    float mn = fmin(fmin(r,g),b);
    float h,s,v;
    if (mx == mn)
        h = 0.0f;
    else if (mx == r)
        h = .16666666667f*(g-b)/(mx-mn);
    else if (mx == g)
        h = .16666666667f*(b-r)/(mx-mn)+.33333333f;
    else
        h = .16666666667f*(r-g)/(mx-mn)+.66666667f;
    h = h-floor(h);
    if (mx == 0.0f)
        s = 0.0f;
    else
        s = (mx-mn)/(mx);
    v = mx;
    if (v > 1.0f)
    {
        if (h < .33333333f)
        {
            h += (.16666667f-h)*(1.0f-powr(.75f,v-1.0f));
        }
        else if (h < 0.5f)
        {
            h += (h-0.5f)*(1.0f-powr(.75f,v-1.0f));
        }
        else if (h > 0.8333333f)
        {
            h += (h-0.8333333f)*(1.0f-powr(.75f,v-1.0f));
        }
        //float l = .2126f*r+.7152f*g+.0722f*b;
        //float l = (40.0f*r+20.0f*g+b)/61.0f;
        float l = 0.4f+0.4f*cos(2.0f*PI*(h-0.16666666667f));
        s = min(s*powr(1.0f/v,0.6f*(1.0f-l)),s);
    }
    return (float4)(h,s,v,color.w);
}

float4 HSVtoRGB(float4 color)
{
    float h = color.x;
    float s = color.y;
    float v = color.z;
    float r,g,b;
    int hi = ((int)floor(h*6.0f))%6;
    float f = h*6.0f-floor(h*6.0f);
    float p = v*(1.0f-s);
    float q = v*(1.0f-f*s);
    float t = v*(1.0f-(1.0f-f)*s);
    switch (hi)
    {
        case 0:
        {
            r = v;
            g = t;
            b = p;
        }break;
        case 1:
        {
            r = q;
            g = v;
            b = p;
        }break;
        case 2:
        {
            r = p;
            g = v;
            b = t;
        }break;
        case 3:
        {
            r = p;
            g = q;
            b = v;
        }break;
        case 4:
        {
            r = t;
            g = p;
            b = v;
        }break;
        case 5:
        {
            r = v;
            g = p;
            b = q;
        }break;
    }
    return (float4)(r,g,b,color.w);
}

#ifndef __IMAGE_SUPPORT__
// for devices that do not provide OpenCL Image support like ATI 4XXX GPUs
float4 read_image(__global float4 * image, int length, float index)
{
    float clampedIndex = index - floor(index);
    float scaledIndex = clampedIndex*(float)(length - 1);
    int iLow = floor(scaledIndex);
    int iHigh = ceil(scaledIndex);
    float iFract = scaledIndex - floor(scaledIndex);
    float4 c0 = image[iLow];
    float4 c1 = image[iHigh];
    return iFract*c1+(1.0f-iFract)*c0;
}
#endif

float4 read_imageStepMode(__global float4 * image, int length, float index)
{
    float clampedIndex = index - floor(index);
    float scaledIndex = clampedIndex*(float)(length - 1);
    int iLow = floor(scaledIndex);
    return image[iLow];
}

float sinhcosh(float theta, __private float* ch)
{
    float expt = exp(theta);
    float exptinv = 1.0f / expt;
    *ch =  (expt + exptinv) * 0.5f;
    return (expt - exptinv) * 0.5f;
}

__VARIATION_FUNCTIONS__

void iteratePoint(__constant struct VariationListNode *varUsageList,
                  __constant float *varpars,
                  __constant struct FlameParams *d_g_Flame,
                  unsigned int index,
                  __constant struct xForm* xform,
                  uint xformIndex,
                  float epsilon,
                  __private struct point *fromPoint,
                  __private struct point *activePoint,
                  __private unsigned int *randStates,
                  __global unsigned int *km,
                  __global uint *permutations,
                  __global float4 *gradients)
{
    activePoint[index] = *fromPoint;
    
    float s0 = xform->symmetry;
    float s1 = .5f-.5f*s0;
    float __pal = (activePoint[index].pal+xform->color)*s1+activePoint[index].pal*s0;
    float pal0 = __pal;
    
    float __x = xform->a*activePoint[index].x+xform->b*activePoint[index].y+xform->c;
    float __y = xform->d*activePoint[index].x+xform->e*activePoint[index].y+xform->f;
#ifndef FOR_2D
    float __z = activePoint[index].z; // 3d hack does not transform them here
#endif
    float __r2, __r, __rinv, __phi, __theta;
    float __px = __x;  // note that enterGroup action will handle resetting these to zero -- also works correctly for xforms with NO variations set
    float __py = __y;
#ifndef FOR_2D
    float __pz = __z;
#endif
    
    uint varIndex = 0;
    while ((varIndex = varUsageList->variationID) != 0) {
        __constant float *varparCluster = &varpars[varUsageList->varparOffset];
        
        // apply on entry to Pre/Normal/Post groups
        if (varUsageList->enterGroup) {
            __x = __px;
            __y = __py;
#ifndef FOR_2D
            __z = __pz;
#endif
            __r2 = __x*__x+__y*__y;
            __r = sqrt(__r2);
            __rinv = 1.0f/__r;
            
            __phi = atan2(__x,__y);
            __theta = .5f*PI-__phi;
            if (__theta > PI)
                __theta -= 2.0f*PI;
            
            __px = 0.f;
            __py = 0.f;
#ifndef FOR_2D
            __pz = 0.f;
#endif
        }
        switch (varIndex) {
                //Now apply the Variations
                __VARIATION_SWITCH_CASES__
            default:
                break;
        }
        varUsageList++;
    }
    
    activePoint[index].x = xform->pa*__px+xform->pb*__py+xform->pc;
    activePoint[index].y = xform->pd*__px+xform->pe*__py+xform->pf;
#ifndef FOR_2D
    activePoint[index].z = __pz;
#endif
    //    activePoint[index].z=z;  // 3d hack does not transform them here

    if (d_g_Flame->symmetryKind != 0.0f && xformIndex < d_g_Flame->numTrans) // does not apply to final xform
    {
        if (d_g_Flame->symmetryKind > 0.0f)
        {
            float rn;
            rn = RANDFLOAT();
            float cosa;
            float sina = sincos(2.0f*PI*floor(rn*d_g_Flame->symmetryKind)/d_g_Flame->symmetryKind, &cosa);
            
            __x = cosa*activePoint[index].x-sina*activePoint[index].y;
            __y = sina*activePoint[index].x+cosa*activePoint[index].y;
            activePoint[index].x = __x;
            activePoint[index].y = __y;
        }
        else
        {
            //pick a random symmetry plane and reflect across it.
            float rn;
            float rn2;
            rn2 = RANDFLOAT();
            rn = RANDFLOAT();
            float cosa;
            float sina = sincos(2.0f*PI*floor(rn*d_g_Flame->symmetryKind)/d_g_Flame->symmetryKind, &cosa);
            
            __x = cosa*activePoint[index].x-sina*activePoint[index].y;
            __y = sina*activePoint[index].x+cosa*activePoint[index].y;
            if (rn2>0.5f)
                __x = -__x;
            activePoint[index].x = __x;
            activePoint[index].y = __y;
        }
    }
    // range of pal is [0.f, 1.f) ==> never = 1.f
    activePoint[index].pal =  fmax(0.f, fmin(pal0 + xform->var_color * (__pal - pal0), 0x1.fffffep-1f));
}

#ifndef FOR_2D
void applyRotation(__constant struct FlameParams *d_g_Flame, __private struct point* point, float rotatedViewOffsetx, float rotatedViewOffsety)
{
    point->x += rotatedViewOffsetx;
    point->y += rotatedViewOffsety;
}

void projectJWF(__private struct point *p, __constant struct CameraViewProperties *properties,
                float rnd1, float rnd2)
{
    float px, py, pz, pw;
    px = properties->matrix[0]*p->x + properties->matrix[4]*p->y + properties->matrix[8]*p->z+ properties->matrix[12];
    py = properties->matrix[1]*p->x + properties->matrix[5]*p->y + properties->matrix[9]*p->z+ properties->matrix[13];
    pz = properties->matrix[2]*p->x + properties->matrix[6]*p->y + properties->matrix[10]*p->z+ properties->matrix[14];
    pw = properties->matrix[3]*p->x + properties->matrix[7]*p->y + properties->matrix[11]*p->z+ properties->matrix[15];
    
    // handle Apophysis perspective perspective == 0.f ==> Ortho, perspective == 1.f ==> Normal Perspective
    pw  = 1.f + (pw - 1.f) * properties->perspective;

    if (properties->dof > 1.e-6f) {
        float zdist = properties->zpos - pz;
        float t     = rnd1 * 2.f * M_PI_F;
        float dr    = rnd2 * 0.1f * properties->dof * zdist;
        float cosa;
        float sina = sincos(t, &cosa);
        
        if (zdist > 0.f) {
            p->x = (px + dr*cosa)/pw;
            p->y = (py + dr*sina)/pw;
            p->z = pz/pw;
        }
        else {
            p->x = px/pw;
            p->y = py/pw;
            p->z = pz/pw;
        }
    }
    else {
        p->x = px/pw;
        p->y = py/pw;
        p->z = pz/pw;
    }
}

#else
void applyRotation(__constant struct FlameParams *d_g_Flame, __private struct point* Point, float cosRotation, float sinRotation)
{
    float x,y;
    x = Point->x-d_g_Flame->center[0];
    y = Point->y-d_g_Flame->center[1];

    Point->x = x*cosRotation - y*sinRotation + d_g_Flame->center[0];
    Point->y = x*sinRotation + y*cosRotation + d_g_Flame->center[1];
}

#endif

__kernel
void reductionKernal(__global uint* buffer,
                     __const int block,
                     __const int length,
                     __global ulong* result) {
    
    int global_index  = get_global_id(0) * block;
    ulong accumulator = 0L;
    int upper_bound   = (get_global_id(0) + 1) * block;
    if (upper_bound > length) upper_bound = length;
    while (global_index < upper_bound) {
        uint element = buffer[global_index];
        accumulator +=  element;
        global_index++;
    }
    result[get_group_id(0)] = accumulator;
}



__kernel  __attribute__((vec_type_hint(float)))
void iteratePointsKernal(__constant struct VariationListNode *d_g_varUsages,
                         __constant uint *d_g_varUsageIndexes,
                         __constant struct xForm *d_g_Xforms,
                         __constant float *varpars,
                          __constant float *d_g_switchMatrix,
                          __constant struct FlameParams *d_g_Flame,
#ifndef FOR_2D
                          __constant struct CameraViewProperties *d_g_Camera,
#endif
                          __global float4* renderTarget,
                          __global struct point* points, 
                          __global uint* pointIterations,
                          __global unsigned int* perThreadRandSeeds,
                          __global unsigned int* k,
#ifdef __IMAGE_SUPPORT__            
                         __read_only image2d_t texRef,
#endif
                          __global __read_only float4* palette, 
                          uint numColors,
                          int  paletteStepMode,
                          float epsilon,
                          uint fuseIterations,
                          int xDim,
                          int yDim,
                          __global uint *startingXform,
                          __global uint *markedCounts,
                          __global uint *pixelCounts,
                         uint xformPointPoolSize,
                         __global uint *permutations,
                         __global float4 *gradients,
                         __global uint *shuffle,
                         __global uint *iterationCount)
{
    __private struct point activePoint[BLOCKDIM];
    __private unsigned int randStates[16]; // 16 in case optimizer chooses SSE
    
    uint maxR       = d_g_Flame->numTrans - 1;
    uint blockIndex = get_group_id(1) * get_num_groups(0) + get_group_id(0);
    size_t offset   = 16*blockIndex;
    __global unsigned int *km = k + (offset>>4);
#ifdef FOR_2D
    float cosRotation;
    float sinRotation = sincos(d_g_Flame->rotation, &cosRotation);
#endif
    
    randStates[0]=perThreadRandSeeds[offset+0];
    randStates[1]=perThreadRandSeeds[offset+1];
    randStates[2]=perThreadRandSeeds[offset+2];
    randStates[3]=perThreadRandSeeds[offset+3];
    randStates[4]=perThreadRandSeeds[offset+4];
    randStates[5]=perThreadRandSeeds[offset+5];
    randStates[6]=perThreadRandSeeds[offset+6];
    randStates[7]=perThreadRandSeeds[offset+7];
    
    // want to measure the actual number of batches, suspect driver is not executing all batches
    if (blockIndex == 0)
        iterationCount[0] += 1;
    
#ifdef __IMAGE_SUPPORT__
    const sampler_t sampler = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;
#endif
    
    // Iterate some points!
    for (uint index = 0; index < BLOCKDIM; index++) {
        const int ix = (BLOCKDIM * blockIndex) + index;

        uint fromXform = startingXform[blockIndex * BLOCKDIM + index];
        uint toXform   = 0;
        for (int j = 0; j < NUM_ITERATIONS; j++)
        {
            //Pick xform for this iteration
            float w;
            w=RANDFLOAT();
            uint r = 0;
            while ((r < maxR) && (w > d_g_switchMatrix[fromXform * d_g_Flame->numTrans + r]))
            {
                r++;
            }
            toXform = r;
            
            //Now each thread chooses a point at random from the point pool.  This is done to allow each point to have a seperate xform path while retaining SIMD
            uint p              = shuffle[index + NUM_POINTS*j];
            uint fromPointIndex = fromXform * xformPointPoolSize + NUM_POINTS*blockIndex + p;
            uint toPointIndex   = toXform   * xformPointPoolSize + NUM_POINTS*blockIndex + p;
            uint iterations     = pointIterations[fromPointIndex];
            uint varUsagesIndex = d_g_varUsageIndexes[r];
            
            struct point fromPoint = points[fromPointIndex];
            
            //Iterate the chosen point and store it back to the pool
            iteratePoint(&d_g_varUsages[varUsagesIndex],
                         varpars,
                         d_g_Flame,
                         index,
                         &d_g_Xforms[r],
                         r,
                         epsilon,
                         &fromPoint,
                         activePoint,
                         randStates,
                         k,
                         permutations,
                         gradients);
            
#ifndef FOR_2D
            if (! isfinite(activePoint[index].x + activePoint[index].y + activePoint[index].z))
            {
                // test to add back a random point (ala Flam3) to get Flam3 like images in borderline cases
                activePoint[index].x   = 2.f*RANDFLOAT() - 1.f;
                activePoint[index].y   = 2.f*RANDFLOAT() - 1.f;
                activePoint[index].z   = 2.f*RANDFLOAT() - 1.f;
                iterations = 0;                
            }
#else
            if (! isfinite(activePoint[index].x + activePoint[index].y))
            {
                // test to add back a random point (ala Flam3) to get Flam3 like images in borderline cases
                activePoint[index].x   = 2.f*RANDFLOAT() - 1.f;
                activePoint[index].y   = 2.f*RANDFLOAT() - 1.f;
                iterations = 0;
            }
#endif
            ++iterations;
            struct point toPoint = activePoint[index]; // capture point state before final xform application

            if (iterations >= fuseIterations) { // dont store until fuse for each point is finished
                markedCounts[ix]++;  // keep track of number of iterations that could mark (versus unmarked because of unfused points)
                
                //Prepare the point for displey.  First the final transformation is applied
                if (d_g_Flame->numFinal > 0) {
                    uint varUsagesIndex  = d_g_varUsageIndexes[d_g_Flame->numTrans];
                    iteratePoint(&d_g_varUsages[varUsagesIndex],
                                 varpars,
                                 d_g_Flame,
                                 index,
                                 &d_g_Xforms[d_g_Flame->numTrans],
                                 d_g_Flame->numTrans,
                                 epsilon,
                                 &toPoint,
                                 activePoint,
                                 randStates,
                                 k,
                                 permutations,
                                 gradients);
                }
#ifndef FOR_2D
                projectJWF(&activePoint[index], d_g_Camera, RANDFLOAT(), RANDFLOAT());
                applyRotation(d_g_Flame, &activePoint[index], d_g_Camera->rotatedViewOffsetx, d_g_Camera->rotatedViewOffsety);
#else
                applyRotation(d_g_Flame, &activePoint[index], cosRotation, sinRotation);
#endif

                //Finally, we randomly jitter the point within a 1/2 pixel radius to obtain antialiasing
                float dr = RANDFLOAT();
                dr = exp(d_g_Flame->supersampleWidth*sqrt(-log(dr)))-1.0f;
                float rn = RANDFLOAT();
                float dtheta = (rn)*2.0f*PI;
                float z = (d_g_Flame->clipToNDC != 0) ? activePoint[index].z : 0.f;
                int x,y;
                float cosa;
                float sina = sincos(dtheta, &cosa);
                
                x = floor((( (activePoint[index].x-d_g_Flame->center[0])/d_g_Flame->size[0]+.5f)*(float)xDim)+dr*cosa);
                y = floor(((-(activePoint[index].y-d_g_Flame->center[1])/d_g_Flame->size[1]+.5f)*(float)yDim)+dr*sina);
                //And render the point to the accumulation buffer
                if ((z >= -1.f) && (z <= 1.f) && (x < xDim)&&(y < yDim)&&(x>=0)&&(y>=0))
                {
                    float4 output = (float4)(0.f, 0.f, 0.f, 0.f);
#ifdef __IMAGE_SUPPORT__
                    if (paletteStepMode)
                        output = read_imageStepMode(palette, numColors, activePoint[index].pal);
                    else
                        output = read_imagef(texRef, sampler, (float2)(activePoint[index].pal, 0.f));
#else
                    output = read_image(palette, numColors, activePoint[index].pal);
#endif
                    renderTarget[y*xDim+x] += output*d_g_Xforms[r].opacity;
                    pixelCounts[y*xDim+x]++;
                }
            }
            pointIterations[toPointIndex] = iterations;
            points[toPointIndex] = toPoint;
            fromXform = toXform;
        }
        startingXform[blockIndex * BLOCKDIM + index] = toXform;
    }
    perThreadRandSeeds[offset+0]=randStates[0];
    perThreadRandSeeds[offset+1]=randStates[1];
    perThreadRandSeeds[offset+2]=randStates[2];
    perThreadRandSeeds[offset+3]=randStates[3];
    perThreadRandSeeds[offset+4]=randStates[4];
    perThreadRandSeeds[offset+5]=randStates[5];
    perThreadRandSeeds[offset+6]=randStates[6];
    perThreadRandSeeds[offset+7]=randStates[7];
}

__kernel void postProcessStep1Kernal(__constant struct FlameParams *d_g_Flame,
                                     __global float4* renderTarget,
                                     __global float4* accumBuffer,
                                     uint xDim,
                                     uint yDim,
                                     int blocksY,
                                     float fuseCompensation)
{
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                float k1 = (d_g_Flame->brightness*268.0f)/255.0f;
                float area = fabs(d_g_Flame->size[0]*d_g_Flame->size[1]);
                float k2 = ((float)(xDim*yDim))/(area*fuseCompensation*((float)(NUM_ITERATIONS))*d_g_Flame->numBatches*32.f*1024.0f*((float)blocksY/32.f));
                float4 rgba = accumBuffer[iy*xDim+ix];
                float a = (k1* log(1.0f+k2*rgba.w));
                float ls = a/rgba.w;
                rgba.x = ls*rgba.x;
                rgba.y = ls*rgba.y;
                rgba.z = ls*rgba.z;
                
                accumBuffer[iy*xDim+ix] = rgba;
            }
        }
    }
}

__kernel void postProcessStep2Kernal(__constant struct FlameParams *d_g_Flame,
                                     __global float4* renderTarget,
                                     __global float4* accumBuffer,
                                     uint xDim,
                                     uint yDim,
                                     int blocksY,
                                     float fuseCompensation,
                                     float4 adjust)
{
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                float k1 = (d_g_Flame->brightness*268.0f)/255.0f;
                float area = fabs(d_g_Flame->size[0]*d_g_Flame->size[1]);
                float _k2 = ((float)(xDim*yDim))/
                            (area*fuseCompensation*((float)(NUM_ITERATIONS))*d_g_Flame->numBatches*32.f*1024.0f*((float)blocksY/32.f));
                float gammaThreshold = d_g_Flame->gammaThreshold;
                float gamma          = d_g_Flame->gamma;
                float alphaGamma     = d_g_Flame->alphaGamma;
                
                float4 k2   = _k2/adjust;
                float4 rgba = accumBuffer[iy*xDim+ix];
                float4 a    = (k1* log(1.0f+k2*rgba.w));
                
                float4 fraction = a/gammaThreshold;
                float4 alpha    = powr(a, 1.0f/gamma-1.0f);
                float4 alphaT   = (1.f - fraction) * a * (powr(gammaThreshold, gamma)/gammaThreshold) + fraction * alpha;

                float4 ls   = d_g_Flame->vibrancy*select(alpha, alphaT, a < gammaThreshold);
                float4 sign = select((float4)(-1.f), (float4)(1.f), rgba >= 0.f);
                rgba        = ls*rgba + (1.0f-d_g_Flame->vibrancy)*sign*powr(fabs(rgba), 1.0f/gamma);
                
                alpha       = powr(a, 1.0f/gamma);
                alphaT      = (1.f - fraction) * a * (powr(gammaThreshold, gamma)/gammaThreshold) + fraction * alpha;
                alpha       = min(select(alpha, alphaT, a < gammaThreshold), 1.0f);
                
                float4 alphaC  = powr(a, 1.0f/alphaGamma);
                float4 alphaTC = (1.f - fraction) * a * (powr(gammaThreshold, alphaGamma)/gammaThreshold) + fraction * alphaC;
                alphaC         = min(select(alphaC, alphaTC, a < gammaThreshold), 1.0f);
                
                if (d_g_Flame->highlightPower >= 0.f) {
                    rgba = RGBtoHSVHueAdjusted(rgba);
                    if (rgba.z > 1.0f)
                    {
                        //rgba.y /= rgba.z;
                        rgba.z = 1.0f;
                    }
                    rgba = HSVtoRGB(rgba);
                }
                if (isfinite(rgba.x))
                {
                    renderTarget[iy*xDim+ix].x=rgba.x+renderTarget[iy*xDim+ix].x*(1.0f-alpha.x);
                    renderTarget[iy*xDim+ix].y=rgba.y+renderTarget[iy*xDim+ix].y*(1.0f-alpha.y);
                    renderTarget[iy*xDim+ix].z=rgba.z+renderTarget[iy*xDim+ix].z*(1.0f-alpha.z);
                    renderTarget[iy*xDim+ix].w=alphaC.w;
                }
                else {
                    renderTarget[iy*xDim+ix].w=0.f;
                }
            }
        }
    }
}

__kernel void colorCurveRGB3ChannelsKernal(__global float4* accumBuffer,
                                           __global float* X,
                                           __global float* A,
                                           __global float* B,
                                           __global float* C,
                                           __global float* D,
                                           uint xDim,
                                           uint yDim,
                                           uint cpCount)
{
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                float4 rgba = accumBuffer[iy*xDim+ix];
                // sRGB luma   0.212656 R  0.715158 G  0.072186 B
                // AdobeRGB luma
                float preluma  = 0.297361f * rgba.x + 0.627355f * rgba.y + 0.075285f * rgba.z;
                preluma       /= rgba.w;
                float postluma = curveAdjust(preluma, X, A, B, C, D, cpCount);
                if (preluma != 0.f) {
                    rgba.x = postluma/preluma * rgba.x;
                    rgba.y = postluma/preluma * rgba.y;
                    rgba.z = postluma/preluma * rgba.z;
                    rgba.w = postluma/preluma * rgba.w;
                }
                else {
                    rgba.x = postluma * rgba.w;
                    rgba.y = postluma * rgba.w;
                    rgba.z = postluma * rgba.w;
                    rgba.w = postluma;
                }
                accumBuffer[iy*xDim+ix] = rgba;
            }
        }
    }
}

__kernel void colorCurveRGBChannelKernal(__global float4* accumBuffer,
                                         __global float* X,
                                         __global float* A,
                                         __global float* B,
                                         __global float* C,
                                         __global float* D,
                                         uint xDim,
                                         uint yDim,
                                         uint cpCount,
                                         uint channel)
{
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                float4 rgba = accumBuffer[iy*xDim+ix];
                
                float preluma  = 0.297361f * rgba.x + 0.627355f * rgba.y + 0.075285f * rgba.z;
                switch (channel) {
                    default:
                    case 0:
                        rgba.x = rgba.w * curveAdjust(rgba.x/rgba.w, X, A, B, C, D, cpCount);
                        break;
                    case 1:
                        rgba.y = rgba.w * curveAdjust(rgba.y/rgba.w, X, A, B, C, D, cpCount);
                        break;
                    case 2:
                        rgba.z = rgba.w * curveAdjust(rgba.z/rgba.w, X, A, B, C, D, cpCount);
                        break;
                    case 3:
                        break;
                }                
                float postluma  = 0.297361f * rgba.x + 0.627355f * rgba.y + 0.075285f * rgba.z;
                // maintain same luminance afterwards
                if (preluma != 0)
                    rgba.w *= postluma/preluma;
                else
                    rgba.w  = postluma;

                accumBuffer[iy*xDim+ix] = rgba;
            }
        }
    }
}

__kernel void setBufferKernal(__global float4* renderTarget, float4 value, uint xDim, uint yDim)
{
    //This kernal simply fills the render target with value
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
                renderTarget[iy*xDim+ix] = value;
        }
    }
}

#define DIAMETER (2*DENSITY_KERNAL_RADIUS+1)

__kernel void FlexibleDensityEstimationKernal(__global float4* output, __global float4* input, unsigned int xDim, unsigned int yDim, float baseThreshold, int radius)
{
    __private float part1[DIAMETER*DIAMETER];
    
    for (int y1 = 0; y1 < (2*radius+1); y1++)
        for (int x1 = 0; x1 < (2*radius+1); x1++) {
            float invDist1 = 1.0f/((x1-7)*(x1-7)+(y1-7)*(y1-7)+1.0f);
            part1[y1*(2*radius+1)+x1] = powr(baseThreshold*.9f,sqrt(1.0f/invDist1));
        }
    
    for (uint indexY = 0; indexY < 16; indexY++) {
        for (uint indexX = 0; indexX < 16; indexX++) {
            const int ix = (16*get_group_id(0))+indexX;
            const int iy = (16*get_group_id(1))+indexY;
            //Next, apply the actual filter
            if ((ix < (int)xDim)&& (iy < (int)yDim))
            {
                float4 pnt = input[ix+iy*xDim];
                float4 sum = (float4)(0.0f,0.0f,0.0f,0.0f);
                float count = 0.0f;
                for (int y = -radius; y <= radius; y++)
                {
                    for (int x = -radius; x <= radius; x++)
                    {
                        int nx = min(max(ix+x,0),(int)xDim-1);
                        int ny = min(max(iy+y,0),(int)yDim-1);
                        int cellId = nx+xDim*ny;
                        float invDist = 1.0f/(x*x+y*y+1.0f);
                        
                        float delta = fabs(input[cellId].w-pnt.w)/(sqrt(8.0f*pnt.w)+5.0f);
                        float deviation = 1.f; // uses linear approximation for erf
                        if (delta < 0.5f)
                            deviation = 1.128379*delta;
                        else if (delta < 1.5f) {
                            delta -= 1.f;
                            deviation = 0.8427008f + 0.4151075f*delta - 0.4151075f*delta*delta;
                        }
                        
                        //                      float deviation = fabs(erf((fabs(input[cellId].w-pnt.w))/(sqrt(8.0f*pnt.w)+5.0f))); Uses erf function
                        
                        if (input[cellId].w-pnt.w==0.0f) deviation=0.0f;
                        
                        //                        if (deviation<=half_powr(baseThreshold*.9f,sqrt(1.0f/invDist))*half_powr(pnt.w+1.0f,-0.25f)) // 10.29sec
                        if (deviation<=part1[(y+7)*(2*radius+1)+(x+7)]/(sqrt(sqrt(pnt.w+1.0f)))) // 8.09sec
                        {
                            sum.x += input[cellId].x*invDist;
                            sum.y += input[cellId].y*invDist;
                            sum.z += input[cellId].z*invDist;
                            sum.w += input[cellId].w*invDist;
                            count += invDist;
                        }
                    }
                }
                sum.x/=count;
                sum.y/=count;
                sum.z/=count;
                sum.w/=count;
                //And store the result
                output[ix+xDim*iy] = sum;
            }
        }
    }
}

__kernel void RGBA128FtoRGBA32UKernal(__global uchar4* output, __global float4* input, uint xDim, uint yDim, int useAlpha)
{
    //This kernal converts a 32bit per channel floating point image to a 8bit per channel integer image
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                if (useAlpha)
                {
//                    // straight gamma
//                    if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w) && (input[iy*xDim+ix].w!=0.0f))
//                        output[iy*xDim+ix] = (uchar4)(
//                                                         max(min(input[iy*xDim+ix].x/input[iy*xDim+ix].w,1.0f),0.0f)*255.0f,
//                                                         max(min(input[iy*xDim+ix].y/input[iy*xDim+ix].w,1.0f),0.0f)*255.0f,
//                                                         max(min(input[iy*xDim+ix].z/input[iy*xDim+ix].w,1.0f),0.0f)*255.0f,
//                                                         max(min(input[iy*xDim+ix].w,1.0f),0.0f)*255.0f);
                    // premultiplied gamma
                    if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w) && (input[iy*xDim+ix].w!=0.0f)) {
                        float alpha = max(min(input[iy*xDim+ix].w,1.0f),0.0f)*255.0f;
                        output[iy*xDim+ix] = (uchar4)(
                                                         max(min(input[iy*xDim+ix].x/input[iy*xDim+ix].w,1.0f),0.0f)*alpha,
                                                         max(min(input[iy*xDim+ix].y/input[iy*xDim+ix].w,1.0f),0.0f)*alpha,
                                                         max(min(input[iy*xDim+ix].z/input[iy*xDim+ix].w,1.0f),0.0f)*alpha,
                                                         alpha);
                    }
                    else
                        output[iy*xDim+ix]= (uchar4)(0,0,0,0);
                }
                else
                {
                    if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w))
                        output[iy*xDim+ix] = (uchar4)(
                                                         max(min(input[iy*xDim+ix].x,1.0f),0.0f)*255.0f,
                                                         max(min(input[iy*xDim+ix].y,1.0f),0.0f)*255.0f,
                                                         max(min(input[iy*xDim+ix].z,1.0f),0.0f)*255.0f,
                                                         255);
                    else
                        output[iy*xDim+ix]= (uchar4)(0,0,0,255);
                }
            }
        }
    }
}

__kernel void RGBA128FtoBGRA32UKernal(__global uchar4* output, __global float4* input, uint xDim, uint yDim)
{
    //This kernal converts a 32bit per channel floating point image to a 8bit per channel integer image
    // in BGRA format for little endian Intel with premultiplied alpha
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w))
                    output[iy*xDim+ix] = (uchar4)(
                                                     max(min(input[iy*xDim+ix].z,1.0f),0.0f)*255.0f, // blue
                                                     max(min(input[iy*xDim+ix].y,1.0f),0.0f)*255.0f, // green
                                                     max(min(input[iy*xDim+ix].x,1.0f),0.0f)*255.0f, // red
                                                     255);
                else
                    output[iy*xDim+ix]= (uchar4)(0,0,0,255);
            }
        }
    }
}

__kernel void RGBA128FtoRGBA64UKernal(__global ushort4* output, __global float4* input, uint xDim, uint yDim, int useAlpha)
{
    //This kernal converts a 32bit per channel floating point image to a 16bit per channel integer image
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                if (useAlpha)
                {
//                    // straight gamma
//                    if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w) && (input[iy*xDim+ix].w!=0.0f))
//                        output[iy*xDim+ix] = (ushort4)(
//                                                         max(min(input[iy*xDim+ix].x/input[iy*xDim+ix].w,1.0f),0.0f)*65535.0f,
//                                                         max(min(input[iy*xDim+ix].y/input[iy*xDim+ix].w,1.0f),0.0f)*65535.0f,
//                                                         max(min(input[iy*xDim+ix].z/input[iy*xDim+ix].w,1.0f),0.0f)*65535.0f,
//                                                         max(min(input[iy*xDim+ix].w,1.0f),0.0f)*65535.0f);
                    // premultiplied gamma
                    if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w) && (input[iy*xDim+ix].w!=0.0f)) {
                        float alpha = max(min(input[iy*xDim+ix].w,1.0f),0.0f)*65535.0f;
                        output[iy*xDim+ix] = (ushort4)(
                                                         max(min(input[iy*xDim+ix].x/input[iy*xDim+ix].w,1.0f),0.0f)*alpha,
                                                         max(min(input[iy*xDim+ix].y/input[iy*xDim+ix].w,1.0f),0.0f)*alpha,
                                                         max(min(input[iy*xDim+ix].z/input[iy*xDim+ix].w,1.0f),0.0f)*alpha,
                                                         alpha);
                    }
                    else
                        output[iy*xDim+ix]= (ushort4)(0,0,0,0);
                }
                else
                {
                    if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w))
                        output[iy*xDim+ix] = (ushort4)(
                                                         max(min(input[iy*xDim+ix].x,1.0f),0.0f)*65535.0f,
                                                         max(min(input[iy*xDim+ix].y,1.0f),0.0f)*65535.0f,
                                                         max(min(input[iy*xDim+ix].z,1.0f),0.0f)*65535.0f,
                                                         65535);
                    else
                        output[iy*xDim+ix]= (ushort4)(0,0,0,65535);
                }
            }
        }
    }
}

__kernel void RGBA128FtoRGBA128FKernal(__global float4* output, __global float4* input, uint xDim, uint yDim, int useAlpha)
{
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                if (useAlpha)
                {
//                    // straight gamma
//                    if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w) && (input[iy*xDim+ix].w!=0.0f))
//                        output[iy*xDim+ix] = (float4)(
//                                                         max(min(input[iy*xDim+ix].x/input[iy*xDim+ix].w,1.0f),0.0f),
//                                                         max(min(input[iy*xDim+ix].y/input[iy*xDim+ix].w,1.0f),0.0f),
//                                                         max(min(input[iy*xDim+ix].z/input[iy*xDim+ix].w,1.0f),0.0f),
//                                                         max(min(input[iy*xDim+ix].w,1.0f),0.0f));
                    // premultiplied gamma
                    if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w) && (input[iy*xDim+ix].w!=0.0f)) {
                        float alpha = max(min(input[iy*xDim+ix].w,1.0f),0.0f);
                        output[iy*xDim+ix] = (float4)(
                                                         max(min(input[iy*xDim+ix].x/input[iy*xDim+ix].w,1.0f),0.0f)*alpha,
                                                         max(min(input[iy*xDim+ix].y/input[iy*xDim+ix].w,1.0f),0.0f)*alpha,
                                                         max(min(input[iy*xDim+ix].z/input[iy*xDim+ix].w,1.0f),0.0f)*alpha,
                                                         alpha);
                    }
                    else
                        output[iy*xDim+ix]= (float4)(0.f,0.f,0.f,0.f);
                }
                else
                {
                    if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w))
                        output[iy*xDim+ix] = (float4)(
                                                         max(min(input[iy*xDim+ix].x,1.0f),0.0f),
                                                         max(min(input[iy*xDim+ix].y,1.0f),0.0f),
                                                         max(min(input[iy*xDim+ix].z,1.0f),0.0f),
                                                         1.f);
                    else
                        output[iy*xDim+ix]= (float4)(0.f,0.f,0.f,1.f);
                }
            }
        }
    }
}

__kernel void MergeKernal(__global float4* accum, __global float4* input, uint xDim, uint yDim)
{
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                accum[iy*xDim+ix].x += input[iy*xDim+ix].x;
                accum[iy*xDim+ix].y += input[iy*xDim+ix].y;
                accum[iy*xDim+ix].z += input[iy*xDim+ix].z;
                accum[iy*xDim+ix].w += input[iy*xDim+ix].w;
            }
        }
    }
}

__kernel void readChannelKernel(__global float* output, __global float4* input, uint xDim, uint yDim, uint channel)
{
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                switch (channel) {
                    default:
                    case 0:
                        output[iy*xDim+ix] = input[iy*xDim+ix].x;
                        break;
                    case 1:
                        output[iy*xDim+ix] = input[iy*xDim+ix].y;
                        break;
                    case 2:
                        output[iy*xDim+ix] = input[iy*xDim+ix].z;
                        break;
                    case 3:
                        output[iy*xDim+ix] = input[iy*xDim+ix].w;
                        break;
                }
            }
        }
    }
}

__kernel void writeChannelKernel(__global float4* output, __global float* input, uint xDim, uint yDim, uint channel)
{
    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim)&&(iy < yDim))
            {
                switch (channel) {
                    default:
                    case 0:
                        output[iy*xDim+ix].x = input[iy*xDim+ix];
                        break;
                    case 1:
                        output[iy*xDim+ix].y = input[iy*xDim+ix];
                        break;
                    case 2:
                        output[iy*xDim+ix].z = input[iy*xDim+ix];
                        break;
                    case 3:
                        output[iy*xDim+ix].w = input[iy*xDim+ix];
                        break;
                }
            }
        }
    }
}

__kernel void writeChannelStripedKernel(__global float4* output,
                                        __global float* input,
                                        uint xDim,
                                        uint yDim,
                                        uint channel,
                                        uint supersample)
{
    const uint resampledXdim = xDim / supersample;

    for (int indexX = 0; indexX < 16; indexX++) {
        for (int indexY = 0; indexY < 16; indexY++) {
            const uint ix = (16*get_group_id(0))+indexX;
            const uint iy = (16*get_group_id(1))+indexY;
            if ((ix < xDim) && (iy < yDim) && (ix % supersample == 0) && (iy % supersample == 0))
            {
                const uint x = ix / supersample;
                const uint y = iy / supersample;
                switch (channel) {
                    default:
                    case 0:
                        output[y*resampledXdim+x].x = input[iy*xDim+ix];
                        break;
                    case 1:
                        output[y*resampledXdim+x].y = input[iy*xDim+ix];
                        break;
                    case 2:
                        output[y*resampledXdim+x].z = input[iy*xDim+ix];
                        break;
                    case 3:
                        output[y*resampledXdim+x].w = input[iy*xDim+ix];
                        break;
                }
            }
        }
    }
}

/*
 * Copyright 1993-2007 NVIDIA Corporation.  All rights reserved.
 * OpenCL port & resampling kernels Copyright 2014 Centcom Inc. All rights reserved.
 *
 * NOTICE TO USER:
 *
 * This source code is subject to NVIDIA ownership rights under U.S. and
 * international Copyright laws.  Users and possessors of this source code
 * are hereby granted a nonexclusive, royalty-free license to use this code
 * in individual and commercial software.
 *
 * NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE
 * CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR
 * IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
 * IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS,  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION,  ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOURCE CODE.
 *
 * U.S. Government End Users.   This source code is a "commercial item" as
 * that term is defined at  48 C.F.R. 2.101 (OCT 1995), consisting  of
 * "commercial computer  software"  and "commercial computer software
 * documentation" as such terms are  used in 48 C.F.R. 12.212 (SEPT 1995)
 * and is provided to the U.S. Government only as a commercial end item.
 * Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through
 * 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the
 * source code with only those rights set forth herein.
 *
 * Any use of this source code in individual and commercial software must
 * include, in the user documentation and internal comments to the code,
 * the above Disclaimer and U.S. Government End Users Notice.
 */

// Assuming ROW_TILE_W, KERNEL_RADIUS_ALIGNED and dataW
// are multiples of coalescing granularity size,
// all global memory operations are coalesced in convolutionRowGPU()
#define            ROW_TILE_W 128
#define KERNEL_RADIUS_ALIGNED 16

// Assuming COLUMN_TILE_W and dataW are multiples
// of coalescing granularity size, all global memory operations
// are coalesced in convolutionColumnGPU()
#define COLUMN_TILE_W 16
#define COLUMN_TILE_H 48

__kernel void convolutionRowsKernel(__constant float *d_Kernel,
                                    __global float *d_Result,
                                    __global float *d_Data,
                                    int dataW,
                                    int dataH,
                                    int KERNEL_RADIUS
                                    ){
    //Data cache
    __private float data[KERNEL_RADIUS_ALIGNED + ROW_TILE_W + KERNEL_RADIUS_ALIGNED];
    
    const uint BLOCKDIM_ROW_X = KERNEL_RADIUS_ALIGNED + ROW_TILE_W + KERNEL_RADIUS_ALIGNED;
    
    //Current tile and apron limits, relative to row start
    const int         tileStart = get_group_id(0) * ROW_TILE_W;
    const int           tileEnd = tileStart + ROW_TILE_W - 1;
    const int        apronStart = tileStart - KERNEL_RADIUS;
    const int          apronEnd = tileEnd   + KERNEL_RADIUS;
    
    //Clamp tile and apron limits by image borders
    const int    tileEndClamped = min(tileEnd, dataW - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataW - 1);
    
    //Row start index in d_Data[]
    const int          rowStart = get_group_id(1) * dataW;
    
    //Aligned apron start. Assuming dataW and ROW_TILE_W are multiples
    //of half-warp size, rowStart + apronStartAligned is also a
    //multiple of half-warp size, thus having proper alignment
    //for coalesced d_Data[] read.
    const int apronStartAligned = tileStart - KERNEL_RADIUS_ALIGNED;

    for (uint indexY = 0; indexY < 1; indexY++) {
        for (uint indexX = 0; indexX < BLOCKDIM_ROW_X; indexX++) {
            
            
            const int loadPos = apronStartAligned + indexX;
            //Set the entire data cache contents
            //Load global memory values, if indices are within the image borders,
            //or initialize with zeroes otherwise
            if(loadPos >= apronStart){
                const int smemPos = loadPos - apronStart;
                
                // out of bounds set to 0
                // data[smemPos] =
                //     ((loadPos >= apronStartClamped) && (loadPos <= apronEndClamped)) ? d_Data[rowStart + loadPos] : 0;
                
                // reflected at boundary
                // data[smemPos] =
                // loadPos < apronStartClamped ? d_Data[rowStart + apronStartClamped + apronStartClamped - loadPos] :
                //  (loadPos > apronEndClamped ? d_Data[rowStart + apronEndClamped   + apronEndClamped   - loadPos] :
                //     d_Data[rowStart + loadPos]);
                
                // clamp to border
                data[smemPos] =
                 loadPos < apronStartClamped ? d_Data[rowStart + apronStartClamped] :
                (loadPos > apronEndClamped ? d_Data[rowStart + apronEndClamped] :
                    d_Data[rowStart + loadPos]);
            }
        }
    }
    
    //Ensure the completness of the loading stage
    //because results, emitted by each thread depend on the data,
    //loaded by another threads
    for (uint indexY = 0; indexY < 1; indexY++) {
        for (uint indexX = 0; indexX < BLOCKDIM_ROW_X; indexX++) {
            const int writePos = tileStart + indexX;
            
            //Assuming dataW and ROW_TILE_W are multiples of half-warp size,
            //rowStart + tileStart is also a multiple of half-warp size,
            //thus having proper alignment for coalesced d_Result[] write.
            if(writePos <= tileEndClamped){
                const int smemPos = writePos - apronStart;
                float sum = 0;
                
                for(int k = -KERNEL_RADIUS; k <= KERNEL_RADIUS; k++)
                    sum += data[smemPos + k] * d_Kernel[KERNEL_RADIUS - k];
                
                d_Result[rowStart + writePos] = sum;
            }
        }
    }
}

__kernel void convolutionColumnsKernel(__constant float *d_Kernel,
                                       __global float *d_Result,
                                       __global float *d_Data,
                                       int dataW,
                                       int dataH,
                                       int smemStride,
                                       int gmemStride,
                                       int KERNEL_RADIUS
                                       ){
    //Data cache
    __private float data[COLUMN_TILE_W * (KERNEL_RADIUS_ALIGNED + COLUMN_TILE_H + KERNEL_RADIUS_ALIGNED)];
    
    const uint COLUMN_TILE_BLOCK_YDIM = 8;
    
    //Current tile and apron limits, in rows
    const int         tileStart = get_group_id(1) * COLUMN_TILE_H;
    const int           tileEnd = tileStart + COLUMN_TILE_H - 1;
    const int        apronStart = tileStart - KERNEL_RADIUS;
    const int          apronEnd = tileEnd   + KERNEL_RADIUS;
    
    //Clamp tile and apron limits by image borders
    const int    tileEndClamped = min(tileEnd, dataH - 1);
    const int apronStartClamped = max(apronStart, 0);
    const int   apronEndClamped = min(apronEnd, dataH - 1);

    for (uint indexY = 0; indexY < COLUMN_TILE_H; indexY++) {
        for (uint indexX = 0; indexX < COLUMN_TILE_W; indexX++) {
            //Current column index
            const int columnStart = get_group_id(0) * COLUMN_TILE_W + indexX;
            
            //Shared and global memory indices for current column
            int smemPos    = indexY * COLUMN_TILE_W + indexX;
            int gmemPos    = (apronStart + indexY) * dataW + columnStart;
            int gmemPosMin = columnStart;
            int gmemPosMax = columnStart + dataW * (dataH - 1);
            
            //Cycle through the entire data cache
            //Load global memory values, if indices are within the image borders,
            //or initialize with zero otherwise
            for(int y = apronStart + indexY; y <= apronEnd; y += COLUMN_TILE_BLOCK_YDIM){
                // out of bounds set to 0
                //data[smemPos] =
                //    ((y >= apronStartClamped) && (y <= apronEndClamped)) ? d_Data[gmemPos] : 0;
                
                // reflected at boundary
                // data[smemPos] =
                //     y < apronStartClamped ? d_Data[gmemPosMin + (apronStartClamped - y) * dataW] :
                //    (y > apronEndClamped   ? d_Data[gmemPosMax + (apronEndClamped   - y) * dataW] : d_Data[gmemPos]);
                
                // clamp to border
                data[smemPos] =
                y < apronStartClamped ? d_Data[gmemPosMin] :
                (y > apronEndClamped   ? d_Data[gmemPosMax] : d_Data[gmemPos]);

                smemPos += smemStride;
                gmemPos += gmemStride;
            }
        }
    }
    
    //Ensure the completness of the loading stage
    //because results, emitted by each thread depend on the data,
    //loaded by another threads
    for (uint indexY = 0; indexY < COLUMN_TILE_H; indexY++) {
        for (uint indexX = 0; indexX < COLUMN_TILE_W; indexX++) {
            //Current column index
            const int columnStart = get_group_id(0) * COLUMN_TILE_W + indexX;
            
            //Shared and global memory indices for current column
            int smemPos = (indexY + KERNEL_RADIUS) * COLUMN_TILE_W + indexX;
            int gmemPos = (tileStart + indexY) * dataW + columnStart;
            
            //Cycle through the tile body, clamped by image borders
            //Calculate and output the results
            for(int y = tileStart + indexY; y <= tileEndClamped; y += COLUMN_TILE_BLOCK_YDIM){
                float sum = 0;
                
                for(int k = -KERNEL_RADIUS; k <= KERNEL_RADIUS; k++)
                    sum += data[smemPos + k * COLUMN_TILE_W] * d_Kernel[KERNEL_RADIUS - k];
                
                d_Result[gmemPos] = sum;
                smemPos += smemStride;
                gmemPos += gmemStride;
            }
        }
    }
}

