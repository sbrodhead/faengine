//
//  CudaDeviceKind.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/7/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "CudaDeviceKind.hpp"
#include "CudaContext.hpp"
#include "CudaProgram.hpp"
#include "VariationSet.hpp"
#include "Utilities.hpp"
#include "jsoncpp/json/json.h"

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif

#include <iostream>

CudaDeviceKind::CudaDeviceKind(const std::string & _kind, const std::string & _vendor, SharedDeviceContext _cudaContext, CUcontext _cucontext)
: DeviceKind(_kind, _vendor, _cudaContext), cucontext(_cucontext)
{}

uint CudaDeviceKind::determineWarpSizeForCudaDevice(CUdevice device)
{
    int warpsize = 0;
    
    cuDeviceGetAttribute(&warpsize, CU_DEVICE_ATTRIBUTE_WARP_SIZE, device);
    return warpsize;
}

CUdevice CudaDeviceKind::cudaDeviceForInstanceIndex(size_t index)
{
    if (cudaDeviceInstances.size() == 0)
        return (CUdevice)0;
    return (CUdevice)cudaDeviceInstances[index];
}

bool CudaDeviceKind::createContext()
{
    return true;
}

void CudaDeviceKind::setupSubDevicesFor(void * cpuDeviceID)
{}


#pragma mark Variation Set Support

CudaProgram * CudaDeviceKind::programForVariationSetUuid(const std::string & variationSetUuid, duration<double> & buildTime)
{
    SharedDeviceProgram & program = programs[variationSetUuid];
    
    if (! program)
        return makeProgramFromSource(VariationSet::variationSetForUuid(variationSetUuid), false, buildTime);
    else
        return dynamic_cast<CudaProgram *>(program.get());
}

CudaProgram * CudaDeviceKind::programForVariationSet(const SharedVariationSet & variationSet, duration<double> & buildTime)
{
    SharedDeviceProgram & _program = programs[variationSet->uuid];
    CudaProgram *program             = static_cast<CudaProgram *>(_program.get());
    if (! program) {
        program = makeProgramFromSource(variationSet, false, buildTime);
        return program;
    }
    else {
        buildTime = duration<double>(0.);
        return program;
    }
}

void CudaDeviceKind::removeProgramForVariationSetUuid(std::string &variationSetUuid)
{
    programs.erase(variationSetUuid);
}

CudaProgram * CudaDeviceKind::makeProgramFromSource(const SharedVariationSet & variationSet, bool forceRebuild, duration<double> & buildTime)
{
    SharedCudaContext cudaContext = std::dynamic_pointer_cast<CudaContext>(deviceContext.lock());
    
    std::shared_ptr<CudaProgram> cudaProgram = std::make_shared<CudaProgram>(cudaContext, shared_from_this(), cucontext, forceRebuild, variationSet);
    programs[variationSet->uuid]         = cudaProgram;
    buildTime                            = cudaProgram->buildTime;
    return cudaProgram.get();
}

#pragma mark Serialization

Json::Value CudaDeviceKind::makeJson()
{
    Json::Value value(Json::objectValue);
    
    value["name"]      = kind;
    value["OSVersion"] = osVersion();
    value["uuid"]      = stringWithUUID();
    return value;
}

Json::Value CudaDeviceKind::makeJsonForDeviceKindWithUuid(const std::string & uuid)
{
    Json::Value value(Json::objectValue);
    
    value["name"]      = kind;
    value["OSVersion"] = osVersion();
    value["uuid"]      = uuid;
    return value;
}

// find Json for binary that may or may not be valid for the current OS version, returns nil if no Json entry exists for that name
Json::Value & CudaDeviceKind::existingJsonForDeviceName(const std::string & name, Json::Value & kindsJson)
{
    return kindsJson[name];
}

// find Json for binary that may or may not be valid for the current OS version, returns nil if no Json found
Json::Value & CudaDeviceKind::existingJsonForDeviceName(const std::string & name)
{
    Json::Value kindsJson = DeviceProgram::readDevicesJson();
    return CudaDeviceKind::existingJsonForDeviceName(name, kindsJson);
}

Json::Value & CudaDeviceKind::existingJson()
{
    return CudaDeviceKind::existingJsonForDeviceName(kind);
}

bool CudaDeviceKind::JsonIsValid(Json::Value & Json)
{
    double version = Json["OSVersion"].asDouble();
    return version == osVersion();
}

enum DeviceKindJsonStatus CudaDeviceKind::checkJsonStatus(Json::Value & kindsJson)
{
    if (! kindsJson)
        return noJsonFile;
    
    Json::Value & Json = CudaDeviceKind::existingJsonForDeviceName(kind, kindsJson);
    if (Json.isNull())
        return noJsonEntry;
    if (! CudaDeviceKind::JsonIsValid(Json))
        return binaryInvalid;
    return jsonOK;
}

// process the device kind and return its uuid
const std::string CudaDeviceKind::processDeviceKind()
{
    Json::Value json;;
    Json::Value kindsJson = DeviceProgram::readDevicesJson();
    
    switch (checkJsonStatus(kindsJson)) {
        case noJsonFile:
        {
            kindsJson = Json::Value();
            json      = makeJson();
            kindsJson[kind] = json;
            CudaProgram::writeDevicesJson(kindsJson);
            rebuild = true;
        }
            break;
        case noJsonEntry:
        {
            json = makeJson();
            kindsJson[kind] = json;
            CudaProgram::writeDevicesJson(kindsJson);
            rebuild = true;
        }
            break;
        case binaryInvalid:
        {
            json            = CudaDeviceKind::existingJsonForDeviceName(kind, kindsJson);
            kindsJson[kind] = makeJsonForDeviceKindWithUuid(json["uuid"].asString());
            CudaProgram::writeDevicesJson(kindsJson);
            rebuild = true;
        }
            break;
        case jsonOK:
        default:
            rebuild = false;
            break;
    }
    json = CudaDeviceKind::existingJsonForDeviceName(kind, kindsJson);
    return json["uuid"].asString();
}

void CudaDeviceKind::deviceKindProperties()
{
    CUdevice device = cudaDeviceForInstanceIndex(0);
    
    maxWorkgroupDims = 3;
    workGroupDims = (size_t *)malloc(sizeof(size_t)*maxWorkgroupDims);
    cuDeviceGetAttribute((int *)&workGroupDims[0], CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X, device);
    cuDeviceGetAttribute((int *)&workGroupDims[1], CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y, device);
    cuDeviceGetAttribute((int *)&workGroupDims[2], CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z, device);
    
    cuDeviceGetAttribute((int *)&maxConstantBufSize, CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY, device);
    cuDeviceGetAttribute((int *)&maxWorkGroupSize, CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK, device);
    
    cuDeviceTotalMem(&globalMemSize, device);
    
    maxMemAllocSize = globalMemSize/4;
    //    errNum = clGetDeviceInfo(deviceID, CL_DEVICE_MAX_MEM_ALLOC_SIZE,  sizeof(cl_ulong), &maxMemAllocSize, NULL);
    
    cuDeviceGetAttribute((int *)&localMemSize, CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK, device);
    deviceName = getDeviceName();
    
    determineWarpSizeForCudaDevice(device);
}

std::string CudaDeviceKind::getDeviceVendor()
{
    return "Nvidia";
}

std::string CudaDeviceKind::getDeviceName()
{
    CUdevice device = cudaDeviceForInstanceIndex(0);
    static char name[200];
    cuDeviceGetName(name, sizeof(name), device);
    
    return name;
}

bool CudaDeviceKind::hasLocalMemoryOnDevice()
{
    return true;
}

bool CudaDeviceKind::isCPU()
{
    return false;
}

bool CudaDeviceKind::isGPU()
{
    return true;
}

#ifdef __ORIGINAL__

- (NSString *)summary
{
    NSString *_deviceType = [self isCPU] ? @"CPU:" : @"GPU:";
    if ([kind rangeOfString:vendor].location != NSNotFound)
        vendor = @"";
        return [NSString stringWithFormat:@"%@ %@%@%@ %@",
                _deviceType, vendor, vendor ? @" " : @"",
                kind, @""];
}

#endif
