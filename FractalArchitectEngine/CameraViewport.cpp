//
//  CameraViewport.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/22/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "CameraViewport.hpp"
#include "Flame.hpp"
#include <math.h>
#include "common.hpp"
#include "rand.hpp"
#include "GlhFunctionsCpp.hpp"
#include <stdlib.h>

#ifdef _WIN32
#define ALIGNED_MALLOC(TYPE, target, alignment, size)   target = (TYPE *)_aligned_malloc(size, alignment)
#define ALIGNED_FREE(target) _aligned_free(target)
#else
#define ALIGNED_MALLOC(TYPE, target, alignment, size)   posix_memalign((void **)&target, alignment, size)
#define ALIGNED_FREE(target) free(target)
#endif

CameraViewport::CameraViewport(struct Flame *flame)
{
    
    // From Wikipedia Rotation Formalisms in Three Dimensions - See: 3D Projection too   Pre-Multiplication
    //
    //        ψ = -yaw  or yaw = -ψ                  rotation about Z axis
    //        θ =  0     ==> not used in Apophysis   rotation about Y axis
    //        𝜙 = pitch                              rotation about X axis
    //
    // Ax matrix - pitch
    //        |  1           0                             0    |
    //        |  0           cos𝜙                          sin𝜙 |
    //        |  0          -sin𝜙                          cos𝜙 |
    
    // Ay matrix - roll
    //        |  cosθ        0                            -sinθ |
    //        |  0           1                             0    |
    //        |  sinθ        0                             cosθ |
    
    // Az matrix - yaw
    //        |  cosψ        sinψ                          0    |
    //        | -sinψ        cosψ                          0    |
    //        |  0           0                             1    |
    
    // A = AzAx matrix  - Apophysis camera
    //        |  cosψ        cos𝜙 sinψ                     sin𝜙 sinψ  |
    //        | -sinψ        cos𝜙 cosψ                     sin𝜙 cosψ  |
    //        |  0          -sin𝜙                          cos𝜙       |
    
    // A = AzAyAx matrix
    //        |  cosθ cosψ   cos𝜙 sinψ + sin𝜙 sinθ cosψ    sin𝜙 sinψ  |
    //        | -cosθ sinψ   cos𝜙 cosψ - sin𝜙 sinθ sinψ    sin𝜙 cosψ  |
    //        |  sinθ       -sin𝜙 cosθ                     cos𝜙 cosθ  |
    
    // height = 2 * tan(fov/2) * cameraZ
    // for 90 degree vertical FOV  ==>  cameraZ = height/2
    
    ALIGNED_MALLOC(struct CameraViewProperties, properties, 4096, sizeof(struct CameraViewProperties));
    
    properties->yaw   =  flame->params.cam_yaw;
    properties->pitch =  flame->params.cam_pitch;
    properties->roll  =  flame->params.rotation; // Apo7X stores the negative of the input angle rotation
    
    // setup orientation portion of matrix to be same as Apophysis
    float perspectiveMatrix[16], cameraMatrix[16];
    cameraMatrix[0]    =  cosf(-properties->yaw);
    cameraMatrix[4]    = -sinf(-properties->yaw);
    cameraMatrix[8]    =  0.f;
    cameraMatrix[12]   =  0.f;
    cameraMatrix[1]    =  cosf(properties->pitch) * sinf(-properties->yaw);
    cameraMatrix[5]    =  cosf(properties->pitch) * cosf(-properties->yaw);
    cameraMatrix[9]    = -sinf(properties->pitch);
    cameraMatrix[13]   =  0.f;
    cameraMatrix[2]    =  sinf(properties->pitch) * sinf(-properties->yaw);
    cameraMatrix[6]    =  sinf(properties->pitch) * cosf(-properties->yaw);
    cameraMatrix[10]    =  cosf(properties->pitch);
    cameraMatrix[3]    = 0.f;
    cameraMatrix[7]    = 0.f;
    cameraMatrix[11]   = 0.f;
    cameraMatrix[14]   = 0.f;
    cameraMatrix[15]   = 1.f;
    
    // translate to eyepoint
    glhTranslatef2(cameraMatrix, -flame->params.cam_x, -flame->params.cam_y, -flame->params.cam_z);
    
    //        if (flame->params.cam_perspective == 0.f) {
    //            float xmax = flame->params.cam_orthowide/2.f;
    //            float ymax = xmax;
    //            glhOrthofInfiniteFarPlane(perspectiveMatrix, -xmax, xmax, -ymax, ymax);
    //        }
    //        else
    glhPerspectiveInfiniteFarPlanef2(perspectiveMatrix, flame->params.cam_fov, 1.f, flame->params.cam_near);
    
    MultiplyMatrices4by4OpenGL_FLOAT(properties->matrix, perspectiveMatrix, cameraMatrix);
    
    properties->perspective  = flame->params.cam_perspective;
    properties->dof          = flame->params.cam_dof;
    properties->zpos         = flame->params.cam_zpos;
    properties->cosRoll      = cosf(properties->roll);
    properties->sinRoll      = sinf(properties->roll);
    
    float camX0 = flame->params.center[0] - flame->params.size[0]/2.f;
    float camY0 = flame->params.center[1] - flame->params.size[1]/2.f;
    float camX1 = camX0 + flame->params.size[0];
    float camY1 = camY0 + flame->params.size[1];
    properties->camWidth    = camX1 - camX0;
    properties->camHeight   = camY1 - camY0;
    properties->centerX     = flame->params.center[0] * (1.f - properties->cosRoll) - flame->params.center[1] * properties->sinRoll - camX0;
    properties->centerY     = flame->params.center[1] * (1.f - properties->cosRoll) + flame->params.center[0] * properties->sinRoll - camY0;
    properties->clipToNDC   = flame->params.clipToNDC;
    
    properties->rotatedViewOffsetx = flame->params.center[0] - (flame->params.center[0]*properties->cosRoll - flame->params.center[1]*properties->sinRoll);
    properties->rotatedViewOffsety = flame->params.center[1] - (flame->params.center[0]*properties->sinRoll + flame->params.center[1]*properties->cosRoll);
}

CameraViewport::~CameraViewport()
{
    ALIGNED_FREE(properties);
}

// ===== JWildfire/Apophysis mapping of world point (px, py to viewport ===========================================

// apply  A = AzAx matrix  - Apophysis camera rotation transformation to convert world point to camera relative point
// no translation is applied so the camera is at the center of the world (camera.zpos = 0)
// the camera center is moved by the zpos - BUT ONLY when DOF is applied !!!
// the camera perspective also effectively moves the camera z position - calculations follow:


// HERE is where you apply the lookAt rotation in a future extension
//   OR you can apply the precalculated lookSt rotation matrix to the Apophysis camera transformation matrix (BETTER IDEA)

// apply projection to camera (px, py)

//  Ppx = px/(1 - perspective *pz)    ==> Ppx is projection of px
//  Ppy = py/(1 - perspective *pz)    ==> Ppy is projection of py

// But  Ppx = px/(pz * tan(fov/2))   and   Ppy = py/(pz * tan(fov/2))
//  pz * tan(fov/2) = 1 - pz * perspective
//  C * pz = 1 - pz *perspective   ===>   (C + perspective) * pz = 1  ===>  pz = 1/(C + perspective)
//  1 - C * pz = pz * perspective
//  perspective = 1/pz - C  OR   pz = 1/(C + perspective)  OR pz = 1/(tan(fov/2) + perspective)

//  So:   camera (px, py, pz)  ==>  camera (px, py, 1/(tan(fov/2) + perspective))


// viewport rotation - roll camera point to viewplane orientation (roll about camera Z axis)
// rotate around (roll) the center of the viewport
//  px = ((px - fcx) * cos (roll)) - ((py - fcy) * sin(roll)) + fcx
//  py = ((px - fcx) * sin (roll)) + ((py - fcy) * cos(roll)) + fcy

// viewport scaling - transfoming world point to viewplane pixel (scaling)
//  pixelx = ((px - fcx)/fzx + 1/2) * pixelWidth
//  pixely = (-(py - fcy)/fzy + 1/2) * pixelHeight

CameraViewProperties * CameraViewport::pProperties()
{
    return properties;
}

void CameraViewport::project(FAPoint *p)
{
    float z = p->z - properties->zpos;
    float px = properties->matrix[0]*p->x + properties->matrix[3]*p->y;
    float py = properties->matrix[1]*p->x + properties->matrix[4]*p->y + properties->matrix[7]*z;
    float pz = properties->matrix[2]*p->x + properties->matrix[5]*p->y + properties->matrix[8]*z;
    float w  = 1.f - properties->perspective * pz;
    
    p->x = px/w;
    p->y = py/w;
    p->z = z;
}

void CameraViewport::projectWithDOF(FAPoint *p)
{
    float z = p->z - properties->zpos;
    float px = properties->matrix[0]*p->x + properties->matrix[3]*p->y;
    float py = properties->matrix[1]*p->x + properties->matrix[4]*p->y + properties->matrix[7]*z;
    float pz = properties->matrix[2]*p->x + properties->matrix[5]*p->y + properties->matrix[8]*z;
    float w  = 1.f - properties->perspective * pz;
    
    float t  = randValue() * M_2_PI;
    float dr = randValue() * 0.1f * properties->dof * z;
    float sina = sinf(t);
    float cosa = cosf(t);
    
    p->x = (px + dr*cosa)/w;
    p->y = (py + dr*sina)/w;
    p->z = z;
}

void CameraViewport::applyRotation(struct Flame *flame, FAPoint *p)
{
    float x = p->x - flame->params.center[0];
    float y = p->y - flame->params.center[1];
    p->x = x * cosf(flame->params.rotation) - y * sinf(flame->params.rotation) + flame->params.center[0];
    p->y = x * sinf(flame->params.rotation) + y * cosf(flame->params.rotation) + flame->params.center[1];
}

bool CameraViewport::projectAndClip(FAPoint *p)
{
    FAPoint q;
    q.x = p->x;
    q.y = p->y;
    q.z = p->z;
    project(&q);
    
    float px = q.x * properties->cosRoll + q.y * properties->sinRoll + properties->centerX;
    if ((px < 0) || (px > properties->camWidth))
        return false;
    float py = q.y * properties->cosRoll - q.x * properties->sinRoll + properties->centerY;
    if ((py < 0) || (py > properties->camHeight))
        return false;
    return true;
}


#ifdef __ORIGINAL__

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ yaw:%f pitch:%f cosf(-yaw):%f sinf(-yaw):%f cosf(pitch):%f cosf(pitch):%f",
            [super description], properties->yaw, properties->pitch,
            cosf(-properties->yaw), sinf(-properties->yaw), cosf(properties->pitch), sinf(properties->pitch)];
}

#endif