//
//  CpuMemStats.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "CpuMemStats.hpp"

#ifdef __APPLE__
#include <sys/sysctl.h>
#include <mach/host_info.h>
#include <mach/mach_host.h>


size_t CpuMemStats::freeMemory()
{
    int mib[6];
    mib[0] = CTL_HW;
    mib[1] = HW_PAGESIZE;
    
    int pagesize;
    size_t length;
    length = sizeof (pagesize);
    sysctl (mib, 2, &pagesize, &length, NULL, 0);
    
    mach_msg_type_number_t count = HOST_VM_INFO_COUNT;
    
    vm_statistics_data_t vmstat;
    host_statistics (mach_host_self (), HOST_VM_INFO, (host_info_t) &vmstat, &count);
    return  (unsigned long)vmstat.free_count * pagesize;
    /*
     double total    = vmstat.wire_count + vmstat.active_count + vmstat.inactive_count + vmstat.free_count;
     double wired    = vmstat.wire_count / total;
     double active   = vmstat.active_count / total;
     double inactive = vmstat.inactive_count / total;
     double free     = vmstat.free_count / total;
     */
}

#elif defined(_WIN32)

#include<windows.h>

size_t CpuMemStats::freeMemory()
{
	MEMORYSTATUSEX statex;

	statex.dwLength = sizeof(statex);

	GlobalMemoryStatusEx(&statex);
	return statex.ullAvailPageFile;
}

#else
#include <stdio.h>
#include <inttypes.h>
#include <sys/sysctl.h>
#include <sys/resource.h>

size_t CpuMemStats::freeMemory()
{
    uint64_t vm_size = 0;
    FILE *statm = fopen("/proc/self/statm", "r");
    if (!statm)
        return 0;
    if (fscanf(statm, "%ld", &vm_size) != 1)
    {
        fclose(statm);
        return 0;
    }
    vm_size = (vm_size + 1) * 1024;
    
    struct rlimit lim;
    if (getrlimit(RLIMIT_AS, &lim) != 0)
        return 0;
    if (lim.rlim_cur <= vm_size)
        return 0;
    if (lim.rlim_cur >= 0xC000000000000000ull) // most systems cannot address more than 48 bits
        lim.rlim_cur  = 0xBFFFFFFFFFFFFFFFull;
        return lim.rlim_cur - vm_size;
}

#endif
