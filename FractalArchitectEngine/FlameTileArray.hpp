//
//  FlameTileArray.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/15/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef FlameTileArray_hpp
#define FlameTileArray_hpp

#include <memory>
#include <vector>
#include "Exports.hpp"
#include "Image.hpp"

class FlameTile;
class FlameExpanded;
class ImageRepTileArray;
class VariationSet;

using uint                = unsigned int;
using SharedFlameTile     = std::shared_ptr<FlameTile>;
using SharedFlameExpanded = std::shared_ptr<FlameExpanded>;
using SharedVariationSet  = std::shared_ptr<VariationSet>;
using FlamesVector        = std::vector<SharedFlameExpanded>;
using TilesVector         = std::vector<SharedFlameTile>;
using SharedBlades        = std::shared_ptr<FlamesVector>;


struct FAENGINE_API FlameTileArray : public std::enable_shared_from_this<FlameTileArray> {
    
    FlameTileArray(uint _rows, uint _cols, bool _flipped, uint _supersample);
    
    // calculate the flames for each tile
    FlameTileArray(uint _rows,
                   uint _cols,
                   SharedBlades & _blades,
                   float _width,
                   float _height,
                   bool _flipped,
                   bool transparent,
                   uint _supersample,
                   const SharedVariationSet & variationSet);
    
    ~FlameTileArray();
    
    static void tileFlame(struct Flame *flame,
                          float width,
                          float height,
                          float tileWidth,
                          float tileHeight,
                          float cumWidth,
                          float cumHeight,
                          const SharedVariationSet &variationSet);
    
    size_t size()      { return tiles.size(); }
    float getHeight()  { return height; }    
    float getWidth()   { return width;  }
    
    TilesVector & getTiles() { return tiles;  }
    
    size_t numBatches();

	SharedFlameTile & tileAtIndex(size_t index)         { return tiles[index]; }
	SharedFlameTile & tileAtRow(size_t row, size_t col) { return tiles[row * cols + col]; }
    
    size_t numBatchesAtIndex(size_t index);
    
    struct Flame *flame();

    float tileWidthAtCol(int col);
    float tileHeightAtRow(int row);
    float tileXAtCol(int col);
    float tileYAtRow(int row);
    
    struct Flame *nextFlame();
    struct ElectricSheepStuff *getElc();
    
    FA::Image *tileImageforTileAtRow(int row, int col, FA::Image &image);
    
    ImageRepTileArray *makeImageRepTileArray(bool _flipped);
    
    void handleColumns(int _cols,
                       float fwidth,
                       float fheight,
                       float fcumHeight,
                       float overlap,
                       int i,
                       bool transparent,
                       const SharedVariationSet & variationSet);

    // ========  Members =========
    
    SharedBlades   blades;
    int            rows;
    int            cols;
    bool           flipped;          // are we rendering for flipped y coords
    float          width;           // after supersampling width
    float          height;          // after supersampling height
    uint           supersample;
    TilesVector    tiles;
    FA::Image *backgroundImage;
};

#endif /* FlameTileArray_hpp */
