//
//  FlameTile.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/15/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef FlameTile_hpp
#define FlameTile_hpp

#include <memory>
#include <string>
#include <vector>

#include "Exports.hpp"
#include "Image.hpp"

class DeviceContext;
class Flame;
class FlameExpanded;
class FlameTileArray;

using uint                = unsigned int;
using UniqueFlame         = std::unique_ptr<Flame>;
using SharedFlameExpanded = std::shared_ptr<FlameExpanded>;
using SharedDeviceContext = std::shared_ptr<DeviceContext>;
using FlamesVector        = std::vector<SharedFlameExpanded>;
using SharedBlades        = std::shared_ptr<FlamesVector>;

struct FAENGINE_API  FlameTile {
    
    FlameTile(SharedBlades & blades,
              uint _row,
              uint _column,
              float _width,
              float _height,
              bool transparent,
              bool moreThanOneTile);
   ~FlameTile();
    
    static float overlap();
    std::string description();
    static void allocateBatches(size_t batchesDesired, SharedBlades & blades, float width, float height,
                                const SharedDeviceContext & deviceContext);
    void allocateBatchesToBlades(size_t batchesDesired, const SharedDeviceContext & deviceContext);
    
    static std::string bladeBatchStatistics(SharedBlades & blades);
    std::string bladeBatchStatistics();
    
    // ======== Members =========
    
    SharedBlades   blades;
    size_t         numBatches;
    unsigned       row;
    unsigned       column;
    float          width;
    float          height;
    FA::Image *backgroundImage;
};

#endif /* FlameTile_hpp */
