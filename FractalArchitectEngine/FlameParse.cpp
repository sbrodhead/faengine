//
//  FlameParse.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/7/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <unordered_map>
#include <string>
#include <memory>
#include <fstream>
#include <string.h>

#include "FlameParse.hpp"
#include "Utilities.hpp"
#include "common.hpp"
#include "pugixml.hpp"
#include "Variation.hpp"
#include "VariationSet.hpp"
#include "Flame.hpp"
#include "FlameExpanded.hpp"
#include "NaturalCubicCurve.hpp"
#include "RenderModeSettings.hpp"
#include "rand.hpp"
#include "ColorNode.hpp"
#include "VariationChain.hpp"
#include "VariationGroup.hpp"

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif


using uint = unsigned int;
using namespace pugi;

// get the <palette>'s number attribute value
int getPalIndex(NodeVector & palette, int i)
{
    xml_node & paletteELe = palette[i];
    return paletteELe.attribute("number").as_int();
}

// convert <flame> attributes to a dictionary format
AttrDict FlameParse::attrsAsDict(xml_node & ele)
{
    AttrDict attrDict;
    for (pugi::xml_attribute attr: ele.attributes()) {
        attrDict[attr.name()] = attr;
    }
    return attrDict;
}

std::vector<std::string> split(const std::string &text, char sep) {
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = text.find(sep, start)) != std::string::npos) {
        std::string temp = text.substr(start, end - start);
        if (temp != "") tokens.push_back(temp);
        start = end + 1;
    }
    std::string temp = text.substr(start);
    if (temp != "") tokens.push_back(temp);
    return tokens;
}

// split a string using its embedded whites space - drops out any zero length strings
std::vector<std::string> FlameParse::splitOnSpaces(const std::string &str)
{
    return split(str, ' ');
}

// get the attribute's value as a bool given the attributes name
bool FlameParse::attrAsBool(AttrDict &attrDict, const std::string &key)
{
    return attrDict[key].as_bool();
}

// get the attribute's value as a int given the attributes name
int FlameParse::attrAsInt(AttrDict &attrDict, const std::string &key)
{
    return attrDict[key].as_int();
}

// get the attribute's value as a int given the attributes name, if not found return the defaultValue
int FlameParse::attrAsInt(AttrDict &attrDict, const std::string &key, int defaultValue)
{
    if (attrDict.find(key) == attrDict.end())
        return defaultValue;
    return attrDict[key].as_int(defaultValue);
}

// get the attribute's value as a float given the attributes name
float FlameParse::attrAsFloat(AttrDict &attrDict, const std::string &key)
{
    return attrDict[key].as_float();
}

// get the attribute's value as a float given the attributes name, if not found return the defaultValue
float FlameParse::attrAsFloat(AttrDict &attrDict, const std::string &key, float defaultValue)
{
    if (attrDict.find(key) == attrDict.end())
        return defaultValue;
    return attrDict[key].as_float(defaultValue);
}

// get the attribute's value as a float given the attributes name
double attrAsDouble(AttrDict &attrDict, const std::string &key)
{
    return attrDict[key].as_double();
}

// get the attribute's value as a string given the attributes name, if not found return the defaultValue
std::string FlameParse::attrAsString(AttrDict &attrDict, const char *key)
{
    return std::string(attrDict[key].value());
}

// get the attribute's value as a string given the attributes name, if not found return the defaultValue
std::string FlameParse::attrAsString(AttrDict &attrDict, const char *key, const char *defaultValue)
{
    if (attrDict.find(key) == attrDict.end())
        return defaultValue;
    return std::string(attrDict[key].value());
}

// get the attribute's value as a string given the attributes name, if not found return the defaultValue
std::string FlameParse::attrAsString(AttrDict &attrDict, const std::string &key)
{
    return std::string(attrDict[key].value());
}

// get the attribute's value as a string given the attributes name, if not found return the defaultValue
std::string FlameParse::attrAsString(AttrDict &attrDict, const std::string &key, const std::string & defaultValue)
{
    if (attrDict.find(key) == attrDict.end())
        return defaultValue;
    return std::string(attrDict[key].value());
}

// get the attribute's value as a float given the attributes name, if not found return the defaultValue
SharedPointVector FlameParse::attrAsCurve(AttrDict &attrDict, const std::string &key)
{
    if (attrDict.find(key) == attrDict.end())
        return std::shared_ptr<std::vector<Point>>();
    return NaturalCubicCurve::curveDataFromXmlAttribute(attrDict[key]);
}


void complainAboutMissingPalette()
{
    systemLog("Missing flam3-palettes.xml file. Download & install it.");
}

// enforce the requirement for unique frame names by appending a counter value to the non-unique frame names
void FlameParse::uniqueFrameNames(FlameExpandedVector & flames)
{
    std::unordered_map<std::string, uint> nameDict;
    std::unordered_map<std::string, uint> useDict;
    nameDict.reserve(60); useDict.reserve(60);
    
    // build frame name dictionary with count of name usage
    for (SharedFlameExpanded &fe : flames) {
        std::string &name = fe->meta->name;
        if (name.length() == 0)
            fe->meta->name = name = "noname";
        int count = nameDict[name];
        if (count > 0)
            nameDict[name] = ++count;
        else {
            count = 1;
            nameDict[name] = count;
            useDict[name] = count;
        }
    }
    
    // modify the names for non-unique names by appending a index for the non-unique names
    for (SharedFlameExpanded &fe : flames) {
        std::string &name = fe->meta->name;
        int count = nameDict[name];
        if (count > 1) {
            uint use = useDict[name];
            useDict[name] = use + 1;
            std::string newName = string_format("%s_%u", name.c_str(), use);
            fe->meta->name = newName;
        }
    }
}

// after preprocessing variation sets in file, this is it
SharedVariationSet FlameParse::parseFlameElementForVariationSet(const xml_node &flameEle, const SharedVariationSet &defaultVariationSet)
{
    const char *varSetUUID = flameEle.attribute("varset").value();
    
    SharedVariationSet variationSet;
    if (varSetUUID) {
        variationSet = VariationSet::searchForVariationSetForUuid(varSetUUID);
        if (variationSet)
            return variationSet;
    }
    return variationSet ? variationSet : defaultVariationSet;
}

// parse the Flame XML document
void FlameParse::parseDoc(FlameExpandedVector &flames,
                          pugi::xml_document &doc,
                          const std::string &palettePath,
                          SharedVariationSet &defaultVariationSet)
{
    int flameCount = 0;
    xml_node aNode = doc.document_element();
    if (aNode.type() == node_element && strcmp(aNode.name(),"flame") != 0) // in case there are multiple flames
        aNode = aNode.first_child();
    
    // look for first <flame>
    do {
        if (aNode.type() == node_element && strcmp(aNode.name(),"flame") == 0) {
            SharedFlameExpanded currentFlame = std::make_shared<FlameExpanded>();

            xml_node & flameEle             = aNode;
            SharedVariationSet variationSet = FlameParse::parseFlameElementForVariationSet(flameEle, defaultVariationSet);
            currentFlame->meta              = std::make_unique<FlameMetaData>("", variationSet->getUuid());
            currentFlame->elc               = std::make_unique<ElectricSheepStuff>();
            currentFlame->elc->hasMask      = 0;
            currentFlame->flame             = FlameParse::parseFlame(aNode, currentFlame->meta, currentFlame->elc, flameCount, palettePath, variationSet);
            currentFlame->setUuid(stringWithUUID());
            currentFlame->setVariationSetUuid(variationSet->getUuid());
            flameCount++;
            flames.push_back(currentFlame);
            break;
        }
    } while ((aNode = aNode.next_sibling()));
    
    while ((aNode = aNode.next_sibling())) {
        // filter out flames that can use this variationSet
        if (aNode.type() == node_element && strcmp(aNode.name(),"flame") == 0) {
            SharedFlameExpanded currentFlame = std::make_shared<FlameExpanded>();
            xml_node & flameEle              = aNode;
            SharedVariationSet variationSet  = FlameParse::parseFlameElementForVariationSet(flameEle, defaultVariationSet);
            currentFlame->meta               = std::make_unique<FlameMetaData>(stringWithUUID(), variationSet->getUuid());
            currentFlame->elc                = std::make_unique<ElectricSheepStuff>();
            currentFlame->elc->hasMask       = 0;
            currentFlame->flame              = FlameParse::parseFlame(aNode, currentFlame->meta, currentFlame->elc, flameCount, palettePath, variationSet);
            currentFlame->setUuid(stringWithUUID());
            currentFlame->setVariationSetUuid(variationSet->getUuid());
            flameCount++;
            flames.push_back(currentFlame);
        }
        if (aNode.type() == node_element && strcmp(aNode.name(),"variationSet") == 0) {
        }
    }
}

// parse the Flame XML document
SharedFlame FlameParse::parseFlame(pugi::xml_node &flameEle,
                                   SharedFlameMetaData &flameMeta,
                                   SharedElc &elc,
                                   int flameCount,
                                   const std::string &palettePath,
                                   SharedVariationSet &variationSet)
{
//    NSXMLDocument *paletteDoc = (NSXMLDocument *)nil;

    SharedFlame flm;
    float hue;
    int count       = 0;
    int paletteType = 0;
    int standardPaletteIndex = 0;
    
    // get attrs as a dictionary for <flame>
    AttrDict flameAttrDict = attrsAsDict(flameEle);
    
    NodeVector xForms;
    NodeVector finalXforms;
    NodeVector palette;
    NodeVector edits;
    for (xml_node node: flameEle.children("xform"))
        xForms.push_back(node);
    for (xml_node node: flameEle.children("finalxform"))
        finalXforms.push_back(node);
    for (xml_node node: flameEle.children("color"))
        palette.push_back(node);
    for (xml_node node: flameEle.children("edit"))
        edits.push_back(node);
    
    std::string editXML;
    for (auto it : edits) {
        editXML.append(it.value());
    }
    flameMeta->editXML = editXML;
    
    // get palette info
    if (flameAttrDict.find("palette") == flameAttrDict.end()) {
        
        if (palette.size() == 0) {
            for (xml_node node: flameEle.children("palette"))
                palette.push_back(node);
            
            if (palette.size() == 0) {
                for (xml_node node: flameEle.children("colors"))
                    palette.push_back(node);
                paletteType = 1;   // uses <colors count="256" format="RGB" data="xxxxxxx" />  with big Hex RGB String holding 256 color values
            }
            else {
                paletteType = 2;  // uses <palette count="256" format="RGB"> ...		</palette> with big Hex RGB String holding 256 color values
            }
            count = palette[0].attribute("count").as_int();
        }
        else {
            count = (int)palette.size();  // paletteType = 0 ==> // use 256 0f these:   <color index="244" rgb="255 178 109"/>
        }

    }
    else {
        int palIndex = flameAttrDict["palette"].as_int();
        
        elc->palette  = palIndex;
        elc->hasMask |= hasPalette;
        
        if (palettePath.length() > 0) {
            xml_document paletteDoc;
            xml_parse_result result = paletteDoc.load_file(palettePath.c_str(), parse_cdata | parse_comments);
            if (result) { // succeeded
                xml_node palettes = paletteDoc.document_element();
                for (xml_node node: palettes.children("palette"))
                    palette.push_back(node);
                
                while (palIndex != getPalIndex(palette, standardPaletteIndex)) {
                    standardPaletteIndex++;
                }
                paletteType = 3;  // type 3: uses one of 707 predefined palettes
                count = 256;
            }
            else {
                
                paletteType = 4; // missing palette doc - generate a random palette
                count = 256;
                complainAboutMissingPalette();
            }
        }
        else {
            paletteType = 4; // missing palette doc - generate a random palette
            count = 256;
            complainAboutMissingPalette();
        }
    }
    
    int xformCount = (int)xForms.size();
    if (xformCount > MAX_XFORMS) {  // check if we can fit into the GPU and if not truncate the xforms
        xformCount = MAX_XFORMS;
        
        std::string msg = string_format("Fractal #%u exceeds the limit of %u xforms - truncating those beyond limit", flameCount + 1, MAX_XFORMS);
        systemLog(msg);
    }
    int finalXformCount = (int)finalXforms.size();
    if (finalXformCount > MAX_XFORMS) {  // check if we can fit into the GPU and if not truncate the xforms
        finalXformCount = MAX_XFORMS;
        
        std::string msg = string_format("Fractal #%u exceeds the limit of %u final xforms - truncating those beyond limit", flameCount + 1, MAX_XFORMS);
        systemLog(msg);
    }
    flm = std::make_unique<Flame>(xformCount, count == 1 ? 2 : count, finalXformCount);  // at least two colors
    
    // get symmetryKind
    NodeVector symmetries;
    for (xml_node node : flameEle.children("symmetry"))
        symmetries.push_back(node);
    
    if (symmetries.size() > 0) {
        xml_node &symmetryEle    = symmetries[0];
        flm->params.symmetryKind = symmetryEle.attribute("kind").as_float();
    }
    else {
        flm->params.symmetryKind = 0.0f;
    }
    if (flameAttrDict.find("name") != flameAttrDict.end())
        flameMeta->name = flameAttrDict["name"].value();
    else
        flameMeta->name = string_format("%u", flameCount);

    if (flameAttrDict.find("nick") != flameAttrDict.end())
        flameMeta->nick = flameAttrDict["nick"].value();
    else
        flameMeta->nick = "";
    if (flameAttrDict.find("url") != flameAttrDict.end())
        flameMeta->url = flameAttrDict["url"].value();
    else
        flameMeta->url = "";
    if (flameAttrDict.find("notes") != flameAttrDict.end())
        flameMeta->notes = flameAttrDict["notes"].value();
    else
        flameMeta->notes = "";
    if (flameAttrDict.find("uuid") != flameAttrDict.end())
        flameMeta->uuid = flameAttrDict["uuid"].value();
    else
        flameMeta->uuid = stringWithUUID();

    if (flameAttrDict.find("rgb_curve") != flameAttrDict.end()) {
        flameMeta->rgbCurve = attrAsCurve(flameAttrDict, "rgb_curve");
    }
    if (flameAttrDict.find("red_curve") != flameAttrDict.end()) {
        flameMeta->redCurve = attrAsCurve(flameAttrDict, "red_curve");
    }
    if (flameAttrDict.find("green_curve") != flameAttrDict.end()) {
        flameMeta->greenCurve = attrAsCurve(flameAttrDict, "green_curve");
    }
    if (flameAttrDict.find("blue_curve") != flameAttrDict.end()) {
        flameMeta->blueCurve = attrAsCurve(flameAttrDict, "blue_curve");
    }
    if (flameAttrDict.find("curvesAffectBkgd") != flameAttrDict.end()) {
        flameMeta->applyToBackground = attrAsBool(flameAttrDict, "curvesAffectBkgd");
    }
    if (flameAttrDict.find("qualityAdjust") != flameAttrDict.end()) {
        flameMeta->qualityAdjust = attrAsFloat(flameAttrDict, "qualityAdjust");
    }
    if (flameAttrDict.find("renderMode") != flameAttrDict.end()) {
        const std::string &renderMode  = flameAttrDict["renderMode"].value();
        flameMeta->renderMode         = RenderModeSettings::renderModeFromString(renderMode);
    }
    
    if (flameAttrDict.find("compositeColor") != flameAttrDict.end()) {
        std::vector<std::string> array = splitOnSpaces(flameAttrDict["compositeColor"].value());
        float r =  std::strtof(array[0].c_str(), nullptr);
        float g =  std::strtof(array[1].c_str(), nullptr);
        float b =  std::strtof(array[2].c_str(), nullptr);
        
        // compensate for files that are not saved per Flam3 spec
        if (r > 1.f)
            r /= 255.f;
        if (g > 1.f)
            g /= 255.f;
        if (b > 1.f)
            b /= 255.f;
        
        flameMeta->compositeColor = Color{r, g, b};
    }
    std::string imagePath;
    if (flameAttrDict.find("imagePath") != flameAttrDict.end())
        imagePath = flameAttrDict["imagePath"].value();
    flameMeta->imagePath = imagePath;
    
    elc->interpolation        = interpolationSmooth;
    elc->paletteInterpolation = paletteHSV;
    elc->paletteMode          = modeLinear;
    elc->interpolationType    = interpolationTypeLinear;
    elc->filterShape          = shapeGaussian;
    elc->temporalFilterType   = temporalBox;
    elc->defaultFuseIters     = true;
    elc->cpuFuseIterations    = 30;
    elc->gpuFuseIterations    = 60;
    elc->supersample          = 1;
    flm->params.oversample           = 1;
    
    // Electric Sheep stuff - parse it so we can pass it along
    if (flameAttrDict.find("interpolation") != flameAttrDict.end()) {
        elc->interpolation = ElectricSheepStuff::getFlameInterpolationFor(flameAttrDict["interpolation"].value());
        elc->hasMask |= hasInterpolation;
    }
    if (flameAttrDict.find("palette_interpolation") != flameAttrDict.end()) {
        elc->paletteInterpolation = ElectricSheepStuff::getPaletteInterpolationFor(flameAttrDict["palette_interpolation"].value());
        elc->hasMask |= hasPaletteInterpolation;
    }
    if (flameAttrDict.find("palette_mode") != flameAttrDict.end()) {
        elc->paletteMode = ElectricSheepStuff::getPaletteModeFor(flameAttrDict["palette_mode"].value());
        elc->hasMask |= hasPaletteMode;
    }
    if (flameAttrDict.find("interpolation_type") != flameAttrDict.end()) {
        elc->interpolationType = ElectricSheepStuff::getInterpolationTypeFor(flameAttrDict["interpolation_type"].value());
        elc->hasMask |= hasInterpolationType;
    }
    if (flameAttrDict.find("filter_shape") != flameAttrDict.end()) {
        elc->filterShape = ElectricSheepStuff::getFilterShapeFor(flameAttrDict["filter_shape"].value());
        elc->hasMask |= hasFilterShape;
    }
    if (flameAttrDict.find("temporal_filter_type") != flameAttrDict.end()) {
        elc->temporalFilterType = ElectricSheepStuff::getTemporalTypeFor(flameAttrDict["temporal_filter_type"].value());
        elc->hasMask |= hasTemporalFilterType;
    }
    
    if (flameAttrDict.find("filter") != flameAttrDict.end()) {
        elc->filter = attrAsFloat(flameAttrDict, "filter");
        elc->hasMask |= hasFilter;
    }
    if (flameAttrDict.find("temporal_filter_width") != flameAttrDict.end()) {
        elc->temporalFilterWidth = attrAsFloat(flameAttrDict, "temporal_filter_width");
        elc->hasMask |= hasTemporalFilterWidth;
    }
    if (flameAttrDict.find("temporal_filter_exp") != flameAttrDict.end()) {
        elc->temporalFilterExp = attrAsFloat(flameAttrDict, "temporal_filter_exp");
        elc->hasMask |= hasTemporalFilterExp;
    }
    if (flameAttrDict.find("temporal_samples") != flameAttrDict.end()) {
        elc->temporalSamples = attrAsFloat(flameAttrDict, "temporal_samples");
        elc->hasMask |= hasTemporalSamples;
    }
    if (flameAttrDict.find("gamma_threshold") != flameAttrDict.end()) {
        elc->gammaThreshold = attrAsFloat(flameAttrDict, "gamma_threshold");
        elc->hasMask |= hasGammaThreshold;
    }
    if (flameAttrDict.find("supersample") != flameAttrDict.end()) {
        float ss = attrAsFloat(flameAttrDict, "supersample");
        ss = ss > 4.f ? 4.f : ss;
        ss = ss < 1.f ? 1.f : ss;
        elc->supersample = ceilf(ss);
        flm->params.oversample = elc->supersample;
        
        elc->hasMask |= hasSupersample;
    }
    if (flameAttrDict.find("oversample") != flameAttrDict.end()) {
        float ss = attrAsFloat(flameAttrDict, "oversample");
        ss = ss > 4.f ? 4.f : ss;
        ss = ss < 1.f ? 1.f : ss;
        elc->supersample = ceilf(ss);
        flm->params.oversample = elc->supersample;
        
        elc->hasMask |= hasSupersample;
    }
    if (flameAttrDict.find("passes") != flameAttrDict.end()) {
        elc->passes = attrAsFloat(flameAttrDict, "passes");
        elc->hasMask |= hasPasses;
    }
    if (flameAttrDict.find("estimator_radius") != flameAttrDict.end()) {
        elc->estimatorRadius = attrAsFloat(flameAttrDict, "estimator_radius");
        elc->hasMask |= hasEstimatorRadius;
    }
    else {
        elc->estimatorRadius = 9;
        elc->hasMask |= hasEstimatorRadius;
    }
    if (flameAttrDict.find("estimator_minimum") != flameAttrDict.end()) {
        elc->estimatorMinimum = attrAsFloat(flameAttrDict, "estimator_minimum");
        elc->hasMask |= hasEstimatorMinimum;
    }
    else {
        elc->estimatorMinimum = 0;
        elc->hasMask |= hasEstimatorMinimum;
    }
    if (flameAttrDict.find("estimator_curve") != flameAttrDict.end()) {
        elc->estimatorCurve = attrAsFloat(flameAttrDict, "estimator_curve");
        elc->hasMask |= hasEstimatorCurve;
    }
    else {
        elc->estimatorCurve = 0.6;
        elc->hasMask |= hasEstimatorCurve;
    }
    
    if (flameAttrDict.find("highlight_power") != flameAttrDict.end()) {
        elc->highlightPower = attrAsFloat(flameAttrDict, "highlight_power");
        flm->params.highlightPower = elc->highlightPower;
        elc->hasMask |= hasHighlightPower;
    }
    if (flameAttrDict.find("cpufuse") != flameAttrDict.end()) {
        elc->cpuFuseIterations = attrAsInt(flameAttrDict, "cpufuse");
        elc->defaultFuseIters = false;
    }
    if (flameAttrDict.find("gpufuse") != flameAttrDict.end()) {
        elc->gpuFuseIterations = attrAsInt(flameAttrDict, "gpufuse");
        elc->defaultFuseIters = false;
    }
    if (xformCount == 0)
        elc->xaosMatrix = std::make_unique<XaosForXformsVector>();
    else
        elc->xaosMatrix = std::make_unique<XaosForXformsVector>(xformCount);
    
    // get hue
    if (flameAttrDict.find("hue") != flameAttrDict.end())
        hue = attrAsFloat(flameAttrDict, "hue");
    else
        hue = 0.0f;
    flm->params.hue = hue;
    
    // Get <flame center="??"
    if (flameAttrDict.find("center") != flameAttrDict.end()) {
        std::vector<std::string> array = splitOnSpaces(flameAttrDict["center"].value());
        flm->params.center[0] =  std::strtof(array[0].c_str(), nullptr);
        flm->params.center[1] =  std::strtof(array[1].c_str(), nullptr);
    }
    else
    {
        flm->params.center[0]=0;
        flm->params.center[1]=0;
    }
    
    // Get <flame size="??"
    float x,y;
    if (flameAttrDict.find("size") != flameAttrDict.end()) {
        std::vector<std::string> array = splitOnSpaces(flameAttrDict["size"].value());
        x =  std::strtof(array[0].c_str(), nullptr);
        y =  std::strtof(array[1].c_str(), nullptr);
    }
    else
    {
        x = 100;
        y = 100;
    }
    float scale = attrAsFloat(flameAttrDict, "scale", 1.f);
    flm->params.scale   = scale;
    float zoom = attrAsFloat(flameAttrDict, "zoom", 0.f);
    flm->params.zoom   = zoom;
    flm->params.size[0] = x/scale/powf(2.f, zoom);
    flm->params.size[1] = y/scale/powf(2.f, zoom);
    
    flm->params.quality = flm->params.desiredQuality = attrAsFloat(flameAttrDict, "quality");
    
    // Get <flame background="??"
    if (flameAttrDict.find("background") != flameAttrDict.end()) {
        std::vector<std::string> array = splitOnSpaces(flameAttrDict["background"].value());
        flm->params.background.r =  std::strtof(array[0].c_str(), nullptr);
        flm->params.background.g =  std::strtof(array[1].c_str(), nullptr);
        flm->params.background.b =  std::strtof(array[2].c_str(), nullptr);
        
        // compensate for files that are not saved per Flam3 spec
        if (flm->params.background.r > 1.f)
            flm->params.background.r /= 255.f;
        if (flm->params.background.g > 1.f)
            flm->params.background.g /= 255.f;
        if (flm->params.background.b > 1.f)
            flm->params.background.b /= 255.f;
    }
    else
    {
        flm->params.background.r = 0.f;
        flm->params.background.g = 0.f;
        flm->params.background.b = 0.f;
    }
    flm->params.background.a = 1.f;
    
    flm->params.cam_yaw         = attrAsFloat(flameAttrDict, "cam_yaw", 0.f);
    flm->params.cam_pitch       = attrAsFloat(flameAttrDict, "cam_pitch", 0.f);
    
    // we want reasonable defaults for these even though they are not used in this mode
    // in case the user switches to these
    flm->params.cam_fov         = attrAsFloat(flameAttrDict, "cam_fov", 90.f);
    flm->params.cam_near        = attrAsFloat(flameAttrDict, "cam_near", 0.01f);
    flm->params.cam_orthowide   = attrAsFloat(flameAttrDict, "cam_orthowide", 2.f);
    flm->params.cam_x           = attrAsFloat(flameAttrDict, "cam_x", 0.f);
    flm->params.cam_y           = attrAsFloat(flameAttrDict, "cam_y", 0.f);
    flm->params.cam_z           = attrAsFloat(flameAttrDict, "cam_z", 0.f);
    flm->params.cam_perspective = attrAsFloat(flameAttrDict, "cam_perspective", 0.f);
    flm->params.cam_dof         = attrAsFloat(flameAttrDict, "cam_dof", 0.f);
    flm->params.cam_zpos        = attrAsFloat(flameAttrDict, "cam_zpos", 0.f);
    flm->params.clipToNDC       = attrAsInt(flameAttrDict, "clipToNDC", 0);
    
    if (flameAttrDict.find("cam_dist") != flameAttrDict.end()) {
        float cam_dist = attrAsFloat(flameAttrDict, "cam_dist", 0.f);
        cam_dist = cam_dist == 0.f ? 1.e-6 : cam_dist;
        flm->params.cam_perspective = 1.f / cam_dist;
    }
    
    flm->params.brightness = attrAsFloat(flameAttrDict, "brightness", 6.f);
    flm->params.gamma      = attrAsFloat(flameAttrDict, "gamma", 4.f);
    // if no alpha_gamma in the file, use the same value as standard gamma
    flm->params.alphaGamma = attrAsFloat(flameAttrDict, "alpha_gamma", flm->params.gamma);
    
    if (flameAttrDict.find("gammaThreshold") != flameAttrDict.end())
        flm->params.gammaThreshold = attrAsFloat(flameAttrDict, "gammaThreshold", 0.f);
    else if (flameAttrDict.find("gamma_threshold") != flameAttrDict.end())
        flm->params.gammaThreshold = attrAsFloat(flameAttrDict, "gamma_threshold", 0.f);
    else
        flm->params.gammaThreshold = 0.f;
    
    flm->params.vibrancy = attrAsFloat(flameAttrDict, "vibrancy", 1.f);

    bool hasRotate = flameAttrDict.find("rotate") != flameAttrDict.end();
    flm->params.rotation = hasRotate ? attrAsFloat(flameAttrDict, "rotate")*(2.0*M_PI/360.0) : 0.f;
    
    //	flm->params.supersampleWidth = 0.25f;
    flm->params.supersampleWidth = attrAsFloat(flameAttrDict, "super_sample_width", 0.25f);
    
    if (flameAttrDict.find("estimator_radius") != flameAttrDict.end())
        flm->params.estimatorRadius = attrAsInt(flameAttrDict, "estimator_radius", 7);
    else
        flm->params.estimatorRadius = 7;
    flm->params.frame = attrAsInt(flameAttrDict, "time", 0);
    
    //grab the xForms
    for (int n = 0; n < xformCount; n++)	 {
        AttrDict xformAttrDict = attrsAsDict(xForms[n]);
        extractXform(&flm->trans[n],
                     xForms[n],
                     flm->xformVarChains[n],
                     xformAttrDict,
                     n,
                     xformCount,
                     elc,
                     variationSet,
                     false);
    }
    //grab the xForms
    for (int n = 0; n < finalXformCount; n++)	 {
        AttrDict xformAttrDict = attrsAsDict(finalXforms[n]);
//        struct ElectricSheepStuff *finalElc = new ElectricSheepStuff(); ???
        extractXform(&flm->finals[n],
                     finalXforms[n],
                     flm->finalVarChains[n],
                     xformAttrDict,
                     n,
                     finalXformCount,
                     elc,          // should this be different ???
                     variationSet,
                     true);
    }
    
    //Now read in the palette
    switch (paletteType)
    {
        case 0:  // use 256 0f these:   <color index="244" rgb="255 178 109" location="0.55" />  location field is optional
        {
            for (int n = 0; n < palette.size(); n++)
            {
                AttrDict paletteAttrDict = attrsAsDict(palette[n]);
                int i = attrAsInt(paletteAttrDict, "index");
                
                float location = 0.f;
                if (paletteAttrDict.find("location") != paletteAttrDict.end())
                    location = attrAsFloat(paletteAttrDict, "location");
                else
                    location = palette.size() == 0 ? 0.f : (float)i/(palette.size() - 1);
                
                std::vector<std::string> colorIndices = splitOnSpaces(paletteAttrDict["rgb"].value());
                flm->colorIndex[i].r = std::strtof(colorIndices[0].c_str(), nullptr) /255.0f;
                flm->colorIndex[i].g = std::strtof(colorIndices[1].c_str(), nullptr) /255.0f;
                flm->colorIndex[i].b = std::strtof(colorIndices[2].c_str(), nullptr) /255.0f;
                flm->colorIndex[i].a = 1.0f;
                flm->colorLocations[i] = location;
            }
        }break;
        case 1:
        {
            AttrDict paletteAttrDict = attrsAsDict(palette[0]);
            std::string paletteHex   = attrAsString(paletteAttrDict, "data");
            std::transform(paletteHex.begin(), paletteHex.end(),paletteHex.begin(), ::toupper); // uppercase the string
            
            const char *rawHex = paletteHex.c_str();
            
            // unichar *buffer = (unichar *)calloc([paletteHex length], sizeof( unichar ) );
            // [ paletteHex getCharacters:buffer range:NSMakeRange(0, [paletteHex length])];
            
            int index = 0;
            for (int j = 0; j < count; j++)
            {
                readHexRGBString(rawHex, &flm->colorIndex[j], &index, paletteType);
                flm->colorLocations[j] = count == 0 ? 0.f : (float)j/(count - 1);
            }
        }break;
        case 2: // uses <palette count="256" format="RGB"> ...		</palette> with big Hex RGB String holding 256 color values
        {
            xml_node & node = palette[0];
            std::string paletteHex = node.value();
            std::transform(paletteHex.begin(), paletteHex.end(),paletteHex.begin(), ::toupper); // uppercase the string
            
            const char *rawHex = paletteHex.c_str();
            
            int index = 0;
            for (int j = 0; j < count; j++)
            {
                readHexRGBString(rawHex, &flm->colorIndex[j], &index, paletteType);
                flm->colorLocations[j] = count == 0 ? 0.f : (float)j/(count - 1);
            }
        }break;
        case 3: // uses 1 of 707 predefined palettes
        {
            AttrDict paletteAttrDict = attrsAsDict(palette[standardPaletteIndex]);
            std::string paletteHex   = attrAsString(paletteAttrDict, "data");
            std::transform(paletteHex.begin(), paletteHex.end(),paletteHex.begin(), ::toupper); // uppercase the string

            const char *rawHex = paletteHex.c_str();
            
            int index = 0;
            for (int j = 0; j < count; j++)
            {
                readHexRGBString(rawHex, &flm->colorIndex[j], &index, paletteType);
                flm->colorLocations[j] = count == 0 ? 0.f : (float)j/(count - 1);
            }
        }break;
        case 4: // create random color palette in place of missing flam3 palette
        {
            flm->numColors = 256;
            flm->colorIndex = new struct rgba[256];
            flm->colorLocations = new float[256];
            
            // make a gradient with 3 to 32 random colors
            int nodeCount = randIntInRange(3, 32);
            std::vector<ColorNode> nodes;
            nodes.reserve(nodeCount);
            float offset   = 1.0f/(nodeCount - 1);
            float maxIndex = (float)nodeCount - 1.f;
            for (int i = 0; i < nodeCount; i++) {
                Color color{ rand()/(float)RAND_MAX, rand()/(float)RAND_MAX, rand()/(float)RAND_MAX, 1.0f };
                float locationTarget = i * offset;
                float location       = roundf(maxIndex * locationTarget)/maxIndex; // round to integral location indexes (0 - 255)
                ColorNode node(color, location);
                nodes.push_back(node);
            }
            FlameParse::saveColorNodes(nodes, flm);
        }break;
    }
    if (count == 1) { // for this special case, we allocated two colorindexes, copy the first to the second
        flm->colorIndex[1] = flm->colorIndex[0];
        flm->colorLocations[1] = 1.f;
    }
    
    //Apply the hue transformation, if present, to the palette
    if (hue != 0)
    {
        for (int n = 0; n < flm->numColors; n++)
        {
            float r = flm->colorIndex[n].r;
            float g = flm->colorIndex[n].g;
            float b = flm->colorIndex[n].b;
            float max = fmaxf(fmaxf(r,g),b);
            float min = fminf(fminf(r,g),b);
            float h,s,v;
            if (max==min)
                h = 0.0f;
            else if (max == r)
                h = .16666666666f*(g-b)/(max-min);
            else if (max == g)
                h = .16666666666f*(b-r)/(max-min)+.33333333f;
            else
                h = .16666666666f*(r-g)/(max-min)+.66666666f;
            if (max == 0.0f)
                s = 0.0f;
            else
                s = (max-min)/(max);
            v = max;
            h += hue;
            h = h-floorf(h);
            int hi = ((int)floorf(h*6.0f))%6;
            float f = h*6.0f-floorf(h*6.0f);
            float p = v*(1.0f-s);
            float q = v*(1.0f-f*s);
            float t = v*(1.0f-(1.0f-f)*s);
            switch (hi)
            {
                case 0:
                {
                    r = v;
                    g = t;
                    b = p;
                }break;
                case 1:
                {
                    r = q;
                    g = v;
                    b = p;
                }break;
                case 2:
                {
                    r = p;
                    g = v;
                    b = t;
                }break;
                case 3:
                {
                    r = p;
                    g = q;
                    b = v;
                }break;
                case 4:
                {
                    r = t;
                    g = p;
                    b = v;
                }break;
                case 5:
                {
                    r = v;
                    g = p;
                    b = q;
                }break;
            }
            flm->colorIndex[n].r = r;
            flm->colorIndex[n].g = g;
            flm->colorIndex[n].b = b;
        }
    }
    return flm;
}

//This is called by OpenFlame to parse xforms.
void FlameParse::extractXform(xForm *dest,
                              xml_node &xformEle,
                              SharedVariationChain &chain,
                              AttrDict & attrs,
                              int index,
                              int xformCount,
                              SharedElc & elc,
                              SharedVariationSet &variationSet,
                              bool forFinalXform)
{
    std::vector<std::string> coefs = splitOnSpaces(attrs["coefs"].value());
    dest->a = std::strtof(coefs[0].c_str(), nullptr);
    dest->d = std::strtof(coefs[1].c_str(), nullptr);
    dest->b = std::strtof(coefs[2].c_str(), nullptr);
    dest->e = std::strtof(coefs[3].c_str(), nullptr);
    dest->c = std::strtof(coefs[4].c_str(), nullptr);
    dest->f = std::strtof(coefs[5].c_str(), nullptr);
    
    dest->weight = attrAsFloat(attrs, "weight", 1.f);
    
    std::vector<std::string> color = splitOnSpaces(attrs["color"].value());
    dest->color = color.size() == 0 ? 0 : std::strtof(color[0].c_str(), nullptr);
    
    dest->symmetry  = attrAsFloat(attrs, "symmetry", 0.f);
    dest->var_color = attrAsFloat(attrs, "var_color", 1.f);
    
    // color_speed = (1 - symmetry)/2
    // symmetry    = 1 - 2 * color_speed
    // rotates     = symmetry <= 0
    
    if (dest->symmetry > 0.0f)
        dest->rotates = 0;
        else
            dest->rotates = 1;
    
    bool hasColorspeed = attrs.find("color_speed") != attrs.end();

    dest->symmetry = hasColorspeed ? 1.f - 2.f*attrAsFloat(attrs, "color_speed") : dest->symmetry;
    dest->rotates  = attrAsInt(attrs, "animate", dest->rotates);
    dest->opacity  = attrAsFloat(attrs, "opacity", 1.f);
    
    // set default chaos values
    if (! forFinalXform) {
        for (int i = 0; i < xformCount; i++)
            elc->xaosMatrix->setChaos(index, i, 1.f);
        // override those chaos values
        if (attrs.find("chaos") != attrs.end()) {
            
            std::vector<std::string> chaosWeights = splitOnSpaces(attrs["chaos"].value());
            int to = 0;
            for (uint i = 0; i < chaosWeights.size(); i++) {
                float chaosWeight = std::strtof(chaosWeights[i].c_str(), nullptr);
                elc->xaosMatrix->setChaos(index, to++, chaosWeight);
            }
        }
        if (attrs.find("links") != attrs.end()) {
            std::vector<std::string> chaosLinks = splitOnSpaces(attrs["links"].value());
            int to = 0;
            for (uint i = 0; i < chaosLinks.size(); i++) {
                int chaosLink = std::atoi(chaosLinks[i].c_str());
                bool linked = chaosLink == 0 ? false : true;
                elc->xaosMatrix->setLocked(index, to++, linked);
            }
        }
    }
    
    NodeVector groupEles;
    for (xml_node node : xformEle.children("variationGroup"))
        groupEles.push_back(node);
    if (groupEles.size() == 0) {  // older files dont have variationGroup elements
        SharedVariationGroup &group = chain->operator[](0);
        group->parseGroupAttrs(attrs, variationSet);
    }
    else {
        chain->clear();
        for (xml_node & groupEle : groupEles) {            
            AttrDict groupAttrs        = attrsAsDict(groupEle);
            bool amNormal              = groupEle.attribute("normal").as_bool();
            SharedVariationGroup group = VariationGroup::groupWithAmNormal(amNormal, chain);
            chain->addGroup(group);
            group->parseGroupAttrs(groupAttrs, variationSet);
        }
        chain->checkForNormalGroup();
    }
    
    std::vector<std::string> post = splitOnSpaces(attrAsString(attrs, "post", "1 0 0 1 0 0"));
    dest->pa = std::strtof(post[0].c_str(), nullptr);
    dest->pd = std::strtof(post[1].c_str(), nullptr);
    dest->pb = std::strtof(post[2].c_str(), nullptr);
    dest->pe = std::strtof(post[3].c_str(), nullptr);
    dest->pc = std::strtof(post[4].c_str(), nullptr);
    dest->pf = std::strtof(post[5].c_str(), nullptr);
}

// read the packed RGB hex string and assign them to the rgba color struct instance
void FlameParse::readHexRGBString(const char *rawHex, struct rgba *color, int *_index, int paletteType)
{
    int index = *_index;
    //Yay, we get to read in a bunch of ugly hex values!
    unsigned char i = 0;
    
    while (rawHex[index] == ' ' || rawHex[index] == '\t') {
        index++;
    }
    while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
        index++;
    if (paletteType == 1) // case 1: RGBA with unpopulated A, we ignore their values
        index += 2;  //skip over unused alpha value
    else if (paletteType == 3) { // case 3: RGBA with populated A, we ignore their values
        index++;  //skip over unused alpha value
        while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
            index++;
        index++;
        while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
            index++;
    }
    // case 2: RGB - no alpha values to read or skip over
    if (rawHex[index] <= '9')
        i+=rawHex[index]-'0';
    else
        i+=rawHex[index]-'A'+10;
    index++;
    i = i<<4;
    while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
        index++;
    if (rawHex[index] <= '9')
        i+=rawHex[index]-'0';
    else
        i+=rawHex[index]-'A'+10;
    index++;
    color->r = ((float)i)/255.0f;
    i = 0;
    while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
        index++;
    if (rawHex[index] <= '9')
        i+=rawHex[index]-'0';
    else
        i+=rawHex[index]-'A'+10;
    index++;
    i = i<<4;
    while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
        index++;
    if (rawHex[index] <= '9')
        i+=rawHex[index]-'0';
    else
        i+=rawHex[index]-'A'+10;
    index++;
    color->g = ((float)i)/255.0f;
    i = 0;
    while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
        index++;
    if (rawHex[index] <= '9')
        i+=rawHex[index]-'0';
    else
        i+=rawHex[index]-'A'+10;
    index++;
    i = i<<4;
    while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
        index++;
    if (rawHex[index] <= '9')
        i+=rawHex[index]-'0';
    else
        i+=rawHex[index]-'A'+10;
    index++;
    color->b = ((float)i)/255.0f;
    color->a = 1.0f;
    *_index = index;
}

void FlameParse::saveColorNodes(std::vector<ColorNode> paletteNodes, SharedFlame &flame)
{
    int paletteSize = (int)paletteNodes.size();
    if (flame->numColors != paletteSize) {
        delete [] flame->colorIndex;
        delete [] flame->colorLocations;
        flame->colorIndex = new rgba[paletteSize];
        flame->colorLocations = new float[paletteSize];
        flame->numColors = paletteSize;
    }
    int i = 0;
    for (ColorNode & node : paletteNodes) {
        Color & nodeColor = node.color;
        flame->colorIndex[i].r   = nodeColor.red;
        flame->colorIndex[i].g   = nodeColor.green;
        flame->colorIndex[i].b   = nodeColor.blue;
        flame->colorIndex[i].a   = nodeColor.alpha;
        flame->colorLocations[i] = node.location;
        i++;
    }
}

// parse the Flame XML document and create a dictionary of the embedded variation sets
//  DONT replace variation sets with those already loaded by the app - we want the actual flame file's variation sets
//  the variation sets will be loaded ONLY into the dictionary - NOT loaded for the app to use in general
VariationSetsVector FlameParse::preloadInternalVariationSets(xml_document &doc)
{
    VariationSetsVector variationSetsInDoc;

    xml_node aNode = doc.document_element();
    if (aNode.type() == node_element && strcmp(aNode.name(),"flame") != 0) // in case there are multiple flames
        aNode = aNode.first_child();
    
        // install variation sets as needed
        do {
            if (aNode.type() == node_element && strcmp(aNode.name(),"variationSet") == 0) {
                xml_node & variationSetEle = aNode;
                std::string uuid           = variationSetEle.attribute("uuid").value();
                
                VariationSetsDict variationSetsInDocDict;
                for (SharedVariationSet & vs : variationSetsInDoc) {
                    variationSetsInDocDict[vs->uuid] = vs;
                }
                
                // we dont have this variation set, so install it
                // NOTE: some old source snippets usage of sincos dont work with sincos pattern matching for CUDA
                SharedVariationSet variationSet = VariationSet::makeVariationSet(variationSetEle, false);
                variationSetsInDocDict[uuid] = variationSet;
                // dont overwrite a varset with the same uuid that has already been installed
                VariationSet::variationSets[uuid] = variationSet;
                
                variationSetsInDoc.push_back(variationSet);
            }
        } while ((aNode = aNode.next_sibling()));
    return variationSetsInDoc;
}

void FlameParse::filterVariationNames(StringsSet & _names)
{
    // remove the predefined Names from this set of names
    StringsSet _predefinedNames = std::move(Variation::predefinedNames());
    
    StringsVector namesSet;
    std::copy(_names.begin(), _names.end(), std::inserter(namesSet, namesSet.end()));
    std::sort(namesSet.begin(), namesSet.end());

    StringsVector predefinedNames;
    std::copy(_predefinedNames.begin(), _predefinedNames.end(), std::inserter(predefinedNames, predefinedNames.end()));
    std::sort(predefinedNames.begin(), predefinedNames.end());
    
    StringsSet names;
    set_difference(namesSet.begin(),namesSet.end(),predefinedNames.begin(), predefinedNames.end(),
                   std::inserter(names, names.end()));
    
    // special handling for linear3D - an alias for linear
    if (names.find("linear3D") != names.end()) {
        names.erase("linear3D");
        names.insert("linear");
    }
    if (names.find("flatten") != names.end()) {
        names.erase("flatten");
        names.insert("post_flatten");
    }
}

StringsSet FlameParse::preParseFlameEleForVariationsUsed(xml_node & flameEle)
{
    StringsSet names;
    for (xml_node & xformEle : flameEle.children("xform")) {
        for (xml_attribute & attribute : xformEle.attributes()) {
            names.insert(attribute.name());
        }
    }
    for (xml_node & xformEle : flameEle.children("finalxform")) {
        for (xml_attribute & attribute : xformEle.attributes()) {
            names.insert(attribute.name());
        }
    }
    FlameParse::filterVariationNames(names);
    
    return names;
}

bool FlameParse::flameEleCanUseVariationSet(xml_node &flameEle, SharedVariationSet & variationSet)
{
    StringsSet names = std::move(FlameParse::preParseFlameEleForVariationsUsed(flameEle));
    return variationSet->hasAllVariationsNeededBySet(names);
}

FlameExpandedVector FlameParse::readFromData(const std::string &string)
{
    xml_document doc;
    xml_parse_result result = doc.load_string(string.c_str(), parse_cdata);
    
    if (result.status != status_ok) {
        throw std::runtime_error(result.description());
    }
    xml_node root = doc.document_element();
    
    StringsSet set = FlameParse::preParseFlameEleForVariationsUsed(root);
    
    bool changed = false;
    VariationSetsVector variationSetsInDoc = VariationSet::preParseDocForVariationsUsed(doc, "foo", true, changed);
    
    // DEBUG
    //    std::cout << "All VarSets\n" << VariationSet::allVarsetsDescription(VariationSet::variationSets);
    //    std::cout << "VarSets in Doc\n" << VariationSet::allVarsetsDescription(variationSetsInDoc);
    
    // older docs dont have a variation set - use Flam3 legacy
    SharedVariationSet firstVariationSet = variationSetsInDoc.size() > 0 ? variationSetsInDoc.front() : VariationSet::legacyVariationSet();
    
    FlameExpandedVector flames;
#ifdef _WIN32
	std::string palettePath = ::applicationSupportPath() + "/flam3-palettes.xml";
#else
	std::string palettePath = pathForResource("flam3-palettes", "xml");
#endif
    FlameParse::parseDoc(flames, doc, palettePath, firstVariationSet);
    FlameParse::uniqueFrameNames(flames);
    
    //    for (size_t i = 0; i < flames.size(); i++) {
    //        std::cout << "Fractal #" << i << " =========================\n";
    //        SharedFlameExpanded fe = flames[i];
    //        std::cout << fe->textReport() << "\n\n";
    //    }
    return flames;
}

// when initializing from a varset file
FlameExpandedVector FlameParse::loadFromPath(const std::string & path)
{
    try {
        std::ifstream in(path.c_str());
        if (in.fail()) {
            std::string msg = string_format("Failed to open file: %s", path.c_str());
            throw std::runtime_error(msg);
        }
        std::string contents((std::istreambuf_iterator<char>(in)),
                             std::istreambuf_iterator<char>());
        
        return readFromData(contents);
    } catch (std::exception & e) {
        systemLog(e.what());
        return std::move(FlameExpandedVector());
    }
}
