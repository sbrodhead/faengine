
// Copyright 2008 Steven Brodhead
 
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef ElectricSheepStuff_hpp
#define ElectricSheepStuff_hpp

#include "Exports.hpp"
#include "XaosMatrixVector.hpp"
#include "Enums.hpp"
#include <string>
#include <unordered_map>
#include <vector>
#include <memory>

using uint = unsigned int;
using UniqueXaosMatrix = std::unique_ptr<XaosForXformsVector>;

enum fileHas {
    hasFilter               = 1,
    hasTemporalFilterWidth  = 2,
    hasTemporalFilterExp    = 4,
    hasGammaThreshold       = 8,
    hasSupersample          = 16,
    hasTemporalSamples      = 32,
    hasPasses               = 64,
    hasEstimatorRadius      = 128,
    hasEstimatorMinimum     = 256,
    hasEstimatorCurve       = 512,
    hasPalette              = 1024,
    hasFilterShape          = 2048,
    hasTemporalFilterType   = 4096,
    hasPaletteMode          = 8192,
    hasInterpolationType    = 16384,
    hasInterpolation        = 32768,
    hasPaletteInterpolation = 65536,
    hasHighlightPower       = 131072,
};

enum FlameInterpolation {
    interpolationSmooth = 0,
    interpolationLinear = 1
};

enum PaletteInterpolation {
    paletteHSV   = 0,
    paletteSweep = 1
};

//enum PaletteMode {
//	modeStep   = 0,
//	modeLinear = 1, // the old default
//    modeSmooth = 2  //
//};

enum InterpolationType {
    interpolationTypeLinear = 0,
    interpolationTypeLog    = 1,
    interpolationTypeOld    = 2,
};

enum FilterShape {
    shapeGaussian  = 0, // default
    shapeHermite   = 1,
    shapeBox       = 2,
    shapeTriangle  = 3,
    shapeBspline   = 4,
    shapeMitchell  = 5,
    shapeBlackman  = 6,
    shapeHanning   = 7,
    shapeHamming   = 8,
    shapeQuadratic = 9,
    shapeLanczos2  = 10,
    shapeLanczos3  = 11,
    shapeBell      = 12,
    shapeCatmull   = 13,
    shapeKaiser    = 14,
};

enum TemporalFilterType {
    temporalBox      = 0, // default
    temporalExp      = 1,
    temporalGaussian = 2,
};

struct FAENGINE_API ElectricSheepStuff
{
    int       hasMask;      // bitmask with "fileHas" bits set for every item seen
    
    enum FilterShape          filterShape;
    enum TemporalFilterType   temporalFilterType;
    enum FlameInterpolation   interpolation;
    enum PaletteInterpolation paletteInterpolation;
    enum PaletteMode          paletteMode;
    enum InterpolationType    interpolationType;
    float     filter;
    float     temporalFilterWidth;
    float     temporalFilterExp;
    float     gammaThreshold;
    int       supersample;
    int       temporalSamples;
    int       passes;
    int       estimatorRadius;			// default 9
    int       estimatorMinimum;			// default 0
    float     estimatorCurve;			// default 0.4
    int       palette;
    float     highlightPower;
    bool      defaultFuseIters;
    uint      cpuFuseIterations;
    uint      gpuFuseIterations;
    UniqueXaosMatrix xaosMatrix;
    
    ElectricSheepStuff();
    ElectricSheepStuff(const ElectricSheepStuff &other);
    ElectricSheepStuff(const std::vector<char> &bytes);
    
    bool usesXaos();
    void deleteChildren();
    
    static enum FilterShape getFilterShapeFor(const std::string & choice);
    static enum TemporalFilterType getTemporalTypeFor(const std::string & choice);
    static enum PaletteMode getPaletteModeFor(const std::string & choice);
    static enum PaletteInterpolation getPaletteInterpolationFor(const std::string & choice);
    static enum FlameInterpolation getFlameInterpolationFor(const std::string & choice);
    static enum InterpolationType getInterpolationTypeFor(const std::string &choice);
    
    static std::string stringForFilterShape(enum FilterShape shape);
    static std::string stringForTemporalType(enum TemporalFilterType shape);
    static std::string stringForPaletteMode(enum PaletteMode mode);
    static std::string stringForPaletteInterpolation(enum PaletteInterpolation interpolation);
    static std::string stringForFlameInterpolation(enum FlameInterpolation interpolation);
    static std::string stringForInterpolationType(enum InterpolationType type);
};

#endif /* ElectricSheepStuff_hpp */
