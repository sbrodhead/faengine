//
//  WindowsInterface.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/9/16.
//  Copyright � 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include <Windows.h>
#include <ShlObj.h>
#include <Shlwapi.h>
#include <string>
#include<vector>

#include "resource.h"
#include "WindowsInterface.hpp"

using std::string;

HMODULE hDLLModule = NULL;

// usage
//     CHAR msgText[256];
//     getLastErrorText(msgText,sizeof(msgText));
static CHAR *                      //   return error message
getLastErrorText(                  // converts "Last Error" code into text
	CHAR *pBuf,                        //   message buffer
	ULONG bufSize)                     //   buffer size
{
	DWORD retSize;
	LPTSTR pTemp = NULL;

	if (bufSize < 16) {
		if (bufSize > 0) {
			pBuf[0] = '\0';
		}
		return(pBuf);
	}
	retSize = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_ARGUMENT_ARRAY,
		NULL,
		GetLastError(),
		LANG_NEUTRAL,
		(LPTSTR)&pTemp,
		0,
		NULL);
	if (!retSize || pTemp == NULL) {
		pBuf[0] = '\0';
	}
	else {
		pTemp[strlen(pTemp) - 2] = '\0'; //remove cr and newline character
		sprintf(pBuf, "%0.*s (0x%x)", bufSize - 16, pTemp, GetLastError());
		LocalFree((HLOCAL)pTemp);
	}
	return(pBuf);
}

__declspec(dllexport) BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	hDLLModule = (HMODULE)hModule;
	return TRUE;
}


static HMODULE GetCurrentModule()
{ // NB: XP+ solution!
	HMODULE hModule = NULL;
	BOOL status =
	GetModuleHandleEx(
		GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS  | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
		(LPCTSTR)GetCurrentModule,
		&hModule);

	char buf[256];
	if (status == 0) {
		getLastErrorText(buf, sizeof(buf));
		printf(buf);
	}
	else {
		DWORD status =
		GetModuleFileName(hModule, buf, sizeof(buf));
	}

	return hModule;
}

static std::string getEmbeddedResourceAsString(int resourceID, const char * resType)
{
	HMODULE g_hModDll = GetCurrentModule();

	HRSRC hRscr = FindResource(g_hModDll, MAKEINTRESOURCE(resourceID), resType);
	if (hRscr) {
		HGLOBAL hgRscr = LoadResource(g_hModDll, hRscr);
		if (hgRscr) {
			void * pRscr = LockResource(hgRscr);
			size_t size = SizeofResource(g_hModDll, hRscr);
			return std::string((const char *)pRscr, size);
		}
	}
	return "";
}

static Blob getEmbeddedResourceAsBlob(int resourceID, const char * resType)
{
	HMODULE g_hModDll = GetCurrentModule();

	HRSRC hRscr = FindResource(g_hModDll, MAKEINTRESOURCE(resourceID), resType);
	if (hRscr) {
		HGLOBAL hgRscr = LoadResource(g_hModDll, hRscr);
		if (hgRscr) {
			void * pRscr = LockResource(hgRscr);
			size_t size = SizeofResource(g_hModDll, hRscr);
			return Blob((uchar *)pRscr, (uchar *)pRscr +size);
		}
	}
	return Blob();
}

std::string getExecutablePath()
{
	static char buf[MAX_PATH];
	DWORD length = GetModuleFileNameA(NULL, buf, sizeof(buf));
	return buf;
}

std::string getThisDLLPath()
{
	static char buf[MAX_PATH];
	DWORD length = GetModuleFileNameA(GetCurrentModule(), buf, sizeof(buf));
	return buf;
}

// on Windows resources are embedded in DLL
std::string pathForResource(const char * _basename, const char * _extension)
{
	return getThisDLLPath();
}

std::string cachePath()
{
	static TCHAR szPath[MAX_PATH];
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szPath)))
	{
		std::string path = szPath;
		path.append("/FractalArchitectEngine/Caches/com.centcom.FractalArchitectEngine");
		return path;
	}
	return  "";
}



std::string variationSetsDirectoryPath()
{
	static TCHAR szPath[MAX_PATH];
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szPath)))
	{
		std::string path = szPath;
		path.append("/FractalArchitectEngine/Application Support/VariationSets");
		return path;
	}
	return "";
}

std::string applicationSupportPath()
{
	static TCHAR szPath[MAX_PATH];
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szPath)))
	{
		std::string path = szPath;
		path.append("/FractalArchitectEngine/Application Support");
		return path;
	}
	return "";
}

std::string testDataPath()
{
	static TCHAR szPath[MAX_PATH];
	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_MYDOCUMENTS, NULL, 0, szPath)))
	{
		std::string path = szPath;
		path.append("/TestData");
		return path;
	}
	return "";
}


static std::string cachedMyLibraryPath()
{
	return variationSetsDirectoryPath() + "/myLibrary.xml";
}

static std::string cachedLibraryPath()
{
	return variationSetsDirectoryPath() + "/library.xml";
}

const std::string loadEmbeddedResource(const char * _basename, const char * _extension)
{
	std::string basename(_basename);
	std::string extension(_extension);
	if (basename == "library")
		return getEmbeddedResourceAsString(IDR_LIBRARY_XML, "LIBRARY_XML");
	else if (basename == "myLibrary")
		return getEmbeddedResourceAsString(IDR_MY_LIBRARY_XML, "LIBRARY_XML");
	else if (basename == "Flam4_3dKernal_Template") {
		if (extension == "cu")
			return getEmbeddedResourceAsString(IDR_CUDA_TEMPLATE1, "CUDA_TEMPLATE");
		else
			return getEmbeddedResourceAsString(IDR_CL_KERNEL_GPU, "CL_TEMPLATE");
	}
	else if (basename == "Flam4_3dKernalCPU_Template") {
		return getEmbeddedResourceAsString(IDR_CL_KERNEL_CPU, "CL_TEMPLATE");
	}
	else if (basename == "flam3-palettes")
		return getEmbeddedResourceAsString(IDR_PALETTES, "XML");

	return "";
}

const Blob loadEmbeddedProfile(const char * _name)
{
	std::string name(_name);
	if (name == "sRGB.icc")
		return getEmbeddedResourceAsBlob(IDR_SRGB, "ICC");
	else if (name == "AdobeRGB1998.icc")
		return getEmbeddedResourceAsBlob(IDR_ADOBERGB, "ICC");
	else if (name == "WideGamutRGB.icc")
		return getEmbeddedResourceAsBlob(IDR_WIDEGAMUT, "ICC");
	else
		return getEmbeddedResourceAsBlob(IDR_PROPHOTO, "ICC");
}

double osVersion()
{
	return 1.; // dont know what to report for Windows here ???
}

// Expand relative path to absolute path
const char *expandTildeInPath(const char *_path)
{
	static char buf[MAX_PATH];

	return _fullpath(buf, _path, sizeof(buf));
}

void foo()
{
	//--------------------------------------------------------------------
	//  Declare variables.

	HCRYPTPROV hCryptProv;
	HCRYPTHASH hHash;

	//--------------------------------------------------------------------
	// Get a handle to a cryptography provider context.


	if (CryptAcquireContext(
		&hCryptProv,
		NULL,
		NULL,
		PROV_RSA_FULL,
		0))
	{
		printf("CryptAcquireContext complete. \n");
	}
	else
	{
		printf("Acquisition of context failed.\n");
		exit(1);
	}
	//--------------------------------------------------------------------
	// Acquire a hash object handle.

	if (CryptCreateHash(
		hCryptProv,
		CALG_MD5,
		0,
		0,
		&hHash))
	{
		printf("An empty hash object has been created. \n");
	}
	else
	{
		printf("Error during CryptBeginHash!\n");
		exit(1);
	}

	// Insert code that uses the hash object here.

	//--------------------------------------------------------------------
	// After processing, hCryptProv and hHash must be released.

	if (hHash)
		CryptDestroyHash(hHash);
	if (hCryptProv)
		CryptReleaseContext(hCryptProv, 0);

}

std::string MD5(const BYTE * bytes, size_t byteLength)
{
	HCRYPTPROV hProv;
	HCRYPTHASH hHash;
	// Create provider and hash algorithm
	CryptAcquireContext(&hProv, nullptr, nullptr, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);
	CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash);
	// Hash the data    
	CryptHashData(hHash, bytes, byteLength, 0);
	// Get back the hash value    
	BYTE hashBytes[128 / 8];
	DWORD paramLength = sizeof(hashBytes);
	CryptGetHashParam(hHash, HP_HASHVAL, hashBytes, &paramLength, 0);
	// Cleanup
	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv, 0);

	// Convert hash to base64 string
	DWORD base64Length;
	CryptBinaryToStringA(hashBytes, sizeof(hashBytes), CRYPT_STRING_BASE64, nullptr, &base64Length);

	TCHAR *buf = (TCHAR *)malloc(base64Length *sizeof(TCHAR));
	CryptBinaryToStringA(hashBytes, sizeof(hashBytes), CRYPT_STRING_BASE64, buf, &base64Length);

	std::string result = std::string(buf);
	free(buf);
	return result;
}

std::string md5DigestString(const std::string & str)
{
	return MD5((unsigned char *)str.c_str(), str.length() + 1);
}

// get array of file names in this directory
std::vector<std::string> directoryFileNamesContents(const char *path)
{
	std::vector<std::string> names;
	WIN32_FIND_DATA ffd;
	TCHAR szDir[MAX_PATH + 1];

	strncpy(szDir, path, MAX_PATH);
	strcat(szDir, "\\*");

	// Find the first file in the directory.
	HANDLE hFind = FindFirstFile(szDir, &ffd);

	if (INVALID_HANDLE_VALUE == hFind)
	{
		return names;
	}

	// List all the files in the directory with some info about them.
	do
	{
		if (! (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
		{
			names.emplace_back(ffd.cFileName);
		}
	} while (FindNextFile(hFind, &ffd) != 0);
	FindClose(hFind);
	return names;
}

std::vector<std::string> directorySubDirContents(const char *path)
{
	std::vector<std::string> names;
	WIN32_FIND_DATA ffd;
	TCHAR szDir[MAX_PATH + 1];

	strncpy(szDir, path, MAX_PATH);
	strcat(szDir, "\\*");

	// Find the first file in the directory.
	HANDLE hFind = FindFirstFile(szDir, &ffd);

	if (INVALID_HANDLE_VALUE == hFind)
	{
		return names;
	}

	// List all the files in the directory with some info about them.
	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			names.emplace_back(ffd.cFileName);
		}
	} while (FindNextFile(hFind, &ffd) != 0);
	FindClose(hFind);
	return names;
}

int DeleteDirectory(const std::string &refcstrRootDirectory,
	bool              bDeleteSubdirectories = true)
{
	bool            bSubdirectory = false;       // Flag, indicating whether
												 // subdirectories have been found
	HANDLE          hFile;                       // Handle to directory
	std::string     strFilePath;                 // Filepath
	std::string     strPattern;                  // Pattern
	WIN32_FIND_DATA FileInformation;             // File information

	strPattern = refcstrRootDirectory + "\\*.*";
	hFile = ::FindFirstFile(strPattern.c_str(), &FileInformation);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (FileInformation.cFileName[0] != '.')
			{
				strFilePath.erase();
				strFilePath = refcstrRootDirectory + "\\" + FileInformation.cFileName;

				if (FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if (bDeleteSubdirectories)
					{
						// Delete subdirectory
						int iRC = DeleteDirectory(strFilePath, bDeleteSubdirectories);
						if (iRC)
							return iRC;
					}
					else
						bSubdirectory = true;
				}
				else
				{
					// Set file attributes
					if (::SetFileAttributes(strFilePath.c_str(),
						FILE_ATTRIBUTE_NORMAL) == FALSE)
						return ::GetLastError();

					// Delete file
					if (::DeleteFile(strFilePath.c_str()) == FALSE)
						return ::GetLastError();
				}
			}
		} while (::FindNextFile(hFile, &FileInformation) == TRUE);

		// Close handle
		::FindClose(hFile);

		DWORD dwError = ::GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
			return dwError;
		else
		{
			if (!bSubdirectory)
			{
				// Set directory attributes
				if (::SetFileAttributes(refcstrRootDirectory.c_str(),
					FILE_ATTRIBUTE_NORMAL) == FALSE)
					return ::GetLastError();

				// Delete directory
				if (::RemoveDirectory(refcstrRootDirectory.c_str()) == FALSE)
					return ::GetLastError();
			}
		}
	}
	return 0;
}

// empty a directory - but leave the directory itself
void empty_dir(const char *path)
{
	std::string directory(path);
	DeleteDirectory(path, true);
}

// find unit test data file
std::string pathForTestResource(const char *_bundlePath, const char *_filename)
{
	std::string path = testDataPath();
	path.append("/").append(_filename);
	return path;
}
