//
//  Variation.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 4/30/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef Variation_hpp
#define Variation_hpp

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <memory>

#include "Exports.hpp"
#include "pugixml.hpp"

enum VariationDimension {
    dim2D     = 0,
    dim3D     = 1,
    both2D_3D = 2,
};

enum WhichUsage {
    preUsage = 0,
    normalUsage,
    postUsage,
    linkedUsage,
    finalXformUsage,
};

enum VariationStatus {
    useable = 0,
    broken,
    inDevelopment,
};

class Variation;
class VariationSet;
class VariationParameter;

using SharedParameter      = std::shared_ptr<VariationParameter>;
using SharedVariation      = std::shared_ptr<Variation>;
using SharedVariationConst = std::shared_ptr<Variation const>;
using ParametersDictionary = std::unordered_map<std::string, SharedParameter>;
using ParametersVector     = std::vector<SharedParameter>;
using StringsSet           = std::unordered_set<std::string>;
using StringsVector        = std::vector<std::string>;
using SharedVariationSet  = std::shared_ptr<VariationSet>;
using WeakPVariationSet    = std::weak_ptr<VariationSet>;
using VariationsVector     = std::vector<Variation>;
using PairVariation        = std::pair<std::string, SharedVariation>;



class FAENGINE_API Variation : public std::enable_shared_from_this<Variation>  {
    friend class Flam4ClRuntime;
    friend class Flam4CudaRuntime;
    friend class VariationGroup;
    friend class VariationSet;
    friend class VariationParameter;

    using UniquenessFunc = bool (Variation::*)(const std::string &string) const;
    
private:
    Variation(const Variation &other);
public:
    Variation(SharedVariationSet set, const std::string &_name, const std::string &_key, float value);
    static SharedVariation makeSharedCopy(const Variation &v);
    
    Variation & operator=(const Variation &other);
    
    bool operator==(const Variation &matrix) const;
    bool operator!=(const Variation &matrix) const;
    
#pragma mark Description
    const char * is3DString();
    const char * directColorString();
    
    std::string description();
    
#pragma mark Getters & Setters

    std::string getKey() const                 { return key; }
    void setKey(const std::string &_key);

    std::string getName() const                { return name; }
    void setName(const std::string &_name);
    
    size_t getXformIndex() const               { return xformIndex; }
    void setXformIndex(size_t _xformIndex);
    
    const std::string & getSource() const;
    void setSource(const std::string &_source);
    
    const std::string & getFunctionsSource() const;
    void setFunctionsSource(const std::string &_functionsSource);
    
    size_t parameterCount() const;
    bool hasParameters() const;
    ParametersDictionary & getParameters()   { return parameters; }
    
    float weight() const;
    
    static StringsSet predefinedNames();
    
//    Variation();  // compiler keeps asking for VariationParameter() - but it needs this
private:
    
    void setKeyAsIs (const std::string &variationKey)    { key=variationKey; }
    void setNameAsIs(const std::string &variationName)   { name=variationName; }

    bool keyIsNotUnique(const std::string &variationKey) const;
    bool nameIsNotUnique(const std::string &variationName) const;
    
    void changedName(const std::string &oldName, const std::string &newName);
    
    static bool nameCompare(const SharedVariation &a, const SharedVariation &b);
    static bool namePairCompare(const PairVariation &a, const PairVariation &b);
    static bool xformIndexCompare(const SharedVariation &a, const SharedVariation &b);
    static bool xformIndexPairCompare(const PairVariation &a, const PairVariation &b);
    static bool prePostXformIndexCompare(const SharedVariation &a, const SharedVariation &b);
    static bool prePostXformIndexPairCompare(const PairVariation &_a, const PairVariation &_b);
    
    static size_t replace(const std::string &pattern, const std::string &replacement, const std::string &string);
    
    std::string compressedWhiteSpaceSource() const;
    std::string compressedWhiteSpaceFunctionsSource() const;
    
    bool isCompressedEqualTo(const SharedVariation &v) const;
    
    static std::string compressWhiteSpace    (const std::string string);
    static std::string replaceXformWithVarpar(const std::string &string);
    static std::string replaceVarparWithXform(const std::string &string);
    
    static std::string cudaCompatibleSincos(const std::string &string);
    static std::string convertToCudaCompatibleSincos(const std::string &lines);
    static std::string convertToCUDAfunctions(const std::string  &string);    

    void parameterChangedName(const SharedParameter &parameter, const std::string &oldName);
    void changedKey(const std::string &oldKey);
    void localXformIndexParamOrdering();
    
    std::string autoVersion(std::string string, UniquenessFunc f, const StringsSet &names, bool bumpVersion);
    std::string autoVersionKeyKeepDifferentFrom(const StringsSet &names, bool bumpVersion);
    std::string autoVersionNameKeepDifferentFrom(const StringsSet &names, bool bumpVersion);
    void autoVersionDifferentFrom(const StringsSet &names);
    
    void xmlElement(pugi::xml_node & parent);
    
    
    // ==== Members ====

    WeakPVariationSet  variationSet;
    std::string     name;		// name for display
    std::string     key;		// KVC key
    std::string     source;
    std::string     functionsSource;
    std::string     functionsPrototype;
    std::string     license;
    std::string     author;
    std::string     copyright;
    std::string     md5sum;
    std::string     md5sum2;
    ParametersDictionary parameters;
    float           defaultValue;
    bool            selected;
    size_t          xformIndex;  // index into xformVars array for this variation
    enum VariationDimension dimension;
    bool            usesDirectColor;
    enum WhichUsage usage;
    enum VariationStatus status;
    size_t          usageCount; // how many variation sets use this variation?
    
};

#endif /* Variation_hpp */
