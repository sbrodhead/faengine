//
//  Image.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//  Created by Steven Brodhead on 5/16/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef Image_hpp
#define Image_hpp

#include <vector>
#include <string>
#include <memory>

#include "Exports.hpp"

namespace FA {
    class Image;
}

using uchar       = unsigned char;
using uint        = unsigned int;
using Blob        = std::vector<uchar>;
using UniqueBlob  = std::unique_ptr<Blob>;
using SharedImage = std::shared_ptr<FA::Image>;

namespace FA {
    
    
    struct Image {
        std::unique_ptr<uchar[]> bitmap;
        uint bitDepth;
        uint width;
        uint height;
        std::string map;
        UniqueBlob profile;
        bool flipped;
		bool transparent;

        Image();
        Image(uint _width, uint _height, const std::string & _map, uint _bitDepth, bool _transparent, uchar * _bitmap);
        Image(const Image &);
        
        FAENGINE_API SharedImage RGBAtoRGB();
        
        uint bytesPerRow();
        uint bitsPerPixel();
        uint bytesPerPixel() { return bitsPerPixel()/8; }
        uint samplesPerPixel();
        static uint bytesPerRowFor(const std::string &map, uint width, uint bitDepth);
        
        Image *clippedImage(uint x, uint y, uint width, uint height);
        
    };
}

#endif /* Image_hpp */
