//
//  ClProgram.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/17/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef ClProgram_hpp
#define ClProgram_hpp

#ifdef __APPLE__
#include <OpenCL/OpenCL.h>
#elif defined(_WIN32)
#include <CL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include <vector>

#include "Exports.hpp"
#include "DeviceProgram.hpp"

class ClContext;
class ClDeviceKind;
class ClProgram;
class VariationSet;

using uint            = unsigned int;
using SharedClContext = std::shared_ptr<ClContext>;
using SharedClProgram = std::shared_ptr<ClProgram>;

class FAENGINE_API ClProgram : public DeviceProgram {
    friend class Flam4ClRuntime;
public:
    ClProgram(SharedClContext ctx, SharedDeviceKind _deviceKind, bool forceRebuild, const SharedVariationSet &_variationSet);
    virtual ~ClProgram();
    
    static size_t getMaxWorkGroupSizeForKernel(cl_kernel kernel,  cl_device_id device);
    
    ClDeviceKind *clDeviceKind();
    
private:
    cl_program loadProgramFromBinaryContents(std::vector<char> & binaryContents, cl_device_id *deviceIDs);
    cl_program createProgramFromSourceWithWarpSize(uint warpSize, std::string & includeDirective);
    void logErrStatusForProgramName(std::string &programName, cl_device_id * deviceIDs);
    
    void saveBinariesForDeviceIDs(cl_device_id * deviceIDs);
    void saveBinaryForDeviceID(cl_device_id deviceID);
    
    void makeKernelsForDeviceIDs(cl_device_id * deviceIDs);
    
    // ========== Members =====================    
    cl_program program;
    
    cl_kernel iteratePointsKernal;
    cl_kernel setBufferKernal;
    cl_kernel reductionKernal;
    cl_kernel postProcessStep1Kernal;
    cl_kernel postProcessStep2Kernal;
    cl_kernel colorCurveRGB3ChannelsKernal;
    cl_kernel colorCurveRGBChannelKernal;
    cl_kernel FlexibleDensityEstimationKernal;
    cl_kernel RGBA128FtoRGBA32UKernal;
    cl_kernel RGBA128FtoBGRA32UKernal;
    cl_kernel RGBA128FtoRGBA64UKernal;
    cl_kernel RGBA128FtoRGBA128FKernal;
    cl_kernel MergeKernal;
    cl_kernel readChannelKernal;
    cl_kernel writeChannelKernal;
    cl_kernel writeChannelStripedKernal;
    cl_kernel convolveRowKernal;
    cl_kernel convolveColKernal;
};

#endif /* ClProgram_hpp */
