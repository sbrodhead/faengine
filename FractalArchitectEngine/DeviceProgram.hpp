//
//  DeviceProgram.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef DeviceProgram_hpp
#define DeviceProgram_hpp

#include "Exports.hpp"
#include "Enums.hpp"
#include "jsoncpp/json/json.h"

#include <memory>
#include <string>
#include <chrono>

class DeviceContext;
class DeviceKind;
class DeviceProgram;
class VariationSet;

using SharedVariationSet  = std::shared_ptr<VariationSet>;
using SharedDeviceContext = std::shared_ptr<DeviceContext>;
using SharedDeviceKind    = std::shared_ptr<DeviceKind>;
using WeakDeviceContext   = std::weak_ptr<DeviceContext>;
using WeakDeviceKind      = std::weak_ptr<DeviceKind>;
using SharedDeviceProgram = std::shared_ptr<DeviceProgram>;

using namespace std::chrono;

class FAENGINE_API DeviceProgram {
public:
    DeviceProgram(SharedDeviceContext ctx, SharedDeviceKind _deviceKind, const SharedVariationSet & _variationSet);
    virtual ~DeviceProgram() = default;
    
    static std::string cachePath();
    static std::string bundleIdentifier();
    static void setupFAEngineCaches();
    
    static Json::Value readDevicesJson();
    static void writeDevicesJson(Json::Value & json);
    
    std::string pathForBinaryForUuid(const std::string & _uuid);
    std::string pathForBinariesFolderForUuid(const std::string & _uuid);
    
    std::vector<char> binaryContentsForUuid(const std::string & _uuid);
    void writeBinaryForUuid(const std::string &_uuid, const char * data, size_t length);
    std::string sourceContents(enum GpuPlatformUsed gpuPlatformInUse);
    
    // ========= Members ============
    SharedVariationSet  variationSet;
    std::string         basename;
    std::string         bitcodeFilename;
    std::string         uuid;
    SharedDeviceContext context;
    WeakDeviceKind      deviceKind;
    size_t              maxWorkGroupSize; // evaluated as minimum of these kernels
    duration<double>    buildTime;
};

#endif /* DeviceProgram_hpp */
