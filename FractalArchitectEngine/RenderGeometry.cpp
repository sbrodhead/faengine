//
//  RenderGeometry.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/26/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "RenderGeometry.hpp"
#include "Flame.hpp"
#include "VariationSet.hpp"
#include <math.h>

using namespace FA;

// crop the fractal by x,y expressed in view coordinates
void RenderGeometry::cropResizeFlame(Flame *flame, Rect cropRect, Size from)
{
    Point viewCenter = Point(from.width/2.f, from.height/2.f);
    Point cropCenter = Point(midX(cropRect), midY(cropRect));
    
    // offset the viewCenter (view will be in lower left corner of new crop without this)
    viewCenter.x += cropCenter.x - viewCenter.x;
    viewCenter.y += cropCenter.y - viewCenter.y;
    
    float deltaX = cropCenter.x - viewCenter.x;
    float deltaY = -(cropCenter.y - viewCenter.y); // adjust for flipped Y coordinate
    
    float docViewWidth  =  flame->params.size[0] * flame->params.scale * powf(2.f, flame->params.zoom);
    float docViewHeight =  flame->params.size[1] * flame->params.scale * powf(2.f, flame->params.zoom);
    float viewScaleX = from.width  / docViewWidth;
    float viewScaleY = from.height / docViewHeight;
    
    // first rescale translation delta based on view window size changes
    float sDeltaX = deltaX / viewScaleX;
    float sDeltaY = deltaY / viewScaleY;
    
    // scale deltaX, deltaY to normalized fractal coordinates
    // then unrotate & untranslate to get the new centers in normalized fractal coordinates
    float xDelta = sDeltaX/(flame->params.scale * powf(2.f, flame->params.zoom));
    float yDelta = sDeltaY/(flame->params.scale * powf(2.f, flame->params.zoom));
    
    float x1 = xDelta * cosf(-flame->params.rotation) - yDelta * sinf(-flame->params.rotation);
    float y1 = xDelta * sinf(-flame->params.rotation) + yDelta * cosf(-flame->params.rotation);
    
    flame->params.center[0] += x1;
    flame->params.center[1] += y1;
    
    float fwidth  = flame->params.size[0] * flame->params.scale * powf(2.f, flame->params.zoom); // normalized file coordinates
    float fheight = flame->params.size[1] * flame->params.scale * powf(2.f, flame->params.zoom);
    
    float ftileWidth  = roundf(cropRect.size.width/from.width *fwidth);
    float ftileHeight = roundf(cropRect.size.height/from.height *fheight);
    
    flame->params.size[0] = ftileWidth/flame->params.scale/powf(2.f, flame->params.zoom);
    flame->params.size[1] = ftileHeight/flame->params.scale/powf(2.f, flame->params.zoom);
}

// crop the fractal by x,y expressed in view coordinates
void RenderGeometry::cropResizeFlame(Flame *flame, Rect cropRect, Size from, VariationSet *variationSet)
{
    Point viewCenter = Point(from.width/2.f, from.height/2.f);
    Point cropCenter = Point(midX(cropRect), midY(cropRect));
    
    // offset the viewCenter (view will be in lower left corner of new crop without this)
    viewCenter.x += cropCenter.x - viewCenter.x;
    viewCenter.y += cropCenter.y - viewCenter.y;
    
    float deltaX = cropCenter.x - viewCenter.x;
    float deltaY = -(cropCenter.y - viewCenter.y); // adjust for flipped Y coordinate
    
    float docViewWidth  =  flame->params.size[0] * flame->params.scale * powf(2.f, flame->params.zoom);
    float docViewHeight =  flame->params.size[1] * flame->params.scale * powf(2.f, flame->params.zoom);
    float viewScaleX = from.width  / docViewWidth;
    float viewScaleY = from.height / docViewHeight;
    
    // first rescale translation delta based on view window size changes
    float sDeltaX = deltaX / viewScaleX;
    float sDeltaY = deltaY / viewScaleY;
    
    // scale deltaX, deltaY to normalized fractal coordinates
    // then unrotate & untranslate to get the new centers in normalized fractal coordinates
    float xDelta = sDeltaX/(flame->params.scale * powf(2.f, flame->params.zoom));
    float yDelta = sDeltaY/(flame->params.scale * powf(2.f, flame->params.zoom));
    
    float x1, y1;
    if (variationSet->is3DCapable) {
        x1 = xDelta * cosf(flame->params.rotation) - yDelta * sinf(flame->params.rotation);
        y1 = xDelta * sinf(flame->params.rotation) + yDelta * cosf(flame->params.rotation);
    }
    else {
        x1 = xDelta * cosf(-flame->params.rotation) - yDelta * sinf(-flame->params.rotation);
        y1 = xDelta * sinf(-flame->params.rotation) + yDelta * cosf(-flame->params.rotation);
    }
    
    flame->params.center[0] += x1;
    flame->params.center[1] += y1;
    
    float fwidth  = flame->params.size[0] * flame->params.scale * powf(2.f, flame->params.zoom); // normalized file coordinates
    float fheight = flame->params.size[1] * flame->params.scale * powf(2.f, flame->params.zoom);
    
    float ftileWidth  = roundf(cropRect.size.width/from.width *fwidth);
    float ftileHeight = roundf(cropRect.size.height/from.height *fheight);
    
    flame->params.size[0] = ftileWidth/flame->params.scale/powf(2.f, flame->params.zoom);
    flame->params.size[1] = ftileHeight/flame->params.scale/powf(2.f, flame->params.zoom);
}

void RenderGeometry::changeAspectRatio(Flame *flame, float width, float height)
{
    float ftileWidth  = flame->params.size[0] * flame->params.scale/powf(2.f, flame->params.zoom);
    float ftileHeight = flame->params.size[1] * flame->params.scale/powf(2.f, flame->params.zoom);
    Size  imageSize = Size(ftileWidth, ftileHeight);
    Rect targetRect = Rect(0.f, 0.f, width, height);
    
    float imageScaleFactor = sqrtf((targetRect.size.width * targetRect.size.height)/(imageSize.width * imageSize.height));
    Size fromSize          = Size(imageScaleFactor * imageSize.width, imageScaleFactor * imageSize.height);
    
    cropResizeFlame(flame, Rect(0.f, 0.f, width, height), fromSize);
}

