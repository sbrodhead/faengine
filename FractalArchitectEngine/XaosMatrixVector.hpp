//
//  XaosMatrixVector.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/5/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef XaosMatrixVector_hpp
#define XaosMatrixVector_hpp

#include <stddef.h>
#include "Exports.hpp"

struct FAENGINE_API Xaos
{
    float weight;
    bool  locked;
    
    Xaos();
    Xaos & operator=(const Xaos &other);
};

struct FAENGINE_API XaosVector {
    size_t num;        // number of xforms
    Xaos    *toWeight;
    
    XaosVector();
    ~XaosVector();
    
};

struct FAENGINE_API XaosForXformsVector {
    size_t             num;
    struct XaosVector *from;
    
    XaosForXformsVector();
    XaosForXformsVector(size_t num);
    XaosForXformsVector(size_t num, XaosForXformsVector *old);
    XaosForXformsVector(size_t num, size_t num2, void *brick);
    XaosForXformsVector(size_t num, XaosForXformsVector *old, size_t deleteOldIndex);
    XaosForXformsVector(const XaosForXformsVector &old, size_t splitIndex);
    XaosForXformsVector(const XaosForXformsVector &other);
    XaosForXformsVector(size_t num, void *brick);
    XaosForXformsVector(const XaosForXformsVector &other, unsigned *permutedIndex);
    
    ~XaosForXformsVector();
    void deleteChildren();
    
    float chaos(size_t from, size_t to);
    bool locked(size_t from, size_t to);
    bool equivalent(XaosForXformsVector *other);
    void setChaos(size_t from, size_t to, float newChaos, bool locked=false);
    void setLocked(size_t from, size_t to, bool locked);
    Xaos  *brick();
    float *weightsBrick();
    Xaos *brick2(size_t count);
    bool usesXaos();    
};

#endif /* XaosMatrixVector_hpp */
