//
//  FlameParse.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/7/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef FlameParse_hpp
#define FlameParse_hpp

#include <vector>
#include <memory>
#include <unordered_set>

#include "Exports.hpp"
#include "pugixml.hpp"
#include "Utilities.hpp"
#include "common.hpp"
#include "Flame.hpp"

class ColorNode;
class ElectricSheepStuff;
class Flame;
class FlameExpanded;
class FlameMetaData;
class VariationSet;

using SharedFlame         = std::shared_ptr<Flame>;
using SharedFlameMetaData = std::shared_ptr<FlameMetaData>;
using SharedElc           = std::shared_ptr<ElectricSheepStuff>;
using SharedFlameExpanded = std::shared_ptr<FlameExpanded>;
using SharedVariationSet  = std::shared_ptr<VariationSet>;
using FlameExpandedVector = std::vector<SharedFlameExpanded>;
using AttrDict            = std::unordered_map<std::string, pugi::xml_attribute>;
using NodeVector          = std::vector<pugi::xml_node>;
using SharedPointVector   = std::shared_ptr<std::vector<FA::Point>>;
using VariationSetsDict   = std::unordered_map<std::string, SharedVariationSet>;
using VariationSetsVector = std::vector<SharedVariationSet>;
using StringsSet          = std::unordered_set<std::string>;


class FAENGINE_API FlameParse {
public:
    static void uniqueFrameNames(FlameExpandedVector & data);
    static SharedVariationSet parseFlameElementForVariationSet(const pugi::xml_node &flameEle, const SharedVariationSet &defaultVariationSet);
    
    static SharedFlame parseFlame(pugi::xml_node &flameEle,
                                  SharedFlameMetaData &flameMeta,
                                  SharedElc &elc,
                                  int flameCount,
                                  const std::string &palettePath,
                                  SharedVariationSet &variationSet);
    static void parseDoc(FlameExpandedVector &flames,
                         pugi::xml_document &doc,
                         const std::string &palettePath,
                         SharedVariationSet &defaultVariationSet);
    static AttrDict attrsAsDict(pugi::xml_node & ele);
    static void extractXform(xForm *dest,
                             pugi::xml_node &xformEle,
                             SharedVariationChain &chain,
                             AttrDict & attrs,
                             int index,
                             int xformCount,
                             SharedElc & elc,
                             SharedVariationSet &variationSet,
                             bool forFinalXform);

    static SharedPointVector attrAsCurve(AttrDict &attrDict, const std::string &key);
    static std::vector<std::string> splitOnSpaces(const std::string &str);
    static void readHexRGBString(const char *rawHex, struct rgba *color, int *_index, int paletteType);
    static void saveColorNodes(std::vector<ColorNode> paletteNodes, SharedFlame &flame);
    
    static float attrAsFloat(AttrDict &attrDict, const std::string &key);
    static float attrAsFloat(AttrDict &attrDict, const std::string &key, float defaultValue);
    static std::string attrAsString(AttrDict &attrDict, const char *key);
    static std::string attrAsString(AttrDict &attrDict, const std::string &key);
    static std::string attrAsString(AttrDict &attrDict, const char *key, const char *defaultValue);
    static std::string attrAsString(AttrDict &attrDict, const std::string &key, const std::string & defaultValue);
    static int attrAsInt(AttrDict &attrDict, const std::string &key);
    static int attrAsInt(AttrDict &attrDict, const std::string &key, int defaultValue);
    static bool attrAsBool(AttrDict &attrDict, const std::string &key);
    
    static VariationSetsVector preloadInternalVariationSets(pugi::xml_document &doc);
    static StringsSet preParseFlameEleForVariationsUsed(pugi::xml_node & flameEle);
    static void filterVariationNames(StringsSet & names);
    static bool flameEleCanUseVariationSet(pugi::xml_node &flameEle, SharedVariationSet & variationSet);
    
    static FlameExpandedVector readFromData(const std::string &string);
    static FlameExpandedVector loadFromPath(const std::string & _path);
};

#endif /* FlameParse_hpp */
