//
//  NaturalCubicCurve.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

// Numerical Analysis (pth Edition) R L Burden & J D Faires
// http://www.personal.psu.edu/jjb23/web/html/sl455SP12/ch3/CH03_5B.pdf
//
//    If f is defined at a=x0 <x1 <···<xn =b,then f has a unique natural spline interpolant S on the nodes x0, x1, . . ., xn;
//    that is, a spline interpolant that satisfies the natural boundary conditions
//    S′′(a) = 0 and S′′(b) = 0

//    Using the notation
//    Sj(x) = aj + bj(x−xj) + cj(x−xj)2 + dj(x−xj)3
//    the boundary conditions in this case imply that cn = 1/2*S′′n(xn)*2 = 0
//    and that 0 = S′′(x0) = 2c0 +6d0(x0 −x0) so c0 =0.
//    The two equations c0 = 0 and cn = 0 together with the equations
//
//    hj−1cj−1 +2(hj−1 +hj)cj +hjcj+1 = 3/hj (aj+1 −aj)− 3/hj-1 (aj −aj−1)
//
//    produce a linear system described by the vector equation Ax = b:

//    A is the (n + 1) × (n + 1) matrix:
//         1   0       0        ···   ···        0 
//         h0 2(h0+h1) h1                        0 
//         0  h1       2(h1+h2) h2       ...     0 
//         . .. .. .. ..                         0 
//        ￼ . .          hn−2    2(hn−2+hn−1)  hn−1 
//         0            0       0                1 

//    b and x are the vectors:

//    b =
//                   0                              
//         3/h1(a2 −a1)  − 3/h0 (a1 −a0)            |
//         ...                                      
//         3/hn-1 (an − an−1) − 3/hn-2 (an−1 − an−2)
//                   0                              
//
//    x =
//        c0 
//        c1 
//           
//        cn 

//    Given a function f defined on [a, b] and a set of nodes
//    a=x0 <x1 <···<xn =b, a cubics pline interpolant S for f is a function that satisfies the following conditions:
//    (a) S(x) is a cubic polynomial, denoted Sj(x),
//        on the subinterval [xj , xj +1 ] for each j = 0, 1, . . . , n − 1;
//    (b) Sj(xj)        = f(xj) and   Sj(xj+1) = f(xj+1) for each j=0,1,...,n−1;
//    (c) Sj+1(xj+1)    = Sj(xj+1)   for each j = 0,1,...,n−2; (Impliedby(b).)
//    (d) S′j+1 (xj+1)  = S′j(xj+1)  for each j = 0,1,...,n−2;
//    (e) S′′j+1 (xj+1) = S′′j(xj+1) for each j = 0,1,...,n−2; j+1 j
//
//    (f) One of the following sets of boundary conditions is satisfied:
//    (i) S′′(x0) = S′′(xn) = 0 (natural (or free) boundary);
//    (ii) S′(x0) = f′(x0) and S′(xn) = f′(xn) (clamped boundary).

//    To construct the cubic spline interpolant S for the function f,
//    defined at the numbers x0 < x1 < · · · < xn,
//    satisfying S′′(x0) = S′′(xn) = 0
//        (Note: S(x)=Sj(x)=aj +bj(x−xj)+cj(x−xj)2 +dj(x−xj)3 for xj ≤ x ≤ xj+1.):

//        INPUT     n; x0,x1,...,xn;  a0 = f(x0),a1 = f(x1),...,an = f(xn)
//        OUTPUT    aj,bj,cj,dj for j =0,1,...,n−1
//        Step 1    For i = 0,1,...,n−1 set hi =xi+1 − xi
//        Step 2    For i = 1, 2, . . . , n − 1 set
//                      αi=3/hi(ai+1 − ai)− 3/hi-1 (ai − ai−1)
//
//        Step 3    Set l0 =1   μ0 = 0   z0 = 0
//        Step 4    For i=1,2,...,n−1
//                        set li = 2(xi+1 − xi−1) − hi−1μi−1
//                        μi     = hi/li
//                        zi     = (αi − hi−1zi−1)/li
//        Step 5    Set ln = 1  zn = 0  cn = 0
//        Step 6    For j = n−1,n−2,...,0
//                    set cj = zj −μjcj+1
//                    bj     = (aj+1 − aj)/hj − hj(cj+1 + 2cj)/3
//                    dj     = (cj+1 − cj)/(3hj)
//        Step OUTPUT (aj,bj,cj,dj for j = 0,1,...,n−1)&STOP

#include "NaturalCubicCurve.hpp"
#include "FlameParse.hpp"
#include <memory>
#include <cstdlib>

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif

NaturalCubicCurve::NaturalCubicCurve()
: CurveDefinition<Point, eNaturalCubic>(0.f, 0.f, false, false, false), As(), Bs(), Cs(), Ds(), Xs()
{
    initAsLinearRamp();
    solveCubics();
}

NaturalCubicCurve::NaturalCubicCurve(std::vector<Point> *_controlPoints,
                                     bool _flipped,
                                     bool _mirrored)
: CurveDefinition<Point, eNaturalCubic>(0.f, 0.f, _flipped, _mirrored, false), As(), Bs(), Cs(), Ds(), Xs()
{
    if (! _controlPoints)
        initAsLinearRamp();
    else
        controlPoints = *_controlPoints;
    solveCubics();
}

NaturalCubicCurve::NaturalCubicCurve(const SharedPointVector & _controlPointsData,
                                     bool _flipped,
                                     bool _mirrored)
: CurveDefinition<Point, eNaturalCubic>(0.5f, 0.5f, _flipped, _mirrored, false), As(), Bs(), Cs(), Ds(), Xs()
{
    if (! _controlPointsData)
        initAsLinearRamp();
    else
        setControlPointsData(_controlPointsData);
    solveCubics();
}

NaturalCubicCurve::NaturalCubicCurve(const NaturalCubicCurve &other)
: CurveDefinition<Point, eNaturalCubic>(0.f, 0.f, false, false, false), As(), Bs(), Cs(), Ds(), Xs()
{
    initAsLinearRamp();
    setControlPoints(other.controlPoints);
    solveCubics();
}

void NaturalCubicCurve::initAsLinearRamp()
{
    controlPoints = std::vector<Point>();
    
    controlPoints.push_back(Point{ 0.f, 0.f });
    controlPoints.push_back(Point{ 1.f, 1.f });
}

SharedPointVector NaturalCubicCurve::linearRampControlPointsData()
{
    SharedPointVector controlPointsData = std::make_shared<std::vector<Point>>(2);
    Point *_controlPoints               = (Point *)controlPointsData->data();
    
    _controlPoints[0] = Point{0.f, 0.f};
    _controlPoints[1] = Point{1.f, 1.f};
    return controlPointsData;
}

UniqueNatural NaturalCubicCurve::curveWithControlPointsData(const SharedPointVector &controlPointsData)
{
    if (! controlPointsData)
        return std::make_unique<NaturalCubicCurve>();
    else
        return std::make_unique<NaturalCubicCurve>(controlPointsData, false, false);
}

void NaturalCubicCurve::setControlPoints(const std::vector<Point> &_controlPoints)
{
    if (controlPoints.data() == _controlPoints.data())
        return;
    controlPoints = _controlPoints;
}

size_t NaturalCubicCurve::cpCount() const
{
    return controlPoints.size();
}

void NaturalCubicCurve::solveCubics()
{
    size_t n     = controlPoints.size() - 1;
    UniqueFloatVector x     = std::make_unique<std::vector<float>>(n+1, 0.f);
    UniqueFloatVector y     = std::make_unique<std::vector<float>>(n+1, 0.f);
    UniqueFloatVector h     = std::make_unique<std::vector<float>>(n,   0.f);
    UniqueFloatVector alpha = std::make_unique<std::vector<float>>(n,   0.f);
    UniqueFloatVector l     = std::make_unique<std::vector<float>>(n+1, 0.f);
    UniqueFloatVector mu    = std::make_unique<std::vector<float>>(n+1, 0.f);
    UniqueFloatVector z     = std::make_unique<std::vector<float>>(n+1, 0.f);
    UniqueFloatVector B     = std::make_unique<std::vector<float>>(n,   0.f);
    UniqueFloatVector c     = std::make_unique<std::vector<float>>(n+1, 0.f);
    UniqueFloatVector d     = std::make_unique<std::vector<float>>(n,   0.f);
    
    for (size_t i = 0; i <= n; i++) {
        Point p = controlPoints[i];
        x->operator[](i)           = p.x;
        y->operator[](i)           = p.y;
    }
    for (size_t i = 0; i < n; i++)
        h->operator[](i) = x->operator[](i+1) - x->operator[](i);
        
        alpha->operator[](0)     = 0.f;
        for (size_t i = 1; i < n; i++)
            alpha->operator[](i) = 3.f/h->operator[](i)*(y->operator[](i+1) - y->operator[](i)) - 3.f/h->operator[](i-1)*(y->operator[](i) - y->operator[](i-1));
            
            l->operator[](0) = 1.f;
            mu->operator[](0) = 0.f;
            z->operator[](0) = 0.f;
            
            for (size_t i = 1; i < n; i++) {
                l->operator[](i)  = 2.f*(x->operator[](i+1) - x->operator[](i-1)) - h->operator[](i-1)*mu->operator[](i-1);
                mu->operator[](i) = h->operator[](i)/l->operator[](i);
                z->operator[](i)  = (alpha->operator[](i) - h->operator[](i-1)*z->operator[](i-1))/l->operator[](i);
            }
    
    l->operator[](n) = 1.f;
    c->operator[](n) = 0.f;
    z->operator[](n) = 0.f;
    
    //    [self logMatricesWithH:h alpha:alpha z:z n:n];
    
    for (int j = (int)n - 1; j >= 0; j--) {
        c->operator[](j) = z->operator[](j) - mu->operator[](j)*c->operator[](j+1);
        B->operator[](j) = (y->operator[](j+1) - y->operator[](j))/h->operator[](j) - h->operator[](j)*(c->operator[](j+1) + 2.f*c->operator[](j))/3.f;
        d->operator[](j) = (c->operator[](j+1) - c->operator[](j))/(3.f*h->operator[](j));
    }
    
    Xs = std::move(x);
    As = std::move(y);
    Bs = std::move(B);
    Cs = std::move(c);
    Ds = std::move(d);
}

// evaluate for x in range X0 to Xn
float NaturalCubicCurve::evaluateForX(float x)
{
    //  defined at the numbers x0 < x1 < · · · < xn
    //  return Sj(x) = aj + bj(x−xj) + cj(x−xj)^2 + dj(x−xj)^3    for  xj ≤ x ≤ xj+1.
    
    float *A = (float *)As->data();
    float *B = (float *)Bs->data();
    float *C = (float *)Cs->data();
    float *D = (float *)Ds->data();
    float *X = (float *)Xs->data();
    
    if (x < X[0])
        return A[0]; // return first
    
    size_t n = controlPoints.size() - 1;
    for (size_t j = 0; j < n; j++) {
        if (x <= X[j+1]) {
            float t = x - X[j];
            return A[j] + B[j]*t + C[j]*t*t + D[j]*t*t*t;
        }
    }
    return A[n];  // return last
}

float NaturalCubicCurve::evaluateForXClipped(float x)
{
    float val = evaluateForX(x);
    val       = val < 0.f ? 0.f : val;
    val       = val > 1.f ? 1.f : val;
    return val;
}

// evaluate for x in range X0 to Xn
float NaturalCubicCurve::evaluateIntegralForX(float x)
{
    //  defined at the numbers x0 < x1 < · · · < xn
    //  return Sj(x) = aj + bj(x−xj) + cj(x−xj)^2 + dj(x−xj)^3    for  xj ≤ x ≤ xj+1.
    
    //                3      2
    //    Integrate[d t  + c t  + b t + a, t] ==
    //
    //    t (12 a + t (6 b + t (4 c + 3 d t)))
    //    ------------------------------------
    //    12
    
    // Note integral of linear ramp is:  1/2 for x in range of 0 to 1
    
    float *A = (float *)As->data();
    float *B = (float *)Bs->data();
    float *C = (float *)Cs->data();
    float *D = (float *)Ds->data();
    float *X = (float *)Xs->data();
    
    if (x < X[0])
        return x * A[0];
        
        float sum    = 0.f;
        size_t n = controlPoints.size() - 1;
        for (size_t j = 0; j < n; j++) {
            float t    = (x <= X[j+1]) ? x - X[j] : X[j+1] - X[j];
            float high = t * (12.f * A[j] + t * (6.f * B[j] + t * (4.f * C[j] + 3.f * D[j] * t)));
            sum += high/12.f;
            
            if (x <= X[j+1])
                break;
        }
    sum += (1.f - X[n]) * A[n];
    return sum;
}

// evaluate for x in range 0 to 1 analytic exact solution
float NaturalCubicCurve::integral()
{
    return evaluateIntegralForX(1.f);
}

using uint = unsigned int;
using std::string;

// evaluate for u in range 0 to 1 - ranges from first control point X through last control point X
float NaturalCubicCurve::evaluateForU(float u)
{
    size_t n = controlPoints.size() - 1;
    float *X = (float *)Xs->data();
    float val = evaluateForX(X[0] + u * (X[n] - X[0]));
    return flipped ? 1.f - val : val;
}

// evaluate for u in range 0 to 1 - X range is supplied
float NaturalCubicCurve::evaluateForU(float u, float start, float end)
{
    float val = evaluateForX(start + u * (end - start));
    return flipped ? 1.f - val : val;
}

float NaturalCubicCurve::evaluateForUClipped(float u)
{
    size_t n = controlPoints.size() - 1;
    float *X = (float *)Xs->data();
    float val = evaluateForX(X[0] + u * (X[n] - X[0]));
    val       = val < 0.f ? 0.f : val;
    val       = val > 1.f ? 1.f : val;
    return flipped ? 1.f - val : val;
}

// evaluate for u in range 0 to 1 - X range is supplied
float NaturalCubicCurve::evaluateForUClipped(float u, float start, float end)
{
    float val = evaluateForX(start + u * (end - start));
    val       = val < 0.f ? 0.f : val;
    val       = val > 1.f ? 1.f : val;
    return flipped ? 1.f - val : val;
}


// evaluate for x in range 0 to 1 using Composite Simpson's 1/3 Rule (quadratic approximation)
float NaturalCubicCurve::integrateSimpson_1_3_rule()
{
    uint n = 100;
    float h = 1.f/(float)n;
    
    float sum = evaluateForU(0.f) + evaluateForU(1.f);
    for (uint i = 1; i <= n-1; i+=2) {
        sum += 4.f * evaluateForU((float)i*h);
    }
    for (uint i = 2; i <= n-2; i+=2) {
        sum += 2.f * evaluateForU((float)i*h);
    }
    return h * sum / 3.f;
}

// evaluate for x in range 0 to 1 using Composite Simpson's 3/8 Rule (cubic approximation)
float NaturalCubicCurve::integrateSimpson_3_8_rule()
{
    uint n = 99;
    float h = 1.f/(float)n;
    
    float sum = evaluateForU(0.f) + evaluateForU(1.f);
    for (uint i = 1; i <= n-2; i+=3) {
        sum += 3.f * evaluateForU((float)i*h);
    }
    for (uint i = 2; i <= n-1; i+=3) {
        sum += 3.f * evaluateForU((float)i*h);
    }
    for (uint i = 3; i <= n-3; i+=3) {
        sum += 2.f * evaluateForU((float)i*h);
    }
    return 3.f * h * sum / 8.f;
}

// evaluate for x in range 0 to 1 using Composite Simpson's 1/3 Rule (quadratic approximation)
float NaturalCubicCurve::integrateSimpson_1_3_ruleClipped()
{
    uint n  = 100;
    float h = 1.f/(float)n;
    
    float sum = evaluateForUClipped(0.f) + evaluateForUClipped(1.f);
    for (uint i = 1; i <= n-1; i+=2) {
        sum += 4.f * evaluateForUClipped((float)i*h);
    }
    for (uint i = 2; i <= n-2; i+=2) {
        sum += 2.f * evaluateForUClipped((float)i*h);
    }
    return h * sum / 3.f;
}

// evaluate for x in range 0 to 1 using Composite Simpson's 3/8 Rule (cubic approximation)
float NaturalCubicCurve::integrateSimpson_3_8_ruleClipped()
{
    uint n  = 99;
    float h = 1.f/(float)n;
    
    float sum = evaluateForUClipped(0.f) + evaluateForUClipped(1.f);
    for (uint i = 1; i <= n-2; i+=3) {
        sum += 3.f * evaluateForUClipped((float)i*h);
    }
    for (uint i = 2; i <= n-1; i+=3) {
        sum += 3.f * evaluateForUClipped((float)i*h);
    }
    for (uint i = 3; i <= n-3; i+=3) {
        sum += 2.f * evaluateForUClipped((float)i*h);
    }
    return 3.f * h * sum / 8.f;
}

// evaluate for x in range 0 to 1 using Composite Simpson's 3/8 Rule (cubic approximation)
float NaturalCubicCurve::integrateSimpson_3_8_ruleForCurve(NaturalCubicCurve *curve)
{
    return curve->integrateSimpson_3_8_ruleClipped();
}

// evaluate curve2(curve1(u))    the composite curve2 o curve1
float NaturalCubicCurve::evaluateForU(float u, NaturalCubicCurve *curve1, NaturalCubicCurve *curve2)
{
    return curve2->evaluateForU(curve1->evaluateForU(u));
}

// evaluate curve2(curve1(u))    the composite curve2 o curve1
float NaturalCubicCurve::evaluateForUClipped(float u, NaturalCubicCurve *curve1, NaturalCubicCurve *curve2)
{
    // dont clip the intermediate result
    return curve2->evaluateForUClipped(curve1->evaluateForU(u));
}

// evaluate for x in range 0 to 1 using Composite Simpson's 3/8 Rule (cubic approximation)
float NaturalCubicCurve::integrateSimpson_3_8_ruleFor(NaturalCubicCurve *curve1, NaturalCubicCurve *curve2)
{
    // handle all combinations of curves and no curves
    if (curve1 && ! curve2)
        return curve1->integrateSimpson_3_8_rule();
    else if (! curve1 && curve2)
        return curve2->integrateSimpson_3_8_rule();
    else if (! curve1 && ! curve2)
        return 0.5f;
    
    uint n  = 99;
    float h = 1.f/(float)n;
    
    float sum = NaturalCubicCurve::evaluateForU(0.f, curve1, curve2) + NaturalCubicCurve::evaluateForU(1.f, curve1, curve2);
    for (uint i = 1; i <= n-2; i+=3) {
        sum += 3.f * NaturalCubicCurve::evaluateForU((float)i*h, curve1, curve2);
    }
    for (uint i = 2; i <= n-1; i+=3) {
        sum += 3.f * NaturalCubicCurve::evaluateForU((float)i*h, curve1, curve2);
    }
    for (uint i = 3; i <= n-3; i+=3) {
        sum += 2.f * NaturalCubicCurve::evaluateForU((float)i*h, curve1, curve2);
    }
    return 3.f * h * sum / 8.f;
}

// evaluate for x in range 0 to 1 using Composite Simpson's 3/8 Rule (cubic approximation)
float NaturalCubicCurve::integrateSimpson_3_8_ruleClippedFor(NaturalCubicCurve *curve1, NaturalCubicCurve *curve2)
{
    // handle all combinations of curves and no curves
    if (curve1 && ! curve2)
        return curve1->integrateSimpson_3_8_ruleClipped();
    else if (! curve1 && curve2)
        return curve2->integrateSimpson_3_8_ruleClipped();
    else if (! curve1 && ! curve2)
        return 0.5f;
    
    uint n  = 99;
    float h = 1.f/(float)n;
    
    float sum = NaturalCubicCurve::evaluateForUClipped(0.f, curve1, curve2) + NaturalCubicCurve::evaluateForUClipped(1.f, curve1, curve2);
    for (uint i = 1; i <= n-2; i+=3) {
        sum += 3.f * NaturalCubicCurve::evaluateForUClipped((float)i*h, curve1, curve2);
    }
    for (uint i = 2; i <= n-1; i+=3) {
        sum += 3.f * NaturalCubicCurve::evaluateForUClipped((float)i*h, curve1, curve2);
    }
    for (uint i = 3; i <= n-3; i+=3) {
        sum += 2.f * NaturalCubicCurve::evaluateForUClipped((float)i*h, curve1, curve2);
    }
    return 3.f * h * sum / 8.f;
}

void NaturalCubicCurve::k2AdjustFor(NaturalCubicCurve *rgbCurve,
                                    NaturalCubicCurve *redCurve,
                                    NaturalCubicCurve *greenCurve,
                                    NaturalCubicCurve *blueCurve,
                                    float *adjust) // cl_float4 *adjust
{
    float linearRampIntegral = 0.5f;
    float redIntegral        = NaturalCubicCurve::integrateSimpson_3_8_ruleClippedFor(rgbCurve, redCurve);
    float greenIntegral      = NaturalCubicCurve::integrateSimpson_3_8_ruleClippedFor(rgbCurve, greenCurve);
    float blueIntegral       = NaturalCubicCurve::integrateSimpson_3_8_ruleClippedFor(rgbCurve, blueCurve);
    
    adjust[0] = redIntegral  /linearRampIntegral;
    adjust[1] = greenIntegral/linearRampIntegral;
    adjust[2] = blueIntegral /linearRampIntegral;
    adjust[3] = 1.f;
}

void NaturalCubicCurve::k2AdjustForData(const SharedPointVector &rgbData,
                                        const SharedPointVector &redData,
                                        const SharedPointVector &greenData,
                                        const SharedPointVector &blueData,
                                        float *adjust) // cl_float4 *adjust
{
    UniqueNatural rgbCurve   = NaturalCubicCurve::curveWithControlPointsData(rgbData);
    UniqueNatural redCurve   = NaturalCubicCurve::curveWithControlPointsData(redData);
    UniqueNatural greenCurve = NaturalCubicCurve::curveWithControlPointsData(greenData);
    UniqueNatural blueCurve  = NaturalCubicCurve::curveWithControlPointsData(blueData);
    NaturalCubicCurve::k2AdjustFor(rgbCurve.get(), redCurve.get(), greenCurve.get(), blueCurve.get(), adjust);
}

void NaturalCubicCurve::getCurvePoints(float *array, size_t count)
{
    if (mirrored) {
        float u         = 1.f;
        float increment = 1.f/((float)count - 1.f);
        
        for (int i = 0; i < count; i++) {
            array[i] = evaluateForUClipped(u);
            u -= increment;
        }
    }
    else {
        float u         = 0.f;
        float increment = 1.f/((float)count - 1.f);
        
        for (int i = 0; i < count; i++) {
            array[i] = evaluateForUClipped(u);
            u += increment;
        }
    }
}

bool NaturalCubicCurve::isDefaultCurve()
{
    if (controlPoints.size() != 2)
        return false;
    Point point0 = controlPoints[0];
    Point point1 = controlPoints[1];
    
    if (point0.x == 0.f && point0.y == 0.f && point1.x == 1.f && point1.y == 1.f)
        return true;
    return false;
}

Point NaturalCubicCurve::cpAtIndex(size_t index)
{
    return controlPoints[index];
}

std::shared_ptr<std::vector<float>> NaturalCubicCurve::curveForCount(int count)
{
    std::shared_ptr<std::vector<float>> curvePoints = std::make_shared<std::vector<float>>(count, 0.f);
    getCurvePoints((float *)curvePoints->data(), count);
    return curvePoints;
}

std::shared_ptr<std::vector<float>> NaturalCubicCurve::curveForCountAlternate(int count)
{
    std::shared_ptr<std::vector<float>> curvePoints = std::make_shared<std::vector<float>>(count, 0.f);
    getCurvePoints((float *)curvePoints->data(), count);
    
    double u         = 0.;
    double increment = 1./((double)count - 1.);
    for (int i = 0; i < count; i++) {
        curvePoints->operator[](i) = evaluateForUClipped(mirrored ? 1.f - u : u);
        u += increment;
    }
    
    return curvePoints;
}

std::shared_ptr<std::vector<float>> NaturalCubicCurve::curveForCountAlternate(int count, float start, float end)
{
    std::shared_ptr<std::vector<float>> curvePoints = std::make_shared<std::vector<float>>(count, 0.f);
    getCurvePoints((float *)curvePoints->data(), count);
    
    double u         = 0.;
    double increment = 1./((double)count - 1.);
    for (int i = 0; i < count; i++) {
        curvePoints->operator[](i) = evaluateForUClipped(mirrored ? 1.f - u : u, start, end);
        u += increment;
    }
    
    return curvePoints;
}

// u is between 0 and 1
double NaturalCubicCurve::curveAtOffset(double u)
{
    return evaluateForUClipped(mirrored ? 1.f - u : u);
}

Point NaturalCubicCurve::interpolatePointForU(float u)
{
    return Point{ 0.f, 0.f };
}

SharedPointVector NaturalCubicCurve::curveDataFromAttributeString(const std::string &string)
{
    std::vector<std::string> array = FlameParse::splitOnSpaces(string);
    size_t count                   =  array.size()/2;
    
    SharedPointVector controlPointsData = std::make_shared<std::vector<Point>>(count);
    Point *_controlPoints               = (Point *)controlPointsData->data();
    
    for (uint i = 0; i < count; i++) {
        const std::string &xString = array[2*i];
        const std::string &yString = array[2*i + 1];
        _controlPoints[i]  = Point{ std::strtof(xString.c_str(), nullptr), std::strtof(yString.c_str(), nullptr) };
    }
    return controlPointsData;
}

SharedPointVector NaturalCubicCurve::curveDataFromXmlAttribute(const pugi::xml_attribute &attr)
{
    const std::string & name = attr.name();
    if (name != "rgb_curve" &&
        name != "red_curve" &&
        name != "green_curve" &&
        name != "blue_curve")
        return SharedPointVector();
    return NaturalCubicCurve::curveDataFromAttributeString(attr.value());
}

std::string NaturalCubicCurve::description()
{
    string s;
    s.append("\nControl Points =======\n");
    
    for (size_t i = 0; i < controlPoints.size(); i++) {
        Point p = controlPoints[i];
        s.append("{ ").append(prettyFloat(p.x)).append(", ").append(prettyFloat(p.y)).append(" }");
        s.append("\n");
    }
    return s;
}


#ifdef __ORIGINAL__

- (NSString *)integralsDescription
{
    return [NSString stringWithFormat:@"Exact: %f  Simpson's 1/3 Rule: %f  Simpson's 3/8 Rule: %f  Default Ramp: %f",
            [self integral], [self integrateSimpson_1_3_rule], [self integrateSimpson_3_8_rule], 0.5f];
}

- (void)logControlPoints
{
    NSLog(@"%s", [self description]);
}

- (NSString *)description
{
    NSMutableString *s = [NSMutableString stringWithCapacity:200];
    [s appendString:@"\nControl Points =======\n"];
    for (NSUInteger i = 0; i < [controlPoints count]; i++) {
        NSValue *value = [controlPoints objectAtIndex:i];
        NSPoint p      = [value pointValue];
        [s appendString:NSStringFromPoint(p)];
        [s appendString:@"\n"];
    }
    return s;
}

- (NSString *)longDescription
{
    NSMutableString *s = [NSMutableString stringWithCapacity:200];
    [s appendString:[self description]];
    
    float *A = (float *)[As bytes];
    float *B = (float *)[Bs bytes];
    float *C = (float *)[Cs bytes];
    float *D = (float *)[Ds bytes];
    float *X = (float *)[Xs bytes];
    
    [s appendFormat:@"\nXs cpCount=%lu segmentCount=%lu =======\n", (unsigned long)[controlPoints count], [controlPoints count]-1];
    
    NSUInteger n = [controlPoints count] - 1;
    for (NSUInteger j = 0; j <= n; j++) {
        if (j == n) {
            [s appendString:@"["];
            [s appendString:[NSString prettyFloat:X[j]]];
            [s appendString:@"]"];
        }
        else
            [s appendString:[NSString prettyFloat:X[j]]];
        [s appendString:@"\n"];
    }
    [s appendString:@"\nAs =======\n"];
    
    n = [controlPoints count] - 1;
    for (NSUInteger j = 0; j <= n; j++) {
        if (j == n) {
            [s appendString:@"["];
            [s appendString:[NSString prettyFloat:A[j]]];
            [s appendString:@"]"];
        }
        else
            [s appendString:[NSString prettyFloat:A[j]]];
        [s appendString:@"\n"];
    }
    [s appendString:@"\nSubCurves =======\n"];
    for (NSUInteger j = 0; j < n; j++) {
        [s appendFormat:[NSString stringWithFormat:@"%@ + %@ * (x - %@) + %@ * (x - %@)*(x - %@) + %@ * (x - %@)*(x - %@)*(x - %@)\n",
                         [NSString prettyFloat:A[j]],
                         [NSString prettyFloat:B[j]],
                         [NSString prettyFloat:X[j]],
                         [NSString prettyFloat:C[j]],
                         [NSString prettyFloat:X[j]],
                         [NSString prettyFloat:X[j]],
                         [NSString prettyFloat:D[j]],
                         [NSString prettyFloat:X[j]],
                         [NSString prettyFloat:X[j]],
                         [NSString prettyFloat:X[j]]]];
    }
    return s;
}

- (NSXMLElement *)xmlElement
{
    NSXMLElement *ele = [super xmlElement];
    [ele addAttribute:[NSXMLNode attributeWithName:@"cpCount" stringValue:[[NSNumber numberWithUnsignedInt:[controlPoints count]] stringValue]]];
    
    NSXMLElement *controlPointsEle = [NSXMLNode elementWithName:@"controlPoints"];
    for (uint i = 0; i < [controlPoints count]; i++) {
        NSXMLElement *controlPointEle = [NSXMLNode elementWithName:@"controlPoint"];
        NSPoint controlPoint = [self cpAtIndex:i];
        [controlPointEle addAttribute:[NSXMLNode attributeWithName:@"cpX"
                                                       stringValue:[[NSNumber numberWithFloat:controlPoint.x] stringValue]]];
        [controlPointEle addAttribute:[NSXMLNode attributeWithName:@"cpY"
                                                       stringValue:[[NSNumber numberWithFloat:controlPoint.y] stringValue]]];
        [controlPointsEle addChild:controlPointsEle];
    }
    [ele addChild:controlPointsEle];
    
    return ele;
}

// for embedding in flame file header
- (NSString *)xmlAttributeContents
{
    NSMutableString *s = [NSMutableString stringWithCapacity:200];
    for (uint i = 0; i < [controlPoints count]; i++) {
        NSPoint controlPoint = [self cpAtIndex:i];
        
        [s appendString:[NSString prettyFloat:controlPoint.x]];
        [s appendString:@" "];
        [s appendString:[NSString prettyFloat:controlPoint.y]];
        [s appendString:@" "];
    }
    return s;
}

+ (NSString *)xmlAttributeContentsForControlPointsData:(NSData *)cpData cpCount:(NSUInteger)cpCount
{
    NSMutableString *s = [NSMutableString stringWithCapacity:200];
    NSPoint *points    = (NSPoint *)[cpData bytes];
    // dont save the default linear response curve
    if (cpCount == 2) {
        NSPoint point0 = points[0];
        NSPoint point1 = points[1];
        if (point0.x == 0.f && point0.y == 0.f && point1.x == 1.f && point1.y == 1.f)
            return (NSString *)nil;
    }
    
    for (uint i = 0; i < cpCount; i++) {
        NSPoint controlPoint = points[i];
        
        [s appendString:[NSString prettyFloat:controlPoint.x]];
        [s appendString:@" "];
        [s appendString:[NSString prettyFloat:controlPoint.y]];
        [s appendString:@" "];
    }
    return s;
}

+ (NSString *)cpDataDescription:(NSData *)controlPointsData
{
    NSUInteger cpCount     = [controlPointsData length]/sizeof(NSPoint);
    NSPoint *_controlPoints = (NSPoint *)[controlPointsData bytes];
    NSMutableArray *array   = [NSMutableArray arrayWithCapacity:cpCount];
    for (NSUInteger i = 0; i < cpCount; i++) {
        NSPoint point = _controlPoints[i];
        [array addObject:[NSValue valueWithPoint:point]];
    }
    
    NSMutableString *s = [NSMutableString stringWithCapacity:200];
    [s appendString:@"\nControl Points =======\n"];
    for (NSUInteger i = 0; i < [array count]; i++) {
        NSValue *value = [array objectAtIndex:i];
        NSPoint p      = [value pointValue];
        [s appendString:NSStringFromPoint(p)];
        [s appendString:@"\n"];
    }
    return s;
}

- (NSDictionary *)propertyList
{
    NSUInteger length          = sizeof(NSPoint) * [controlPoints count];
    NSData *controlPointsData  = [NSData dataWithBytesNoCopy:malloc(length) length:length];
    NSPoint *_controlPoints    = (NSPoint *)[controlPointsData bytes];
    
    for (NSUInteger i = 0; i < [controlPoints count]; i++)
        _controlPoints[i] = [(NSValue *)[controlPoints objectAtIndex:i] pointValue];
        
        
        return [NSDictionary dictionaryWithObjectsAndKeys:
                [NSNumber numberWithInt:[super curveType]],  @"curveType",
                [NSNumber numberWithFloat:[super a]],        @"a",
                [NSNumber numberWithFloat:[super b]],        @"b",
                [NSNumber numberWithBool:[super flipped]],   @"flipped",
                [NSNumber numberWithBool:[super mirrored]],  @"mirrored",
                [NSNumber numberWithBool:[super integralA]], @"integralA",
                [NSNumber numberWithUnsignedInt:[controlPoints count]], @"cpCount",
                controlPointsData, @"controlPoints",
                nil];
}



// find which range of X holds the key, returns the left index of the containing range
static int binary_range_search(float* X, float key, int imin, int imax)
{
    int omax = imax;
    
    // continue searching while [imin,imax] is not empty
    while (imax >= imin)
    {
        // calculate the midpoint for roughly equal partition
        int imid = (imin + imax)/2;
        if (X[imid] == key)
            // key found at index imid
            return imid;
        // determine which subarray to search
        else if (X[imid] < key) {
            // check for enclosing range  Xi < key < Xi+1 (if == binary search will find that one)
            if (imid + 1 <= omax && X[imid+1] > key)
                return imid;
            // change min index to search upper subarray
            imin = imid + 1;
        }
        else {  //   Xi > key
            // check for enclosing range  Xi-1 < key < Xi  (if == binary search will find that one)
            if (imid - 1 >= 0 && X[imid-1] < key)
                return imid - 1;
            // change max index to search lower subarray
            imax = imid - 1;
        }
    }
    // key was not found
    return -1;
}


static float curveAdjust(float x,
                         float* X,
                         float* A,
                         float* B,
                         float* C,
                         float* D,
                         uint cpCount)
{
    if (x <= X[0])
        return A[0];    // return first
    
    int index = binary_range_search(X, x, 0, cpCount - 1);
    
    if (index >= 0 && index < (int)cpCount - 1) {
        float t = x - X[index];
        return A[index] + B[index]*t + C[index]*t*t + D[index]*t*t*t;
    }
    return A[cpCount - 1]; // return last;
}

- (NSString *)testBlackCurve
{
    struct rgba initial = { 0.f, 0.f, 0.f, 1.f };
    return [self testCurve:initial];
}

- (NSString *)testGreyCurve
{
    struct rgba initial = { 0.5f, 0.5f, 0.5f, 1.f };
    return [self testCurve:initial];
}

- (NSString *)testWhiteCurve
{
    struct rgba initial = { 1.f, 1.f, 1.f, 1.f };
    return [self testCurve:initial];
}

- (NSString *)testCurve:(struct rgba)initial
{
    struct rgba result;
    
    float preluma  = 0.297361f * initial.r + 0.627355f * initial.g + 0.075285f * initial.b;
    float postluma = curveAdjust(preluma,
                                 (float *)[Xs bytes],
                                 (float *)[As bytes],
                                 (float *)[Bs bytes],
                                 (float *)[Cs bytes],
                                 (float *)[Ds bytes],
                                 [self cpCount]);
    
    if (preluma != 0.f) {
        result.r = postluma/preluma * initial.r;
        result.g = postluma/preluma * initial.g;
        result.b = postluma/preluma * initial.b;
    }
    else {
        result.r = postluma;
        result.g = postluma;
        result.b = postluma;
    }
    return [NSString stringWithFormat:@"result red=%@ green=%@ blue=%@",
            [NSString prettyFloat:result.r],
            [NSString prettyFloat:result.g],
            [NSString prettyFloat:result.b]];
}

@end

#endif
