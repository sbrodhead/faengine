//
//  RenderHarness.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 6/6/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "RenderHarness.hpp"

#include "../FAEngineUnitTests/RenderTest.hpp"
#include "../FAEngineUnitTests/VariationSetTest.hpp"
#include "FlameExpanded.hpp"
#include "VariationSet.hpp"
#include "ClContext.hpp"
#ifndef NO_CUDA
#include "CudaContext.hpp"
#include "CudaDeviceKind.hpp"
#endif
#include "ClDeviceKind.hpp"
#include "RenderTile.hpp"
#include "RenderGeometry.hpp"
#include "DeviceUsage.hpp"
#include "ImageSlot.hpp"
#include "RenderQueue.hpp"
#include "pugixml.hpp"
#include "FlameParse.hpp"
#include "ClPlatform.hpp"

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include "CocoaInterface.h"
#endif

#include <iostream>
#include <fstream>
#include <string>

#ifdef __linux
namespace std {
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}
}
#endif

using std::string;
using namespace pugi;

RenderHarness * RenderHarness::openClSingleton;
RenderHarness * RenderHarness::cudaSingleton;

RenderHarness::RenderHarness(std::set<uint> _deviceNums)
: deviceContext(), deviceNums(_deviceNums), renderQueue(std::make_unique<class RenderQueue>())
{}

void RenderHarness::variationSetSetup()
{
    VariationSet::setupFAEngineApplicationSupport();
    DeviceProgram::setupFAEngineCaches();
    
    if (! VariationSet::cachedLibraryExists())
        VariationSet::cacheFactoryLibrary();
    
    if (! VariationSet::cachedMyLibraryExists())
        VariationSet::cacheMyLibrary();
    
    SharedVariationSet vs = VariationSet::loadVariationsLibrary();
    VariationSet::loadMyVariationsLibrary();
    VariationSet::updateMergedLibrary();
    
    VariationSet::makeLegacyVariationSet();
    VariationSet::makeStandard3DVariationSet();
    
    VariationSet::setupDefaultVariationSet();
}

void RenderHarness::printDeviceList(bool targetGPU)
{
    ClContext::printDeviceList(targetGPU ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU);
}

// devices usages:  is the vector of device usage structs that is reported back by the OpenCL
// selectedDevices: is the filtered, sorted subset of these device usages that will be actually used for rendering

// targetGPU:         Do you want to render on a GPU?
// forceRebuild:      Do we want to force rebuilding the kernels
//                        - typically app will not, but for testing you will
// ignoreQuarantine:  You can ignore quarantined devices for testing
//                     The list of Quarantined devices contains devices that are known to be incompatible
//                     (but the quarantine list can be out-of-date as vendors fix their drivers)

// the deviceNums set is a collection of indexes referring to the device vector returned by OpenCL
// each deviceNum value in the set is an index into the device vector returned by OpenCL
// if a deviceNum value >= number of devices in the device vector, it is removed from the deviceNums set
void RenderHarness::openClContextSetup(bool targetGPU, std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine)
{
    deviceNums = _deviceNums;
    deviceContext =
    ClContext::makeClContext(targetGPU ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU,
                             forceRebuild,
                             VariationSet::currentVariationSet,
                             true,
                             true,
                             true);
    
    SharedClContext clContext = std::dynamic_pointer_cast<ClContext>(deviceContext);
    
    std::vector<uint> eraseThese;
    
    // remove invalid device nums from the set
    for (std::set<uint>::iterator it = deviceNums.begin(); it != deviceNums.end(); ++it) {
        if (*it >= deviceContext->numDevices) {
            eraseThese.push_back(*it);
            continue;
        }
        
        // some older devices dont support shared memory and are unusable for rendering
        // (because our kernels require shared memory support)
        ClDeviceKind *deviceKind = (ClDeviceKind *)deviceContext->deviceKindForDevice(*it).get();
        if (deviceKind->isGPU() && !deviceKind->hasLocalMemoryOnDevice()) {
            eraseThese.push_back(*it);
            continue;
        }
    }
    
    for (uint offset : eraseThese) {
        deviceNums.erase(offset);
    }
    
    // the default device (if none specified on command line is the first one) -- also check if deviceNum is out-of-range
//    uint deviceCount = deviceContext->numDevices;
    uint deviceCount = deviceContext->numDevices;
    if (targetGPU && deviceNums.size() == 0) {
        targetGPU = false;
        deviceContext =
        ClContext::makeClContext(CL_DEVICE_TYPE_CPU,
                                 forceRebuild,
                                 VariationSet::currentVariationSet,
                                 true,
                                 false,
                                 true);
        deviceCount = deviceContext->numDevices;
    }
    if (deviceCount == 0) // creating a context failed
        return;
    
    if (deviceNums.size() == 0)
        deviceNums.insert((uint)deviceContext->firstUsableDeviceNum());
    
    // turn off quarantine
    if (ignoreQuarantine)
        deviceContext->predicate = DeviceUsage::predicateForAvailableIgnoreQuarantine;
    else
        deviceContext->predicate = DeviceUsage::predicateForAvailable;
    
    // selected devices are now just the remaining available devices - typically shown to user in GUI
    deviceContext->rearrangeSelectedDevices();
    
    // now lets "use" just the target device
    for (uint i = 0; i < deviceContext->deviceUsages.size(); i++) {
        SharedDeviceUsage & deviceUsage = deviceContext->deviceUsages[i];
        
        if (deviceNums.find(i) != deviceNums.end()) {
            deviceUsage->used = true;
            deviceUsage->stagedUsed =true;
        }
        else {
            deviceUsage->used = false;
            deviceUsage->stagedUsed =false;
        }
    }
    
    for (uint i = 0; i < deviceContext->deviceUsages.size(); i++) {
        SharedDeviceUsage & deviceUsage = deviceContext->deviceUsages[i];
        string deviceName = ClContext::getDeviceNameForDeviceID((cl_device_id)deviceUsage->deviceID);
        printf("Device:%u  %s Platform:%s  %s\n",
               i, deviceName.c_str(), ClPlatform::platformName(clContext->getPlatformIDs()[i]).c_str(), deviceUsage->used ? "<Used for Render>" : "");
    }
    printf("\n");
    
    // change the predicate to only "used" devices
    deviceContext->predicate = DeviceUsage::predicateForUsed;
    deviceContext->rearrangeSelectedDevices();
}

#ifndef NO_CUDA
void RenderHarness::cudaContextSetup(std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine)
{
    deviceNums = _deviceNums;
    deviceContext =
    CudaContext::makeCudaContext(forceRebuild,
                                 VariationSet::currentVariationSet,
                                 true,
                                 true,
                                 true);
    
    SharedCudaContext cudaContext = std::dynamic_pointer_cast<CudaContext>(deviceContext);
    
    // the default device (if none specified on command line is the first one) -- also check if deviceNum is out-of-range
    uint deviceCount = deviceContext->numDevices;
    if (deviceCount == 0) // creating a context failed
        return;
    
    // remove invalid device nums from the set
    for (std::set<uint>::iterator it = deviceNums.begin(); it != deviceNums.end(); ++it) {
        if (*it >= deviceCount)
            deviceNums.erase(it);
    }
    if (deviceNums.size() == 0)
        deviceNums.insert((uint)deviceContext->firstUsableDeviceNum());
    
    // turn off quarantine
    if (ignoreQuarantine)
        deviceContext->predicate = DeviceUsage::predicateForAvailableIgnoreQuarantine;
    else
        deviceContext->predicate = DeviceUsage::predicateForAvailable;
    
    // selected devices are now just the remaining available devices - typically shown to user in GUI
    deviceContext->rearrangeSelectedDevices();
    
    // now lets "use" just the target device
    for (uint i = 0; i < deviceContext->deviceUsages.size(); i++) {
        SharedDeviceUsage & deviceUsage = deviceContext->deviceUsages[i];

        if (deviceNums.find(i) != deviceNums.end())
            deviceUsage->used = true;
        else
            deviceUsage->used = false;
    }
    
    for (uint i = 0; i < deviceContext->deviceUsages.size(); i++) {
        SharedDeviceUsage & deviceUsage = deviceContext->deviceUsages[i];
        printf("CUDA Device:%u  %s  %s\n",
               i, deviceContext->nameForDevice(i).c_str(), deviceUsage->used ? "<Used for Render>" : "");
    }
    printf("\n");
    
    // change the predicate to only "used" devices
    deviceContext->predicate = DeviceUsage::predicateForUsed;
    deviceContext->rearrangeSelectedDevices();
}
#endif

uint RenderHarness::deviceCount()
{
    return deviceContext->numDevices;
}

SharedImage RenderHarness::flam4Render(SharedDeviceContext & deviceContext,
                                       SharedFlameExpanded & fe,
                                       int deviceNum,
                                       string &url,
                                       string &outURL,
                                       float width,
                                       float height,
                                       int quality,
                                       int bitsPerPixel,
                                       bool hasAlpha,
                                       const char *colorspaceName,
                                       bool flipped,
                                       float epsilon,
                                       bool verbose,
                                       duration<double> & _duration,
                                       duration<double> & deDuration)
{
    SharedBlades blades = std::make_shared<BladesVector>();
    blades->push_back(fe);
    Flame *flame             = fe->getFlame();
    uint maxWorkGroupSize    = (uint)deviceContext->maxWorkGroupSizeForDevice(deviceNum);
    
    // adjust preview size by the file's viewport aspect ratio
    float imageWidth  = width;
    float imageHeight = height;
    RenderGeometry::changeAspectRatio(flame, imageWidth, imageHeight);
	SharedImage rep = RenderTile::renderAsPreview(deviceContext,
                                                  (uint)deviceContext->selectedDeviceNumForDeviceNum(deviceNum),
                                                  blades,
                                                  Rect(0.f, 0.f, imageWidth, imageHeight),
                                                  quality,
                                                  hasAlpha,
                                                  bitsPerPixel,
                                                  colorspaceName,
                                                  flipped,
                                                  epsilon,
                                                  maxWorkGroupSize,
                                                  verbose,
                                                  _duration,
                                                  deDuration);
    if (! rep) {
        static char buf[100];
        snprintf(buf, sizeof(buf), "Rendering flame recipe failed for: %s", url.c_str());
        systemLog(buf);
    }
    return rep;
}

int RenderHarness::flam4QueuedRender(SharedDeviceContext & deviceContext,
                                     SharedFlameExpanded & fe,
                                     int deviceNum,
                                     string &url,
                                     string &outURL,
                                     float width,
                                     float height,
                                     int quality,
                                     int bitsPerPixel,
                                     bool hasAlpha,
                                     const char *colorspaceName,
                                     bool flipped,
                                     float epsilon,
                                     bool verbose,
                                     uint index,
                                     uint indexMax)
{
    SharedBlades blades = std::make_shared<BladesVector>();
    blades->push_back(fe);
    Flame *flame             = fe->getFlame();
    
    // adjust preview size by the file's viewport aspect ratio
    float imageWidth  = width;
    float imageHeight = height;
    RenderGeometry::changeAspectRatio(flame, imageWidth, imageHeight);
    
    enum GpuPlatformUsed gpuPlatform = useOpenCL;
#ifndef NO_CUDA
    if (std::dynamic_pointer_cast<SharedCudaContext>(deviceContext)) {
        gpuPlatform = useCUDA;
    }
#endif

    auto params = std::make_shared<FileOutputRenderParams>((SharedDeviceContext &)deviceContext);
    
    params->listener         = this;
    params->viewListener     = this;
    params->size             = Size(width, height);
    params->quality          = quality;
    params->bitDepth         = bitsPerPixel;
    params->hasAlpha         = hasAlpha;
    params->eColorSpace      = RenderParams::colorSpaceFor(colorspaceName);
    params->blades           = blades;
    params->url              = url;
    params->batchStart       = index == 0;
    params->batchEnd         = index == indexMax;
    params->flipped          = true;
    params->pixelFormat            = "RGBA";
    params->backgroundImageRep     = nullptr;
    params->usePriorFraction       = usePrior;
    params->overrideFuseIterations = fe->getElc()->defaultFuseIters;
    params->cpuFuseIterations      = fe->getElc()->cpuFuseIterations;
    params->gpuFuseIterations      = fe->getElc()->gpuFuseIterations;
    params->gpuPlatform            = gpuPlatform;
    params->selectedDeviceNum      = (uint)deviceContext->selectedDeviceNumForDeviceNum(deviceNum);
    
    params->outURL           = outURL;
    params->saveRenderState  = false;
    params->maxTileMemory    = 0LL;
    params->continuousMode   = false;
    params->fusedFraction    = 1.f;
    params->uuid             = fe->uuid;
    params->renderingIntent  = RenderingIntentSaturation;
    params->retainedFraction = 1.f;
    
    renderQueue->enqueue(params);
    return 0;
}

void printFlames(FlameExpandedVector & flames)
{
    for (size_t i = 0; i < flames.size(); i++) {
        std::cout << "Fractal #" << i << " =========================\n";
        SharedFlameExpanded fe = flames[i];
        std::cout << fe->textReport() << "\n\n";
    }
}

string getPNGOutURL(string & inputPath)
{
    size_t pos = inputPath.rfind(".");
    if (pos != string::npos)
        return inputPath.substr(0, pos) + ".png";
    else
        return inputPath + ".png";
}

string getPNGOutURL(string & inputPath, uint frame)
{
    size_t pos = inputPath.rfind(".");
    if (pos != string::npos)
        return inputPath.substr(0, pos) + "-" + itoa(frame) + ".png";
    else
        return inputPath + "-" + itoa(frame) + ".png";
}

string getJPEGOutURL(string & inputPath)
{
    size_t pos = inputPath.rfind(".");
    if (pos != string::npos)
        return inputPath.substr(0, pos) + ".jpeg";
    else
        return inputPath + ".jpeg";
}

string getJPEGOutURL(string & inputPath, uint frame)
{
    size_t pos = inputPath.rfind(".");
    if (pos != string::npos)
        return inputPath.substr(0, pos) + "-" + itoa(frame) + ".jpeg";
    else
        return inputPath + "-" + itoa(frame) + ".jpeg";
}

string getTIFFOutURL(string & inputPath)
{
    size_t pos = inputPath.rfind(".");
    if (pos != string::npos)
        return inputPath.substr(0, pos) + ".tiff";
    else
        return inputPath + ".tiff";
}

string getTIFFOutURL(string & inputPath, uint frame)
{
    size_t pos = inputPath.rfind(".");
    if (pos != string::npos)
        return inputPath.substr(0, pos) + "-" + itoa(frame) + ".tiff";
    else
        return inputPath + "-" + itoa(frame) + ".tiff";
}

static std::string flamePathFromInputPath(const std::string &path)
{
    size_t len = path.length();
    if (len > 3) {
        if (path.substr(len - 3) == ".fa") {
            return path + "/fractal.flame";
        }
    }
    return path;
}

static FlameExpandedVector readFromData(const std::string &string)
{
    xml_document doc;
    xml_parse_result result = doc.load_string(string.c_str(), parse_cdata);
    
    if (result.status != status_ok) {
        throw std::runtime_error(result.description());
    }
    xml_node root = doc.document_element();
    
    StringsSet set = FlameParse::preParseFlameEleForVariationsUsed(root);
    
    bool changed = false;
    VariationSetsVector variationSetsInDoc = VariationSet::preParseDocForVariationsUsed(doc, "foo", true, changed);
    
    // DEBUG
    //    std::cout << "All VarSets\n" << VariationSet::allVarsetsDescription(VariationSet::variationSets);
    //    std::cout << "VarSets in Doc\n" << VariationSet::allVarsetsDescription(variationSetsInDoc);
    
    // older docs dont have a variation set - use Flam3 legacy
    SharedVariationSet firstVariationSet = variationSetsInDoc.size() > 0 ? variationSetsInDoc.front() : VariationSet::legacyVariationSet();
    
    //    std::cout << firstVariationSet->description();
    
    FlameExpandedVector flames;
    std::string         palettePath;
    
    FlameParse::parseDoc(flames, doc, palettePath, firstVariationSet);
    FlameParse::uniqueFrameNames(flames);
    
    //    for (size_t i = 0; i < flames.size(); i++) {
    //        std::cout << "Fractal #" << i << " =========================\n";
    //        SharedFlameExpanded fe = flames[i];
    //        std::cout << fe->textReport() << "\n\n";
    //    }
    return flames;
}

// when initializing from a varset file
FlameExpandedVector RenderHarness::loadFromPath(const std::string & _path)
{
    std::string path = flamePathFromInputPath(_path);
    try {
        std::ifstream in(path.c_str());
        if (in.fail()) {
            std::string msg = string_format("Failed to open file: %s", path.c_str());
            throw std::runtime_error(msg);
        }
        std::string contents((std::istreambuf_iterator<char>(in)),
                             std::istreambuf_iterator<char>());
        
        return readFromData(contents);
    } catch (std::exception & e) {
        systemLog(e.what());
        return std::move(FlameExpandedVector());
    }
}

bool RenderHarness::renderFlames(std::string path,
                                 bool useQueue,
                                 OutputType outputType)
{
//	return renderFlames(path, useQueue, 800.f, 600.f, 100, outputType);
    return renderFlames(path, useQueue, 1440.f, 960.f, 1000, outputType);
}

bool RenderHarness::renderFlames(std::string path,
                                 bool useQueue,
                                 float width,
                                 float height,
                                 int quality,
                                 OutputType outputType)
{
    return renderFlames(path, useQueue, width, height, quality, 8, false, "AdobeRGB", false, 0.f, true, outputType);
}

bool RenderHarness::renderFlames(std::string path,
                                 bool useQueue,
                                 float width,
                                 float height,
                                 int quality,
                                 uint bitsPerPixel,
                                 bool hasAlpha,
                                 const char * colorspaceName,
                                 bool flipped,
                                 float epsilon,
                                 bool verbose,
                                 OutputType outputType)
{
    try
    {
        FlameExpandedVector flames = loadFromPath(path);
        
        //        printFlames(flames);
        
        for (uint i = 0; i < flames.size(); i++) {
            SharedFlameExpanded fe = flames[i];
            
            string outURL;
            switch (outputType) {
                case OutputType::ePNG:
                    outURL = flames.size() > 1 ? getPNGOutURL(path, i + 1) : getPNGOutURL(path);
                    break;
                case OutputType::eJPEG:
                    outURL = flames.size() > 1 ? getJPEGOutURL(path, i + 1) : getJPEGOutURL(path);
                    break;
                case OutputType::eTIFF:
                    outURL = flames.size() > 1 ? getTIFFOutURL(path, i + 1) : getTIFFOutURL(path);
                    break;
                   
            }
            printf("Rendering %s frame #%u to:   %s\n", fileNameFromPath(path).c_str(), i + 1, outURL.c_str());
            
            uint deviceNum = *deviceNums.begin();  // the first device num - this will be used for the density estimation and post processing
            if (useQueue)
                flam4QueuedRender(deviceContext,
                                  fe,
                                  deviceNum,
                                  path,
                                  outURL,
                                  width,   // width
                                  height,  // height
                                  quality,
                                  bitsPerPixel,       // bitsPerPixel
                                  hasAlpha,   // has alpha
                                  colorspaceName,
                                  flipped,   // flipped
                                  epsilon,     // epsilon
                                  verbose,    // verbose
                                  i,
                                  (uint)(flames.size() - 1));
            else {
                duration<double> _duration(0.);
                duration<double> deDuration(0.);
                SharedImage rep =
                flam4Render(deviceContext,
                            fe,
                            deviceNum,
                            path,
                            outURL,
                            width,   // width
                            height,   // height
                            quality,
                            bitsPerPixel,       // bitsPerPixel
                            hasAlpha,   // has alpha
                            colorspaceName,
                            flipped,   // flipped
                            epsilon,     // epsilon
                            verbose,    // verbose
                            _duration, deDuration);
                
                setImage(rep, outURL, hasAlpha, Size(width, height), _duration);
            }
        }
        return true;
    }
    catch (std::exception & e)
    {
        std::cerr << e.what() << std::endl;
        return false;
    }
}

void RenderHarness::beforeRender()
{}

void RenderHarness::renderDone()
{}

void RenderHarness::renderStats(std::string filename,
                             duration<double> _duration,
                             duration<double> deDuration,
                             float width,
                             float height,
                             float actualQuality,
                             size_t numBatches,
                             std::string tilesLabel,
                             float fusedFraction,
                             float retainedFraction,
                             uint supersample)
{
    RenderListener::logRenderStats(filename,
                                   _duration,
                                   deDuration,
                                   width,
                                   height,
                                   actualQuality,
                                   numBatches,
                                   tilesLabel,
                                   fusedFraction,
                                   retainedFraction,
                                   supersample);
}

void RenderHarness::submitImage(SharedImageSlot slot)
{}

void RenderHarness::imageBatchTime(duration<double> duration)
{}

void RenderHarness::setRenderResult(UniqueRender result)
{}

void RenderHarness::enqueueOpenClStop()
{
    openClSingleton->renderQueue->enqueueStop(openClSingleton->deviceContext);
}

void RenderHarness::enqueueCudaStop()
{
    cudaSingleton->renderQueue->enqueueStop(cudaSingleton->deviceContext);
}

