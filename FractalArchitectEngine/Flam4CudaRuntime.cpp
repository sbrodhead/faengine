//
//  Flam4CudaRuntime.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/8/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "Flam4CudaRuntime.hpp"
#include "CudaContext.hpp"
#include "Utilities.hpp"
#include "Flame.hpp"
#include "Variation.hpp"
#include "VariationParameter.hpp"
#include "VariationChain.hpp"
#include "VariationGroup.hpp"
#include "VarParInstance.hpp"
#include "VariationSet.hpp"
#include "CudaDeviceKind.hpp"
#include "noise.h"
#include "CameraViewProps.hpp"
#include "CameraViewport.hpp"
#include "CudaProgram.hpp"
#include "ElectricSheepStuff.hpp"
#include "RenderState.hpp"
#include "RenderInfo.hpp"
#include "NaturalCubicCurve.hpp"
#include "FlameExpanded.hpp"
#include "SpatialFilter.hpp"
#include "Histogram.hpp"


#ifdef __APPLE__
#include <mach/mach_time.h>
#endif

#ifdef _WIN32
#include <Windows.h>
#include "WindowsInterface.hpp"
#define ALIGNED_MALLOC(TYPE, target, alignment, size)   target = (TYPE *)_aligned_malloc(size, alignment)
#else
#include "CocoaInterface.h"
#define ALIGNED_MALLOC(TYPE, target, alignment, size)   posix_memalign((void **)&target, alignment, size)
#endif


#include <iostream>
#include <stdlib.h>
#include <mutex>
#include <assert.h>

using std::string;

// these are kernel structures we have to make and pass to the renderer

struct alignas(16) VariationListNode // each xform has a variable length list of active variations and each variation has its own specific variable sized varpar struct
{                        // all of the lists are concatenated into a single buffer - a separate xformUsageIndex has the offset to the xform's first variation in this list
    uint variationID;    // the numeric value identifying the variation from the variation set - NOTE id of zero is used to signify end of list
    uint varparOffset;   // the offset in varpar union list for this variation's specific varpar struct
    uint enterGroup;     // the state transition that handles Pre, Normal, and Post variation groups
    // entering group, last in group, inside group
};

#define MULTI_DEVICE_BATCHES_MIN 10

// ============ Statics =====================
static std::once_flag initialized_flag;

// only need to initialize random starting values once and resuse them over and over
static FAPoint *randomPointPool;
static int     *randomSeeds;
static uint    *initialPointIterations;
static uint    *initialPointIterationsRecharge;
static int     *initialK;
static uint    *pointShuffledBlockWarp16;
static uint    *pointShuffledBlockWarp32;
static uint    *pointShuffledBlockWarp64;
static uint    *pointShuffledBlockWarp128;

static void shuffleByBlock(uint *array, uint numPoints);

static void initializeStatics()
{
    if (randomPointPool)                ALIGNED_FREE(randomPointPool);
    if (randomSeeds)                    ALIGNED_FREE(randomSeeds);
    if (initialPointIterations)         ALIGNED_FREE(initialPointIterations);
    if (initialPointIterationsRecharge) ALIGNED_FREE(initialPointIterationsRecharge);
    if (initialK)                       ALIGNED_FREE(initialK);
    
    const size_t singlePointPoolSize = NUM_POINTS_WARPSIZE_32 * BLOCKSX_WARPSIZE_32 * 32;
    const size_t pointPoolSize = MAX_XFORMS * singlePointPoolSize;
    ALIGNED_MALLOC(FAPoint, randomPointPool, 4096, pointPoolSize * sizeof(FAPoint));
    
#ifdef __APPLE__
    srand((unsigned)(0x7FFFFFFF & mach_absolute_time()));
#elif defined(_WIN32)
    DWORD time1 = GetTickCount();
    srand((unsigned)(0x7FFFFFFF & time1));
#else
    timespec time1;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
    srand((unsigned)(0x7FFFFFFF & time1.tv_nsec));
#endif
    for (int n = 0; n < singlePointPoolSize; n++)
    {
        randomPointPool[n].x = 2.f*(float)rand()/(float)RAND_MAX - 1.f;
        randomPointPool[n].y = 2.f*(float)rand()/(float)RAND_MAX - 1.f;
        randomPointPool[n].z = 2.f*(float)rand()/(float)RAND_MAX - 1.f;
        randomPointPool[n].pal = 2.f*(float)rand()/(float)RAND_MAX - 1.f;
    }
    for (int n = 1; n < MAX_XFORMS; n++) {
        for (int i = 0; i < singlePointPoolSize; i++) {
            randomPointPool[i + singlePointPoolSize * n].x   = randomPointPool[i].x;
            randomPointPool[i + singlePointPoolSize * n].y   = randomPointPool[i].y;
            randomPointPool[i + singlePointPoolSize * n].z   = randomPointPool[i].z;
            randomPointPool[i + singlePointPoolSize * n].pal = randomPointPool[i].pal;
        }
    }
    
    // NOTE: BLOCKDIM * BLOCKSX is independent of warpsize
    // NOTE: NUMPOINTS * BLOCKSX is independent of warpsize
    // NOTE: BLOCKSY is independent of warpsize
    const size_t seedPoolSize = BLOCKSX_WARPSIZE_32 * 32 * 32 * warpsPerBlock;
    ALIGNED_MALLOC(int, randomSeeds, 4096, sizeof(int)*seedPoolSize);
    for (int n = 0; n < seedPoolSize; n++)
        randomSeeds[n] = rand();
    
    const size_t kPoolSize = BLOCKSX_WARPSIZE_32 * 32;
    ALIGNED_MALLOC(int, initialK, 4096, sizeof(int)*kPoolSize);
    
    for (int n = 0; n < kPoolSize; n++)
        initialK[n] = 0;
    
    ALIGNED_MALLOC(uint, initialPointIterations, 4096, sizeof(uint)*pointPoolSize);
    for (int n = 0; n < pointPoolSize; n++)
        initialPointIterations[n] = 0;
    
    ALIGNED_MALLOC(uint, initialPointIterationsRecharge, 4096, sizeof(uint)*pointPoolSize);
    for (int n = 0; n < pointPoolSize; n++)
        initialPointIterationsRecharge[n] = 15;
    
    ALIGNED_MALLOC(uint, pointShuffledBlockWarp16, 4096, sizeof(uint)*numPointsWarp16*NUM_ITERATIONS);
    ALIGNED_MALLOC(uint, pointShuffledBlockWarp32, 4096, sizeof(uint)*numPointsWarp32*NUM_ITERATIONS);
    ALIGNED_MALLOC(uint, pointShuffledBlockWarp64, 4096, sizeof(uint)*numPointsWarp64*NUM_ITERATIONS);
    ALIGNED_MALLOC(uint, pointShuffledBlockWarp128, 4096, sizeof(uint)*numPointsWarp128*NUM_ITERATIONS);
    
#ifdef __APPLE__
    srand((unsigned)(0x7FFFFFFF & mach_absolute_time()));
#elif defined(_WIN32)
    time1 = GetTickCount();
    srand((unsigned)(0x7FFFFFFF & time1));
#else
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time1);
    srand((unsigned)(0x7FFFFFFF & time1.tv_nsec));
#endif
    for (uint n = 0; n < NUM_ITERATIONS; n++) {
        shuffleByBlock(&pointShuffledBlockWarp16[n * numPointsWarp16], numPointsWarp16);
        shuffleByBlock(&pointShuffledBlockWarp32[n * numPointsWarp32], numPointsWarp32);
        shuffleByBlock(&pointShuffledBlockWarp64[n * numPointsWarp64], numPointsWarp64);
        shuffleByBlock(&pointShuffledBlockWarp128[n * numPointsWarp128], numPointsWarp128);
    }
}


Flam4CudaRuntime::Flam4CudaRuntime(SharedCudaContext _cudaContext, uint _numBatches)
: DeviceRuntime(_numBatches,
                _numBatches < MULTI_DEVICE_BATCHES_MIN ? 1 : (uint)_cudaContext->selectedDevices.size())
{
    uint _numDevices = _numBatches < MULTI_DEVICE_BATCHES_MIN ? 1 : (uint)_cudaContext->selectedDevices.size();
    assert(_numDevices > 0);
    
    std::call_once(initialized_flag, initializeStatics); // guaranteed to be called just once

    cudaContext = _cudaContext.get();
    
    d_g_Palette         = nullptr;
    d_g_PaletteTexture  = nullptr;
    d_g_Camera          = nullptr;
    d_g_varUsages       = nullptr;
    varpars             = nullptr;
    d_g_varUsageIndexes = nullptr;
    d_g_accumBuffer     = nullptr;
    d_g_resultStaging   = nullptr;
    d_g_startingXform   = nullptr;
    d_g_markCounts      = nullptr;
    d_g_pixelCounts     = nullptr;
    d_g_reduction       = nullptr;
    d_g_pointList       = nullptr;
    d_g_pointIterations = nullptr;
    d_g_randSeeds       = nullptr;
    d_g_randK           = nullptr;
    d_g_renderTarget    = nullptr;
    d_g_switchMatrix    = nullptr;
    pin_mergeStaging    = (CUdeviceptr)0;
    
    mergeStagingBuffer  = nullptr;
    isMergePinned       = true;
    permutations        = nullptr;
    gradients           = nullptr;
    shuffle             = nullptr;
    iterationCount      = nullptr;
    
    streams             = new CUstream[numDevices];
    contexts            = new CUcontext[numDevices];
    totalUsageSize      = 0;
    totalUsageSize      = 0;
    status              = CUDA_SUCCESS;
    buf                 = nullptr;
    
    _fuseIterations = new uint[numDevices];
    
    if (cudaContext->selectedDevices.size() == 0) {
        cudaContext->refreshMemoryStats();
        cudaContext->rearrangeSelectedDevices();
    }
    
    // NOTE: BLOCKDIM * BLOCKSX is independent of warpsize
    // NOTE: NUMPOINTS * BLOCKSX is independent of warpsize
    // NOTE: BLOCKSY is independent of warpsize
    for (uint selectedDeviceNum = 0; selectedDeviceNum < numDevices; selectedDeviceNum++) {
        uint warpSize = cudaContext->warpSizeForSelectedDevice(selectedDeviceNum);
        
        if (warpSize == 1 ) { // CPU - use pseudo Warpsize = 32
            BLOCKDIM[selectedDeviceNum]  = 32*warpsPerBlock;
            BLOCKSX[selectedDeviceNum]   = BLOCKSX_WARPSIZE_32;
            NUMPOINTS[selectedDeviceNum] = NUM_POINTS_WARPSIZE_32;
        }
        else {
            BLOCKDIM[selectedDeviceNum]  = warpSize*warpsPerBlock;
            BLOCKSX[selectedDeviceNum]   = BLOCKSX_WARPSIZE_32*32/warpSize;
            NUMPOINTS[selectedDeviceNum] = NUM_POINTS_WARPSIZE_32/32*warpSize;
        }
        BLOCKSY[selectedDeviceNum] = 32;
        _fuseIterations[selectedDeviceNum] = cudaContext->fuseIterationsForSelectedDevice(selectedDeviceNum);
        
        CUcontext _context = cudaContext->cucontextForSelectedDevice(selectedDeviceNum);
        CUstream  _stream  = cudaContext->streamForSelectedDevice(selectedDeviceNum);
        contexts[selectedDeviceNum] = _context;
        streams[selectedDeviceNum]  = _stream;
        status = cuCtxPushCurrent(_context);
        status = cuCtxPopCurrent(&_context);
    }
    initGlobalBuffers();
}

// initialize global host buffers
void Flam4CudaRuntime::initGlobalBuffers()
{
    if (!d_g_Palette)			d_g_Palette         = new CUdeviceptr[numDevices];
    if (!d_g_PaletteTexture)	d_g_PaletteTexture  = new CUarray[numDevices];
    if (!d_g_varUsages)         d_g_varUsages       = new CUdeviceptr[numDevices];
    if (!varpars)               varpars             = new CUdeviceptr[numDevices];
    if (!d_g_varUsageIndexes)   d_g_varUsageIndexes = new CUdeviceptr[numDevices];
    if (!d_g_Camera)            d_g_Camera          = new CUdeviceptr[numDevices];
    if (!d_g_accumBuffer)       d_g_accumBuffer     = new CUdeviceptr[numDevices];
    if (!d_g_resultStaging)     d_g_resultStaging   = new CUdeviceptr[numDevices];
    if (!d_g_startingXform)     d_g_startingXform   = new CUdeviceptr[numDevices];
    if (!d_g_markCounts)        d_g_markCounts      = new CUdeviceptr[numDevices];
    if (!d_g_pixelCounts)       d_g_pixelCounts     = new CUdeviceptr[numDevices];
    if (!d_g_reduction)         d_g_reduction       = new CUdeviceptr[numDevices];
    if (!d_g_pointList)         d_g_pointList       = new CUdeviceptr[numDevices];
    if (!d_g_pointIterations)   d_g_pointIterations = new CUdeviceptr[numDevices];
    if (!d_g_randSeeds)         d_g_randSeeds       = new CUdeviceptr[numDevices];
    if (!d_g_renderTarget)      d_g_renderTarget    = new CUdeviceptr[numDevices];
    if (!d_g_randK)             d_g_randK           = new CUdeviceptr[numDevices];
    if (!d_g_switchMatrix)      d_g_switchMatrix    = new CUdeviceptr[numDevices];
    if (!permutations)			permutations        = new CUdeviceptr[numDevices];
    if (!gradients)			    gradients           = new CUdeviceptr[numDevices];
    if (!shuffle)			    shuffle             = new CUdeviceptr[numDevices];
    if (!iterationCount)        iterationCount      = new CUdeviceptr[numDevices];
    
    for (int n = 0; n < numDevices; n++)
    {
        d_g_Palette[n]         = (CUdeviceptr)0;
        d_g_PaletteTexture[n]  = (CUarray)0;
        d_g_varUsages[n]       = (CUdeviceptr)0;
        varpars[n]             = (CUdeviceptr)0;
        d_g_varUsageIndexes[n] = (CUdeviceptr)0;
        d_g_Camera[n]          = (CUdeviceptr)0;
        d_g_accumBuffer[n]     = (CUdeviceptr)0;
        d_g_resultStaging[n]   = (CUdeviceptr)0;
        d_g_markCounts[n]      = (CUdeviceptr)0;
        d_g_pixelCounts[n]     = (CUdeviceptr)0;
        d_g_reduction[n]       = (CUdeviceptr)0;
        d_g_startingXform[n]   = (CUdeviceptr)0;
        d_g_pointList[n]       = (CUdeviceptr)0;
        d_g_pointIterations[n] = (CUdeviceptr)0;
        d_g_randSeeds[n]       = (CUdeviceptr)0;
        d_g_renderTarget[n]    = (CUdeviceptr)0;
        d_g_randK[n]           = (CUdeviceptr)0;
        d_g_switchMatrix[n]    = (CUdeviceptr)0;
        permutations[n]        = (CUdeviceptr)0;
        gradients[n]           = (CUdeviceptr)0;
        shuffle[n]             = (CUdeviceptr)0;
        iterationCount[n]      = (CUdeviceptr)0;
        buf[n]                 = new char[bufsize];
    }
}

Flam4CudaRuntime::~Flam4CudaRuntime()
{
    delete _fuseIterations;
    
    if (d_g_Palette)			delete [] d_g_Palette;
    if (d_g_PaletteTexture)     delete [] d_g_PaletteTexture;
    if (d_g_varUsages)			delete [] d_g_varUsages;
    if (d_g_varUsageIndexes)	delete [] d_g_varUsageIndexes;
    if (varpars)                delete [] varpars;
    
    if (d_g_Camera)             delete [] d_g_Camera;
    if (d_g_accumBuffer)        delete [] d_g_accumBuffer;
    if (d_g_resultStaging)      delete [] d_g_resultStaging;
    if (d_g_startingXform)      delete [] d_g_startingXform;
    if (d_g_markCounts)         delete [] d_g_markCounts;
    if (d_g_pixelCounts)        delete [] d_g_pixelCounts;
    if (d_g_reduction)          delete [] d_g_reduction;
    if (d_g_pointList)          delete [] d_g_pointList;
    if (d_g_pointIterations)    delete [] d_g_pointIterations;
    if (d_g_randSeeds)          delete [] d_g_randSeeds;
    if (d_g_renderTarget)       delete [] d_g_renderTarget;
    if (d_g_randK)              delete [] d_g_randK;
    if (d_g_switchMatrix)       delete [] d_g_switchMatrix;
    if (permutations)           delete [] permutations;
    if (gradients)              delete [] gradients;
    if (shuffle)                delete [] shuffle;
    if (iterationCount)         delete [] iterationCount;
    
    for (int n = 0; n < numDevices; n++)
        delete buf[n];
    
    if (buf)                    delete [] buf;
}

// To initialize an array a of n elements to a randomly shuffled copy of source, both 0-based:
//    a[0] ← source[0]
//    for i from 1 to n − 1 do
//      j ← random integer with 0 ≤ j ≤ i
//      if j ≠ i
//          a[i] ← a[j]
//      a[j] ← source[i]
static void shuffleByBlock(uint *array, uint numPoints)
{
    array[0] = 0;
    for (uint i = 1; i < numPoints; i ++) {
        uint j = rand() % (i + 1);
        if (j != i)
            array[i] = array[j];
        array[j] = i;
    }
    //    logArray(array, numPoints);
}

//static void shuffleByWarp(uint *array, uint numPoints)
//{
//    uint warpSize = numPoints/warpsPerBlock;
//    array[0] = 0;
//    for (uint i = 1; i < warpSize; i ++) {
//        uint j = random() % (i + 1);
//        if (j != i)
//            array[i] = array[j];
//        array[j] = i;
//    }
//    
//    array[warpSize] = warpSize;
//    for (uint i = 1; i < warpSize; i ++) {
//        uint j = random() % (i + 1);
//        if (j != i)
//            array[warpSize + i] = array[warpSize + j];
//        array[warpSize + j] = i + warpSize;
//    }
//    //    logArray(array, numPoints);
//}

void Flam4CudaRuntime::logVarlist(Flame & flame,
                                uint selectedDeviceNum,
                                unsigned int *xformVarUsageIndexes,
                                struct VariationListNode *varList,
                                float *varparInstances,
                                uint total,
                                uint totalVarPar,
                                const SharedVariationSet & variationSet)
{
    systemLog("\nXform VarList Offsets =======");
    for (int xform = 0; xform < flame.params.numTrans; xform++) {
        snprintf(buf[selectedDeviceNum], bufsize,
                 "   Xform#%u VarList Offset:%u", xform, xformVarUsageIndexes[xform]);
        systemLog(buf[selectedDeviceNum]);
    }
    systemLog("\nFinal Xform VarList Offsets =======");
    for (int xform = 0; xform < flame.params.numFinal; xform++) {
        snprintf(buf[selectedDeviceNum], bufsize,
                 "   FinalXform#%u VarList Offset:%u", xform, xformVarUsageIndexes[flame.params.numTrans + xform]);
        systemLog(buf[selectedDeviceNum]);
    }
    
    systemLog("\nVarList =======");
    for (int i = 0; i < total; i++) {
        struct VariationListNode list = varList[i];
        
        Variation * variation = variationSet->variationForXformIndex(list.variationID);
        const char * name = variation ? variation->name.c_str() : "";
        
        snprintf(buf[selectedDeviceNum], bufsize,
                 "   Index:%u  VarPar Offset:%u xformIndex:%u name:%s enterGroup:%u", i, list.varparOffset, list.variationID, name, list.enterGroup);
        systemLog(buf[selectedDeviceNum]);
    }
    
    systemLog("\nVarPars =======");
    for (int i = 0; i < totalVarPar; i++) {
        snprintf(buf[selectedDeviceNum], bufsize, "   Index:%u  VarPar %f", i, varparInstances[i]);
        systemLog(buf[selectedDeviceNum]);
    }
}

// Each xform has a variable length list of variation instances
// Each variation instance has its own variable sized varpar cluster struct (struct has weight + any parameter values)

// all of the lists are concatenated into a single buffer (one buffer per list type)

// kernel receives three lists
//    d_g_varUsages       - list of VariationListNode structs (one per variation instance)
//                              uint variationID;   - value identifying the variation from the variation set
//                                                  - NOTE: id of zero is used to signify end of list
//                              uint varparOffset;  - offset in varpars list for this variation's specific varpar struct
//                              uint enterGroup;    - the state transition that handles entering Pre, Normal, and Post variation groups

//    d_g_varUsageIndexes - list of indexes (one per xform) - has the offset to each xform's first variation in varUsages list
//    varpars             - list of varpar clusters

// so kernel takes current xform index - it finds the sub-list of variation indexes:  varUsagesIndex = d_g_varUsageIndexes[xformIndex]
//          its first VariationListNode is:            node = d_g_varUsages[varUsagesIndex]
//          the varpar struct for that instance is at: varpars[node.varparOffset]
//    kernel walks an xform's d_g_varUsages sub-list - rendering each variation instance until node wth variationID of zero is reached

void Flam4CudaRuntime::makeVarUsageListsForSelectedDeviceNum(uint selectedDeviceNum, Flame & flame, const SharedVariationSet & variationSet)
{
    status = CUDA_SUCCESS;
    status = cuCtxPushCurrent(contexts[selectedDeviceNum]);
    
#ifdef SHOW_VARLIST
    bool showVarlist = true;
#else
    bool showVarlist = false;
#endif
    unsigned int xformVarUsageCounts      [MAX_XFORMS + MAX_XFORMS];
    unsigned int xformVarParUsageCounts   [MAX_XFORMS + MAX_XFORMS];
    unsigned int xformVarParOffsetIndexes [MAX_XFORMS + MAX_XFORMS];
    unsigned int xformVarUsageIndexes     [MAX_XFORMS + MAX_XFORMS];
    
    for (uint i = 0; i < MAX_XFORMS + MAX_XFORMS; i++) {
        xformVarUsageCounts[i]       = 0;
        xformVarParUsageCounts[i]    = 0;
        xformVarParOffsetIndexes[i]  = 0;
        xformVarUsageIndexes[i]      = 0;
    }
    
    // count number of variation instance slots used by all xforms AND number of varpar slots
    uint total = 0, totalVarPar = 0;
    for (int xform = 0; xform < flame.params.numTrans; xform++)
    {
        xformVarUsageCounts[xform] = 1; // for the trailing NULL slot
        xformVarParUsageCounts[xform] = 1;
        
        SharedVariationChain & chain = flame.xformVarChains[xform];
        for (uint k = 0; k < chain->size(); k++) {
            SharedVariationGroup & group = chain->operator[](k);
            for (string  & key : group->allKeys()) {
                if (VarParInstance::instanceNumOfKey(key) == string::npos)
                    continue;
                
                const Variation * variation = variationSet->variationForKey(key);
                if (variation) {
                    SharedVarParInstance & varpar = group->operator[](key);
                    if (varpar->floatValue == 0.f)
                        continue;
                    
                    xformVarUsageCounts[xform]++;
                    xformVarParUsageCounts[xform] += 1 + variation->parameters.size();
                }
            }
        }
        total       += xformVarUsageCounts[xform];
        totalVarPar += xformVarParUsageCounts[xform];
    }
    
    for (int xform = 0; xform < flame.params.numFinal; xform++)
    {
        xformVarUsageCounts[flame.params.numTrans + xform] = 1; // for the trailing NULL slot
        xformVarParUsageCounts[flame.params.numTrans + xform] = 1;
        
        SharedVariationChain & chain = flame.finalVarChains[xform];
        for (uint k = 0; k < chain->size(); k++) {
            SharedVariationGroup & group = chain->operator[](k);
            for (string  & key : group->allKeys()) {
                if (VarParInstance::instanceNumOfKey(key) == string::npos)
                    continue;
                
                const Variation * variation = variationSet->variationForKey(key);
                if (variation) {
                    SharedVarParInstance & varpar = group->operator[](key);
                    if (varpar->floatValue == 0.f)
                        continue;
                    
                    xformVarUsageCounts[flame.params.numTrans + xform]++;
                    xformVarParUsageCounts[flame.params.numTrans + xform] += 1 + variation->parameters.size();
                }
            }
        }
        total       += xformVarUsageCounts[flame.params.numTrans + xform];
        totalVarPar += xformVarParUsageCounts[flame.params.numTrans + xform];
    }
    
    size_t oldVarUsageSize = totalUsageSize;
    size_t oldVarparSize   = totalVarparSize;
    
    size_t totalUsageSize  = sizeof(struct VariationListNode)*total + 8; // <=== pad by 8 bytes
    size_t totalVarparSize = sizeof(float)*totalVarPar + 8;
    
    struct VariationListNode *varList = (struct VariationListNode *)malloc(totalUsageSize);
    float *varparInstances            = (float *)malloc(totalVarparSize);
    
    xformVarUsageIndexes[0] = 0;
    for (int xform = 1; xform < flame.params.numTrans; xform++)
        xformVarUsageIndexes[xform] = xformVarUsageIndexes[xform-1] + xformVarUsageCounts[xform-1];
    
    for (int xform = flame.params.numTrans; xform < flame.params.numTrans + flame.params.numFinal; xform++)
        xformVarUsageIndexes[xform] = xformVarUsageIndexes[xform-1] + xformVarUsageCounts[xform-1];
    
    xformVarParOffsetIndexes[0] = 0;
    for (int xform = 1; xform < flame.params.numTrans; xform++)
        xformVarParOffsetIndexes[xform] = xformVarParOffsetIndexes[xform-1] + xformVarParUsageCounts[xform-1];
    
    for (int xform = flame.params.numTrans; xform < flame.params.numTrans + flame.params.numFinal; xform++)
        xformVarParOffsetIndexes[xform] = xformVarParOffsetIndexes[xform-1] + xformVarParUsageCounts[xform-1];
    
    
    bool enterGroup = true;
    uint index = 0, offset = 0;
    for (int xform = 0; xform < flame.params.numTrans; xform++)
    {
        if (showVarlist)
        { snprintf(buf[selectedDeviceNum], bufsize, "Xform #%i =======\n", xform); systemLog(buf[selectedDeviceNum]); }
        index  = xformVarUsageIndexes[xform];
        offset = xformVarParOffsetIndexes[xform];
        enterGroup = true;
        
        SharedVariationChain & chain = flame.xformVarChains[xform];
        for (uint k = 0; k < chain->size(); k++) {
            SharedVariationGroup & group = chain->operator[](k);
            enterGroup = group->size() > 0;  // group might be empty
            
            if (group->amPostMatrix(variationSet)) {
                enterGroup = false; // dont wast time doing precalcs for this specialized variation group
            }
            for (std::string & key : keysSortedByValue(group->getDictionary(), VarParInstance::xformIndexPairCompare)) {
                if (VarParInstance::instanceNumOfKey(key) == string::npos)
                    continue;
                Variation * variation = variationSet->variationForKey(key);
                if (variation) {
                    SharedVarParInstance & varpar = group->operator[](key);
                    if (varpar->floatValue == 0.f)
                        continue;
                    
                    size_t instanceNum          = varpar->instanceNum;
                    varList[index].varparOffset = offset;
                    varList[index].variationID  = (uint)variation->xformIndex;
                    varList[index].enterGroup   = enterGroup;
                    enterGroup = false;
                    index++;
                    varparInstances[offset++]   = varpar->floatValue;
                    if (showVarlist)
                    { snprintf(buf[selectedDeviceNum], bufsize, "%s\n", varpar->description().c_str()); systemLog(buf[selectedDeviceNum]); }
                    
                    for (std::string & parameterName :  keysSortedByValue(variation->parameters, VariationParameter::xformIndexPairCompare)) {
                        SharedParameter & parameter     = variation->parameters.operator[](parameterName);
                        string instancedParamKey        = VarParInstance::makeInstancedKey(parameter->key, instanceNum);
                        SharedVarParInstance & varparam = group->operator[](instancedParamKey);
                        varparInstances[offset++]       = varparam->floatValue;
                        if (showVarlist)
                        { snprintf(buf[selectedDeviceNum], bufsize, "%s\n", varparam->description().c_str()); systemLog(buf[selectedDeviceNum]); }
                    }
                }
            }
        }
        varList[index].varparOffset = 0;
        varList[index].variationID  = 0;
        varList[index].enterGroup   = enterGroup;
        index++;
    }
    
    for (int xform = 0; xform < flame.params.numFinal; xform++)
    {
        if (showVarlist)
        { snprintf(buf[selectedDeviceNum], bufsize, "Final Xform #%i =======\n", xform); systemLog(buf[selectedDeviceNum]); }
        index  = xformVarUsageIndexes[flame.params.numTrans + xform];
        offset = xformVarParOffsetIndexes[flame.params.numTrans + xform];
        enterGroup = true;
        
        SharedVariationChain & chain = flame.finalVarChains[xform];
        for (uint k = 0; k < chain->size(); k++) {
            SharedVariationGroup & group = chain->operator[](k);
            enterGroup = group->size() > 0;  // group might be empty
            
            if (group->amPostMatrix(variationSet)) {
                enterGroup = false; // dont wast time doing precalcs for this specialized variation group
            }
            for (std::string & key : keysSortedByValue(group->getDictionary(), VarParInstance::xformIndexPairCompare)) {
                if (VarParInstance::instanceNumOfKey(key) == string::npos)
                    continue;
                Variation * variation = variationSet->variationForKey(key);
                if (variation) {
                    SharedVarParInstance & varpar = group->operator[](key);
                    if (varpar->floatValue == 0.f)
                        continue;
                    
                    size_t instanceNum          = varpar->instanceNum;
                    varList[index].varparOffset = offset;
                    varList[index].variationID  = (uint)variation->xformIndex;
                    varList[index].enterGroup   = enterGroup;
                    enterGroup = false;
                    index++;
                    varparInstances[offset++]   = varpar->floatValue;
                    if (showVarlist)
                    { snprintf(buf[selectedDeviceNum], bufsize, "%s\n", varpar->description().c_str()); systemLog(buf[selectedDeviceNum]); }
                    
                    for (std::string & parameterName :  keysSortedByValue(variation->parameters, VariationParameter::xformIndexPairCompare)) {
                        SharedParameter & parameter     = variation->parameters.operator[](parameterName);
                        string instancedParamKey        = VarParInstance::makeInstancedKey(parameter->key, instanceNum);
                        SharedVarParInstance & varparam = group->operator[](instancedParamKey);
                        varparInstances[offset++]       = varparam->floatValue;
                        if (showVarlist)
                        { snprintf(buf[selectedDeviceNum], bufsize, "%s\n", varparam->description().c_str()); systemLog(buf[selectedDeviceNum]); }
                    }
                }
            }
        }
        varList[index].varparOffset = 0;
        varList[index].variationID  = 0;
        varList[index].enterGroup   = enterGroup;
        index++;
    }
    
    if (showVarlist) {
        logVarlist(flame, selectedDeviceNum, xformVarUsageIndexes, varList, varparInstances, total, totalVarPar, variationSet);
    }

    if (d_g_varUsages[selectedDeviceNum] != 0) {
        if (oldVarUsageSize < totalUsageSize) {
            status = cuMemFree_v2(d_g_varUsages[selectedDeviceNum]);
            status = cuMemAlloc(&d_g_varUsages[selectedDeviceNum], totalUsageSize);
        }
    }
    else {
        status = cuMemAlloc(&d_g_varUsages[selectedDeviceNum], totalUsageSize);
    }
    
    if (varpars[selectedDeviceNum] != 0) {
        if (oldVarparSize < totalVarparSize) {
            status = cuMemFree_v2(varpars[selectedDeviceNum]);
            status = cuMemAlloc(&varpars[selectedDeviceNum], totalVarparSize);
        }
    }
    else {
        status = cuMemAlloc(&varpars[selectedDeviceNum], totalVarparSize);
    }
    
    CUstream queue = streams[selectedDeviceNum];
    status = cuMemcpyHtoDAsync_v2(d_g_varUsages[selectedDeviceNum],       varList, totalUsageSize, queue);
    status = cuMemcpyHtoDAsync_v2(varpars[selectedDeviceNum],             varparInstances, totalVarparSize, queue);
    status = cuMemcpyHtoDAsync_v2(d_g_varUsageIndexes[selectedDeviceNum], xformVarUsageIndexes, sizeof(uint)*(MAX_XFORMS+MAX_XFORMS), queue);
    status = cuStreamSynchronize(queue);
    
    free(varList);
    free(varparInstances);
    
    CUcontext context;
    status = cuCtxPopCurrent(&context);
}

void Flam4CudaRuntime::optBlockYforQuality(int quality, uint selectedDeviceNum)
{
    size_t singlePointPoolSize   = NUM_POINTS_WARPSIZE_32 * BLOCKSX_WARPSIZE_32 * 32;
    double requiredIterationsPerTile = ((double)xDim*yDim*quality);
    size_t fuseIterations       = singlePointPoolSize * _fuseIterations[selectedDeviceNum];
    int optBlockY                    = ceil((requiredIterationsPerTile +  fuseIterations) /
                                            (double)(BLOCKSX[selectedDeviceNum]*BLOCKDIM[selectedDeviceNum]*NUM_ITERATIONS_FUSE_REDUCED));
    
    // Note: BLOCKSX*BLOCKSY*BLOCKDIM*NUM_ITERATIONS = xDim*yDim*actualQuality
    BLOCKSY[selectedDeviceNum] = optBlockY > 32 ? 32 : optBlockY; // Cap at 32
}

void Flam4CudaRuntime::saveRenderTarget(CUstream queue, int selectedDeviceNum)
{
    status = cuCtxPushCurrent(contexts[selectedDeviceNum]);
    
    status = cuMemcpyDtoH_v2(hostAccumBuffer, d_g_renderTarget[selectedDeviceNum], accumBufferSize());
    
    CUcontext context;
    status = cuCtxPopCurrent(&context);
}

CUresult Flam4CudaRuntime::getStatus()
{
    return status;
}

void Flam4CudaRuntime::setStatus(CUresult _status)
{
    status = _status;
    
    if (status == CUDA_ERROR_OUT_OF_MEMORY) {
        size_t free = 0, total = 0;
        cuMemGetInfo_v2(&free, &total);
        
        snprintf(buf[0], sizeof(buf[0]), "Free Memory:%zu", free);
        systemLog(buf[0]);
    }
    if (status != CUDA_SUCCESS) {
        const char *name = nullptr;
        const char *errorStr = nullptr;
        cuGetErrorName(_status, &name);
        cuGetErrorString(_status, &errorStr);
        snprintf(buf[0], sizeof(buf[0]), "%s: %s", name, errorStr);
        systemLog(buf[0]);
    }
}

float Flam4CudaRuntime::logHistogramStatsFromData(std::vector<float4> data,
                                                const char * prefix,
                                                uint batches,
                                                size_t maxTheoretical,
                                                uint selectedDeviceNum)
{    
    Histogram<float4> histogram(data, xDim, yDim, g_pFlame[selectedDeviceNum], batches);
    
    double sumW = histogram.getSumW();
    if ((size_t)sumW > maxTheoretical) {
        snprintf(buf[selectedDeviceNum], bufsize, "<============= Too Many Hits ==================> MaxTheoretical: %zu SumW:%zu", maxTheoretical, (size_t)sumW);
        systemLog(buf[selectedDeviceNum]);
        checkRenderedBatchCounts();
    }
    
    float4 point  = histogram.pointAtPointOffset(Point(xDim*0.5f, yDim*0.5f));
    
    snprintf(buf[selectedDeviceNum], bufsize, "%s %s sumLimit=%zu w@Center=%f w/batches=%f Sdm:%u",
             prefix, histogram.description().c_str(), maxTheoretical, point.w, point.w/batches, selectedDeviceNum);
    systemLog(buf[selectedDeviceNum]);
    
    float avgHitsPerBatch = histogram.avgHitsPerBatch();
    return avgHitsPerBatch;
}

float Flam4CudaRuntime::logHistogramStatsFromQueue(CUstream queue,
                                                 uint selectedDeviceNum,
                                                 const char * prefix,
                                                 uint batches)
{
    size_t length = xDim*yDim*sizeof(float4);
    float4 *buf  = nullptr;
    
    ALIGNED_MALLOC(float4, buf, 4096, length);
    status = cuMemcpyDtoHAsync_v2(buf, d_g_renderTarget[selectedDeviceNum], length, queue);
    status = cuStreamSynchronize(queue);
    
    std::vector<float4> data(buf, buf + xDim*yDim);
    ALIGNED_FREE(buf);
    
    size_t maxTheoretical  =
        batches * NUM_ITERATIONS*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    
    return logHistogramStatsFromData(data, prefix, batches, maxTheoretical, selectedDeviceNum);
}

float Flam4CudaRuntime::logAccumHistogramStatsFromQueue(CUstream queue,
                                                   uint selectedDeviceNum,
                                                   const char * prefix)
{
    size_t length = xDim*yDim*sizeof(float4);
    float4 *buf  = nullptr;
    
    ALIGNED_MALLOC(float4, buf, 4096, length);
    status = cuMemcpyDtoHAsync_v2(buf, d_g_accumBuffer[selectedDeviceNum], length, queue);
    status = cuStreamSynchronize(queue);
    
    std::vector<float4> data(buf, buf + xDim*yDim);
    ALIGNED_FREE(buf);
    
    uint batches = g_pFlame[selectedDeviceNum]->params.numBatches;
    
    size_t maxTheoretical  =
    batches * NUM_ITERATIONS*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    
    return logHistogramStatsFromData(data, prefix, batches, maxTheoretical, selectedDeviceNum);
}

void Flam4CudaRuntime::logHostAccumHistogramStatsFromSelectedDeviceNum(int selectedDeviceNum, const std::string & prefix)
{
    uint batches           = g_pFlame[selectedDeviceNum]->params.numBatches;
    size_t maxTheoretical  = batches * NUM_ITERATIONS*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    float4 *buf            = (float4 *)hostAccumBuffer;
    std::vector<float4> data(buf, buf + xDim*yDim);

    logHistogramStatsFromData(data, prefix.c_str(), batches, maxTheoretical, selectedDeviceNum);
}

void Flam4CudaRuntime::addToAccumBufferForSelectedDeviceNum(uint selectedDeviceNum)
{
    status = cuCtxPushCurrent(contexts[selectedDeviceNum]);
    
    CUstream queue = streams[selectedDeviceNum];
    
    size_t length = accumBufferSize();
    FAPoint *buf      = NULL;
	ALIGNED_MALLOC(FAPoint, buf, 4096, length);
    status = cuMemcpyDtoHAsync_v2(buf, d_g_renderTarget[selectedDeviceNum], length, queue);
    status = cuStreamSynchronize(queue);
    
    length     /= sizeof(float);
    float *from = (float *)buf;
    float *to   = (float *)hostAccumBuffer;
    for (size_t  i = 0; i < length; i++) {
        *to++ += *from++;
    }
	ALIGNED_FREE(buf);

    CUcontext context;
    status = cuCtxPopCurrent(&context);
}

#pragma mark Buffer Retain & Release

void Flam4CudaRuntime::unpinBuffers(CUstream queue, uint selectedDeviceNum)
{
    if (mergeStagingBuffer == nullptr)
        return;
    status = cuCtxPushCurrent(contexts[selectedDeviceNum]);
    
    if (isMergePinned) {
        status = cuMemFreeHost(mergeStagingBuffer);
    }
    else {
        delete[] mergeStagingBuffer;
        mergeStagingBuffer = nullptr;
    }
    
    CUcontext context;
    status = cuCtxPopCurrent(&context);
}

void Flam4CudaRuntime::deleteCUDAbuffers(CUstream queue, uint selectedDeviceNum)
{
    status = cuCtxPushCurrent(contexts[selectedDeviceNum]);
    
    status = cuStreamSynchronize(queue);
    status = cuCtxSynchronize();
    
    if (selectedDeviceNum == 0 && pin_mergeStaging != 0 && numDevices > 1)
    {
        unpinBuffers(queue, selectedDeviceNum);
    }
    
    if (d_g_Palette[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_Palette[selectedDeviceNum]);
        d_g_Palette[selectedDeviceNum] = 0;
    }
    if (d_g_PaletteTexture[selectedDeviceNum] != 0)
    {
        status = cuArrayDestroy(d_g_PaletteTexture[selectedDeviceNum]);
        d_g_PaletteTexture[selectedDeviceNum] = 0;
    }
    
    if (d_g_varUsages[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_varUsages[selectedDeviceNum]);
        d_g_varUsages[selectedDeviceNum] = 0;
    }
    if (varpars[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(varpars[selectedDeviceNum]);
        varpars[selectedDeviceNum] = 0;
    }
    
    if (d_g_varUsageIndexes[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_varUsageIndexes[selectedDeviceNum]);
        d_g_varUsageIndexes[selectedDeviceNum] = 0;
    }
    if (d_g_Camera[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_Camera[selectedDeviceNum]);
        d_g_Camera[selectedDeviceNum] = 0;
    }
    if (d_g_switchMatrix[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_switchMatrix[selectedDeviceNum]);
        d_g_switchMatrix[selectedDeviceNum] = 0;
    }
    
    if (d_g_accumBuffer[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_accumBuffer[selectedDeviceNum]);
        d_g_accumBuffer[selectedDeviceNum] = 0;
    }
    if (d_g_resultStaging[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_resultStaging[selectedDeviceNum]);
        d_g_resultStaging[selectedDeviceNum] = 0;
    }
    if (d_g_startingXform[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_startingXform[selectedDeviceNum]);
        d_g_startingXform[selectedDeviceNum] = 0;
    }
    if (d_g_markCounts[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_markCounts[selectedDeviceNum]);
        d_g_markCounts[selectedDeviceNum] = 0;
    }
    if (d_g_pixelCounts[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_pixelCounts[selectedDeviceNum]);
        d_g_pixelCounts[selectedDeviceNum] = 0;
    }
    if (d_g_reduction[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_reduction[selectedDeviceNum]);
        d_g_reduction[selectedDeviceNum] = 0;
    }
    if (d_g_pointList[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_pointList[selectedDeviceNum]);
        d_g_pointList[selectedDeviceNum] = 0;
    }
    if (d_g_pointIterations[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_pointIterations[selectedDeviceNum]);
        d_g_pointIterations[selectedDeviceNum] = 0;
    }
    if (d_g_randSeeds[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_randSeeds[selectedDeviceNum]);
        d_g_randSeeds[selectedDeviceNum] = 0;
    }
    if (d_g_renderTarget[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_renderTarget[selectedDeviceNum]);
        d_g_renderTarget[selectedDeviceNum] = 0;
    }
    if (d_g_randK[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_randK[selectedDeviceNum]);
        d_g_randK[selectedDeviceNum] = 0;
    }
    if (permutations[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(permutations[selectedDeviceNum]);
        permutations[selectedDeviceNum] = 0;
    }
    if (gradients[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(gradients[selectedDeviceNum]);
        gradients[selectedDeviceNum] = 0;
    }
    if (shuffle[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(shuffle[selectedDeviceNum]);
        shuffle[selectedDeviceNum] = 0;
    }
    if (iterationCount[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(iterationCount[selectedDeviceNum]);
        iterationCount[selectedDeviceNum] = 0;
    }
    
    CUcontext context;
    status = cuCtxPopCurrent(&context);
}

#pragma mark Create Buffers

void Flam4CudaRuntime::createCUDAbuffers(CudaContext * _cudaContext,
                                         uint selectedDeviceNum,
                                         uint bitDepth,
                                         uint warpSize,
                                         uint desiredQuality,
                                         bool saveRenderState,
                                         size_t maxXformCount,
                                         size_t maxPointPoolCount,
                                         size_t xformCount)
{
    status = cuCtxPushCurrent(contexts[selectedDeviceNum]);
    
    optBlockYforQuality(desiredQuality, selectedDeviceNum);
    
    status = cuMemAlloc(&permutations[selectedDeviceNum], sizeof(permutation));
    status = cuMemAlloc(&gradients[selectedDeviceNum],    sizeof(grad4));
    status = cuMemcpyHtoD_v2(permutations[selectedDeviceNum], permutation, sizeof(permutation));
    status = cuMemcpyHtoD_v2(gradients[selectedDeviceNum], grad4,       sizeof(grad4));
    
    uint* pointShuffled;
    uint  sizeShuffled;
    switch (warpSize) {
        case 16:
            pointShuffled = pointShuffledBlockWarp16;
            sizeShuffled  = sizeof(uint)*numPointsWarp16*NUM_ITERATIONS;
            break;
        case 32:
        default:
            pointShuffled = pointShuffledBlockWarp32;
            sizeShuffled  = sizeof(uint)*numPointsWarp32*NUM_ITERATIONS;
            break;
        case 64:
            pointShuffled = pointShuffledBlockWarp64;
            sizeShuffled  = sizeof(uint)*numPointsWarp64*NUM_ITERATIONS;
            break;
        case 128:
            pointShuffled = pointShuffledBlockWarp128;
            sizeShuffled  = sizeof(uint)*numPointsWarp128*NUM_ITERATIONS;
            break;
}
    
    status = cuMemAlloc(&shuffle[selectedDeviceNum], sizeShuffled);
    status = cuMemcpyHtoD_v2(shuffle[selectedDeviceNum], pointShuffled, sizeShuffled);
    
    if (selectedDeviceNum == 0 && numDevices > 1) {
        status = cuMemHostAlloc((void **)&mergeStagingBuffer, sizeof(float4)*xDim*yDim, CU_MEMHOSTALLOC_PORTABLE | CU_MEMHOSTALLOC_DEVICEMAP);
        status = cuMemHostGetDevicePointer_v2(&pin_mergeStaging, mergeStagingBuffer, 0);
        
        if (status == CUDA_SUCCESS) {
            isMergePinned = true;
        }
        else {
            isMergePinned = false;
            mergeStagingBuffer = new float[4*xDim*yDim];;
        }
    }
    status = cuMemAlloc_v2(&iterationCount[selectedDeviceNum], sizeof(unsigned int));
    status = cuMemsetD8(iterationCount[selectedDeviceNum],  0, sizeof(unsigned int));
    
    status = cuMemAlloc(&d_g_varUsageIndexes[selectedDeviceNum], sizeof(unsigned int)*(MAX_XFORMS+MAX_XFORMS));
    status = cuMemAlloc(&d_g_Camera[selectedDeviceNum],          sizeof(struct CameraViewProperties));
    status = cuMemAlloc(&d_g_switchMatrix[selectedDeviceNum],    sizeof(float)*xformCount*xformCount);
    status = cuMemAlloc(&d_g_accumBuffer[selectedDeviceNum],     sizeof(float4)*xDim*yDim);
    status = cuMemAlloc(&d_g_renderTarget[selectedDeviceNum],    sizeof(float4)*xDim*yDim);
    
    status = cuMemsetD8(d_g_varUsageIndexes[selectedDeviceNum], 0, sizeof(unsigned int)*(MAX_XFORMS+MAX_XFORMS));
    status = cuMemsetD8(d_g_accumBuffer[selectedDeviceNum],     0, sizeof(float4)*xDim*yDim);
    status = cuMemsetD8(d_g_renderTarget[selectedDeviceNum],    0, sizeof(float4)*xDim*yDim);
    
    // if we need an accum Buffer and we have not read it from file, create one
    if ((! hostAccumBuffer) && (saveRenderState || numBatches > CLEAR_FREQUENCY)) {
        void *buf = nullptr;
        ALIGNED_MALLOC(cl_uchar4, buf, 4096, xDim*yDim * sizeof(cl_float4));
        memset(buf, '\0', xDim*yDim * sizeof(cl_float4));
        hostAccumBuffer = (char *)buf;
    }
    
    if (bitDepth == 8)
        status = cuMemAlloc(&d_g_resultStaging[selectedDeviceNum], sizeof(cl_uchar4)*resampledXdim*resampledYdim);
    else if (bitDepth == 16)
        status = cuMemAlloc(&d_g_resultStaging[selectedDeviceNum], sizeof(cl_ushort4)*resampledXdim*resampledYdim);
    else
        status = cuMemAlloc(&d_g_resultStaging[selectedDeviceNum], sizeof(cl_float4)*resampledXdim*resampledYdim);
    
    size_t length = sizeof(uint)*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    status = cuMemAlloc(&d_g_startingXform[selectedDeviceNum], length);
    status = cuMemAlloc(&d_g_markCounts[selectedDeviceNum],    length);
    status = cuMemsetD8(d_g_startingXform[selectedDeviceNum], 0, length);
    status = cuMemsetD8(d_g_markCounts[selectedDeviceNum],    0, length);
    
    // warpsize dependent
    uint blocksTarget = ((xDim * yDim) + (BLOCKDIM[selectedDeviceNum] - 1))/BLOCKDIM[selectedDeviceNum];
    length            = sizeof(unsigned)*blocksTarget;
    status = cuMemAlloc(&d_g_reduction[selectedDeviceNum], length);
    status = cuMemsetD8(d_g_reduction[selectedDeviceNum],  0, length);
    
    length = xDim*yDim * sizeof(uint);
    status = cuMemAlloc(&d_g_pixelCounts[selectedDeviceNum], length);
    status = cuMemsetD8(d_g_pixelCounts[selectedDeviceNum],  0, length);
    
    length = sizeof(FAPoint)*(maxPointPoolCount*NUMPOINTS[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]);
    status = cuMemAlloc(&d_g_pointList[selectedDeviceNum], length);
    status = cuMemcpyHtoD_v2(d_g_pointList[selectedDeviceNum], randomPointPool, length);
    
    length = sizeof(uint)*(maxPointPoolCount*NUMPOINTS[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]);
    status = cuMemAlloc(&d_g_pointIterations[selectedDeviceNum], length);
    status = cuMemcpyHtoD_v2(d_g_pointIterations[selectedDeviceNum], initialPointIterations, length);
    
    length = sizeof(int)*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]*BLOCKDIM[selectedDeviceNum];
    status = cuMemAlloc(&d_g_randSeeds[selectedDeviceNum], length);
    status = cuMemcpyHtoD_v2(d_g_randSeeds[selectedDeviceNum], randomSeeds, length);
    
    d_g_randK[selectedDeviceNum]     = (CUdeviceptr)0;
    
    //    [Flam4ClRuntime logMD5sumFor:flame->colorIndex length:flame->numColors*sizeof(float4) prefix:@"Palette Digest: "];
    
    CUcontext context;
    status = cuCtxPopCurrent(&context);
}

void Flam4CudaRuntime::setupFlameConstants(Flame* flm, CUstream queue, uint selectedDeviceNum, const SharedVariationSet & variationSet)
{
    duration<double> buildTime(0.);
    SharedDeviceKind deviceKind = cudaContext->deviceKindForSelectedDevice(selectedDeviceNum);
    SharedCudaDeviceKind cudaDeviceKind = std::dynamic_pointer_cast<CudaDeviceKind>(deviceKind);
    CudaProgram *program        = cudaDeviceKind->programForVariationSet(variationSet, buildTime);
    
    //deep copy flm to the device
    CUdeviceptr dptr;
    size_t size;
    status = cuModuleGetGlobal_v2(&dptr, &size, program->module(), "d_g_Flame");
    status = cuMemcpyHtoDAsync_v2(dptr, &(flm->params), sizeof(FlameParams), queue);
    
    status = cuModuleGetGlobal_v2(&dptr, &size, program->module(), "d_g_Xforms");
    status = cuMemcpyHtoDAsync_v2(dptr, &(flm->trans[0]), flm->params.numTrans*sizeof(struct xForm), queue);
    
    if (flm->params.numFinal > 0) {
        struct xForm * devXformPtr = (struct xForm *)dptr;
        
        status = cuMemcpyHtoDAsync_v2((CUdeviceptr)&devXformPtr[flm->params.numTrans],
                                      &(flm->finals[0]),
                                      flm->params.numFinal*sizeof(struct xForm),
                                      queue);
    }
    
    status = cuMemcpyHtoDAsync_v2(d_g_switchMatrix[selectedDeviceNum], flm->switchMatrix, sizeof(float) * flm->params.numTrans * flm->params.numTrans, queue);
    
    CameraViewport cv(g_pFlame[selectedDeviceNum]);
    status = cuMemcpyHtoDAsync_v2(d_g_Camera[selectedDeviceNum], cv.pProperties(), sizeof(CameraViewProperties), queue);
}

void Flam4CudaRuntime::setupPalette(Flame*flm, CUstream queue, uint selectedDeviceNum, const SharedVariationSet & variationSet)
{
    //    CudaDeviceKind *deviceKind = (CudaDeviceKind *)cudaContext->deviceKindForSelectedDevice(selectedDeviceNum);
    //    CudaProgram *program       = [deviceKind programForVariationSet:variationSet];
    //
    //    if (d_g_PaletteTexture[selectedDeviceNum] != nullptr)
    //    {
    //        status = cuArrayDestroy(d_g_PaletteTexture[selectedDeviceNum]);
    //        d_g_PaletteTexture[selectedDeviceNum] = nullptr;
    //    }
    //
    //    // create an array to hold the texture values and read static texture reference and set it up
    //    status = CUDA_SUCCESS;
    //    CUtexref texRef;
    //
    //    CUDA_ARRAY_DESCRIPTOR desc2D;
    //    desc2D.Format = CU_AD_FORMAT_FLOAT;
    //    desc2D.Width  = g_pFlame[selectedDeviceNum]->numColors;
    //    desc2D.Height = 1;
    //    desc2D.NumChannels = 4;
    //
    //    status = cuArrayCreate_v2(&d_g_PaletteTexture[selectedDeviceNum], &desc2D);
    //
    //    status = cuMemcpyHtoA_v2(d_g_PaletteTexture[selectedDeviceNum], 0, g_pFlame[selectedDeviceNum]->colorIndex,
    //                                  g_pFlame[selectedDeviceNum]->numColors*sizeof(cl_float4));
    //
    //    status = cuModuleGetTexRef(&texRef, program->module(), "texRef");
    //    status = cuTexRefSetArray(texRef, d_g_PaletteTexture[selectedDeviceNum], CU_TRSA_OVERRIDE_FORMAT);
    //
    //    status = cuTexRefSetFilterMode(texRef,   CU_TR_FILTER_MODE_LINEAR);
    //    status = cuTexRefSetFlags(texRef,        CU_TRSF_NORMALIZED_COORDINATES);
    
    if (d_g_Palette[selectedDeviceNum] != 0)
    {
        status = cuMemFree_v2(d_g_Palette[selectedDeviceNum]);
        d_g_Palette[selectedDeviceNum] = 0;
    }
    size_t length = sizeof(cl_float4)*g_pFlame[selectedDeviceNum]->numColors;
    status = cuMemAlloc(&d_g_Palette[selectedDeviceNum], length);
    status = cuMemcpyHtoD_v2(d_g_Palette[selectedDeviceNum], g_pFlame[selectedDeviceNum]->colorIndex, length);
}

void Flam4CudaRuntime::logBufferInfoForBufferName(const std::string & name, uint selectedDeviceNum, CUdeviceptr buffer)
{
    if (buffer != 0) {
        CUdeviceptr addr = 0;
        size_t size = 0;
        status = cuMemGetAddressRange_v2(&addr, &size, buffer);
        snprintf(buf[selectedDeviceNum], sizeof(buf[selectedDeviceNum]), "Device:%i Buffer %s: len:%zu hostPtr:%llu", selectedDeviceNum, name.c_str(), size, addr);
    }
    else {
        snprintf(buf[selectedDeviceNum], sizeof(buf[selectedDeviceNum]), "Device:%i Buffer %s is zero", selectedDeviceNum, name.c_str());
    }
    systemLog(buf[selectedDeviceNum]);
}

void Flam4CudaRuntime::logAllBuffersForDeviceNum(uint selectedDeviceNum)
{
    logBufferInfoForBufferName("palette", selectedDeviceNum, d_g_Palette[selectedDeviceNum]);
    //    logBufferInfoForBufferName("paletteTexture", selectedDeviceNum, d_g_PaletteTexture[selectedDeviceNum]);
    logBufferInfoForBufferName("varUsages", selectedDeviceNum, d_g_varUsages[selectedDeviceNum]);
    logBufferInfoForBufferName("varUsageIndexes", selectedDeviceNum, d_g_varUsageIndexes[selectedDeviceNum]);
    logBufferInfoForBufferName("varpars", selectedDeviceNum, varpars[selectedDeviceNum]);
    //    logBufferInfoForBufferName("flame", selectedDeviceNum, d_g_Flame[selectedDeviceNum]);
    logBufferInfoForBufferName("camera", selectedDeviceNum, d_g_Camera[selectedDeviceNum]);
    //    logBufferInfoForBufferName("xforms", selectedDeviceNum, d_g_Xforms[selectedDeviceNum]);
    logBufferInfoForBufferName("accum", selectedDeviceNum, d_g_accumBuffer[selectedDeviceNum]);
    logBufferInfoForBufferName("resultStaging", selectedDeviceNum, d_g_markCounts[selectedDeviceNum]);
    logBufferInfoForBufferName("pixelCounts", selectedDeviceNum, d_g_pixelCounts[selectedDeviceNum]);
    logBufferInfoForBufferName("reduction", selectedDeviceNum, d_g_reduction[selectedDeviceNum]);
    logBufferInfoForBufferName("startingXform", selectedDeviceNum, d_g_startingXform[selectedDeviceNum]);
    logBufferInfoForBufferName("pointList", selectedDeviceNum, d_g_pointList[selectedDeviceNum]);
    logBufferInfoForBufferName("pointIterations", selectedDeviceNum, d_g_pointIterations[selectedDeviceNum]);
    logBufferInfoForBufferName("randSeeds", selectedDeviceNum, d_g_randSeeds[selectedDeviceNum]);
    logBufferInfoForBufferName("randK", selectedDeviceNum, d_g_randK[selectedDeviceNum]);
    logBufferInfoForBufferName("renderTarget", selectedDeviceNum, d_g_renderTarget[selectedDeviceNum]);
    logBufferInfoForBufferName("switchMatrix", selectedDeviceNum, d_g_switchMatrix[selectedDeviceNum]);
    logBufferInfoForBufferName("permutations", selectedDeviceNum, permutations[selectedDeviceNum]);
    logBufferInfoForBufferName("gradients", selectedDeviceNum, gradients[selectedDeviceNum]);
    logBufferInfoForBufferName("shuffle", selectedDeviceNum, shuffle[selectedDeviceNum]);
    logBufferInfoForBufferName("iterationCount", selectedDeviceNum, iterationCount[selectedDeviceNum]);
}

void Flam4CudaRuntime::runFuseForSelectedDeviceNum(uint selectedDeviceNum,
                                                   float epsilon,
                                                   uint maxWorkGroupSize,
                                                   const SharedVariationSet & variationSet)
{
    CUstream queue = streams[selectedDeviceNum];

    setupFlameConstants(g_pFlame[selectedDeviceNum], queue, selectedDeviceNum, variationSet);
    makeVarUsageListsForSelectedDeviceNum(selectedDeviceNum, *g_pFlame[selectedDeviceNum], variationSet);
    
    //Render it!
    size_t length = sizeof(int)*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum]*BLOCKDIM[selectedDeviceNum];
    status = cuMemcpyHtoDAsync_v2(d_g_randSeeds[selectedDeviceNum], randomSeeds, length, queue);
    
    // no fuse is done now - done as part of renderBatches
}

#pragma mark Fuse

void Flam4CudaRuntime::readPointIterations(CUstream queue, uint selectedDeviceNum, uint perXformPointCount, uint xformCount)
{
    const uint FUSE_ITERATIONS = 30;
    size_t length = xformCount * perXformPointCount * sizeof(uint);
    uint *buffer  = nullptr;
	ALIGNED_MALLOC(uint, buffer, 4096, length);

    status = cuMemcpyDtoHAsync_v2(buffer, d_g_pointIterations[selectedDeviceNum], length, queue);
    status = cuStreamSynchronize(queue);
    
    //    size_t totalIterations = BLOCKSX[selectedDeviceNum] * BLOCKSY[selectedDeviceNum] * BLOCKDIM[selectedDeviceNum] * NUM_ITERATIONS;
    for (size_t j = 0; j < xformCount; j++) {
        int min = 1000000;
        int max = 0;
        uint sum = 0.f;
        size_t maxLocation = std::string::npos;
        size_t count = perXformPointCount;
        size_t unfusedCount = 0;
        
        for (size_t i = 0; i < count; i++) {
            uint value = buffer[j * perXformPointCount + i];
            min = min >= value ? value : min;
            maxLocation = max <= value ? i : maxLocation;
            max = max <= value ? value : max;
            sum += value;
            if (value < FUSE_ITERATIONS)
                unfusedCount++;
        }
        float mean = (float)sum / count;
        //        float markEfficiency = (float)sum /totalIterations;
        float unfusedFraction = (float)unfusedCount/count;
        float sum2 = 0.f;
        for (size_t i = 0; i < count; i++) {
            uint value = buffer[j * perXformPointCount + i];
            sum2 += (value - mean)*(value - mean);
        }
        float stddev = sqrtf(sum2/count);
        
        if (j == 0)
            snprintf(buf[selectedDeviceNum], sizeof(buf[selectedDeviceNum]),
                     "Xform #%zu Point iterations min:%u mean:%f max:%u stdDev:%f unfused:%f%%", j + 1, min, mean, max, stddev, unfusedFraction*100.f);
        else
            snprintf(buf[selectedDeviceNum], sizeof(buf[selectedDeviceNum]),
                     "      #%zu Point iterations min:%u mean:%f max:%u stdDev:%f unfused:%f%%", j + 1, min, mean, max, stddev, unfusedFraction*100.f);
        systemLog(buf[selectedDeviceNum]);
    }
	ALIGNED_FREE(buffer);
}

#pragma mark StartFrame

void Flam4CudaRuntime::clearRenderTargetForSelectedDeviceNum(unsigned selectedDeviceNum,
                                                             uint maxWorkGroupSize,
                                                             const SharedVariationSet & variationSet)
{
    duration<double> buildTime(0.);
    const SharedDeviceKind & deviceKind = cudaContext->deviceKindForSelectedDevice(selectedDeviceNum);
    CudaDeviceKind * cudaDeviceKind     = (CudaDeviceKind *)deviceKind.get();
    CudaProgram *program                = cudaDeviceKind->programForVariationSet(variationSet, buildTime);
    CUstream queue                      = streams[selectedDeviceNum];

    //    size_t globalWorkSize[2] = { ((xDim + 15)/16)*16, ((yDim + 15)/16)*16 };
    //    size_t localWorkSize[2]  = { 16, 16 };
    uint globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    uint localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    
    //    __global__ void setBufferKernal(__global float4* renderTarget, float4 value, uint xDim, uint yDim)
    
    float4 black = { 0.f, 0.f, 0.f, 0.f };
    void *args[] = {
        &d_g_renderTarget[selectedDeviceNum],
        &black,
        &xDim,
        &yDim,
    };
    status =
    cuLaunchKernel(program->setBufferKernal,
                   globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                   localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args, 0);           // arguments
}

void Flam4CudaRuntime::startFrameForSelectedDeviceNum(uint selectedDeviceNum,
                                                      uint maxWorkGroupSize,
                                                      const SharedVariationSet & variationSet,
                                                      duration<double> & buildTime)
{
    const SharedDeviceKind & deviceKind = cudaContext->deviceKindForSelectedDevice(selectedDeviceNum);
    CudaDeviceKind * cudaDeviceKind     = (CudaDeviceKind *)deviceKind.get();
    CudaProgram *program                = cudaDeviceKind->programForVariationSet(variationSet, buildTime);
    CUstream queue                      = streams[selectedDeviceNum];
    
    setupFlameConstants(g_pFlame[selectedDeviceNum], queue, selectedDeviceNum, variationSet);
    setupPalette(g_pFlame[selectedDeviceNum], queue, selectedDeviceNum, variationSet);
    
    //Render it!
    //    size_t globalWorkSize[2] = { ((xDim + 15)/16)*16, ((yDim + 15)/16)*16 };
    //    size_t localWorkSize[2]  = { 16, 16 };
    uint globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    uint localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    
    float4 black = { 0.f, 0.f, 0.f, 0.f };
    
    //    __global__ void setBufferKernal(__global float4* renderTarget, float4 value, uint xDim, uint yDim)
    
    void *args[] = {
        &d_g_accumBuffer[selectedDeviceNum],
        &black,
        &xDim,
        &yDim,
    };
    status = cuCtxSynchronize();
    status =
    cuLaunchKernel(program->setBufferKernal,
                   globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                   localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args, 0);           // arguments
    
    void *args2[] = {
        &d_g_renderTarget[selectedDeviceNum],
        &black,
        &xDim,
        &yDim,
    };
    status =
    cuLaunchKernel(program->setBufferKernal,
                   globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                   localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args2, 0);           // arguments
}

#pragma mark Render Batch

void Flam4CudaRuntime::renderBatchForSelectedDeviceNum(unsigned selectedDeviceNum,
                                                       SharedElc & elc,
                                                       float epsilon,
                                                       uint maxWorkGroupSize,
                                                       const SharedVariationSet & variationSet,
                                                       uint batchNum)
{
    duration<double> buildTime(0.);
    const SharedDeviceKind & deviceKind = cudaContext->deviceKindForSelectedDevice(selectedDeviceNum);
    CudaDeviceKind * cudaDeviceKind     = (CudaDeviceKind *)deviceKind.get();
    CudaProgram *program                = cudaDeviceKind->programForVariationSet(variationSet, buildTime);
    CUstream queue                      = streams[selectedDeviceNum];
    Flame * flame                       = g_pFlame[selectedDeviceNum];

    uint numColors       = flame->numColors;
    int paletteStepMode  = (int)(elc->paletteMode == modeStep);
    uint perXformPointPoolSize = NUMPOINTS[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    
    //deep copy flame to the device
    setupFlameConstants(flame, queue, selectedDeviceNum, variationSet);
    
    //Render it!
    //    size_t globalWorkSize[2] = { BLOCKSX[selectedDeviceNum]*BLOCKDIM[selectedDeviceNum], BLOCKSY[selectedDeviceNum] };
    //    size_t localWorkSize[2]  = { BLOCKDIM[selectedDeviceNum], 1 };
    uint globalWorkSize[2] = { BLOCKSX[selectedDeviceNum], BLOCKSY[selectedDeviceNum] };
    uint localWorkSize[2]  = { 1, 1 };
    
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= BLOCKDIM[selectedDeviceNum];
        localWorkSize[0]  *= BLOCKDIM[selectedDeviceNum];
    }
    
    //        __global__ void iteratePointsKernal(__constant struct VariationListNode *d_g_varUsages,
    //                                            __constant uint *d_g_varUsageIndexes,
    //                                            __constant float *varpars,
    //                                            __constant float *d_g_switchMatrix,
    //#ifndef FOR_2D
    //                                            __constant struct CameraViewProperties *d_g_Camera,
    //#endif
    //                                            __global float4* renderTarget,
    //                                            __global struct point* points,
    //                                            __global uint* pointIterations,
    //                                            __global uint* perThreadRandSeeds,
    //                                            __global __read_only float4* palette,
    //                                            uint numColors,
    //                                            int  paletteStepMode,
    //                                            float epsilon,
    //                                            uint fuseIterations,
    //                                            int xDim,
    //                                            int yDim,
    //                                            __global uint *startingXform,
    //                                            __global uint *markedCounts,
    //                                            __global uint *pixelCounts,
    //                                            uint xformPointPoolSize,
    //                                            __global uint *permutations,
    //                                            __global float4 *gradients,
    //                                            __global uint *shuffle,
    //                                            __global uint *iterationCount)
    
    void **args = nullptr;
    if (variationSet->is3DCapable) {
        void *args3D[] = {
            &d_g_varUsages[selectedDeviceNum],
            &d_g_varUsageIndexes[selectedDeviceNum],
            &varpars[selectedDeviceNum],
            &d_g_switchMatrix[selectedDeviceNum],
            
            &d_g_Camera[selectedDeviceNum],
            
            &d_g_renderTarget[selectedDeviceNum],
            &d_g_pointList[selectedDeviceNum],
            &d_g_pointIterations[selectedDeviceNum],
            &d_g_randSeeds[selectedDeviceNum],
            &d_g_Palette[selectedDeviceNum],
            &numColors,
            &paletteStepMode,
            &epsilon,
            &_fuseIterations[selectedDeviceNum],
            &xDim,
            &yDim,
            &d_g_startingXform[selectedDeviceNum],
            &d_g_markCounts[selectedDeviceNum],
            &d_g_pixelCounts[selectedDeviceNum],
            &perXformPointPoolSize,
            &permutations[selectedDeviceNum],
            &gradients[selectedDeviceNum],
            &shuffle[selectedDeviceNum],
            &iterationCount[selectedDeviceNum],
        };
        args = args3D;
    }
    else {
        void *args2D[] = {
            &d_g_varUsages[selectedDeviceNum],
            &d_g_varUsageIndexes[selectedDeviceNum],
            &varpars[selectedDeviceNum],
            &d_g_switchMatrix[selectedDeviceNum],
            &d_g_renderTarget[selectedDeviceNum],
            &d_g_pointList[selectedDeviceNum],
            &d_g_pointIterations[selectedDeviceNum],
            &d_g_randSeeds[selectedDeviceNum],
            &d_g_Palette[selectedDeviceNum],
            &numColors,
            &paletteStepMode,
            &epsilon,
            &_fuseIterations[selectedDeviceNum],
            &xDim,
            &yDim,
            &d_g_startingXform[selectedDeviceNum],
            &d_g_markCounts[selectedDeviceNum],
            &d_g_pixelCounts[selectedDeviceNum],
            &perXformPointPoolSize,
            &permutations[selectedDeviceNum],
            &gradients[selectedDeviceNum],
            &shuffle[selectedDeviceNum],
            &iterationCount[selectedDeviceNum],
        };
        args = args2D;
    }
    
    status =
    cuLaunchKernel(program->iteratePointsKernal,
                   globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                   localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args, 0);           // arguments
    status = cuStreamSynchronize(queue);
    //    [self logHistogramStatsFromQueue:queue selectedDeviceNum:selectedDeviceNum prefix:@"Test"];
}

void Flam4CudaRuntime::grabFrameFromSelectedDeviceNum(uint selectedDeviceNum)
{
    status = cuCtxPushCurrent(contexts[selectedDeviceNum]);
    
    CUstream queue = streams[selectedDeviceNum];
    status = cuMemcpyDtoHAsync_v2(mergeStagingBuffer, d_g_renderTarget[selectedDeviceNum], xDim*yDim*sizeof(float4), queue);
    
    CUcontext context;
    status = cuCtxPopCurrent(&context);
}

void Flam4CudaRuntime::synchronizeQueueForSelectedDeviceNum(uint selectedDeviceNum)
{
    CUstream queue = streams[selectedDeviceNum];
    status = cuStreamSynchronize(queue);
}

void Flam4CudaRuntime::mergeFramesToSelectedDeviceNum(uint selectedDeviceNum,
                                                    uint fromSelectedDeviceNum,
                                                    uint maxWorkGroupSize,
                                                    const SharedVariationSet & variationSet,
                                                    SharedRenderState & renderState)
{
    status = cuCtxPushCurrent(contexts[selectedDeviceNum]);
    
    duration<double> buildTime(0.);
    const SharedDeviceKind & deviceKind = cudaContext->deviceKindForSelectedDevice(selectedDeviceNum);
    CudaDeviceKind * cudaDeviceKind     = (CudaDeviceKind *)deviceKind.get();
    CudaProgram *program                = cudaDeviceKind->programForVariationSet(variationSet, buildTime);
    CUstream queue                      = streams[selectedDeviceNum];

    uint fromBatches = 0;
    uint toBatches   = 0;
    
    status = cuMemcpyDtoHAsync_v2(&fromBatches, iterationCount[fromSelectedDeviceNum], sizeof(uint), streams[fromSelectedDeviceNum]);
    status = cuStreamSynchronize(streams[fromSelectedDeviceNum]);
    
    status = cuMemcpyDtoHAsync_v2(&toBatches, iterationCount[selectedDeviceNum], sizeof(uint), queue);
    status = cuStreamSynchronize(queue);
    
    if (fromBatches != renderedBatches[fromSelectedDeviceNum]) {
        snprintf(buf[selectedDeviceNum], sizeof(buf[selectedDeviceNum]), "Frame:%u Device:%u Expected:%u  Actual Count:%u", renderState->renderInfo->frameNumber,
              fromSelectedDeviceNum, renderedBatches[fromSelectedDeviceNum], fromBatches);
    }
    
    if (toBatches != renderedBatches[selectedDeviceNum]) {
        snprintf(buf[selectedDeviceNum], sizeof(buf[selectedDeviceNum]), "Frame:%u Device:%u Expected:%u  Actual Count:%u", renderState->renderInfo->frameNumber,
              selectedDeviceNum, renderedBatches[selectedDeviceNum], toBatches);
        systemLog(buf[selectedDeviceNum]);
    }
    
    toBatches += fromBatches;
    
    status = cuMemcpyHtoDAsync_v2(iterationCount[selectedDeviceNum], &toBatches, sizeof(uint), queue);
    
    status = cuMemcpyHtoDAsync_v2(d_g_accumBuffer[selectedDeviceNum], mergeStagingBuffer, xDim*yDim*sizeof(float4), queue);
    
    //    size_t globalWorkSize[2] = { ((xDim + 15)/16)*16, ((yDim + 15)/16)*16 };
    //    size_t localWorkSize[2]  = { 16, 16 };
    uint globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    uint localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    
    void *args[] = {
        &d_g_renderTarget[selectedDeviceNum],
        &d_g_accumBuffer[selectedDeviceNum],
        &xDim,
        &yDim,
    };
    status =
    cuLaunchKernel(program->MergeKernal,
                   globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                   localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args, 0);           // arguments
    status = cuStreamSynchronize(queue);
    
    CUcontext context;
    status = cuCtxPopCurrent(&context);
}

#pragma mark Finish Frame

void Flam4CudaRuntime::rgbCurveAdjust(const SharedPointVector & curveData,
                                      uint                      selectedDeviceNum,
                                      CUfunction                colorCurveRGB3ChannelsKernal,
                                      uint                    * globalWorkSize,
                                      uint                    * localWorkSize)

{
    NaturalCubicCurve cubicCurve(curveData, false, false);
    size_t cpCount = cubicCurve.cpCount();
    
    CUstream queue = streams[selectedDeviceNum];
    
    CUdeviceptr XsBuffer, AsBuffer, BsBuffer, CsBuffer, DsBuffer;
    status = cuMemAlloc_v2(&XsBuffer, sizeof(float)*cpCount);
    status = cuMemAlloc_v2(&AsBuffer, sizeof(float)*cpCount);
    status = cuMemAlloc_v2(&BsBuffer, sizeof(float)*(cpCount-1));
    status = cuMemAlloc_v2(&CsBuffer, sizeof(float)*cpCount);
    status = cuMemAlloc_v2(&DsBuffer, sizeof(float)*(cpCount-1));
    
    status = cuMemcpyHtoD_v2(XsBuffer, cubicCurve.Xs->data(), sizeof(cl_float)*cpCount);
    status = cuMemcpyHtoD_v2(AsBuffer, cubicCurve.As->data(), sizeof(cl_float)*cpCount);
    status = cuMemcpyHtoD_v2(BsBuffer, cubicCurve.Bs->data(), sizeof(cl_float)*(cpCount-1));
    status = cuMemcpyHtoD_v2(CsBuffer, cubicCurve.Cs->data(), sizeof(cl_float)*cpCount);
    status = cuMemcpyHtoD_v2(DsBuffer, cubicCurve.Ds->data(), sizeof(cl_float)*(cpCount-1));
    status = cuStreamSynchronize(queue);
    
    void *args[] = {
        &d_g_accumBuffer[selectedDeviceNum],
        &XsBuffer,
        &AsBuffer,
        &BsBuffer,
        &CsBuffer,
        &DsBuffer,
        &xDim,
        &yDim,
        &cpCount,
    };
    status =
    cuLaunchKernel(colorCurveRGB3ChannelsKernal,
                   globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                   localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args, 0);           // arguments
    
    status = cuStreamSynchronize(queue);
    
    status = cuMemFree_v2(XsBuffer);
    status = cuMemFree_v2(AsBuffer);
    status = cuMemFree_v2(BsBuffer);
    status = cuMemFree_v2(CsBuffer);
    status = cuMemFree_v2(DsBuffer);
}

void Flam4CudaRuntime::curveAdjust(const SharedPointVector & curveData,
                                   uint                      channel,
                                   uint                      selectedDeviceNum,
                                   CUfunction                colorCurveRGBChannelKernal,
                                   uint                    * globalWorkSize,
                                   uint                    * localWorkSize)
{
    NaturalCubicCurve cubicCurve(curveData, false, false);
    size_t cpCount = cubicCurve.cpCount();
    CUstream queue = streams[selectedDeviceNum];
    
    CUdeviceptr XsBuffer, AsBuffer, BsBuffer, CsBuffer, DsBuffer;
    status = cuMemAlloc_v2(&XsBuffer, sizeof(float)*cpCount);
    status = cuMemAlloc_v2(&AsBuffer, sizeof(float)*cpCount);
    status = cuMemAlloc_v2(&BsBuffer, sizeof(float)*(cpCount-1));
    status = cuMemAlloc_v2(&CsBuffer, sizeof(float)*cpCount);
    status = cuMemAlloc_v2(&DsBuffer, sizeof(float)*(cpCount-1));
    
    status = cuMemcpyHtoD_v2(XsBuffer, cubicCurve.Xs->data(), sizeof(cl_float)*cpCount);
    status = cuMemcpyHtoD_v2(AsBuffer, cubicCurve.As->data(), sizeof(cl_float)*cpCount);
    status = cuMemcpyHtoD_v2(BsBuffer, cubicCurve.Bs->data(), sizeof(cl_float)*(cpCount-1));
    status = cuMemcpyHtoD_v2(CsBuffer, cubicCurve.Cs->data(), sizeof(cl_float)*cpCount);
    status = cuMemcpyHtoD_v2(DsBuffer, cubicCurve.Ds->data(), sizeof(cl_float)*(cpCount-1));
    status = cuStreamSynchronize(queue);
    
    void *args[] = {
        &d_g_accumBuffer[selectedDeviceNum],
        &XsBuffer,
        &AsBuffer,
        &BsBuffer,
        &CsBuffer,
        &DsBuffer,
        &xDim,
        &yDim,
        &cpCount,
        &channel,
    };
    status =
    cuLaunchKernel(colorCurveRGBChannelKernal,
                   globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                   localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args, 0);           // arguments
    
    status = cuStreamSynchronize(queue);
    
    status = cuMemFree_v2(XsBuffer);
    status = cuMemFree_v2(AsBuffer);
    status = cuMemFree_v2(BsBuffer);
    status = cuMemFree_v2(CsBuffer);
    status = cuMemFree_v2(DsBuffer);
}

size_t Flam4CudaRuntime::reduceMarkCountsForSelectedDeviceNum(uint selectedDeviceNum,
                                                              uint maxWorkGroupSize,
                                                              CUfunction reductionKernal,
                                                              CUstream queue)
{
    if (d_g_reduction[selectedDeviceNum] == 0)
        return 0;
    
    // reduce fused fractions down to single value
    uint countsLength = BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    uint globalWorkSizeR[1] = { BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum] };
    uint localWorkSizeR[1]  = { BLOCKDIM[selectedDeviceNum] };
    
    void *args[] = {
        &d_g_markCounts[selectedDeviceNum],
        &countsLength,
        &d_g_reduction[selectedDeviceNum],
    };
    status =
    cuLaunchKernel(reductionKernal,
                   globalWorkSizeR[0]/localWorkSizeR[0], 1, 1, // blocks per grid
                   localWorkSizeR[0], 1, 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args, 0);           // arguments
    
    status = cuStreamSynchronize(queue);
    
    unsigned *reducedResults  = nullptr;
    uint blocksTarget = ((xDim * yDim) + (BLOCKDIM[selectedDeviceNum] - 1))/BLOCKDIM[selectedDeviceNum];
	ALIGNED_MALLOC(unsigned, reducedResults, 4096, blocksTarget * sizeof(unsigned));
	memset(reducedResults, '\0', blocksTarget * sizeof(unsigned));

    status = cuMemcpyDtoHAsync_v2(reducedResults, d_g_reduction[selectedDeviceNum], blocksTarget*sizeof(unsigned), queue);
    status = cuStreamSynchronize(queue);
    
    size_t totalIterationCount = 0;
    for (uint i = 0; i < blocksTarget; i++)
        totalIterationCount += reducedResults[i];
        
        free(reducedResults);
        
        return totalIterationCount;
}

size_t Flam4CudaRuntime::reduceMarkCountsForAllDevices(uint maxWorkGroupSize, CUfunction reductionKernal)
{
    size_t count = 0;
    for (uint i = 0; i < numDevices; i++) {
        // reduce fused fractions down to single value
        CUstream queue = streams[i];
        count += reduceMarkCountsForSelectedDeviceNum(i, maxWorkGroupSize, reductionKernal, queue);
    }
    return count;
}

size_t Flam4CudaRuntime::reducePixelCountsForAllDevices(uint maxWorkGroupSize, CUfunction reductionKernal)
{
    size_t count = 0;
    for (uint i = 0; i < numDevices; i++) {
        // reduce fused fractions down to single value
        CUstream queue = streams[i];
        count += reducePixelCountsForSelectedDeviceNum(i, maxWorkGroupSize, reductionKernal, queue);
    }
    return count;
}

size_t Flam4CudaRuntime::reducePixelCountsForSelectedDeviceNum(uint selectedDeviceNum,
                                                               uint maxWorkGroupSize,
                                                               CUfunction reductionKernal,
                                                               CUstream queue)
{
    if (d_g_reduction[selectedDeviceNum] == 0)
        return 0;
    
    // reduce per pixel mark counts down to single value
    uint globalWorkSizeR2[1] = { (((xDim * yDim) + (BLOCKDIM[selectedDeviceNum] - 1))/BLOCKDIM[selectedDeviceNum])*BLOCKDIM[selectedDeviceNum] };
    uint localWorkSizeR2[1]  = { BLOCKDIM[selectedDeviceNum] };
    
    uint countsLength = xDim*yDim;
    
    // reduce per pixel mark counts down to single value
    void *args2[] = {
        &d_g_pixelCounts[selectedDeviceNum],
        &countsLength,
        &d_g_reduction[selectedDeviceNum],
    };
    status =
    cuLaunchKernel(reductionKernal,
                   globalWorkSizeR2[0]/localWorkSizeR2[0], 1, 1, // blocks per grid
                   localWorkSizeR2[0], 1, 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args2, 0);           // arguments
    
    status = cuStreamSynchronize(queue);
    
    unsigned *reducedPixelResults = nullptr;
    size_t _BLOCKSX = ((xDim * yDim) + (BLOCKDIM[selectedDeviceNum] - 1))/BLOCKDIM[selectedDeviceNum];
	ALIGNED_MALLOC(unsigned, reducedPixelResults, 4096, _BLOCKSX * sizeof(unsigned));
	memset(reducedPixelResults, '\0', _BLOCKSX * sizeof(unsigned));

    status = cuMemcpyDtoHAsync_v2(reducedPixelResults, d_g_reduction[selectedDeviceNum], _BLOCKSX*sizeof(unsigned), queue);
    status = cuStreamSynchronize(queue);
    
    size_t totalPixelCount = 0;
    for (uint i = 0; i < _BLOCKSX; i++) {
        totalPixelCount += reducedPixelResults[i];
    }
    
    free(reducedPixelResults);
    return totalPixelCount;
}

Point Flam4CudaRuntime::finishFrame(SharedRenderState &renderState,
                                    void *image,
                                    float *backgroundImage,
                                    size_t renderedBatches,
                                    const SharedVariationSet & variationSet,
                                    SharedElc &elc,
                                    SharedFE & flameExpanded,
                                    duration<double> & deDuration)
{
    deDuration = duration<double>(0.);
    
    duration<double> buildTime(0.);
    uint selectedDeviceNum              = renderState->selectedDeviceNum;
    bool useAlpha                       = renderState->renderInfo->useAlpha;
    uint bitDepth                       = renderState->renderInfo->bitDepth;
    uint maxWorkGroupSize               = renderState->maxWorkGroupSize;
    std::string pixelFormat             = renderState->renderInfo->pixelFormat;
    PriorUse usePriorFuseFraction       = renderState->usePriorFuseFraction;
    float priorFraction                 = renderState->priorFraction;
    Flame * flame                       = g_pFlame[renderState->selectedDeviceNum];
    const SharedDeviceKind & deviceKind = cudaContext->deviceKindForSelectedDevice(selectedDeviceNum);
    CudaDeviceKind * cudaDeviceKind     = (CudaDeviceKind *)deviceKind.get();
    CudaProgram *program                = cudaDeviceKind->programForVariationSet(variationSet, buildTime);
    CUstream queue                      = streams[selectedDeviceNum];

    //    size_t globalWorkSize[2] = { ((xDim + 15)/16)*16, ((yDim + 15)/16)*16 };
    //    size_t localWorkSize[2]  = { 16, 16 };
    uint globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    uint localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    
    if (hostAccumBuffer) { // add current acccum buffer to current render state
        status = cuMemcpyHtoDAsync_v2(d_g_accumBuffer[selectedDeviceNum], hostAccumBuffer, accumBufferSize(), queue);
        
        void *args[] = {
            &d_g_accumBuffer[selectedDeviceNum],
            &d_g_renderTarget[selectedDeviceNum],
            &xDim,
            &yDim,
        };
        status =
        cuLaunchKernel(program->MergeKernal,
                       globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                       localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                       0, queue,             // extra dynamic shared mem size and stream
                       args, 0);           // arguments
        
        // update the host accumulation buffer with the merge results
        status = cuMemcpyDtoHAsync_v2((void *)hostAccumBuffer, d_g_accumBuffer[selectedDeviceNum], accumBufferSize(), queue);
        status = cuStreamSynchronize(queue);
        
        
        // swap buffers in prep for Density estimation
        if (flame->params.estimatorRadius > 0)
            status = cuMemcpyAsync(d_g_renderTarget[selectedDeviceNum], d_g_accumBuffer[selectedDeviceNum],  xDim*yDim*sizeof(float4), queue);
    }
    size_t totalIterationCount = reduceMarkCountsForAllDevices(maxWorkGroupSize, program->reductionKernal);
    
    size_t maxTheoretical       = renderedBatches * NUM_ITERATIONS*BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    size_t maxFusedTheoretical  = maxTheoretical - _fuseIterations[selectedDeviceNum] *BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    float fusedFraction = (float)((double)totalIterationCount/maxFusedTheoretical);
    
    
    size_t totalPixelCount = reducePixelCountsForAllDevices(maxWorkGroupSize, program->reductionKernal);
    
    // for multiple tile images we must use the same fuse fraction for all tiles - we use that from the first tile
    float retainedFraction = (float)((totalIterationCount > 0) ? (double)totalPixelCount/totalIterationCount : (double)totalPixelCount/maxFusedTheoretical);
    float bothFraction     = priorFraction; // usePrior
    
    if (usePriorFuseFraction == noPrior)
        bothFraction = fusedFraction * retainedFraction;
    else if (usePriorFuseFraction == nonAdaptive)
        bothFraction = 1.f;
    
    if (flame->params.estimatorRadius > 0)
    {
        float baseThreshold = 0.9f;
        
        uint maxEstimatorRadius       = cudaContext->maxEstimatorRadiusForSelectedDevices();
        flame->params.estimatorRadius = flame->params.estimatorRadius > maxEstimatorRadius ? maxEstimatorRadius : flame->params.estimatorRadius;
        
        //        __global__ void FlexibleDensityEstimationKernal(__global float* output, __global float* input, unsigned int xDim, unsigned int yDim, float baseThreshold, int radius)
        
        void *args[] = {
            &d_g_accumBuffer[selectedDeviceNum],
            &d_g_renderTarget[selectedDeviceNum],
            &xDim,
            &yDim,
            &baseThreshold,
            &flame->params.estimatorRadius,
        };
        status =
        cuLaunchKernel(program->FlexibleDensityEstimationKernal,
                       globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                       localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                       0, queue,             // dynamic shared mem size and stream
                       args, 0);           // arguments
    }
    else if (!hostAccumBuffer) // accumbuffer has the data in this case - dont overwrite it
    {
        status = cuMemcpyAsync(d_g_accumBuffer[selectedDeviceNum], d_g_renderTarget[selectedDeviceNum], xDim*yDim*sizeof(float4), queue);
    }
    
    if (backgroundImage) {
        status = cuMemcpyHtoDAsync_v2(d_g_renderTarget[selectedDeviceNum], backgroundImage, xDim*yDim*sizeof(float4), queue);
    }
    else {
        float4 background = { flame->params.background.r,flame->params.background.g,flame->params.background.b,flame->params.background.a };
        
        //    __global__ void setBufferKernal(__global float4* renderTarget, float4 value, uint xDim, uint yDim)
        
        void *args[] = {
            &d_g_renderTarget[selectedDeviceNum],
            &background,
            &xDim,
            &yDim,
        };
        status =
        cuLaunchKernel(program->setBufferKernal,
                       globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                       localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                       0, queue,             // extra dynamic shared mem size and stream
                       args, 0);           // arguments
    }
    
    // read the actual iteration count
    uint _actualBatches = 0;
    status = cuMemcpyDtoHAsync_v2(&_actualBatches, iterationCount[selectedDeviceNum], sizeof(uint), queue);
    status = cuStreamSynchronize(queue);
    
    // correct for fan blades having set the numBatches to another value
    flame->params.numBatches = _actualBatches > 0 ? _actualBatches : renderedBatches;
    
    //deep copy flame to the device
    CUdeviceptr flameDptr;
    size_t size;
    status = cuModuleGetGlobal_v2(&flameDptr, &size, program->module(), "d_g_Flame");
    status = cuMemcpyHtoDAsync_v2(flameDptr, &(flame->params), sizeof(FlameParams), queue);
    
    status = cuStreamSynchronize(queue);
    
    if (flameExpanded->rgbCurve) {
        rgbCurveAdjust(flameExpanded->rgbCurve,
                       selectedDeviceNum,
                       program->colorCurveRGB3ChannelsKernal,
                       globalWorkSize,
                       localWorkSize);
        cuStreamSynchronize(queue);
    }
    if (flameExpanded->redCurve) {
        curveAdjust(flameExpanded->redCurve,
                    0,
                    selectedDeviceNum,
                    program->colorCurveRGBChannelKernal,
                    globalWorkSize,
                    localWorkSize);
        cuStreamSynchronize(queue);
    }
    if (flameExpanded->greenCurve) {
        curveAdjust(flameExpanded->greenCurve,
                    1,
                    selectedDeviceNum,
                    program->colorCurveRGBChannelKernal,
                    globalWorkSize,
                    localWorkSize);
        cuStreamSynchronize(queue);
    }
    if (flameExpanded->blueCurve) {
        curveAdjust(flameExpanded->blueCurve,
                    2,
                    selectedDeviceNum,
                    program->colorCurveRGBChannelKernal,
                    globalWorkSize,
                    localWorkSize);
        cuStreamSynchronize(queue);
    }
    
    //    __global__ void postProcessStep1Kernal(__global float4* renderTarget,
    //                                           __global float4* accumBuffer,
    //                                           uint xDim,
    //                                           uint yDim,
    //                                           int blocksY,
    //                                           float fuseCompensation)
    
    void *args3[] = {
        &d_g_renderTarget[selectedDeviceNum],
        &d_g_accumBuffer[selectedDeviceNum],
        &xDim,
        &yDim,
        &BLOCKSY[selectedDeviceNum],
        &bothFraction,
    };
    status =
    cuLaunchKernel(program->postProcessStep1Kernal,
                   globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                   localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args3, 0);           // arguments
    status = cuStreamSynchronize(queue);
    
    float4 k2Adjust = { 1.f, 1.f, 1.f, 1.f };
    NaturalCubicCurve::k2AdjustForData(flameExpanded->rgbCurve,
                                       flameExpanded->redCurve,
                                       flameExpanded->greenCurve,
                                       flameExpanded->blueCurve,
                                       (float *)&k2Adjust);
    
    //    __global__ void postProcessStep2Kernal(__global float4* renderTarget,
    //                                           __global float4* accumBuffer,
    //                                           uint xDim,
    //                                           uint yDim,
    //                                           int blocksY,
    //                                           float fuseCompensation,
    //                                           float4 adjust)
    void *args4[] = {
        &d_g_renderTarget[selectedDeviceNum],
        &d_g_accumBuffer[selectedDeviceNum],
        &xDim,
        &yDim,
        &BLOCKSY[selectedDeviceNum],
        &bothFraction,
        &k2Adjust,
    };
    status =
    cuLaunchKernel(program->postProcessStep2Kernal,
                   globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                   localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                   0, queue,             // extra dynamic shared mem size and stream
                   args4, 0);           // arguments
    
    // do optional filtering
    if (flame->params.oversample > 1 || elc->filter > 0.f)
    {
        resample(program->readChannelKernal,
                 program->writeChannelKernal,
                 program->writeChannelStripedKernal,
                 program->convolveRowKernal,
                 program->convolveColKernal,
                 selectedDeviceNum,
                 maxWorkGroupSize,
                 elc);
    }
    
    if (image != nullptr)
    {
        void *args[] = {
            &d_g_resultStaging[selectedDeviceNum],
            &d_g_renderTarget[selectedDeviceNum],
            &resampledXdim,
            &resampledYdim,
            &useAlpha,
        };
        if (bitDepth == 8 && pixelFormat != "BGRA") {
            //            __global__ void RGBA128FtoRGBA32UKernal(__global uchar4* output, __global float4* input, uint xDim, uint yDim, int useAlpha)
            
            status =
            cuLaunchKernel(program->RGBA128FtoRGBA32UKernal,
                           globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                           localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                           0, queue,             // extra dynamic shared mem size and stream
                           args, 0);           // arguments
            
            status = cuMemcpyDtoHAsync_v2(image, d_g_resultStaging[selectedDeviceNum], resampledXdim*resampledYdim*sizeof(cl_uchar4), queue);
        }
        else if (bitDepth == 8 && pixelFormat == "BGRA") {
            void *args1[] = {
                &d_g_resultStaging[selectedDeviceNum],
                &d_g_renderTarget[selectedDeviceNum],
                &resampledXdim,
                &resampledYdim,
            };
            status =
            cuLaunchKernel(program->RGBA128FtoBGRA32UKernal,
                           globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                           localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                           0, queue,             // extra dynamic shared mem size and stream
                           args1, 0);           // arguments
            
            status = cuMemcpyDtoHAsync_v2(image, d_g_resultStaging[selectedDeviceNum], resampledXdim*resampledYdim*sizeof(cl_uchar4), queue);
        }
        else if (bitDepth == 16) {
            status =
            cuLaunchKernel(program->RGBA128FtoRGBA64UKernal,
                           globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                           localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                           0, queue,             // extra dynamic shared mem size and stream
                           args, 0);           // arguments
            
            status = cuMemcpyDtoHAsync_v2(image, d_g_resultStaging[selectedDeviceNum], resampledXdim*resampledYdim*sizeof(cl_ushort4), queue);
        }
        else {
            status =
            cuLaunchKernel(program->RGBA128FtoRGBA128FKernal,
                           globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                           localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                           0, queue,             // extra dynamic shared mem size and stream
                           args, 0);           // arguments
            
            status = cuMemcpyDtoHAsync_v2(image, d_g_resultStaging[selectedDeviceNum], resampledXdim*resampledYdim*sizeof(float4), queue);
        }
        status = cuStreamSynchronize(queue);
    }
    return Point(fusedFraction, retainedFraction);
}

void Flam4CudaRuntime::resample(CUfunction readChannelKernal,
                                CUfunction writeChannelKernal,
                                CUfunction writeChannelStripedKernal,
                                CUfunction convolveRowKernal,
                                CUfunction convolveColKernal,
                                uint selectedDeviceNum,
                                uint maxWorkGroupSize,
                                SharedElc & elc)
{
    uint supersample = g_pFlame[selectedDeviceNum]->params.oversample;
    
    const uint KERNEL_RADIUS_ALIGNED = 16;
    const uint ROW_TILE_W            = 128;
    const uint COLUMN_TILE_W         = 16;
    const uint COLUMN_TILE_H         = 48;
    const uint BLOCKDIM_ROW_X        = KERNEL_RADIUS_ALIGNED + ROW_TILE_W + KERNEL_RADIUS_ALIGNED;
    
    uint globalWorkSizeRow[2] = { (xDim + ROW_TILE_W - 1)/ROW_TILE_W, (yDim + 15)/16*16 };
    uint localWorkSizeRow[2]  = { 1, 1 };
    
    uint globalWorkSizeCol[2] = { (xDim + COLUMN_TILE_W - 1)/COLUMN_TILE_W, (yDim + COLUMN_TILE_H - 1)/COLUMN_TILE_H };
    uint localWorkSizeCol[2]  = { 1, 1 };
    
    const uint COLUMN_TILE_BLOCK_YDIM = 8;
    
    if (maxWorkGroupSize > 1) {
        globalWorkSizeRow[0] *= BLOCKDIM_ROW_X;
        globalWorkSizeRow[1] *= 1;
        localWorkSizeRow[0]  *= BLOCKDIM_ROW_X;
        localWorkSizeRow[1]  *= 1;
        
        globalWorkSizeCol[0] *= COLUMN_TILE_W;
        globalWorkSizeCol[1] *= COLUMN_TILE_BLOCK_YDIM;
        localWorkSizeCol[0]  *= COLUMN_TILE_W;
        localWorkSizeCol[1]  *= COLUMN_TILE_BLOCK_YDIM;
    }
    
    // calculate buffer padding needed
    uint tempWidth1  = (xDim + ROW_TILE_W - 1)/ROW_TILE_W*ROW_TILE_W;
    uint tempHeigth1 = (yDim + 15)/16*16;
    uint size1       = tempWidth1 * tempHeigth1;
    uint tempWidth2  = (xDim + COLUMN_TILE_W - 1)/COLUMN_TILE_W*COLUMN_TILE_W;
    uint tempHeigth2 = (yDim + COLUMN_TILE_H - 1)/COLUMN_TILE_H*COLUMN_TILE_H;
    uint size2       = tempWidth2 * tempHeigth2;
    
    uint bufferSize = size1 > size2 ? size1 : size2;
    
    uint globalWorkSize[2] = { (xDim + 15)/16, (yDim + 15)/16 };
    uint localWorkSize[2]  = { 1, 1 };
    if (maxWorkGroupSize > 1) {
        globalWorkSize[0] *= 16;
        globalWorkSize[1] *= 16;
        localWorkSize[0]  *= 16;
        localWorkSize[1]  *= maxWorkGroupSize == 128 ? 8 : 16;
    }
    
    uint filterWidth = 0;
    float *filter    = SpatialFilter::createFilter(*elc, &filterWidth, 2*KERNEL_RADIUS_ALIGNED + 1);
    
    uint filterRadius = filterWidth/2;
    if (filterRadius > KERNEL_RADIUS_ALIGNED) {
        if (filter) free(filter);
        return;
    }
    if (!filter && supersample == 1)
        return;
    
    CUstream queue = streams[selectedDeviceNum];
    
    CUdeviceptr tempBuffer, temp2Buffer, filterBuffer;
    status = cuMemAlloc_v2(&tempBuffer,   sizeof(float)*bufferSize);
    status = cuMemAlloc_v2(&temp2Buffer,  sizeof(float)*bufferSize);
    
    if (filter) {
        status = cuMemAlloc_v2(&filterBuffer, sizeof(float)*filterWidth);
        status = cuMemcpyHtoDAsync_v2(filterBuffer, filter, sizeof(float)*filterWidth, queue);
    }
    
    for (uint channel = 0; channel < 4; channel++) {
        if (filter || supersample > 1) {
            void *args[] = {
                &tempBuffer,
                &d_g_renderTarget[selectedDeviceNum],
                &xDim,
                &yDim,
                &channel,
            };
            status =
            cuLaunchKernel(readChannelKernal,
                           globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                           localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                           0, queue,             // extra dynamic shared mem size and stream
                           args, 0);           // arguments
        }
        if (filter) {
            
            void *args[] = {
                &filterBuffer,
                &temp2Buffer,
                &tempBuffer,
                &xDim,
                &yDim,
                &filterRadius,
            };
            status =
            cuLaunchKernel(convolveRowKernal,
                           globalWorkSizeRow[0]/localWorkSizeRow[0], globalWorkSizeRow[1]/localWorkSizeRow[1], 1, // blocks per grid
                           localWorkSizeRow[0], localWorkSizeRow[1], 1,                                     // threads per block
                           0, queue,             // extra dynamic shared mem size and stream
                           args, 0);           // arguments
            
            uint smemstride = COLUMN_TILE_W * COLUMN_TILE_BLOCK_YDIM;
            uint gmemstride = xDim * COLUMN_TILE_BLOCK_YDIM;
            
            status = cuStreamSynchronize(queue);
            
            void *args2[] = {
                &filterBuffer,
                &tempBuffer,
                &temp2Buffer,
                &xDim,
                &yDim,
                &smemstride,
                &gmemstride,
                &filterRadius,
            };
            status =
            cuLaunchKernel(convolveColKernal,
                           globalWorkSizeCol[0]/localWorkSizeCol[0], globalWorkSizeCol[1]/localWorkSizeCol[1], 1, // blocks per grid
                           localWorkSizeCol[0], localWorkSizeCol[1], 1,                                     // threads per block
                           0, queue,             // extra dynamic shared mem size and stream
                           args2, 0);           // arguments
            status = cuStreamSynchronize(queue);
        }
        
        if (supersample > 1) {
            void *args[] = {
                &d_g_renderTarget[selectedDeviceNum],
                &tempBuffer,
                &xDim,
                &yDim,
                &channel,
                &supersample,
            };
            status =
            cuLaunchKernel(writeChannelStripedKernal,
                           globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                           localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                           0, queue,             // extra dynamic shared mem size and stream
                           args, 0);           // arguments
        }
        else if (filter){
            void *args[] = {
                &d_g_renderTarget[selectedDeviceNum],
                &tempBuffer,
                &xDim,
                &yDim,
                &channel,
            };
            status =
            cuLaunchKernel(writeChannelKernal,
                           globalWorkSize[0]/localWorkSize[0], globalWorkSize[1]/localWorkSize[1], 1, // blocks per grid
                           localWorkSize[0], localWorkSize[1], 1,                                     // threads per block
                           0, queue,             // extra dynamic shared mem size and stream
                           args, 0);           // arguments
        }
    }
    status = cuStreamSynchronize(queue);
    
    status = cuMemFree_v2(tempBuffer);
    status = cuMemFree_v2(temp2Buffer);
    if (filter)
        status = cuMemFree_v2(filterBuffer);
}

void Flam4CudaRuntime::logPostProcessConstants(uint selectedDeviceNum)
{
    float k1   = (g_pFlame[selectedDeviceNum]->params.brightness*268.0f)/255.0f;
    float area = fabs(g_pFlame[selectedDeviceNum]->params.size[0]*g_pFlame[selectedDeviceNum]->params.size[1]);
    float k2   = ((float)(xDim*yDim))/(area*((float)(NUM_ITERATIONS))*g_pFlame[selectedDeviceNum]->params.numBatches*32.f*1024.0f*((float)BLOCKSY[selectedDeviceNum]/32.f));
    snprintf(buf[selectedDeviceNum], bufsize, "k1=%f k2=%f batches=%f", k1, k2, g_pFlame[selectedDeviceNum]->params.numBatches);
    systemLog(buf[selectedDeviceNum]);
}

size_t Flam4CudaRuntime::sumMarkCountsWithQueue(CUstream queue, uint selectedDeviceNum)
{
    // counting double check
    uint countsLength          = BLOCKDIM[selectedDeviceNum]*BLOCKSX[selectedDeviceNum]*BLOCKSY[selectedDeviceNum];
    size_t markCountsSize  = (size_t)countsLength * sizeof(uint);
    size_t totalMarkCounts = 0;
    uint *markCounts     = nullptr;
	ALIGNED_MALLOC(uint, markCounts, 4096, markCountsSize);
	memset(markCounts, '\0', markCountsSize);

    status = cuMemcpyDtoHAsync_v2(markCounts, d_g_markCounts[selectedDeviceNum], markCountsSize, queue);
    status = cuStreamSynchronize(queue);
    for (uint i = 0; i < countsLength; i++)
        totalMarkCounts += markCounts[i];
    return totalMarkCounts;
}

size_t Flam4CudaRuntime::sumHistogramCountsWithQueue(CUstream queue, uint selectedDeviceNum)
{
    // counting double check
    uint countsLength           = xDim * yDim;
    size_t pixelCountsSize  = (size_t)countsLength * sizeof(uint);
    size_t totalPixelCounts = 0;
    uint *pixelCounts           = nullptr;
	ALIGNED_MALLOC(uint, pixelCounts, 4096, pixelCountsSize);
	memset(pixelCounts, '\0', pixelCountsSize);

    status = cuMemcpyDtoHAsync_v2(pixelCounts, d_g_pixelCounts[selectedDeviceNum], pixelCountsSize, queue);
    status = cuStreamSynchronize(queue);
    for (uint i = 0; i < countsLength; i++) {
        totalPixelCounts += pixelCounts[i];
    }
    return totalPixelCounts;
}

size_t Flam4CudaRuntime::logMarkCountsWithTotalIterCount(size_t totalIterationCount, CUstream queue, uint selectedDeviceNum)
{
    size_t totalMarkCounts = sumMarkCountsWithQueue(queue, selectedDeviceNum);
    snprintf(buf[selectedDeviceNum], bufsize, "TotalMarkCounts:%zu TotalIterationCount:%zu ", totalMarkCounts, totalIterationCount);
    systemLog(buf[selectedDeviceNum]);
    return totalMarkCounts;
}

size_t Flam4CudaRuntime::logPixelCountsWithTotalIterCount(size_t totalIterationCount, CUstream queue, uint selectedDeviceNum)
{
    size_t totalPixelCounts = sumHistogramCountsWithQueue(queue, selectedDeviceNum);
    snprintf(buf[selectedDeviceNum], bufsize, "TotalPixelCounts:%zu TotalIterationCount:%zu ", totalPixelCounts, totalIterationCount);
    systemLog(buf[selectedDeviceNum]);
    return totalPixelCounts;
}


#pragma mark Quality to Batches Conversion

size_t Flam4CudaRuntime::batchesPerTileForActualAdaptive(double actualQuality, double width, double height, uint tiles, uint selectedDeviceNum)
{
    double requiredIterationsPerTile = width * height /tiles * actualQuality;
    double optBlockY                 = ceil((requiredIterationsPerTile + _fuseIterations[selectedDeviceNum])/
                                            (BLOCKSX[selectedDeviceNum] * BLOCKDIM[selectedDeviceNum] * NUM_ITERATIONS));
    if (optBlockY > 32)  // Cap at 32
        optBlockY = 32;
    if (optBlockY < 32)
        return 1;
    
    size_t iterationsPerBatch = BLOCKSX[selectedDeviceNum] * BLOCKSY[selectedDeviceNum] * BLOCKDIM[selectedDeviceNum] * NUM_ITERATIONS;
    size_t batches            = ceil((requiredIterationsPerTile + _fuseIterations[selectedDeviceNum]) / iterationsPerBatch);
    
    return batches;
}

// check and see if we can allocate a the CUDA device buffers
// return success/failure and the total amount that the test was able to allocate
bool Flam4CudaRuntime::memCheck(uint    deviceNumber,
                                size_t  area,
                                uint    numColors,
                                size_t & amountAlloced,
                                int     bitDepth,
                                int     warpSize,
                                bool    deviceImageSupport,
                                uint    xformCount,
                                uint    supersample)
{
    int BLOCKSX    = BLOCKSX_WARPSIZE_32*32/warpSize;
    int BLOCKSY    = 32;
    int NUMPOINTS  = NUM_POINTS_WARPSIZE_32/32*warpSize;
    int BLOCKDIM   = warpSize*warpsPerBlock;;
    bool succeeded = false;
    
    cudaSetDevice(deviceNumber);
    
    int warpsPerBlock = 2;
    BLOCKDIM = warpSize*warpsPerBlock;
    
    void *pointList           = nullptr;
    void *accumBuffer         = nullptr;
    void *renderTarget        = nullptr;
    void *resultStagingBuffer = nullptr;
    void *perThreadRandSeeds  = nullptr;
    void *perWarpRandSeeds    = nullptr;
    cudaArray *array          = nullptr;
    
    void *pointIterations     = nullptr;
    void *lastPointIndexes    = nullptr;
    void *pixelCounts         = nullptr;
    void *startingXforms      = nullptr;
    void *markCounts          = nullptr;
    
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0,	cudaChannelFormatKindFloat);
    
    // Note: formula for amount of memory is: 1 Mb  +  (36 * area / 1048576) Mb
    // or  max area is: (X - 1)*1048576/36 with X in Mb units
    //
    // so for 35 Mb total, the max area is: 990,322 pixels squared or about 1000x990 pixels
    // so for 45 Mb total, the max area is: 1,273,271 or about 1280x994  pixels
    // so for 90 Mb total, the max area is: 2,546,542 or about 2560x994 pixels
    // 1440x900 => 45.49 Mb, 1366x768 = 37.02 Mb, 1920x1280 = 85.375 Mb
    float resampleArea = area/(supersample * supersample);
    
    amountAlloced = 0;
    
    cudaError_t flag = cudaSuccess;
    
    size_t size = sizeof(float4)*NUMPOINTS*BLOCKSX*BLOCKSY;
    flag = cudaMalloc(&pointList, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(float4)*area;
    flag = cudaMalloc(&accumBuffer, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(float4)*area;
    flag = cudaMalloc(&renderTarget, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    if (bitDepth == 8)
        size = sizeof(uchar4)*resampleArea; // 4 * resampleArea /(1024*1024) Mb
    else if (bitDepth == 16)
        size = sizeof(ushort4)*resampleArea;
    else
        size = sizeof(float4)*resampleArea;
    
    flag = cudaMalloc(&resultStagingBuffer, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(int)*BLOCKSX*BLOCKSY*BLOCKDIM;
    flag = cudaMalloc(&perThreadRandSeeds, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(int)*BLOCKSX*BLOCKSY*warpsPerBlock;
    flag = cudaMalloc(&perWarpRandSeeds, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = 16*numColors;
    flag = cudaMallocArray(&array, &channelDesc, numColors, 1);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    
    amountAlloced += size;
    
    
    
    size = sizeof(int)*xformCount*NUMPOINTS*BLOCKSX*BLOCKSY; // 0.78 Mb
    flag = cudaMalloc(&pointIterations, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(uint)*BLOCKDIM*BLOCKSX*BLOCKSY; // 0.125 Mb
    flag = cudaMalloc(&lastPointIndexes, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(float)*area;  // 4 * area /(1024*1024) Mb
    flag = cudaMalloc(&pixelCounts, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(int)*BLOCKSX*BLOCKSY*BLOCKDIM; // 0.125 Mb
    flag = cudaMalloc(&startingXforms, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    size = sizeof(int)*BLOCKSX*BLOCKSY*BLOCKDIM; // 0.125 Mb
    flag = cudaMalloc(&markCounts, size);
    if (flag == cudaErrorMemoryAllocation)
        goto freeThem;
    else
        amountAlloced += size;
    
    succeeded = true;
    
freeThem:
    if (pointList)				cudaFree(pointList);
    if (pointIterations)        cudaFree(pointIterations);
    if (lastPointIndexes)       cudaFree(lastPointIndexes);
    if (accumBuffer)			cudaFree(accumBuffer);
    if (renderTarget)			cudaFree(renderTarget);
    if (resultStagingBuffer)	cudaFree(resultStagingBuffer);
    if (perThreadRandSeeds)		cudaFree(perThreadRandSeeds);
    if (perWarpRandSeeds)		cudaFree(perWarpRandSeeds);
    if (array)					cudaFreeArray(array);
    if (pixelCounts)			cudaFree(pixelCounts);
    if (startingXforms)			cudaFree(startingXforms);
    if (markCounts)			    cudaFree(markCounts);
    
    return succeeded;
}

#pragma mark CUDA GPU Memory Size Checking

// return the largest tile size for this GPU
bool Flam4CudaRuntime::deviceAllocCheck(int deviceNum, float & area, size_t & amountAlloced, uint xformCount, uint supersample)
{
    // variables used to find the largest area we can support
    int numColors         = 256;
    bool success          = false;
    float reductionFactor = 0.8f;
    while (!success) {
        if (!(success =  Flam4CudaRuntime::memCheck(deviceNum,
                                                    area,
                                                    numColors,
                                                    amountAlloced,
                                                    4,
                                                    32,
                                                    true,
                                                    xformCount,
                                                    supersample))) {
            area = floorf(area * reductionFactor);
            if (area <= 1.0f) // the smallest possible tile size!
                return false;
        }
    }
    return success;
}

// return the largest tile size for this GPU
bool Flam4CudaRuntime::deviceAllocCheck(int deviceNum, float & xOptDim, float & yOptDim, size_t & amountAlloced, uint xformCount, uint supersample)
{
    float    reductionFactor = 0.8f; // amount memory is reduced each pass for GPU memory consumption test
    int numColors            = 256;
    float aspectRatio        = xOptDim / yOptDim;
    bool success             = false;
    while (!success) {
        float xDim = xOptDim;
        if (!(success =  Flam4CudaRuntime::memCheck(deviceNum,
                                                    xDim * yOptDim,
                                                    numColors,
                                                    amountAlloced,
                                                    4,
                                                    32,
                                                    true,
                                                    xformCount,
                                                    supersample))) {
            xOptDim = floorf(xDim * sqrtf(reductionFactor));
            yOptDim = floorf(xDim / aspectRatio * sqrtf(reductionFactor));
            if (xOptDim <= 1.0f || yOptDim <= 1.0f) // the smallest possible tile size for fixed aspect ratio!
                return false;
        }
    }
    return success;
}

// return the largest tile size for all GPUs
bool Flam4CudaRuntime::allDevicesAllocCheck(unsigned numDevices, float & area, size_t & amountAlloced, float niceFactor, uint xformCount, uint supersample)
{
    bool success = false;
    for (int i = 0; i < numDevices; i++) {
        success = deviceAllocCheck(i, area, amountAlloced, xformCount, supersample);
        if (!success)
            return false;
    }
    area = floorf(niceFactor * area);
    return success;
}

// return the largest tile size for all GPUs
bool Flam4CudaRuntime::allDevicesAllocCheck(unsigned numDevices, float & xOptDim, float & yOptDim, size_t & amountAlloced, float niceFactor, uint xformCount, uint supersample)
{
    bool success = false;
    for (int i = 0; i < numDevices; i++) {
        success = deviceAllocCheck(i, xOptDim, yOptDim, amountAlloced, xformCount, supersample);
        if (!success)
            return false;
    }
    xOptDim = floorf(niceFactor * xOptDim);
    yOptDim = floorf(niceFactor * yOptDim);
    return success;
}

// report the current largest tile size (can change every call)
std::string Flam4CudaRuntime::getAllocSummary(bool success, float xOptDim, float yOptDim, size_t amountAlloced)
{
    static char buf[bufsize];
    if (success) {
        if (amountAlloced < 45*1024*1024)
            snprintf(buf, sizeof(buf), "GPU available memory is low: %zu less than the recommended %u MB needed.\nPlease log out and log back in to restart the GPU.\n", amountAlloced, 45);
        else
            snprintf(buf, sizeof(buf), "Current largest tile size:[%0.fX%0.f] Device Memory Available: %'zd bytes\n",
                     xOptDim, yOptDim, amountAlloced);
    }
    else {
        snprintf(buf, sizeof(buf), "Unable to allocate enough memory for any tile size.\n");
    }
    return buf;
}

// report the current largest tile size (can change every call)
std::string Flam4CudaRuntime::getAllocSummary(bool success, float area, size_t amountAlloced)
{
    float x = sqrtf(1.333333333f*area);
    float y = x/1.3333333f;
    return getAllocSummary(success, x, y, amountAlloced);
}


// report the current largest tile size (can change every call)
std::string Flam4CudaRuntime::deviceAllocSummary(unsigned numDevices,
                                                 float & xOptDim,
                                                 float & yOptDim,
                                                 size_t & amountAlloced,
                                                 float niceFactor,
                                                 uint xformCount,
                                                 uint supersample)
{
    bool success = Flam4CudaRuntime::allDevicesAllocCheck(numDevices, xOptDim, yOptDim, amountAlloced, niceFactor, xformCount, supersample);
    return Flam4CudaRuntime::getAllocSummary(success, xOptDim, yOptDim, amountAlloced);
}

bool Flam4CudaRuntime::checkGPUMemAvailable(size_t & memoryAvailable,
                                            float & area,
                                            float niceFactor,
                                            uint xformCount,
                                            uint supersample)
{
    int deviceCount = 0;
    cudaError_t status = cudaGetDeviceCount(&deviceCount);
    
    struct cudaDeviceProp deviceProperties;
    size_t maxAmount = UINT_MAX;
    float    maxArea   = area;
    bool     success   = true;
    for (int i = 0; i < deviceCount; i++) {
        status = cudaGetDeviceProperties(&deviceProperties, i);
        float mbTotalGlobal = deviceProperties.totalGlobalMem;
        maxAmount = mbTotalGlobal;
        // bool canMapHostMemory = deviceProperties.canMapHostMemory;
        
        size_t totalGpuMemory = 0;
        size_t freeGpuMemory  = 0;
        
        cudaSetDevice(i);
        cudaMemGetInfo(&freeGpuMemory, &totalGpuMemory);
        
        float areaGlobal = floorf(mbTotalGlobal/4.f); // a reasonable starting point just grab everything assuming RGBA 4 bytes per pixel
        areaGlobal       = maxArea < areaGlobal ? maxArea : areaGlobal; // start at requested area if less than the areaGlobal
        
        size_t amountAlloced;
        success = Flam4CudaRuntime::allDevicesAllocCheck(deviceCount, areaGlobal, amountAlloced, niceFactor, xformCount, supersample);
//        [(AppController *)[NSApp delegate] setFreeGPUMemory:freeGpuMemory/1048576.f];
        maxArea   = (areaGlobal <= maxArea)      ? areaGlobal : maxArea;
        maxAmount = (amountAlloced <= maxAmount) ? amountAlloced : maxAmount;
        
        //		*area = floorf(niceFactor * *area);
        
    }
    memoryAvailable = maxAmount;
    area            = maxArea;
    return success;
}

size_t Flam4CudaRuntime::maxXformCount(FlamesVector & blades)
{
    int numXforms = 0;
    for (SharedFlameExpanded &fe : blades)
        if (fe->flame->params.numTrans + fe->flame->params.numFinal > numXforms)
            numXforms = fe->flame->params.numTrans + fe->flame->params.numFinal;
    return numXforms;
}

// final xforms dont have their own point pool
size_t Flam4CudaRuntime::maxPointPoolCount(FlamesVector & blades)
{
    int numXforms = 0;
    for (SharedFlameExpanded &fe : blades)
        if (fe->flame->params.numTrans > numXforms)
            numXforms = fe->flame->params.numTrans;
    return numXforms;
}

#pragma mark OCL Wrappers

void Flam4CudaRuntime::startCuda(SharedRenderState & renderState, uint selectedDeviceNum)
{
    Flame *flame  = g_pFlame[selectedDeviceNum];
    xDim          = renderState->renderInfo->width;
    yDim          = renderState->renderInfo->height;
    resampledXdim = xDim / flame->params.oversample;
    resampledYdim = yDim / flame->params.oversample;
    
    SharedCudaContext & _cudaContext = (SharedCudaContext &)renderState->deviceContext;
    
    createCUDAbuffers(_cudaContext.get(),
                      selectedDeviceNum,
                      renderState->renderInfo->bitDepth,
                      renderState->deviceContext->warpSizeForSelectedDevice(selectedDeviceNum),
                      renderState->renderInfo->quality,
                      renderState->renderInfo->saveRenderState,
                      maxXformCount(*renderState->renderInfo->blades),
                      maxPointPoolCount(*renderState->renderInfo->blades),
                      flame->params.numTrans);
}

void Flam4CudaRuntime::stopCudaForSelectedDeviceNum(uint selectedDeviceNum)
{
    if (g_pFlame[selectedDeviceNum])
        delete g_pFlame[selectedDeviceNum];
    g_pFlame[selectedDeviceNum] = NULL;
    
    CUstream queue = streams[selectedDeviceNum];
    deleteCUDAbuffers(queue, selectedDeviceNum);
}

void Flam4CudaRuntime::fuseIterations(uint fuseIterations, uint selectedDeviceNum)
{
    _fuseIterations[selectedDeviceNum] = fuseIterations;
}

#ifdef __ORIGINAL__
static void logArray(uint *array, uint numPoints)
{
    uint warpSize = numPoints/warpsPerBlock;
    
    NSLog(@"WarpSize: %u", warpSize);
    NSMutableString *s = [NSMutableString stringWithCapacity:4*numPoints + 10];
    [s appendString:@"FirstWarp:["];
    for (uint i = 0; i < warpSize; i ++)
        [s appendFormat:@"%u, ", array[i]];
    [s appendString:@"]\n"];
    [s appendString:@"  2ndWarp:["];
    for (uint i = warpSize; i < numPoints; i ++)
        [s appendFormat:@"%u, ", array[i]];
    [s appendString:@"]\n"];
    
    NSLog(@"%@", s);
}
#endif
