//
//  DeviceUsage.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#include "DeviceUsage.hpp"
#include <math.h>
#include "DeviceKind.hpp"

DeviceUsage::DeviceUsage(void *_queue, const std::string & _name, const std::string & _summary, bool _used)
:
    deviceID(0),
    platformID(nullptr),
    cudaDevice(0),
    queue(_queue),
    name(_name),
    summary(_summary),
    used(_used),
    stagedUsed(_used),
    maxMemory(0),
    availableMemory(0),
    empty(true),
    isGPU(false),
    warpSize(1),
    deviceKind(),
    available(true),
    licensed(false),
    tempLicense(false),
    quarantined(false)
{}

DeviceUsage::DeviceUsage(void *_deviceID, void * _platformID, void *_queue, const std::string & _name, const std::string & _summary, bool _used)
: DeviceUsage(_queue, _name, _summary, _used)
{
    deviceID   = _deviceID;
    platformID = _platformID;
}

DeviceUsage::DeviceUsage(int _cudaDevice, void *_queue, const std::string & _name, const std::string & _summary, bool _used)
: DeviceUsage(_queue, _name, _summary, _used)
{
    cudaDevice = _cudaDevice;
}

std::string DeviceUsage::maxMemString()
{
    static char buf[20];
    float mb   = roundf((float)(maxMemory/1048576));
    float gb   = mb/1024.f;
    bool useGB = mb >= 1024.f;
    snprintf(buf, sizeof(buf), "%.0f%s", useGB ? gb : mb, useGB ? "Gb" : "Mb");
    return buf;
}

std::string DeviceUsage::availableMemString()
{
    static char buf[20];
    float mb   = roundf((float)(availableMemory/1048576));
    float gb   = mb/1024.f;
    bool useGB = mb >= 1024.f;
    snprintf(buf, sizeof(buf), "%.0f%s", useGB ? gb : mb, useGB ? "Gb" : "Mb");
    return buf;
}

std::string DeviceUsage::description()
{
    static char buf[100];
    
    snprintf(buf, sizeof(buf), "%s MaxMemory:%s StagedUsed:%s Used:%s Available:%s Licensed:%s TempLicense:%s Quarantined:%s Empty:%s",
            summary.c_str(),
            maxMemString().c_str(),
            stagedUsed  ? "YES" : "NO",
            used        ? "YES" : "NO",
            available   ? "YES" : "NO",
            licensed    ? "YES" : "NO",
            tempLicense ? "YES" : "NO",
            quarantined ? "YES" : "NO",
            empty       ? "YES" : "NO");
    return buf;
}

bool DeviceUsage::getStagedUsed()
{
    return stagedUsed;
}

void DeviceUsage::setStagedUsed(bool _stagedUsed)
{
    stagedUsed = _stagedUsed;
    
    DeviceContext * devContext = deviceKind->deviceContext.lock().get();
    
    // enforce single device limit, if applicable
    if (stagedUsed && devContext->singleDeviceAllowed) {
        for (SharedDeviceUsage & deviceUsage : devContext->deviceUsages) {
            if (deviceUsage.get() != this) {
                deviceUsage->setStagedUsed(false);
            }
        }
    }
}

bool DeviceUsage::notEmpty()
{
    return ! empty;
}

std::string DeviceUsage::notEmptyString()
{
    return notEmpty() ? "Yes" : "No";
}

void DeviceUsage::setNotEmpty(bool notEmpty)
{
    
}

uint DeviceUsage::fuseIterations()
{
    return isGPU ? DeviceContext::getGpuFuseIterations() : DeviceContext::getCpuFuseIterations();
}

void DeviceUsage::enforceLicense(bool canUseGPU)
{
    licensed = deviceKind->isCPU() || canUseGPU;
    tempLicense = false;
}

bool DeviceUsage::predicateForAvailable(const SharedDeviceUsage &u)
{
    return u->available == true && u->quarantined == false && u->empty == false;
}

bool DeviceUsage::predicateForAvailableIgnoreQuarantine(const SharedDeviceUsage &u)
{
    return u->available == true && u->empty == false;
}

bool DeviceUsage::predicateForLicensedAvailable(const SharedDeviceUsage &u)
{
    return u->licensed == true && u->available == true && u->quarantined == false && u->empty == false;
}

bool DeviceUsage::predicateForLicensedAvailableIgnoreQuarantine(const SharedDeviceUsage &u)
{
    return u->licensed == true && u->available == true && u->empty == false;
}

bool DeviceUsage::predicateForTempLicenseAvailable(const SharedDeviceUsage &u)
{
    return u->tempLicense == true && u->available == true && u->quarantined == false && u->empty == false;
}

bool DeviceUsage::predicateForTempLicenseAvailableIgnoreQuarantine(const SharedDeviceUsage &u)
{
    return u->tempLicense == true && u->available == true && u->empty == false;
}

bool DeviceUsage::predicateForUsed(const SharedDeviceUsage &u)
{
    return u->used == true;
}

bool DeviceUsage::predicateForQuarantined(const SharedDeviceUsage &u)
{
    return u->quarantined == true;
}

bool DeviceUsage::predicateForLicensedAvailableAndUsed(const SharedDeviceUsage &u)
{
    return predicateForLicensedAvailable(u) && predicateForUsed(u);
}

bool DeviceUsage::predicateForLicensedAvailableAndUsedIgnoreQuarantine(const SharedDeviceUsage &u)
{
    return predicateForLicensedAvailableIgnoreQuarantine(u) && predicateForUsed(u);
}

bool DeviceUsage::predicateForTempLicenseAvailableAndUsed(const SharedDeviceUsage &u)
{
    return predicateForTempLicenseAvailable(u) && predicateForUsed(u);
}

bool DeviceUsage::predicateForTempLicenseAvailableAndUsedIgnoreQuarantine(const SharedDeviceUsage &u)
{
    return predicateForTempLicenseAvailableIgnoreQuarantine(u) && predicateForUsed(u);
}

bool DeviceUsage::compare(const SharedDeviceUsage &a, const SharedDeviceUsage &b) {
    if (a->isGPU == b->isGPU) {
        return a->availableMemory < b->availableMemory;
    }
    return !a->isGPU && b->isGPU;
}
