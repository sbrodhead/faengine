//
//  DeviceUsage.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/14/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef DeviceUsage_hpp
#define DeviceUsage_hpp

#include <string>

#include "Exports.hpp"
#include "DeviceKind.hpp"

class DeviceKind;

using uint = unsigned int;

struct FAENGINE_API DeviceUsage {
    SharedDeviceKind deviceKind;
    void *           deviceID;
    void *           platformID;
    int              cudaDevice;
    void *           queue;
    bool             isGPU;
    std::string      name;
    std::string      summary;
    bool             used;
    bool             stagedUsed;
    bool             empty;     // for low memory availability
    bool             available; // as reported by OpenCL driver
    bool             licensed;
    bool             tempLicense;
    bool             quarantined;
    size_t           maxMemory;
    size_t          availableMemory;
    uint             warpSize;
   
private:
    DeviceUsage(void *_queue, const std::string & _name, const std::string & _summary, bool _used);
    
public:
    DeviceUsage(void *_deviceID, void * _platform, void *_queue, const std::string & _name, const std::string & _summary, bool _used);
    DeviceUsage(int _cudaDevice, void *_queue, const std::string & _name, const std::string & _summary, bool _used);
    
    std::string description();
    
    std::string maxMemString();
    std::string availableMemString();
    
    bool getStagedUsed();
    void setStagedUsed(bool _stagedUsed);
    
    bool notEmpty();
    void setNotEmpty(bool notEmpty);
    std::string notEmptyString();
    
    uint fuseIterations();
    
    void enforceLicense(bool canUseGPU);
    
    static bool predicateForAvailable(const SharedDeviceUsage &u);
    static bool predicateForAvailableIgnoreQuarantine(const SharedDeviceUsage &u);
    static bool predicateForLicensedAvailable(const SharedDeviceUsage &u);
    static bool predicateForLicensedAvailableIgnoreQuarantine(const SharedDeviceUsage &u);
    static bool predicateForTempLicenseAvailable(const SharedDeviceUsage &u);
    static bool predicateForTempLicenseAvailableIgnoreQuarantine(const SharedDeviceUsage &u);
    static bool predicateForUsed(const SharedDeviceUsage &u);
    static bool predicateForQuarantined(const SharedDeviceUsage &u);
    static bool predicateForLicensedAvailableAndUsed(const SharedDeviceUsage &u);
    static bool predicateForLicensedAvailableAndUsedIgnoreQuarantine(const SharedDeviceUsage &u);
    static bool predicateForTempLicenseAvailableAndUsed(const SharedDeviceUsage &u);
    static bool predicateForTempLicenseAvailableAndUsedIgnoreQuarantine(const SharedDeviceUsage &u);
    
    static bool compare(const SharedDeviceUsage &a, const SharedDeviceUsage &b);
};

#endif /* DeviceUsage_hpp */
