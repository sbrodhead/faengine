//
//  VariationChain.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/2/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef VariationChain_hpp
#define VariationChain_hpp

#include <vector>
#include <memory>
#include "Exports.hpp"

class NamedAffineMatrix;
class VariationChain;
class VariationGroup;
class VariationSet;
class VarParInstance;

using SharedVariationSet   = std::shared_ptr<VariationSet>;
using SharedVariationGroup = std::shared_ptr<VariationGroup>;
using SharedVariationChain = std::shared_ptr<VariationChain>;
using SharedVarParInstance = std::shared_ptr<VarParInstance>;
using VariationGroupVector = std::vector<SharedVariationGroup>;


class FAENGINE_API VariationChain : public std::enable_shared_from_this<VariationChain> {
    friend class VariationGroup;
    friend class Flame;

//    VariationChain(bool pre, bool post);
    VariationChain();
    VariationChain(const VariationChain &v);
public:
    
    SharedVariationGroup & operator[](size_t index);
    
    VariationGroupVector::iterator begin();
    VariationGroupVector::iterator end();
    
    
    static SharedVariationChain makeVariationChain();
    static SharedVariationChain makeVariationChain(bool pre, bool post);
    static SharedVariationChain makeCopyOf(const VariationChain &v);
    
    void clear()  { array.clear(); }
    size_t size() { return array.size(); }
    
    SharedVarParInstance *currentVarparWithKey(const std::string &key, size_t groupIndex) const;
    SharedVarParInstance *currentVarparMatching(const SharedVarParInstance &varpar) const;
    
    size_t indexOfGroup(SharedVariationGroup group) const;
    size_t indexOfGroup(SharedVariationGroup group, size_t defaultIndex) const;

    std::string groupNameForVariationGroup(const SharedVariationGroup &group) const;
    std::string groupNameForIndex(size_t index) const;
    
    NamedAffineMatrix currentMatrixWithKey(const std::string &key, size_t groupIndex) const;
    std::vector<SharedVarParInstance> insertMatrix(const NamedAffineMatrix &matrix, const SharedVariationSet &variationSet);
    SharedVariationGroup addNewGroup();
    void addGroup(SharedVariationGroup & group);
    const SharedVariationGroup & normalGroup() const;
    size_t normalGroupIndex() const;
    void checkForNormalGroup() const;
    bool checkForNonBlankChain() const;
    
    VariationGroupVector preGroups() const;
    VariationGroupVector postGroups() const;
    std::string description();
    std::string deepDescription();
    
private:
    void insertVarParInstance(const SharedVarParInstance &varpar);
    
    //======= Members =========
    VariationGroupVector array;
};

#endif /* VariationChain_hpp */
