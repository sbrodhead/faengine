//
//  CudaDeviceKind.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 7/7/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//     Fractal Architect Render Engine - a GPU accelerated flame fractal renderer written in C++
//
//     This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
//     General Public License as published by the Free Software Foundation; either version 2.1 of the
//     License, or (at your option) any later version.
//
//     This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
//     even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//     Lesser General Public License for more details.
//
//     You should have received a copy of the GNU Lesser General Public License along with this software;
//     if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
//     02110-1301 USA, or see the FSF site: http://www.fsf.org.

#ifndef CudaDeviceKind_hpp
#define CudaDeviceKind_hpp

#include "Exports.hpp"
#include "DeviceKind.hpp"

#include <cuda.h>
#include <string>
#include "CudaProgram.hpp"

class CudaContext;
class CudaDeviceKind;
class CudaProgram;
class VariationSet;

using StringsSet           = std::unordered_set<std::string>;
using SharedCudaProgram    = std::shared_ptr<CudaProgram>;
using SharedCudaContext    = std::shared_ptr<CudaContext>;
using SharedCudaDeviceKind = std::shared_ptr<CudaDeviceKind>;
using WeakCudaContext      = std::weak_ptr<CudaContext>;

class FAENGINE_API CudaDeviceKind : public DeviceKind {
    friend class CudaProgram;
    friend class CudaContext;
    friend class Flam4CudaRuntime;
public:
    CudaDeviceKind(const std::string & _kind, const std::string & _vendor, SharedDeviceContext _deviceContext, CUcontext _cucontext);
    
//    virtual ~CudaDeviceKind();
    
    size_t numDevices() { return cudaDeviceInstances.size(); }
    
    std::string getDeviceVendor() override;
    std::string getDeviceName() override;
    std::string getDeviceOpenCudaVersion();
    std::string getDeviceExtensions();
    CUdevice cudaDeviceForInstanceIndex(size_t index);
    
    IntVector & deviceIDs()  { return cudaDeviceInstances; }
    
    CudaContext *cudaContext() { return (CudaContext *)deviceContext.lock().get(); }
    
    virtual bool isCPU() override;
    virtual bool isGPU() override;
    virtual bool hasLocalMemoryOnDevice() override;

    
private:
    bool createContext() override;
    void setupSubDevicesFor(void * cpuDeviceID) override;
    
    virtual CudaProgram * programForVariationSetUuid(const std::string & variationSetUuid, duration<double> & buildTime) override;
    virtual CudaProgram * programForVariationSet(const SharedVariationSet & variationSet, duration<double> & buildTime)  override;
    void removeProgramForVariationSetUuid(std::string &variationSetUuid);
    virtual CudaProgram * makeProgramFromSource(const SharedVariationSet & variationSet, bool forceRebuild, duration<double> & buildTime) override;
    
    void deviceKindProperties() override;
    
    uint determineWarpSizeForCudaDevice(CUdevice device);
    
    Json::Value makeJson();
    Json::Value makeJsonForDeviceKindWithUuid(const std::string & uuid);
    static Json::Value & existingJsonForDeviceName(const std::string & name, Json::Value & kindsJson);
    static Json::Value & existingJsonForDeviceName(const std::string & name);
    
    Json::Value & existingJson();
    static bool JsonIsValid(Json::Value & Json);
    enum DeviceKindJsonStatus checkJsonStatus(Json::Value & kindsJson);
    const std::string processDeviceKind() override;
    
    
    // ============= Members ================
    
    CUcontext cucontext;
};


#endif /* CudaDeviceKind_hpp */
