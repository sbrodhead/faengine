//
//  RenderTest.cpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/27/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//    The views and conclusions contained in the software and documentation are those
//    of the authors and should not be interpreted as representing official policies,
//    either expressed or implied, of the FreeBSD Project.

#include "RenderTest.hpp"

#ifdef _WIN32
#include "WindowsInterface.hpp"
#else
#include <unistd.h>
#include "CocoaInterface.h"
#endif
#include "Utilities.hpp"
#include <Magick++.h>

#include "ClContext.hpp"
#ifndef NO_CUDA
#include "CudaContext.hpp"
#endif

Magick::StorageType storageTypeFor(uint bitsPerPixel)
{
    Magick::StorageType storageType;
    if (bitsPerPixel == 8)
        storageType = Magick::CharPixel;
    else if (bitsPerPixel == 16)
        storageType = Magick::ShortPixel;
    else
        storageType = Magick::FloatPixel;
    return storageType;
}

bool hasEnding (std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

void RenderTest::setImage(SharedImage _image, std::string & url, bool useAlpha, Size _size, duration<double> _duration)
{
    if (! _image) {
        static char buf[100];
        snprintf(buf, sizeof(buf), "Rendering flame recipe failed for: %s", url.c_str());
        systemLog(buf);
        return;
    }
    SharedImage image;
    if (! _image->transparent) {
        image = _image->RGBAtoRGB();
    }
    else {
        image = _image;
    }
    
    Magick::Image mimage(image->width, image->height, image->map, storageTypeFor(image->bitDepth), image->bitmap.get());
    if (image->flipped)
        mimage.flip();
    
    Magick::Blob blob(_image->profile->data(), _image->profile->size());
    mimage.iccColorProfile(blob);

    if (hasEnding(url, "jpeg"))
        mimage.magick("JPEG");
    else if (hasEnding(url, "tiff"))
        mimage.magick("TIFF");

	if (::fileExists(url))
#ifdef _WIN32
        _unlink(url.c_str());
#else
        unlink(url.c_str());
#endif
	mimage.write(url);
}

RenderHarness * RenderTest::makeOpenClSingleton(bool targetGPU, unsigned _deviceNum, bool forceRebuild, bool ignoreQuarantine)
{
    std::set<uint> deviceNums;
    deviceNums.insert(_deviceNum);
    
    return RenderTest::makeOpenClSingleton(targetGPU, deviceNums, forceRebuild, ignoreQuarantine);
}

RenderHarness * RenderTest::makeOpenClSingleton(bool targetGPU, std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine)
{
    printf("Available Devices for %s ======\n", targetGPU ? "GPU" : "CPU");
    ClContext::printDeviceList(targetGPU);
    printf("\n");
    
    if (! openClSingleton) {
        openClSingleton = new RenderTest(_deviceNums);
        openClSingleton->openClContextSetup(targetGPU, _deviceNums, forceRebuild, ignoreQuarantine);
    }
    return openClSingleton;
}

#ifndef NO_CUDA
RenderHarness * RenderTest::makeCudaSingleton(unsigned _deviceNum, bool forceRebuild, bool ignoreQuarantine)
{
    std::set<uint> deviceNums;
    deviceNums.insert(_deviceNum);
    
    return RenderTest::makeCudaSingleton(deviceNums, forceRebuild, ignoreQuarantine);
}

RenderHarness * RenderTest::makeCudaSingleton(std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine)
{
    printf("Available CUDA Devices for GPU ======\n");
    CudaContext::printDeviceList();
    printf("\n");
    
    if (! cudaSingleton) {
        cudaSingleton = new RenderTest(_deviceNums);
        cudaSingleton->cudaContextSetup(_deviceNums, forceRebuild, ignoreQuarantine);
    }
    return cudaSingleton;
}

bool RenderTest::cudaTest1()
{
    std::string path = pathForTestResource("TestData.bundle", "JuliaJulia.fa");
    //    return cudaSingleton->renderFlames(path, true, OutputType::ePNG);
    return cudaSingleton->renderFlames(path, false, OutputType::ePNG);
    
}
#endif

bool RenderTest::test1()
{
    std::string path = pathForTestResource("TestData.bundle", "JuliaJulia.fa");
	return openClSingleton->renderFlames(path, true, OutputType::ePNG);

}

bool RenderTest::IntelLockerTest1()
{
    std::string path = pathForTestResource("TestData.bundle", "IntelLocker.flame");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
    
}

bool RenderTest::test2()
{
    // Does use final xform
    std::string path = pathForTestResource("TestData.bundle", "electricsheep.243.16751.flam3");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test3()
{
    std::string path = pathForTestResource("TestData.bundle", "electricsheep.244.00738.flam3");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test4()
{
    std::string path = pathForTestResource("TestData.bundle", "electricsheep.244.01376.flam3");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test5()
{
    std::string path = pathForTestResource("TestData.bundle", "electricsheep.244.01510.flam3");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test6()
{
    // Does use final xform
    std::string path = pathForTestResource("TestData.bundle", "electricsheep.244.01563.flam3");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test7()
{
    std::string path = pathForTestResource("TestData.bundle", "electricsheep.244.01850.flam3");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test8()
{
    // Does use final xform    
    std::string path = pathForTestResource("TestData.bundle", "electricsheep.244.01917.flam3");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test9()
{
    std::string path = pathForTestResource("TestData.bundle", "BoxedIn.flame");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test10()
{
    std::string path = pathForTestResource("TestData.bundle", "Exercise1.fa");
    return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test11()
{
	std::string path = pathForTestResource("TestData.bundle", "electricsheep.245.01848.flam3");
	return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}

bool RenderTest::test12()
{
	std::string path = pathForTestResource("TestData.bundle", "electricsheep.245.07662.flam3");
	return openClSingleton->renderFlames(path, true, OutputType::ePNG);
}
