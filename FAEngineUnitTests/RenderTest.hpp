//
//  RenderTest.hpp
//  FractalArchitectEngine
//
//  Created by Steven Brodhead on 5/27/16.
//  Copyright © 2016 Centcom Inc. All rights reserved.
//
//    Redistribution and use in source and binary forms, with or without
//    modification, are permitted provided that the following conditions are met:
//
//    1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
//    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//    The views and conclusions contained in the software and documentation are those
//    of the authors and should not be interpreted as representing official policies,
//    either expressed or implied, of the FreeBSD Project.

#ifndef RenderTest_hpp
#define RenderTest_hpp

#include <memory>
#include <set>

using uint = unsigned int;

#include "RenderHarness.hpp"

class RenderTest : public RenderHarness {
    
    RenderTest(std::set<uint> _deviceNums)
        : RenderHarness(_deviceNums) {}
    
public:
    void setImage(SharedImage image, std::string & url, bool useAlpha, Size _size, duration<double> _duration) override;
    
    static RenderHarness * makeOpenClSingleton(bool targetGPU, unsigned _deviceNum, bool forceRebuild, bool ignoreQuarantine);
    static RenderHarness * makeOpenClSingleton(bool targetGPU, std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine);
    
#ifndef NO_CUDA
    static RenderHarness * makeCudaSingleton(unsigned _deviceNum, bool forceRebuild, bool ignoreQuarantine);
    static RenderHarness * makeCudaSingleton(std::set<uint> _deviceNums, bool forceRebuild, bool ignoreQuarantine);

    static bool cudaTest1();
#endif

    static bool IntelLockerTest1();
    
    static bool test1();
    static bool test2();
    static bool test3();
    static bool test4();
    static bool test5();
    static bool test6();
    static bool test7();
    static bool test8();
    static bool test9();
    static bool test10();
	static bool test11();
	static bool test12();
};

#endif /* RenderTest_hpp */
